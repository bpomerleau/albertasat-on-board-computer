/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_mocks
 * @author Brendan Bruner
 * @date Feb 23, 2015
 */

#include "dependency_injection.h"

#define HIGH_VOLTAGE 	((eps_vbatt_t) 17000)
#define LOW_VOLTAGE		((eps_vbatt_t) 15000)
#define SAFE_VOLTAGE	((eps_vbatt_t) 14000)

/************************************************************************/
/* ALWAYS NORMAL														*/
/************************************************************************/
static eps_vbatt_t always_low_v( eps_t *eps )
{
	return LOW_VOLTAGE;
}

static eps_mode_t always_critical_mode( eps_t *eps )
{
	return EPS_MODE_NORMAL;
}

/* eps which always outputs very low battery voltage / power safe mode. */
void initialize_eps_always_normal( eps_t *eps )
{
	eps->get_vbatt = &always_low_v;
	eps->mode = always_critical_mode;
}



/************************************************************************/
/* ALWAYS OPTIMAL														*/
/************************************************************************/
static eps_vbatt_t always_high_v( eps_t *eps )
{
	return HIGH_VOLTAGE;
}

static eps_mode_t always_optimal_mode( eps_t *eps )
{
	return EPS_MODE_OPTIMAL;
}

/* eps which always outputs very high voltage / optimal mode. */
void initialize_eps_always_optimal( eps_t *eps )
{
	eps->get_vbatt = &always_high_v;
	eps->mode = &always_optimal_mode;
}

/************************************************************************/
/* ALWAYS SAFE															*/
/************************************************************************/
static eps_vbatt_t always_safe_v( eps_t *eps )
{
	return SAFE_VOLTAGE;
}

static eps_mode_t always_safe_mode( eps_t *eps )
{
	return EPS_MODE_POWER_SAFE;
}

void initialize_eps_always_safe( eps_t *eps )
{
	eps->get_vbatt = &always_safe_v;
	eps->mode = &always_safe_mode;
}
