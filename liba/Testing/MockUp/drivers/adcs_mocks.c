/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file adcs_mocks
 * @author Brendan Bruner
 * @date Feb 25, 2015
 */

#include "dependency_injection.h"
#include "adcs/adcs.h"

/************************************************************************/
/* Always stabilized mock												*/
/************************************************************************/
static int always_stable_mode( adcs_t *adcs )
{
	adcs->adcs_state.adcs_modes &= 0xF8;
	adcs->adcs_state.adcs_modes |= 0x04;
	return 0;
}

void initialize_adcs_always_stabilized( adcs_t *adcs )
{
	adcs->getframe_adcs_state = &always_stable_mode;
}

/************************************************************************/
/* Always idle mock 													*/
/************************************************************************/
static int always_idle_mode( adcs_t *adcs )
{
	adcs->adcs_state.adcs_modes &= 0xF8;
	return 0;
}

void initialize_adcs_always_idle( adcs_t *adcs )
{
	adcs->getframe_adcs_state = &always_idle_mode;
}
