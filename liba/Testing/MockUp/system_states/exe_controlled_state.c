/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file exe_controlled_state
 * @author Brendan Bruner
 * @date Jan 30, 2015
 */

#include <states/state_relay.h>
#include <states/state.h>
#include <telecommands/telecommand.h>

#include <dependency_injection.h>

#ifndef NULL
#define NULL ((void *) 0)
#endif

/************************************************************************/
/* Initialization Method												*/
/************************************************************************/
void initialize_exe_all_state( state_exe_all_t *state, filesystem_t *fs )
{	
	state_config_t config;

	config._uses_dfgm = STATE_ENABLES_EXECUTION;
	config._uses_mnlp = STATE_ENABLES_EXECUTION;
	config._uses_transmit = STATE_ENABLES_EXECUTION;
	config._uses_response = STATE_ENABLES_EXECUTION;
	config._uses_hk = STATE_ENABLES_EXECUTION;

	initialize_state( (state_t *) state, "msea_l.txt", fs, &config );
	((state_t *) state)->_id_ = 			STATE_EXE_CONTROLLED_ID;
}

void initialize_exe_none_state( state_exe_none_t *state, filesystem_t *fs )
{
	state_config_t config;

	config._uses_dfgm = STATE_DISABLES_EXECUTION;
	config._uses_mnlp = STATE_DISABLES_EXECUTION;
	config._uses_transmit = STATE_DISABLES_EXECUTION;
	config._uses_response = STATE_DISABLES_EXECUTION;
	config._uses_hk = STATE_DISABLES_EXECUTION;

	initialize_state( (state_t *) state, "msen_l.txt", fs, &config );
	((state_t *) state)->_id_ = 			STATE_EXE_CONTROLLED_ID;
}
