/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_suites.h
 * @author Brendan Bruner
 * @date 2015-01-10
 *
 * Contains prototypes of all test suites.
 */

#ifndef TEST_COMMANDS_H_
#define TEST_COMMANDS_H_

#include "unit.h"

TEST_SUITE( system_state_relay );
TEST_SUITE( system_states );
TEST_SUITE( commands );
TEST_SUITE( eps );
TEST_SUITE( adcs );
TEST_SUITE( comm );
TEST_SUITE( drivers );
TEST_SUITE( filesystem_api );
TEST_SUITE( file_handle_api );
TEST_SUITE( packet_structure );
TEST_SUITE( logger );
TEST_SUITE( telemetry_priority );
TEST_SUITE( allocator );
TEST_SUITE( rtc_api );
TEST_SUITE( command_api );
TEST_SUITE( command_downlink_confirm_telemetry );
TEST_SUITE( command_set_state );
TEST_SUITE( command_locks );
TEST_SUITE( command_log_wod );
TEST_SUITE( command_beacon_wod );
TEST_SUITE( persistent_timer );
TEST_SUITE( next_state );
TEST_SUITE( interpreter );
TEST_SUITE( non_volatile_variable );
TEST_SUITE( script_daemon );
TEST_SUITE( telemetry_stream );
TEST_SUITE( udos_system );
TEST_SUITE( scv_config );

#endif /* TEST_COMMANDS_H_ */
