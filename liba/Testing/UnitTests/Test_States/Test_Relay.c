/*
 * Test_Satellite.c
 *
 *  Created on: 2015-01-03
 *      Author: brendan
 */

#include <states/state_relay.h>
#include "test_suites.h"
#include "unit.h"
#include "dependency_injection.h"
#include <adcs/adcs_locale_logger.h>

static driver_toolkit_t *kit;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

/* generic setup for testing methods which relay commands. */
#define generic_relay_test( name )	do { \
				state_relay_t	relay;													\
				state_exe_all_t			state_exe_all;\
				state_exe_none_t state_exe_none;							\
				telecommand_counter_t			command;												\
				int 					count = 0;												\
				initialize_exe_all_state( &state_exe_all, kit->fs );	\
				initialize_exe_none_state( &state_exe_none, kit->fs );	\
				initialize_state_relay_simple( &relay, kit );					\
				relay._current_state_ = (state_t *) &state_exe_all;				\
				initialize_command_counter( &command, &count, kit );									\
				ASSERT( "Count is not initially zero", count == 0 );													\
				name ( &relay, (telecommand_t *) &command );														\
				name ( &relay, (telecommand_t *) &command );														\
				ASSERT( "command should be executed twice", count == 2 );													\
				relay._current_state_ = (state_t *) &state_exe_none;											\
				name ( &relay, (telecommand_t *) &command );														\
				name ( &relay, (telecommand_t *) &command );														\
				ASSERT( "command should not have executed again", count == 2 );													\
				/* This test will result in a hard fault if failed. */							\
				name ( &relay, NULL );															\
				((state_t *) &state_exe_all)->destroy( (state_t *) &state_exe_all );\
				((state_t *) &state_exe_none)->destroy( (state_t *) &state_exe_none );\
				destroy_state_relay( &relay );											\
				((telecommand_t *) &command)->destroy( (telecommand_t *) &command );\
									} while(0)

#define STATE_UNUSED_ID ((uint8_t) 100)
TEST(relay_initialization)
{
	/* By setting IDs to a known value, then calling the initialization
	 * routine, we can check if the IDs have been initialized to their
	 * correct value. */
	state_relay_t relay;

	((state_t *) &relay.bring_up)->_id_ = STATE_UNUSED_ID;
	((state_t *) &relay.low_power)->_id_ = STATE_UNUSED_ID;
	((state_t *) &relay.alignment)->_id_ = STATE_UNUSED_ID;
	((state_t *) &relay.science)->_id_ = STATE_UNUSED_ID;

	initialize_state_relay_simple( &relay, kit );

	ASSERT( "bring up id is %d, not %d",
			query_state_id( (state_t *) &relay.bring_up ) == STATE_BRING_UP_ID,
			query_state_id( (state_t *) &relay.bring_up ), STATE_BRING_UP_ID );
	ASSERT( "low power id is %d, not %d",
			query_state_id( (state_t *) &relay.low_power ) == STATE_LOW_POWER_ID,
			query_state_id( (state_t *) &relay.low_power ), STATE_LOW_POWER_ID);
	ASSERT( "science id is %d, not %d",
			query_state_id( (state_t *) &relay.science ) == STATE_SCIENCE_ID,
			query_state_id( (state_t *) &relay.science ), STATE_SCIENCE_ID );

	destroy_state_relay( &relay );
}

TEST(force_state_change)
{
	state_relay_t	relay;
	state_probe_t			probe1, probe2;
	probe_counter_t	counter1, counter2;

	initialize_probe_state( &probe1, &counter1, kit->fs );
	initialize_probe_state( &probe2, &counter2, kit->fs );
	initialize_state_relay_simple( &relay, kit );
	relay._current_state_ = (state_t *) &probe1;

	ASSERT("set up failed", 		counter1.exits == 0 &&
									counter1.enters == 0 &&
									counter1.nexts == 0 &&
									counter2.exits == 0 &&
									counter2.enters == 0 &&
									counter2.nexts == 0 );

	relay_force_change_state( &relay, (state_t *) &probe2 );
	ASSERT("failed to force change",counter1.exits == 1 &&
									counter1.enters == 0 &&
									counter1.nexts == 0 &&
									counter2.exits == 0 &&
									counter2.enters == 1 &&
									counter2.nexts == 0 &&
									(void *) relay._current_state_ == (void *) &probe2 );

	destroy_state_relay( &relay );
	((state_t *) &probe1)->destroy( (state_t *) &probe1 );
	((state_t *) &probe2)->destroy( (state_t *) &probe2 );
}

TEST(set_state_entry_exit)
{
	state_relay_t 	relay;
	state_probe_t			state1;
	probe_counter_t	count1;

	initialize_probe_state( &state1, &count1, kit->fs );
	ASSERT("set_state_entry_exit 1",	count1.enters == 0 &&
										count1.exits == 0 &&
										count1.nexts == 0 );
	initialize_state_relay_simple( &relay, kit );
	relay._current_state_ = (state_t *) &state1;
	relay_next_state( &relay );
	ASSERT("set_state_entry_exit 1",	count1.enters == 1 &&
										count1.exits == 1 &&
										count1.nexts == 1 );


	destroy_state_relay( &relay );
	((state_t *) &state1)->destroy( (state_t *) &state1 );
}

TEST(set_state)
{
	state_relay_t 	relay;
	state_exe_all_t			test_state1;

	initialize_exe_all_state( &test_state1, kit->fs );
	initialize_state_relay_simple( &relay, kit );
	relay._current_state_ = (state_t *) &test_state1;

	ASSERT( "initial state not as expected", (void *) relay._current_state_ == (void *) &test_state1 );

	destroy_state_relay( &relay );
	((state_t *) &test_state1)->destroy( (state_t *) &test_state1 );
}

TEST(get_current_state_id)
{
	state_relay_t 	relay;
	state_exe_all_t			test_state;

	initialize_exe_all_state( &test_state, kit->fs );
	initialize_state_relay_simple( &relay, kit );
	relay._current_state_ = (state_t *) &test_state;

	ASSERT("get current state id failed",
			relay_state_id( &relay ) == STATE_EXE_CONTROLLED_ID );

	destroy_state_relay( &relay );
	((state_t *) &test_state)->destroy( (state_t *) &test_state );
}

TEST(get_aggregate_state_objects)
{
	state_relay_t relay;

	initialize_state_relay_simple( &relay, kit );

	ASSERT("get aggregate state bring up",
			query_state_id( (state_t *) &relay.bring_up ) == STATE_BRING_UP_ID);
	ASSERT("get aggregate state low power",
			query_state_id( (state_t *) &relay.low_power ) == STATE_LOW_POWER_ID);
	ASSERT("get aggregate state science",
			query_state_id( (state_t *) &relay.science ) == STATE_SCIENCE_ID);

	destroy_state_relay( &relay );
}

TEST(relay_command_collect_housekeeping)
{
	generic_relay_test(relay_command_collect_housekeeping);
}

TEST(relay_command_transmit_data)
{
	generic_relay_test(relay_command_transmit_data);
}

TEST(relay_command_handle_MnLP)
{
	generic_relay_test(relay_command_handle_MnLP);
}

TEST(relay_command_handle_DFGM)
{
	generic_relay_test(relay_command_handle_DFGM);
}

TEST( relay_command_respond_to_request )
{
	generic_relay_test( relay_command_respond_to_request );
}

TEST( relay_command_diagnostics )
{
	state_relay_t	relay;
	state_exe_all_t			state_exe_all;
	state_exe_none_t state_exe_none;
	telecommand_counter_t			command;
	int 					count = 0;
	initialize_exe_all_state( &state_exe_all, kit->fs );
	initialize_exe_none_state( &state_exe_none, kit->fs );
	initialize_state_relay_simple( &relay, kit );
	relay._current_state_ = (state_t *) &state_exe_all;
	initialize_command_counter( &command, &count, kit );
	relay_command_diagnostics( &relay, (telecommand_t *) &command );
	relay_command_diagnostics( &relay, (telecommand_t *) &command );
	ASSERT( "when enabled, diag didn't execute", count == 2 );
	relay._current_state_ = (state_t *) &state_exe_none;
	relay_command_diagnostics( &relay, (telecommand_t *) &command );
	relay_command_diagnostics( &relay, (telecommand_t *) &command );
	ASSERT( "when disabled, diag didn't execute (but should have)", count == 4 );
	/* This test will result in a hard fault if failed. */
	relay_command_diagnostics( &relay, NULL );
	((state_t *) &state_exe_all)->destroy( (state_t *) &state_exe_all );
	((state_t *) &state_exe_none)->destroy( (state_t *) &state_exe_none );
	destroy_state_relay( &relay );
	((telecommand_t *) &command)->destroy( (telecommand_t *) &command );
}

TEST(correct_first_state)
{
	state_relay_t relay;

	initialize_state_relay_simple( &relay, kit );
	ASSERT( "incorrect first state", relay._current_state_ == (state_t*) &relay.bring_up );

	destroy_state_relay( &relay );
}

TEST_SUITE(system_state_relay)
{
	/* Initialize locale logger */
	init_adcs_locale_logger( );
	kit = kitp;
	ADD_TEST(correct_first_state);
	ADD_TEST(force_state_change);
	ADD_TEST(set_state);
	ADD_TEST(set_state_entry_exit);
	ADD_TEST(get_aggregate_state_objects);
	ADD_TEST(get_current_state_id);
	ADD_TEST(relay_initialization);
	ADD_TEST(relay_command_collect_housekeeping);
	ADD_TEST(relay_command_transmit_data);
	ADD_TEST(relay_command_handle_MnLP);
	ADD_TEST(relay_command_handle_DFGM);
	ADD_TEST( relay_command_respond_to_request );
	ADD_TEST( relay_command_diagnostics );
}
