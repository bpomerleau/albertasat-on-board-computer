/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_command_log_wod.c
 * @author Brendan Bruner
 * @date Oct 22, 2015
 */

#include "test_suites.h"
#include "dependency_injection.h"
#include <states/state_relay.h>
#include <telecommands/telemetry_alteration/telecommand_log_wod.h>
#include <string.h>
#include <packets/packet_base.h>

static state_relay_t* relay;
#define WOD_TV 1
#define WOD_PER_PACKET 32 /* Private to the command, (wod_packet_size - wod_time_stamp_size)/(wod_entry_size), (260-4)/(8). */

TEST_SETUP( )
{

}
TEST_TEARDOWN( )
{

}

/* Dependency inject a way to control EPS hk values. */
static int hk_val = 1;
static bool_t eps_hk_refresh( eps_t* eps )
{
	kitp->eps->_.hk.vbatt = hk_val;
	kitp->eps->_.hk.cursys = hk_val;
	kitp->eps->_.hk.curout[0] = hk_val;
	kitp->eps->_.hk.curout[1] = hk_val;
	kitp->eps->_.hk.curout[2] = hk_val;
	kitp->eps->_.hk.curout[3] = hk_val;
	kitp->eps->_.hk.curout[4] = hk_val;
	kitp->eps->_.hk.curout[5] = hk_val;
	kitp->eps->_.hk.temp[0] = hk_val;
	kitp->eps->_.hk.temp[1] = hk_val;
	kitp->eps->_.hk.temp[2] = hk_val;
	kitp->eps->_.hk.temp[3] = hk_val;
	kitp->eps->_.hk.temp[4] = hk_val;
	kitp->eps->_.hk.temp[5] = hk_val;
	return 1;
}
static void eps_set_hk( int new_hk_val )
{
	hk_val = new_hk_val;
}

/* Dependency inject a way to control COMM hk. */
static int16_t comm_hk = 1;
static int16_t comm_get_pcb_temp_de( comm_t* comm )
{
	return comm_hk;
}
static void comm_set_hk( int16_t new_temp )
{
	comm_hk = new_temp;
}

TEST( wod_logged )
{
	telecommand_log_wod_t 	command;
	file_t*					file;
	fs_error_t				file_err;
	logger_error_t			logger_err;
	uint8_t 				err;
	uint32_t				file_size;
	uint8_t					logged_wod[PACKET_RAW_WOD_SIZE];
	uint8_t					processed_wod[PACKET_RAW_WOD_SIZE];

	/* Initialize command under test. */
	err = initialize_telecommand_log_wod( &command, relay );
	if( err != TELECOMMAND_SUCCESS )
	{
		ABORT_TEST( "Failed to construct command object" );
	}

	/* Initialize fake WOD values. */
	eps_set_hk( WOD_TV );
	comm_set_hk( WOD_TV );

	/* Run the command. */
	telecommand_execute( (telecommand_t*) &command );
	if( telecommand_status( (telecommand_t*) &command ) != CMND_EXECUTED )
	{
		((telecommand_t*) &command)->destroy( (telecommand_t*) &command );
		ABORT_TEST( "Failed to execute command" );
	}
	((telecommand_t*) &command)->destroy( (telecommand_t*) &command );

	/* Get the log file WOD was saved to. */
	file = logger_peek_head( kitp->wod_logger, &logger_err );
	if( logger_err != LOGGER_OK )
	{
		file->close( file );
		ABORT_TEST( "Failed to get file handle" );
	}

	/* Seek to the last location of inserted wod. */
	file_size = file->size( file, &file_err );
	if( file_err != FS_OK || file_size < PACKET_RAW_WOD_SIZE )
	{
		file->close( file );
		ABORT_TEST( "Failed to get file size" );
	}
	file_err = file->seek( file, file_size - PACKET_RAW_WOD_SIZE );
	if( file_err != FS_OK )
	{
		file->close( file );
		ABORT_TEST( "Failed to seek" );
	}

	/* Read out the wod. */
	file_err = file->read( file, logged_wod, PACKET_RAW_WOD_SIZE, &file_size );
	file->close( file );
	if( file_err != FS_OK )
	{
		ABORT_TEST( "Failed to read" );
	}

	/* Lots of private information to the command being replicated here. */
	extern void packetize_raw_wod( uint8_t *, uint8_t, uint16_t, uint16_t, uint16_t,
									uint16_t, int16_t, int16_t, int16_t );
	packetize_raw_wod( processed_wod, 0, WOD_TV, WOD_TV, WOD_TV, WOD_TV, WOD_TV, WOD_TV, WOD_TV );

	/* Reuse the variable file_size for iterating the for loop. */
	for( file_size = 0; file_size < PACKET_RAW_WOD_SIZE; ++file_size )
	{
		ASSERT( "WOD is incorrect at %d", processed_wod[file_size] == logged_wod[file_size], file_size );
	}
}

TEST( packet_overflow )
{
#define LOGGER_MAX_FILE_NAME_LENGTH (FILESYSTEM_MAX_NAME_LENGTH+1)
	telecommand_log_wod_t command;
	logger_error_t	logger_err;
	uint8_t			err;
	file_t*			file;
	fs_error_t		file_err;
	uint32_t		before_size, after_size, iter;
	char			before_name[LOGGER_MAX_FILE_NAME_LENGTH];
	char			after_name[LOGGER_MAX_FILE_NAME_LENGTH];
	/* Get size of current wod packet file. */
	/* Insert an entire packet of wod. */
	/* Assert size of current wod packet file. */
	/* Assert contents. */

	/* Initialize command under test. */
	err = initialize_telecommand_log_wod( &command, relay );
	if( err != TELECOMMAND_SUCCESS )
	{
		ABORT_TEST( "Failed to construct command object" );
	}

	/* Get the size and name of the current packet. */
	file = logger_peek_head( kitp->wod_logger, &logger_err );
	if( logger_err != LOGGER_OK )
	{
		file->close( file );
		((telecommand_t*) &command)->destroy( (telecommand_t*) &command );
		ABORT_TEST( "Failed to get file handle before burst write" );
	}
	before_size = file->size( file, &file_err );
	file->close( file );
	if( file_err != FS_OK )
	{
		((telecommand_t*) &command)->destroy( (telecommand_t*) &command );
		ABORT_TEST( "Failed to get file size before burst write" );
	}
	strncpy( before_name, fs_handle_get_name( (fs_handle_t*) file ), LOGGER_MAX_FILE_NAME_LENGTH );

	/* Fill in a whole packets worth of wod. */
	for( iter = 0; iter < WOD_PER_PACKET; ++iter )
	{
		telecommand_execute( (telecommand_t*) &command );
		if( telecommand_status( (telecommand_t*) &command ) != CMND_EXECUTED )
		{
			((telecommand_t*) &command)->destroy( (telecommand_t*) &command );
			ABORT_TEST( "Failed to execute command" );
		}
	}
	((telecommand_t*) &command)->destroy( (telecommand_t*) &command );

	/* Open new packet. Get its size and name */
	file = logger_peek_head( (logger_t*) kitp->wod_logger, &logger_err );
	if( logger_err != LOGGER_OK )
	{
		file->close( file );
		ABORT_TEST( "Failed to get file handle after burst write" );
	}
	after_size = file->size( file, &file_err );
	file->close( file );
	if( file_err != FS_OK )
	{
		ABORT_TEST( "Failed to get file size after burst write" );
	}
	strncpy( after_name, fs_handle_get_name( (fs_handle_t*) file ), LOGGER_MAX_FILE_NAME_LENGTH );

	/* Assert size. */
	ASSERT( "Files are not the same size", before_size == after_size );

	/* Assert name. */
	/* Protected method of logger, should never be called inside application. */
	ASSERT( "File name is not in correct sequence", strncmp( before_name, after_name, LOGGER_MAX_FILE_NAME_LENGTH ) != 0 );
}

TEST( time_stamp )
{
	logger_t* 		wod_logger;
	logger_error_t	lerr;
	fs_error_t		ferr;
	file_t*			file;
	rtc_t			*rtc;
	uint32_t		current_time;
	uint8_t			file_time_stamp[sizeof(uint32_t)];
	uint32_t		actual_time_stamp;
	uint32_t		read;
	telecommand_log_wod_t command;
	uint8_t			cerr;
	int				i;

	wod_logger = kitp->wod_logger;
	rtc = kitp->rtc;

	/* Run collect WOD command enough times for it to spill over into a new file - assert the time stamp of */
	/* the new file. */
	cerr = initialize_telecommand_log_wod( &command, relay );
	if( cerr != TELECOMMAND_SUCCESS )
	{
		ABORT_TEST( "Failed to construct command object" );
	}
	for( i = 0; i < WOD_PER_PACKET; ++i ) {
		telecommand_execute((telecommand_t*) &command);
		if( telecommand_status( (telecommand_t*) &command ) != CMND_EXECUTED )
		{
			((telecommand_t*) &command)->destroy( (telecommand_t*) &command );
			ABORT_TEST( "Failed to execute command" );
		}
	}
	((telecommand_t*) &command)->destroy( (telecommand_t*) &command );

	/* Will be only slightly larger than time stamp. */
	current_time = rtc->seconds_since_epoch( rtc, QB50_EPOCH );

	/* Open the packet file and get the time stamp. */
	file = logger_peek_head(wod_logger, &lerr);
	if( lerr != LOGGER_OK )
	{
		file->close(file);
		ABORT_TEST( "Failed get head packet" );
	}
	ferr = file->seek(file, 0);
	if( ferr != FS_OK ) {
		ABORT_TEST("File system err %d", ferr);
	}
	ferr = file->read( file, file_time_stamp, sizeof(uint32_t), &read );
	file->close(file);
	if( ferr != FS_OK )
	{
		ABORT_TEST("File system error %d", ferr);
	}

	/* Assert correct time stamp. */
	to32_from_little_endian( &actual_time_stamp, file_time_stamp );
	ASSERT( "failed to assert time stamp", current_time >= actual_time_stamp && (current_time < 2 ? 0 : (current_time - 2)) <= actual_time_stamp );
}

TEST_SUITE( command_log_wod )
{
	typedef bool_t (*eps_hk_refresh_t)( eps_t* );
	typedef int16_t (*comm_pcb_temp_t)( comm_t* );

	eps_hk_refresh_t eps_actual_hk_refresh;
	comm_pcb_temp_t comm_actual_pcb_temp;

	/* Create a state_relay_t instance to be used by the tests. */
	relay = (state_relay_t*) OBCMalloc( sizeof(state_relay_t) );
	if( relay == NULL )
	{
		UNIT_PRINT( "failure to allocate memory for test\n" );
		return;
	}
	uint8_t err = initialize_state_relay_simple( relay, kitp );
	if( err != STATE_RELAY_SUCCESS )
	{
		UNIT_PRINT( "failure to initialize object for testing\n" );
		OBCFree( relay );
		return;
	}

	/* Dependency inject a way to control hk values of COMM and EPS. */
	eps_actual_hk_refresh = kitp->eps->hk_refresh;
	comm_actual_pcb_temp = kitp->comm->get_pcb_temp;
	kitp->eps->hk_refresh = eps_hk_refresh;
	kitp->comm->get_pcb_temp = comm_get_pcb_temp_de;

	/* Add the tests to the suite. */
	ADD_TEST( wod_logged );
	ADD_TEST( packet_overflow );

	/* Restore COMM and EPS to before dependency injection. */
	kitp->eps->hk_refresh = eps_actual_hk_refresh;
	kitp->comm->get_pcb_temp = comm_actual_pcb_temp;

	ADD_TEST( time_stamp );

	/* Clean up memory. */
	destroy_state_relay( relay );
	OBCFree( relay );
}
