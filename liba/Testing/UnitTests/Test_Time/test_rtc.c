/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_rtc->c
 * @author Brendan Bruner
 * @date Jun 24, 2015
 */

#include "test_suites.h"
#include "rtc.h"
#include "portable_types.h"

static rtc_t *rtc;
static rtc_time_t date;


TEST_SETUP( )
{
	date.seconds = 0;
	date.minutes = 31;
	date.hours = 13;
	date.day_of_month = 24;
	date.month = 6;
	date.year = 2015;
}
TEST_TEARDOWN( )
{
}

TEST( bound_methods )
{
	ASSERT( "enable not bound", rtc->enable != NULL );
	ASSERT( "disable not bound", rtc->disable != NULL );
	ASSERT( "set time not bound", rtc->set_time != NULL );
	ASSERT( "get time not bound", rtc->get_time != NULL );
}

TEST( singleton )
{
	rtc_t *rtc2;
	rtc_time_t cur_time;

#ifdef __LPC17XX__
	rtc2 = get_rtc_lpc( );
#else
	rtc2 = get_rtc_nanomind( 0 );
#endif

	rtc->enable( rtc );
	rtc->set_time( rtc, &date );
	date.year = 2014;
	rtc2->set_time( rtc2, &date );
	rtc->get_time( rtc, &cur_time );
	ASSERT( "not a singleton", cur_time.year == 2014 );

	destroy_rtc( rtc2 );
}

TEST( enable_disable )
{
#ifdef NANOMIND
	ABORT_TEST( "Jenne does not support enable and disable functionality" );
#else
	rtc_time_t time2, time3;

	/* Should be enabled by default. */
	rtc->set_time( rtc, &date );
	task_delay( 2000 * ONE_MS );
	rtc->get_time( rtc, &time2 );
	ASSERT( "time should have changed", time2.seconds > (date.seconds+1) );

	rtc->disable( rtc );
	rtc->get_time( rtc, &time2 );
	task_delay( 1500 * ONE_MS );
	rtc->get_time( rtc, &time3 );
	ASSERT( "time should not have changed after explicit disable, t1: %d, t2: %d", time2.seconds == time3.seconds, time2.seconds, time3.seconds );

	rtc->enable( rtc );
	task_delay( 1500 * ONE_MS );
	rtc->get_time( rtc, &time2 );
	ASSERT( "time should have changed after explicit enable", time2.seconds > date.seconds );
#endif
}

TEST( set_get_time )
{
	rtc_time_t time2;

	rtc->enable( rtc );
	rtc->set_time( rtc, &date );
	task_delay( 3500 * ONE_MS );
	rtc->get_time( rtc, &time2 );
	ASSERT( "time should have changed", time2.seconds > (date.seconds + 2) );
}

TEST( out_of_bounds )
{
	rtc_time_t time2;
	uint8_t err;

	time2.seconds = 100;

	rtc->disable( rtc );
	rtc->set_time( rtc, &date );
	err = rtc->set_time( rtc, &time2 );
	rtc->get_time( rtc, &time2 );
	ASSERT( "out of bounds seconds should not have done something, err: %d, t0: %d, t1: %d", err == RTC_TIME_NOT_SET && time2.seconds >= date.seconds, err, date.seconds, time2.seconds );

	time2.minutes = 100;
	rtc->set_time( rtc, &date );
	err = rtc->set_time( rtc, &time2 );
	rtc->get_time( rtc, &time2 );
	ASSERT( "out of bounds minutes should not have done something, err: %d, t0: %d, t1: %d", err == RTC_TIME_NOT_SET && time2.minutes >= date.minutes, err, date.minutes, time2.minutes );

	time2.hours = 40;
	rtc->set_time( rtc, &date );
	err = rtc->set_time( rtc, &time2 );
	rtc->get_time( rtc, &time2 );
	ASSERT( "out of bounds hours should not have done something, err: %d, t0: %d, t1: %d", err == RTC_TIME_NOT_SET && time2.hours >= date.hours, err, date.hours, time2.hours );

	time2.day_of_month = 40;
	rtc->set_time( rtc, &date );
	err = rtc->set_time( rtc, &time2 );
	rtc->get_time( rtc, &time2 );
	ASSERT( "out of bounds day of month should not have done something, err: %d, t0: %d, t1: %d", err == RTC_TIME_NOT_SET && time2.day_of_month >= date.day_of_month, err, date.day_of_month, time2.day_of_month );

	time2.month = 100;
	rtc->set_time( rtc, &date );
	err = rtc->set_time( rtc, &time2 );
	rtc->get_time( rtc, &time2 );
	ASSERT( "out of bounds month should not have done something, err: %d, t0: %d, t1: %d", err == RTC_TIME_NOT_SET && time2.month >= date.month, err, date.month, time2.month );
}

TEST( epoch )
{
	/* This test does not test the exactness of the seconds_since_epoch method. It */
	/* tests is behaviour is correct. ( is seconds from unix and qb50 epoch should be different */
	rtc_time_t test_time;
	uint32_t epoch_time;
	rtc_time_t unix_epoch_date;
#define TEST_TIME_UNIX_EPOCH 1419120000 /* Estimate not including leap years / seconds */
#define TEST_TIME_QB50_EPOCH 473040000  /* Estimate not including leap years / seconds */
#define TEST_TIME_SLACK 1036800 /* 12 days of slack to account for leap years / seconds */

	test_time = (rtc_time_t){ 0 };
	test_time.year = 2015;

	unix_epoch_date = *(UNIX_EPOCH);
	unix_epoch_date.year += 50;
	rtc->set_time( rtc, &unix_epoch_date );
	epoch_time = rtc->seconds_since_epoch( rtc, UNIX_EPOCH );
	ASSERT( "seconds since unix epoch (%ds) is too large",  epoch_time <= (50*365 + 20)*24*60*60, epoch_time );

	rtc->set_time( rtc, &test_time );
	epoch_time = rtc->seconds_since_epoch( rtc, UNIX_EPOCH ) ;
	ASSERT( "wrong time since unix epoch", epoch_time <= TEST_TIME_UNIX_EPOCH + TEST_TIME_SLACK && epoch_time >= TEST_TIME_UNIX_EPOCH - TEST_TIME_SLACK );

	rtc->set_time( rtc, QB50_EPOCH );
	ASSERT( "seconds since qb50 epoch is too large", rtc->seconds_since_epoch( rtc, QB50_EPOCH ) <= 2 );

	rtc->set_time( rtc, &test_time );
	epoch_time = rtc->seconds_since_epoch( rtc, QB50_EPOCH ) ;
	ASSERT( "wrong time since qb50 epoch", epoch_time <= TEST_TIME_QB50_EPOCH + TEST_TIME_SLACK && epoch_time >= TEST_TIME_QB50_EPOCH - TEST_TIME_SLACK );
}

TEST_SUITE( rtc_api )
{
	rtc = kitp->rtc;

	ADD_TEST( bound_methods );
	ADD_TEST( epoch );
	ADD_TEST( out_of_bounds );
	ADD_TEST( singleton );
	ADD_TEST( enable_disable );
	ADD_TEST( set_get_time );
}


