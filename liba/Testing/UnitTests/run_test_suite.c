#include <test_suites.h>

void run_test_suite( )
{
	INIT_TEST_SUITE_DRIVER_TOOLKIT( );
	INIT_TESTING( );

#if 0
	RUN_TEST_SUITE( logger );
	PRINT_DIAG();
	return;
#endif

	/* Variables that persist through hard power cycles
	 * Note, the scv_config test reboots the processor
	 * several times. It should be the first test run to save time.
	 */
	RUN_TEST_SUITE( scv_config );
	RUN_TEST_SUITE( non_volatile_variable );

	/* OCP Telecommand API, interpretation, and scripting test
	 */
	RUN_TEST_SUITE( command_api );
	RUN_TEST_SUITE( script_daemon );
	RUN_TEST_SUITE( interpreter );

	/* Time
	 */
	RUN_TEST_SUITE( rtc_api );

	/* File handling
	 */
	RUN_TEST_SUITE( filesystem_api );
	RUN_TEST_SUITE( file_handle_api );

	/* S/C state management
	 */
	RUN_TEST_SUITE( system_state_relay );
	RUN_TEST_SUITE( next_state );

	/* Telemetry streams and data logging API tests
	 */
	RUN_TEST_SUITE( logger );
	RUN_TEST_SUITE( telemetry_stream );

	/* System tests
	 */
	RUN_TEST_SUITE( udos_system );

	PRINT_DIAG( );
}
