/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_logger.c
 * @author Brendan Bruner
 * @date May 13, 2015
 */

#include <test_suites.h>
#include <dependency_injection.h>
#include <filesystems/filesystem.h>
#include <filesystems/fs_handle.h>
#include <logger/logger.h>
#include <string.h>
#include <packets/packet_base.h>
#include <printing.h>
#include <inttypes.h>

#define TEST_LOGGER_MAX_CAPACITY 6
#define TEST_LOGGER_DYNAMIC_CAPACITY 3
#define TEST_LOGGER_IDENTIFIER	'T'
#define TEST_LOGGER_CNTRL_FILE 	"MT-TLog.bin"

static driver_toolkit_t *kit;
static filesystem_t *filesystem, *fs;
static logger_t loggerx;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST( empty )
{
	/* There should no HEAD, therefore, assert peek routines return error. */
	/* Pop should fail, since there is no elements in the buffer. */
	logger_error_t 	lerr;
	file_t* 		file;

	/* Check peek head. */
	file = logger_peek_head(&loggerx, &lerr);
	file->close(file);
	ASSERT("peek head returned unexpected error: %d", lerr == LOGGER_EMPTY, lerr);

	/* Check peek tail. */
	file = logger_peek_tail(&loggerx, &lerr);
	ASSERT("peek tail returned unexpected error: %d", lerr == LOGGER_EMPTY, lerr);

	/* Check pop. */
	lerr = logger_pop(&loggerx, NULL);
	ASSERT("pop returned unexpected error: %d", lerr == LOGGER_EMPTY, lerr);
}

TEST( head)
{
	/* Assert we can insert and peek at the HEAD. */
	file_t* 		file;
	logger_error_t 	lerr;
	uint32_t		written;
	fs_error_t		ferr;
	char			dbuff[4];

	file = logger_insert(&loggerx, &lerr, NULL);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("First insert failed with err: %d", lerr);
	}
	/* This write will be used for an assert in another test case. */
	ferr = file->write(file, (uint8_t*) "test", 4, &written);
	file->close(file);

	/* Test peek head gets head after inserting new empty head. */
	file = logger_insert(&loggerx, &lerr, NULL);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to insert with err: %d", lerr);
	}
	ferr = file->write(file, (uint8_t*) "haha", 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in underlying file system");
	}

	file = logger_peek_head(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to insert with err: %d", lerr);
	}
	file->seek(file, 0);
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in underlying file system");
	}
	ASSERT("inconsistent data in head after NULL insert", strncmp("haha", dbuff, 4) == 0);

	/* Test peek head gets head after inserting new non empty head. */
	file = fs->open(fs, &ferr, "tmp.bin", FS_CREATE_ALWAYS, USE_POLLING);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in underlying file system");
	}
	ferr = file->write(file, (uint8_t*) "derp", 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in underlying file system");
	}
	file = logger_insert(&loggerx, &lerr, "tmp.bin");
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to insert with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in underlying file system");
	}
	ASSERT("inconsistent data in head after non NULL insert", strncmp("derp", dbuff, 4) == 0);

	file = logger_peek_head(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to insert with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in underlying file system");
	}
	ASSERT("inconsistent data in head from peak after non NULL insert", strncmp("derp", dbuff, 4) == 0);
}

TEST( tail )
{
	/* Assert we can pop and peek at the tail correctly. */
	/* Should be able to pop two files, first one with "test" in it, second one with "haha" in it. */
	/* Should be able to peek (not pop) a third file with "derp" in it. */
	file_t*			file;
	logger_error_t	lerr;
	fs_error_t		ferr;
	char			dbuff[4];
	uint32_t		written;
	char			popped_file_name[FILESYSTEM_MAX_NAME_LENGTH+1];

	/* Peek test for file with "test" in it. */
	file = logger_peek_tail(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to do first peek with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
			ABORT_TEST("Error in underlying file system");
	}
	ASSERT("incorrect data in first peek", strncmp("test", dbuff, 4) == 0);

	/* Pop test for file with "test" in it. */
	lerr = logger_pop(&loggerx, popped_file_name);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to do first pop with err: %d", lerr);
	}
	//ASSERT("Popped wrong file", 0 == strncmp("000T0000.bin", popped_file_name, FILESYSTEM_MAX_NAME_LENGTH));
	file = fs->open(fs, &ferr, popped_file_name, FS_OPEN_EXISTING, USE_POLLING);
	if( ferr != FS_OK ) {
		ABORT_TEST("Failed due to file system err");
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Failed due to file system err");
	}
	ASSERT("incorrect data in first pop", 0 == strncmp("test", dbuff, 4));

	/* Peek test for file with "haha" in it. */
	file = logger_peek_tail(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to do first peek with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
			ABORT_TEST("Error in underlying file system");
	}
	ASSERT("incorrect data in second peek", strncmp("haha", dbuff, 4) == 0);

	/* Pop test for file with "haha" in it. */
	lerr = logger_pop(&loggerx, popped_file_name);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to do first pop with err: %d", lerr);
	}
	//ASSERT("Popped wrong file", 0 == strncmp("001T0001.bin", popped_file_name, FILESYSTEM_MAX_NAME_LENGTH));
	file = fs->open(fs, &ferr, popped_file_name, FS_OPEN_EXISTING, USE_POLLING);
	if( ferr != FS_OK ) {
		ABORT_TEST("Failed due to file system err");
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Failed due to file system err");
	}
	ASSERT("incorrect data in second pop", 0 == strncmp("haha", dbuff, 4));

	/* Pop test for file with "derp" in it. */
	lerr = logger_pop(&loggerx, popped_file_name);
	ASSERT("Should not be able to pop, but somehow pop worked", lerr == LOGGER_EMPTY);

	/* Peek test for file with "derp" in it. */
	file = logger_peek_tail(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		ABORT_TEST("Failed to do first peek with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
			ABORT_TEST("Error in underlying file system");
	}
	ASSERT("incorrect data in third peek", strncmp("derp", dbuff, 4) == 0);
}

TEST( full )
{
	/* Assert head overwrites tail and tail increments correctly. */
	/* At this point, one file is in the logger, so to fill it up we need to add TEST_LOGGER_MAX_CAPACITY-1 files. */
	int i;
	file_t*			file;
	logger_error_t	lerr;
	fs_error_t		ferr;
	uint32_t		written;
	char			dbuff[4];

	for( i = 0; i < (TEST_LOGGER_MAX_CAPACITY-1); ++i ) {
		file = logger_insert(&loggerx, &lerr, NULL);
		if( lerr != LOGGER_OK ) {
			file->close(file);
			ABORT_TEST("Logger failed with err: %d", lerr);
		}
		dbuff[0] = i + '0';
		ferr = file->write(file, (uint8_t*) dbuff, 1, &written);
		file->close(file);
		if( ferr != FS_OK ) {
			ABORT_TEST("Error in file system: %d", ferr);
		}
	}

	/* The logger is now full, the tail should still be the file with "derp" in it. */
	file = logger_peek_tail(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		file->close(file);
		ABORT_TEST("Logger failed with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 4, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in file system: %d", ferr);
	}
	ASSERT("Tail contains incorrect message", 0 == strncmp("derp", dbuff, 4));

	/* Inserting a new file should overwrite the file at the tail. */
	file = logger_insert(&loggerx, &lerr, NULL);
	if( lerr != LOGGER_OK ) {
		file->close(file);
		ABORT_TEST("Logger failed with err: %d", lerr);
	}
	dbuff[0] = i + '0';
	ferr = file->write(file, (uint8_t*) dbuff, 1, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in file system: %d", ferr);
	}

	/* Now peek at the new tail and assert the old tail was overwritten by this new one. */
	file = logger_peek_tail(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		file->close(file);
		ABORT_TEST("Logger failed with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 1, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("Error in file system: %d", ferr);
	}
	ASSERT("At second peek, tail contains incorrect message", dbuff[0] == '0');
}

TEST( async_removal )
{
	/* Assert the tail and head will recover from asynchronous file removals. */
	/* Using the file system, remove two files from the tail. Then peek at the tail */
	/* using the logger and assert the tail was synchronized. */
	file_t*			file;
	logger_error_t	lerr;
	fs_error_t		ferr;
	uint32_t		written;
	char			dbuff[4];

	/* Files in the tail should be named: */
	/* 004T0004.bin */ /* TAIL */ 	/* <-- */
	/* 005T0005.bin */				/*   | */
	/* 000T0006.bin */				/*   | */
	/* 001T0007.bin */				/*   | */
	/* 002T0008.bin */				/*   | */
	/* 003T0009.bin */ /* HEAD */ 	/*  -- */
	fs->delete(fs, "004T0004.bin");
	fs->delete(fs, "005T0005.bin");

	/* When we look at TAIL, the file should contain the string "2" */
	file = logger_peek_tail(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		file->close(file);
		ABORT_TEST("Logger failed with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 1, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("File system error: %d", ferr);
	}
	ASSERT("Tail did not syncrhonize after async file removal", dbuff[0] == '2');

	/* Remove all but one file, assert tail is synchronized and structure is maintained. */
	fs->delete(fs, "000T0006.bin");
	fs->delete(fs, "001T0007.bin");
	fs->delete(fs, "002T0008.bin");

	/* When we look at TAIL, the file should contain the string "5". */
	file = logger_peek_tail(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		file->close(file);
		ABORT_TEST("Logger failed with err: %d", lerr);
	}
	ferr = file->read(file, (uint8_t*) dbuff, 1, &written);
	file->close(file);
	if( ferr != FS_OK ) {
		ABORT_TEST("File system error: %d", ferr);
	}
	ASSERT("Tail did not syncrhonize after async file removal of all but 1", dbuff[0] == '5');

	/* Remove ALL files, assert peek, pop, and insert function as expected. */
	fs->delete(fs, "003T0009.bin");
	file = logger_peek_tail(&loggerx, &lerr);
	file->close(file);
	ASSERT("Peek tail at empty buffer should have given err %d, but gave %d instead", lerr == LOGGER_EMPTY, LOGGER_EMPTY, lerr);

	file = logger_peek_head(&loggerx, &lerr);
	file->close(file);
	ASSERT("Peek head at empty buffer should have given err %d, but gave %d instead", lerr == LOGGER_EMPTY, LOGGER_EMPTY, lerr);

	lerr = logger_pop(&loggerx, NULL);
	ASSERT("pop with empty buffer should have given err %d, but gave %d instead", lerr == LOGGER_EMPTY, LOGGER_EMPTY, lerr);

	/* Redo all previous tests to ensure functionality is maintained after this. */
	ADD_TEST( empty );
	ADD_TEST( head);
	ADD_TEST( tail );
}

extern unsigned int ocp_atoui( const char* str, size_t len, int base );
static uint32_t logger_test_get_seq_id( file_t* file )
{
	char const* fd_name;
	unsigned int id;

	fd_name = fs_handle_get_name((fs_handle_t*) file);
	id = ocp_atoui(fd_name, 3, 16);
	return (uint32_t) id;
}

static uint32_t logger_test_get_tem_id( file_t* file )
{
	char const* fd_name;
	unsigned int id;

	fd_name = fs_handle_get_name((fs_handle_t*) file);
	id = ocp_atoui(fd_name+4, 4, 10);
	return (uint32_t) id;
}

TEST( dynamic_capacity )
{
	/* Precondition:
	 * 		* (head sequence ID) > x, where x is a positive integer ^  (head sequence ID) MOD (x) != 0
	 * Changes made
	 * 		1. (logger_t::max_capacity) -> (x)
	 * 		2. logger head incremented so that head sequence ID gets incremented
	 * Postconditions
	 * 		* (head sequence ID) = (0)
	 */
	file_t*			file;
	logger_error_t	lerr;
	uint32_t		seq_id, seq_id_post;
	uint32_t		tem_id, tem_id_post;

	file = logger_peek_head(&loggerx, &lerr);
	if( lerr != LOGGER_OK ) {
		EXIT_TEST();
	}
	for( ;; ) {
		seq_id = logger_test_get_seq_id(file);
		tem_id = logger_test_get_tem_id(file);
		file->close(file);
		if( seq_id > TEST_LOGGER_DYNAMIC_CAPACITY && seq_id % loggerx.max_capacity != 0 ) {
			/* Precondition met
			 */
			break;
		}

		/* Add another bucket into logger to try and meet precondition
		 */
		file = logger_insert(&loggerx, &lerr, NULL);
		if( lerr != LOGGER_OK ) {
			EXIT_TEST();
		}

		/* Do dummy write
		 */
		file->seek(file, 3);
	}

	/* Precondition is met
	 * Change max capacity and increment the head.
	 */
	loggerx.max_capacity = TEST_LOGGER_DYNAMIC_CAPACITY;
	file = logger_insert(&loggerx, &lerr, NULL);
	if( lerr != LOGGER_OK ) {
		EXIT_TEST();
	}
	/* Do dummy write
	 */
	file->seek(file, 3);

	seq_id_post = logger_test_get_seq_id(file);
	tem_id_post = logger_test_get_tem_id(file);
	file->close(file);
	UNIT_PRINT("seq id = %" PRIu32 ", seq_id_prev = %" PRIu32, seq_id_post, seq_id);
	UNIT_PRINT("tem_id = %" PRIu32 ", tem_id_prev = %" PRIu32, tem_id_post, tem_id);
	ASSERT("seq id did not go to zero. current id: %" PRIu32 ", prev id: %" PRIu32, seq_id_post == 0, seq_id_post, seq_id);
	ASSERT("tem id did not increment. current id: %" PRIu32 ", prev id: %" PRIu32, tem_id_post == tem_id+1, tem_id_post, tem_id);
}

TEST_SUITE( logger )
{
	kit = kitp;
	fs = kit->fs;
	filesystem = fs;
	fs->delete(fs, TEST_LOGGER_CNTRL_FILE);
	initialize_logger(&loggerx, fs, TEST_LOGGER_CNTRL_FILE, TEST_LOGGER_IDENTIFIER, TEST_LOGGER_MAX_CAPACITY);

	/* MUST be  done in this order. */
	ADD_TEST( empty );
	ADD_TEST( head);
	ADD_TEST( tail );
	ADD_TEST( full );
	ADD_TEST( async_removal );
	ADD_TEST( dynamic_capacity );

	loggerx.destroy(&loggerx);
}
