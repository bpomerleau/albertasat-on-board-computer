/*
 *
 * Copyright (C) 2018  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */

/* test_telemetry_stream.c
 *
 *  Created on: Apr 3, 2018
 *      Author: Brendan Bruner
 */
#include <unit.h>
#include <scv_config.h>
#include <non_volatile/file_variable.h>
#include <dev/cpu.h>
#include <string.h>

#define SCV_TEST_STATE_UNUSED	 				0
#define SCV_TEST_STATE_RAM_COMPLETE				1
#define SCV_TEST_STATE_FLUSH_NO_UPDATE 			2
#define SCV_TEST_STATE_FLUSH_REVERTED 			4
#define SCV_TEST_STATE_FLUSH_UPDATE 			3
#define SCV_TEST_STATE_FLUSH_COMPLETE 			5
#define SCV_TEST_STATE_COMMIT_FIRST_REBOOT 		6
#define SCV_TEST_STATE_COMMIT_SECOND_REBOOT 	7
#define SCV_TEST_STATE_COMMIT_COMPLETE 			8

#define SCV_TEST_VALUE 100

struct scv_test_state
{
	int state;
	int reboot;
	struct scv_config_t default_config;
	struct scv_config_t test_config;

	/* Save/recall state of test suite with this
	 */
	unsigned int unit_asserts_passed;
	unsigned int delta_unit_asserts_passed;
	unsigned int unit_asserts_failed;
	unsigned int delta_unit_asserts_failed;
	unsigned int unit_tests_run;
	unsigned int unit_tests_aborted;
};

static file_variable_t test_cycle;
static struct scv_test_state test_state;

TEST_SETUP( )
{
	/* Load test state. This will load the default
	 * config and test suite state.
	 */
	file_variable_get(&test_cycle, &test_state);

	UNIT_PRINT("Setting up test in state: %d", test_state.state);
	UNIT_PRINT("SCV must be deleted from /boot/ before running this test suite");

	if( test_state.state >= SCV_TEST_STATE_RAM_COMPLETE) {
		/* In the process of running tests that require
		 * reboots. Load the test configuration.
		 */
		scv_config_get_current(&test_state.test_config);

		/* Restore test suite state.
		 */
		unit_asserts_passed = test_state.unit_asserts_passed;
		delta_unit_asserts_passed = test_state.delta_unit_asserts_passed;
		unit_asserts_failed = test_state.unit_asserts_failed;
		delta_unit_asserts_failed = test_state.delta_unit_asserts_failed;
		unit_tests_run = test_state.unit_tests_run;
		unit_tests_aborted = test_state.unit_tests_aborted;
	}
	else {
		/* Save the default configuration.
		 */
		scv_config_get_current(&test_state.default_config);
		file_variable_set(&test_cycle, &test_state);
	}

}
TEST_TEARDOWN( )
{
	/* Save test state.
	 */
	test_state.unit_asserts_passed = unit_asserts_passed;
	test_state.delta_unit_asserts_passed = delta_unit_asserts_passed;
	test_state.unit_asserts_failed = unit_asserts_failed;
	test_state.delta_unit_asserts_failed = delta_unit_asserts_failed;
	test_state.unit_tests_run = unit_tests_run;
	test_state.unit_tests_aborted = unit_tests_aborted;

	if( test_state.reboot ) {
		test_state.reboot = 0;
		file_variable_set(&test_cycle, &test_state);
		UNIT_PRINT("WARNING: rebooting CPU, this is a part of the test");
		task_delay(200); /* Give CSP network time to send the message */
		cpu_reset();
	}
	else {
		file_variable_set(&test_cycle, &test_state);
	}
}

static void test_scv_reboot()
{
	test_state.reboot = 1;
}

static void test_scv_assert_config_default( )
{
	ASSERT("config is not back to default", memcmp(&test_state.default_config, &test_state.test_config, sizeof(struct scv_config_t)) == 0);
}

static void test_scv_set_config( struct scv_config_t* config, size_t x )
{
	config->adcs_locale_capacity = x;
	config->adcs_locale_file_size = x;
	config->athena_logger_capacity = x;
	config->athena_logger_file_size = x;
	config->dfgm_filt1_capacity = x;
	config->dfgm_filt2_capacity = x;
	config->dfgm_filt_file_size = x;
	config->dfgm_hk_capacity = x;
	config->dfgm_hk_file_size = x;
	config->dfgm_raw_capacity = x;
	config->dfgm_raw_file_size = x;
	config->udos_cadence = x;
	config->udos_capacity = x;
	config->udos_file_size = x;
	config->wod_cadence = x;
	config->wod_capacity = x;
	config->wod_file_size = x;
}

static void test_scv_assert_config( struct scv_config_t* config, size_t x )
{
	ASSERT("config->adcs_locale_capacity = x failed", config->adcs_locale_capacity == x);
	ASSERT("config->adcs_locale_file_size = x failed", config->adcs_locale_file_size == x);
	ASSERT("config->athena_logger_capacity = x failed", config->athena_logger_capacity == x);
	ASSERT("config->athena_logger_file_size = x failed", config->athena_logger_file_size == x);
	ASSERT("config->dfgm_filt1_capacity = x failed", config->dfgm_filt1_capacity == x);
	ASSERT("config->dfgm_filt2_capacity = x failed", config->dfgm_filt2_capacity == x);
	ASSERT("config->dfgm_filt_file_size = x failed", config->dfgm_filt_file_size == x);
	ASSERT("config->dfgm_hk_capacity = x failed", config->dfgm_hk_capacity == x);
	ASSERT("config->dfgm_hk_file_size = x failed", config->dfgm_hk_file_size == x);
	ASSERT("config->dfgm_raw_capacity = x failed", config->dfgm_hk_file_size == x);
	ASSERT("config->dfgm_raw_file_size = x failed", config->dfgm_hk_file_size == x);
	ASSERT("config->udos_cadence = x failed", config->udos_cadence == x);
	ASSERT("config->udos_capacity = x failed", config->udos_capacity == x);
	ASSERT("config->udos_file_size = x failed", config->udos_file_size == x);
	ASSERT("config->wod_cadence = x failed", config->wod_cadence == x);
	ASSERT("config->wod_capacity = x failed", config->wod_cadence == x);
	ASSERT("config->wod_file_size = x failed", config->wod_file_size == x);
}

TEST( ram_update )
{
	/* Test set and get functions
	 */
	struct scv_config_t config;

	if( test_state.state > SCV_TEST_STATE_UNUSED ) {
		/* This test has already run
		 */
		return;
	}

	/* Set SCV to new values
	 */
	test_scv_set_config(&config, SCV_TEST_VALUE);
	scv_config_set_current(&config);

	/* Retrieve new values and ASSERT they changed
	 */
	memset(&config, 0, sizeof(config));
	scv_config_get_current(&config);
	test_scv_assert_config(&config, SCV_TEST_VALUE);

	/* Move to next test state and restore
	 * default SCV
	 */
	test_state.state = SCV_TEST_STATE_RAM_COMPLETE;
	scv_config_set_current(&test_state.default_config);
}

TEST( flush )
{
	/* Set all scv variables to a different value then
	 * reboot and check they did not update.
	 * Set all scv variables to a different value then
	 * reboot and check they did update. Reboot again
	 * and check the returned to their default.
	 */
	if( test_state.state == SCV_TEST_STATE_RAM_COMPLETE ) {
		/* Set SCV then reboot without doing a flush
		 */
		test_scv_set_config(&test_state.test_config, SCV_TEST_VALUE);
		scv_config_set_current(&test_state.test_config);
		test_state.state = SCV_TEST_STATE_FLUSH_NO_UPDATE;
		test_scv_reboot();
		return;
	}

	if( test_state.state == SCV_TEST_STATE_FLUSH_NO_UPDATE ) {
		/* SCV was set without flush previous boot. Check that
		 * it reverted to defaults.
		 */
		test_scv_assert_config_default();

		/* Set the SCV using flush now.
		 */
		test_scv_set_config(&test_state.test_config, SCV_TEST_VALUE);
		scv_config_set_current(&test_state.test_config);
		scv_config_flush();
		test_state.state = SCV_TEST_STATE_FLUSH_UPDATE;
		test_scv_reboot();
		return;
	}

	if( test_state.state == SCV_TEST_STATE_FLUSH_UPDATE ) {
		/* SCV was set with flush. Check that it change.
		 */
		test_scv_assert_config(&test_state.test_config, SCV_TEST_VALUE);

		/* SCV should go back to defaults next reboot.
		 */
		test_state.state = SCV_TEST_STATE_FLUSH_REVERTED;
		test_scv_reboot();
		return;
	}

	if( test_state.state == SCV_TEST_STATE_FLUSH_REVERTED ) {
		/* SCV should be back to defaults now.
		 */
		test_scv_assert_config_default();
		test_state.state = SCV_TEST_STATE_FLUSH_COMPLETE;
	}
}

TEST( commit )
{
	/* Set all scv variables. flush. commit. reboot twice.
	 * Check all variables did not return to their
	 * default.
	 */
	if( test_state.state == SCV_TEST_STATE_FLUSH_COMPLETE ) {
		/* Set, flush, commit.
		 */
		test_scv_set_config(&test_state.test_config, SCV_TEST_VALUE);
		scv_config_set_current(&test_state.test_config);
		scv_config_flush();
		scv_config_commit();
		test_state.state = SCV_TEST_STATE_COMMIT_FIRST_REBOOT;
		test_scv_reboot();
		return;
	}

	if( test_state.state == SCV_TEST_STATE_COMMIT_FIRST_REBOOT ) {
		test_state.state = SCV_TEST_STATE_COMMIT_SECOND_REBOOT;
		test_scv_reboot();
		return;
	}

	if( test_state.state == SCV_TEST_STATE_COMMIT_SECOND_REBOOT ) {
		/* SCV should not have gone back to defaults.
		 */
		test_scv_assert_config(&test_state.test_config, SCV_TEST_VALUE);

		/* Set config back to defaults.
		 */
		scv_config_set_current(&test_state.default_config);
		scv_config_flush();
		scv_config_commit();
		test_state.state = SCV_TEST_STATE_COMMIT_COMPLETE;
		test_scv_reboot();
		return;
	}

	if( test_state.state == SCV_TEST_STATE_COMMIT_COMPLETE ) {
		/* SCV back to default, testing complete.
		 */
		test_state.state = SCV_TEST_STATE_UNUSED;
		return;
	}
}

TEST_SUITE( scv_config )
{

	char const* variable_name = "tc3.bin";
	bool_t err;

	err = initialize_file_variable(&test_cycle, kitp->fs, variable_name, sizeof(struct scv_test_state));
	if( !err )
	{
		ABORT_TEST( "Failed to construct variable" );
	}

	ADD_TEST(ram_update);
	ADD_TEST(flush);
	ADD_TEST(commit);

	((non_volatile_variable_t*) &test_cycle)->destroy((non_volatile_variable_t*) &test_cycle);
	kitp->fs->delete(kitp->fs, variable_name);
}


