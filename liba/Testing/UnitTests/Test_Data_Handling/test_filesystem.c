/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file test_logger.c
 * @author Brendan Bruner
 * @date May 7, 2015
 */

#include <filesystems/filesystem.h>
#include <dependency_injection.h>
#include <test_suites.h>
#include <string.h>

#define DEMO_FILE_NAME_0 "derp0.txt"
#define DEMO_FILE_NAME_1 "derp1.txt"
#define DEMO_FILE_NAME_2 "derp2.txt"
#define RENAME_ORIG_0 "rn0.txt"
#define RENAME_ORIG_1 "rn1.txt"
#define MAX_HANDLES 2
#define MAX_OPEN_FILES_GUESS 50
#define TEST_FILESYSTEM_FLUSH_LIMIT 1

static driver_toolkit_t *kit;
static filesystem_t *filesystem, *fs;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST( methods_bound )
{
	ASSERT( "open bound", filesystem->open != NULL );
	ASSERT( "close bound", filesystem->close != NULL );
	ASSERT( "delete bound", filesystem->delete != NULL );
	ASSERT( "reformat bound", filesystem->reformat != NULL );
}

TEST( open )
{
	file_t		*files[MAX_OPEN_FILES_GUESS]; /* 10 is approximate guess of max file */
	fs_error_t		err;
	uint32_t 		i;
	char 			open_name[strlen("derpn.txt")+1];

	open_name[0] = 'd'; open_name[1] = 'e'; open_name[2] = 'r'; open_name[3] = 'p';
	open_name[4] = 'n'; open_name[5] = '.'; open_name[6] = 't'; open_name[7] = 'x';
	open_name[8] = 't'; open_name[9] = '\0';

	for( i = 0; i < MAX_OPEN_FILES_GUESS; ++i )
	{
		open_name[4] = (char)(i + 'A');
		files[i] = filesystem->open( filesystem, &err, open_name, FS_OPEN_ALWAYS, USE_POLLING );
		if( err == FS_TOO_MANY_OPEN_FILES )
		{
			break;
		}
		ASSERT( "%d file didn't open, err = %d", err == FS_OK, i, err );
	}

	ASSERT( "returned handle is not null handle", fs_handle_get_busy_status( (fs_handle_t *) files[i] ) == HANDLE_NULL );

	filesystem->close( filesystem, files[0] );
	files[0] = filesystem->open( filesystem, &err, DEMO_FILE_NAME_2, FS_OPEN_ALWAYS, USE_POLLING );
	ASSERT( "can't open %d'th file after closing another with err %d", err == FS_OK, i+1, err );

	filesystem->close_all( filesystem );
}

TEST( close )
{
	file_t		*file;
	fs_error_t		err;

	file = filesystem->open( filesystem, &err, DEMO_FILE_NAME_2, FS_OPEN_ALWAYS, USE_POLLING );
	err = filesystem->close( filesystem, file );
	ASSERT( "file didn't close", err == FS_OK );
}

TEST( close_all )
{
	uint32_t i, handle_count = 0;
	fs_error_t err;
	char 			open_name[strlen("derpn.txt")+1];

	open_name[0] = 'd'; open_name[1] = 'e'; open_name[2] = 'r'; open_name[3] = 'p';
	open_name[4] = 'n'; open_name[5] = '.'; open_name[6] = 't'; open_name[7] = 'x';
	open_name[8] = 't'; open_name[9] = '\0';


	for( i = 0; i < MAX_OPEN_FILES_GUESS; ++i )
	{
		open_name[4] = (char)(i + 'A');
		filesystem->open( filesystem, &err, open_name, FS_OPEN_ALWAYS, USE_POLLING );
		if( err == FS_TOO_MANY_OPEN_FILES )
		{
			handle_count = i;
			break;
		}
		else if( err != FS_OK )
		{
			filesystem->close_all( filesystem );
			ABORT_TEST( "Failed to open files required to run test" );
		}
	}

	err = filesystem->close_all( filesystem );
	ASSERT( "error %d closing all", err == FS_OK, err );

	for( i = 0; i < handle_count; ++i )
	{
		open_name[4] = (char)(i + 'A');
		filesystem->open( filesystem, &err, open_name, FS_OPEN_ALWAYS, USE_POLLING );
		ASSERT( "%d file didn't open after closing all", err == FS_OK, i );
	}

	filesystem->close_all( filesystem );
}

TEST( delete )
{
	file_t		*file;
	fs_error_t	err, erro, errr, errw;
	uint32_t 	byte_count;

	/* Create file */
	file = filesystem->open( filesystem, &erro, DEMO_FILE_NAME_1, FS_CREATE_ALWAYS, USE_POLLING );
	errw = file->write( file, (uint8_t *) "derp", 4, &byte_count );
	errr = filesystem->close( filesystem, file );
	/* delete it */
	err = filesystem->delete( filesystem, DEMO_FILE_NAME_1 );
	ASSERT( "no error code, err = %d, erro = %d, errr = %d, errw = %d", err == FS_OK, err, erro, errr, errw );
	file = filesystem->open( filesystem, &err, DEMO_FILE_NAME_1, FS_OPEN_EXISTING, USE_POLLING );
	ASSERT( "file does not exist", err == FS_NO_FILE );
}

TEST( delete_handle )
{
	file_t		*file;
	fs_error_t	err;

	/* Create file */
	file = filesystem->open( filesystem, &err, DEMO_FILE_NAME_0, FS_OPEN_ALWAYS, USE_POLLING );
	/* delete it */
	err = filesystem->delete_handle( filesystem, file );
	ASSERT( "no error code, err = %d", err == FS_OK, err );
	file = filesystem->open( filesystem, &err, DEMO_FILE_NAME_0, FS_OPEN_EXISTING, USE_POLLING );
	ASSERT( "file does not exist", err == FS_NO_FILE );
}

TEST( close_null_handle )
{
	file_null_t file_null;
	file_t		*file;
	fs_error_t err;
	initialize_file_null( &file_null, fs );
	file = (file_t *) &file_null;

	err = fs->close( fs, file );
	ASSERT( "close null file failed", err == FS_INVALID_OBJECT );

	((fs_handle_t *) file)->destroy( (fs_handle_t *) file );
}

TEST( rename )
{
	file_t		*file;
	fs_error_t	err;
	char 		*in_msg = "rename test";
	char		out_msg[strlen( "rename test" )];
	uint32_t	byte_count;

	filesystem->delete( filesystem, RENAME_ORIG_1 );

	/* Create file */
	file = filesystem->open( filesystem, &err, RENAME_ORIG_0, FS_CREATE_ALWAYS, USE_POLLING );
	err = file->write( file, (uint8_t *) in_msg, strlen( in_msg ), &byte_count );
	err = filesystem->close( filesystem, file );
	/* rename it */
	err = filesystem->rename( filesystem, RENAME_ORIG_0, RENAME_ORIG_1 );
	ASSERT( "no error code, err = %d", err == FS_OK, err );
	file = filesystem->open( filesystem, &err, RENAME_ORIG_1, FS_OPEN_ALWAYS, USE_POLLING );
	err = file->read( file, (uint8_t *) out_msg, strlen( in_msg ), &byte_count );
	ASSERT( "file renamed, out msg = %.*s", strncmp( in_msg, out_msg, strlen( in_msg ) ) == 0, strlen( in_msg ), out_msg );
	filesystem->close( filesystem, file );
}

TEST( rename_handle )
{
	file_t		*file;
	fs_error_t	err;
	char 		*in_msg = "rename test";
	char		out_msg[strlen( "rename test" )];
	uint32_t	byte_count;

	filesystem->delete( filesystem, RENAME_ORIG_1 );

	/* Create file */
	file = filesystem->open( filesystem, &err, RENAME_ORIG_0, FS_CREATE_ALWAYS, USE_POLLING );
	err = file->write( file, (uint8_t *) in_msg, strlen( in_msg ), &byte_count );
	/* rename it */
	err = filesystem->rename_handle( filesystem, file, RENAME_ORIG_1 );
	ASSERT( "no error code, err = %d", err == FS_OK, err );
	err = file->read( file, (uint8_t *) out_msg, strlen( in_msg ), &byte_count );
	ASSERT( "file renamed, out msg = %s", strncmp( in_msg, out_msg, strlen( in_msg ) ) == 0, out_msg );
	filesystem->close( filesystem, file );
}

TEST( remap )
{
	file_t		*file1;
	uint32_t 	byte_count;
	fs_error_t	err, temp_err;
	char 		*in_msg = "rename test";
	char		out_msg[strlen( "rename test" )];

	file1 = filesystem->open( filesystem, &err, DEMO_FILE_NAME_0, FS_CREATE_ALWAYS, USE_POLLING );
	file1->write( file1, (uint8_t *) in_msg, strlen( in_msg ), &byte_count );

	err = filesystem->remap( filesystem, file1, DEMO_FILE_NAME_1, FS_CREATE_ALWAYS );

	ASSERT( "error, %d, on remap", err == FS_OK, err );
	ASSERT( "mapped to a new file of size %d (should be 0)", file1->size( file1, &temp_err) == 0, file1->size( file1, &temp_err ) );
	filesystem->close( filesystem, file1 );
	file1 = filesystem->open( filesystem, &err, DEMO_FILE_NAME_0, FS_OPEN_ALWAYS, USE_POLLING );
	file1->read( file1, (uint8_t *) out_msg, strlen( in_msg ), &byte_count );
	ASSERT( "original file has wrong msg \"%.*s\"", strncmp( in_msg, out_msg, strlen( in_msg ) ) == 0, strlen( in_msg ), out_msg );
	filesystem->close( filesystem, file1 );
}

TEST_SUITE( filesystem_api )
{
	uint32_t redo;
	kit = kitp;

	fs = kit->fs;
	filesystem = fs;



	for( redo = 0; redo < TEST_FILESYSTEM_FLUSH_LIMIT; ++redo )
	{
		ADD_TEST( methods_bound );
		ADD_TEST( open );
		ADD_TEST( close );
		ADD_TEST( close_all );
		ADD_TEST( delete );
		ADD_TEST( delete_handle );
		/* ADD_TEST( reformat ); destructive test */
		ADD_TEST( close_null_handle );
		ADD_TEST( rename );
		ADD_TEST( rename_handle );
		ADD_TEST( remap );
	}

}
