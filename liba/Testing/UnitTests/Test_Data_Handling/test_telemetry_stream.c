/*
 *
 * Copyright (C) 2018  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */

/* test_telemetry_stream.c
 *
 *  Created on: Apr 3, 2018
 *      Author: Brendan Bruner
 */
#include <unit.h>
#include <string.h>
#include <logger/telemetry_stream.h>

#define TEST_STREAM_LOGGER_MASTER_TABLE "TSLMT.bin"
#define TEST_STREAM_LOGGER_IDENTIFIER	'U'
#define TEST_STREAM_LOGGER_MAX_CAPACITY 3
#define DUMMY_DATA_ "test telemetry stream"
#define DUMMY_DATA ((uint8_t*) DUMMY_DATA_)
#define DUMMY_DATA_LEN ((uint32_t) strlen(DUMMY_DATA_))
#define TEST_STREAM_FILE_COEF 3
#define TEST_STREAM_FILE_SIZE (TEST_STREAM_FILE_COEF*DUMMY_DATA_LEN)
#define TEST_STREAM_BUCKET0 "001U0001.bin"
#define TEST_STREAM_BUCKET1 "002U0002.bin"
#define TEST_STREAM_BUCKET2 "000U0003.bin"
#define TEST_STREAM_BUCKET3 "001U0004.bin"
#define TEST_STREAM_BUCKET4 "002U0005.bin"
#define TEST_STREAM_BUCKET5 "000U0006.bin"
#define TEST_STREAM_BUCKET6 "001U0007.bin"
#define TEST_STREAM_BUCKET7 "002U0008.bin"

static struct telemetry_stream_t test_stream;
static struct logger_t test_logger;

TEST_SETUP( )
{
}
TEST_TEARDOWN( )
{
}

TEST( reopen_empty_stream )
{
	file_t* fd_stream;
	int err;
	uint32_t written;
	const char* fd_name;

	/* Assume logger back end is initially empty. The stream should insert a bucket
	 * into the logger and return a descriptor to it.
	 */
	telemetry_stream_close_file(&test_stream);
	fd_stream = telemetry_stream_get_file(&test_stream, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream);
	ASSERT("Empty stream didn't open new file: err %d", err == TELEMETRY_STREAM_NEW_FILE, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET0) == 0, fd_name );
	fd_stream->write(fd_stream, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	telemetry_stream_close_file(&test_stream);
}

TEST( reopen_non_empty_stream )
{
	file_t* fd_stream;
	int err;
	uint32_t written;
	const char* fd_name;

	/* Assume logger back end has one bucket in it. The stream should return
	 * a descriptor to that bucket.
	 */
	telemetry_stream_close_file(&test_stream);
	fd_stream = telemetry_stream_get_file(&test_stream, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream);
	ASSERT("non empty stream didn't re-open file: err %d", err == TELEMETRY_STREAM_OK, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET0) == 0, fd_name );
	fd_stream->write(fd_stream, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	telemetry_stream_close_file(&test_stream);
}

TEST( open_next_in_stream )
{
	file_t* fd_stream;
	int err;
	uint32_t written;
	const char* fd_name;

	/* Assume logger back end has one bucket in it. The stream should insert
	 * a new bucket and return a descriptor to it.
	 */
	telemetry_stream_next_file(&test_stream);
	fd_stream = telemetry_stream_get_file(&test_stream, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream);
	ASSERT("Non empty stream didn't open new file: err %d", err == TELEMETRY_STREAM_NEW_FILE, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET1) == 0, fd_name );
	fd_stream->write(fd_stream, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	telemetry_stream_close_file(&test_stream);
}

TEST( stream_already_opened )
{
	file_t* fd_stream0;
	file_t* fd_stream1;
	int err;
	uint32_t written;
	const char* fd_name;

	/* Assume logger back end has two buckets in it. The stream should open the
	 * HEAD bucket with the first get(), then return the same bucket with the second
	 * get()
	 */
	telemetry_stream_close_file(&test_stream);
	fd_stream0 = telemetry_stream_get_file(&test_stream, &err);
	fd_stream1 = telemetry_stream_get_file(&test_stream, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream1);
	ASSERT("Stream descriptors are different", fd_stream0 == fd_stream1);
	ASSERT("Non empty stream didn't re-open file: err %d", err == TELEMETRY_STREAM_OK, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET1) == 0, fd_name );
	fd_stream1->write(fd_stream1, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	telemetry_stream_close_file(&test_stream);
}

TEST( apply_constraints )
{
	file_t* fd_stream;
	int err, i;
	uint32_t written;
	const char* fd_name;

	/* Assume logger back end has two buckets in it. The stream should open the
	 * HEAD bucket and this test will fill it beyond capacity. The next call to get()
	 * should result in a third bucket put in the stream.
	 */
	telemetry_stream_close_file(&test_stream);
	fd_stream = telemetry_stream_get_file(&test_stream, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream);
	ASSERT("Non empty stream didn't re-open file: err %d", err == TELEMETRY_STREAM_OK, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET1) == 0, fd_name );


	/* This will fill the stream beyond it's max capacity.
	 */
	for( i = 0; i < TEST_STREAM_FILE_COEF+1; ++i ) {
		fd_stream->write(fd_stream, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	}

	fd_stream = telemetry_stream_get_file(&test_stream, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream);
	ASSERT("over capacity stream didn't insert new bucket: err %d", err == TELEMETRY_STREAM_NEW_FILE, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET2) == 0, fd_name );
	fd_stream->write(fd_stream, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	telemetry_stream_close_file(&test_stream);
}


TEST( reuse_bucket_after_reset )
{
	struct telemetry_stream_t stream2;
	file_t* fd_stream;
	int err;
	uint32_t written;
	const char* fd_name;

	telemetry_stream_init(&stream2, &test_logger, (size_t) TEST_STREAM_FILE_SIZE, TELEMETRY_STREAM_REUSE);

	/* Assume logger back end has three buckets in it. The stream should return the HEAD bucket.
	 */
	fd_stream = telemetry_stream_get_file(&stream2, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream);
	ASSERT("New stream didn't reopen HEAD bucket: err %d", err == TELEMETRY_STREAM_OK, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET2) == 0, fd_name );
	fd_stream->write(fd_stream, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	telemetry_stream_close_file(&stream2);

}

TEST( new_bucket_after_reset )
{
	struct telemetry_stream_t stream2;
	file_t* fd_stream;
	int err;
	uint32_t written;
	const char* fd_name;

	telemetry_stream_init(&stream2, &test_logger, (size_t) TEST_STREAM_FILE_SIZE, TELEMETRY_STREAM_NEW);

	/* Assume logger back end has three buckets in it. The stream should insert a new bucket
	 * and return that.
	 */
	fd_stream = telemetry_stream_get_file(&stream2, &err);
	fd_name = fs_handle_get_name((fs_handle_t*) fd_stream);
	ASSERT("New stream didn't insert new HEAD bucket: err %d", err == TELEMETRY_STREAM_NEW_FILE, err);
	ASSERT("stream descriptor is incorrect: %s", strcmp(fd_name, TEST_STREAM_BUCKET3) == 0, fd_name );
	fd_stream->write(fd_stream, DUMMY_DATA, DUMMY_DATA_LEN, &written);
	telemetry_stream_close_file(&stream2);
}

TEST_SUITE( telemetry_stream )
{
	logger_error_t tl_err;

	tl_err = initialize_logger(&test_logger,
							kitp->fs,
							TEST_STREAM_LOGGER_MASTER_TABLE,
							TEST_STREAM_LOGGER_IDENTIFIER,
							TEST_STREAM_LOGGER_MAX_CAPACITY );
	if( tl_err != LOGGER_OK ) {
		UNIT_PRINT("Failed to init logger");
		return;
	}

	telemetry_stream_init(&test_stream, &test_logger, (size_t) TEST_STREAM_FILE_SIZE, TELEMETRY_STREAM_REUSE);

	/* Must do tests in this order
	 */
	ADD_TEST( reopen_empty_stream );
	ADD_TEST( reopen_non_empty_stream );
	ADD_TEST( open_next_in_stream );
	ADD_TEST( stream_already_opened );
	ADD_TEST( apply_constraints );
	ADD_TEST( reuse_bucket_after_reset );
	ADD_TEST( new_bucket_after_reset );

	/* Delete all buckets created by this test.
	 */
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET0);
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET1);
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET2);
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET3);
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET4);
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET5);
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET6);
	kitp->fs->delete(kitp->fs, TEST_STREAM_BUCKET7);
}


