/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file uart_lpc.h
 * @author Brendan Bruner
 * @date Sep 25, 2015
 */
#ifndef INCLUDE_UART_UART_LPC_H_
#define INCLUDE_UART_UART_LPC_H_

#include <uart/uart.h>
#include <portable_types.h>
#include <chip.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define UART_PORT0	0
#define UART_PORT1	1
#define UART_PORT2	2
#define UART_PORT3  3

#ifdef __LPC17XX__
#define UART_PRINT_BUFFER_SIZE 256
extern char uart_print_buffer[];
#define serial_printf( ... ) \
	do{																						\
		uart_t* uart = (uart_t*) initialize_uart_lpc_simple( UART_PORT2 );					\
		int written = 0;																	\
		written = snprintf( uart_print_buffer, UART_PRINT_BUFFER_SIZE, __VA_ARGS__ );		\
		if( written > 0 ){																	\
			uart->send( uart, (uint8_t*) uart_print_buffer, (uint32_t) written );			\
		}																					\
		task_delay( 100 );																	\
	} while( 0 )
#endif


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct uart_lpc_t
 * @extends uart_t
 * @brief
 * 		Structure for intefacing with the LPC1769's uart.
 * @details
 * 		Structure for intefacing with the LPC1769's uart.
 * @attention
 * 		Only uart port 2 has an implementation. Other ports will not construct.
 * @var uart_lpc_t::_read_mutex_
 * 		<b>Private</b> Provide mutual exclusion to uart_t::read.
 * @var uart_lpc_t::_send_mutex_
 * 		<b>Private</b> Provide mutual exclusion to uart_t::send.
 * @var uart_lpc_t::_uart_port_type_
 * 		<b>Private</b> Reference to lpc type uart port being using.
 * @var uart_lpc_t::_uart_port_num_
 * 		<b>Private</b> Integer reference to uart port being used.
 * @var uart_lpc_t::_irq_
 * 		<b>Private</b> Reference to the uarts interrupt.
 * @var uart_lpc_t::_port_buffer_
 * 		<b>Private</b> Reference to the uarts rings buffer.
 * @var uart_lpc_t::_is_init_
 * 		<b>Private</b> Used to prevent duplicate uart hardware initialization.
 * @var uart_lpc_t::_int_status_
 * 		<b>Private</b> Interrupt status.
 */
typedef struct uart_lpc_t uart_lpc_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct uart_lpc_t
{
	uart_t _super_; /* Must be first. */
	mutex_t _read_mutex_;
	semaphore_t _send_mutex_;
	LPC_USART_T* _uart_port_type_;
	uint8_t _uart_port_num_;
	IRQn_Type _irq_;
	uint8_t _is_init_;
	RINGBUFF_T* _rx_ring_;
	RINGBUFF_T* _tx_ring_;
	uint8_t* _rx_buff_;
	uint8_t* _tx_buff_;
	PINMUX_GRP_T const* _pin_mux_;
	volatile FlagStatus _int_status_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
uart_lpc_t* initialize_uart_lpc
(
	uint8_t uart_port,
	uint32_t baud,
	uint32_t uart_config, /* See Chip_UART_ConfigData( ). */
	uint32_t fifo_config /* See Chip_UART_SetupFIFOS( ). */
);

uart_lpc_t* initialize_uart_lpc_simple( uint8_t uart_port );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_UART_UART_LPC_H_ */
