/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file subystem_defines.h
 * @author Brendan Bruner
 * @date May 19, 2015
 */
#ifndef INCLUDE_SUBSYSTEM_DEFINES_H_
#define INCLUDE_SUBSYSTEM_DEFINES_H_

/********************************************************************************/
/* Uart																			*/
/********************************************************************************/
/* Comment out the defines for the uarts not being used. */
//#define USE_UART0
#define USE_UART1
#define USE_UART2
#define USE_UART3

#endif /* INCLUDE_SUBSYSTEM_DEFINES_H_ */
