/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mem.h
 * @author Brendan Bruner
 * @date Nov 12, 2015
 */
#ifndef INCLUDE_MEM_H_
#define INCLUDE_MEM_H_

#include <stdlib.h>

extern void* pvPortMalloc( size_t );
extern void vPortFree( void* );

#define OBCMalloc( size ) pvPortMalloc( size )
#define OBCFree( mem ) vPortFree( mem )

#endif /* INCLUDE_MEM_H_ */
