/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file post_ejection_pin
 * @author Brendan Bruner
 * @date Feb 12, 2015
 *
 * This file defines the API for querying the post ejection pin. This pin
 * will read "ejected"  or "not ejected" depending on if the satellite is
 * in its pod or not.
 */
#ifndef INCLUDE_EJECTION_PIN_H_
#define INCLUDE_EJECTION_PIN_H_

/* Required typedefs */
typedef struct ejection_pin_t ejection_pin_t;
typedef unsigned char eject_status_t;

/* Required defines */
#define NOT_EJECTED	((eject_status_t) 0)
#define EJECTED		((eject_status_t) 1)

/**
 * @struct ejection_pin_t
 * @brief
 * 	 	API for determining when the satellite has been ejected
 * 	 	from its pod.
 *
 * @details
 * 		API for determining when the satellite has been ejected
 * 		from its pod. This structure must be initialized before being used.
 * 		For example,
 * 			@code
 * 			ejection_pin_t pin_mock_up;
 * 			initialize_ejection_pin_mock_up( &pin_mock_up );
 * 			@endcode
 *
 * @var ejection_pin_t::is_ejected
 * 		A function pointer with prototype,
 * 			@code
 * 			eject_status_t is_ejected( ejection_pin_t * );
 * 			@endcode
 * 		The structure used to access this function pointer must be the
 * 		same structure passed in as the function argument.
 * 		The returned value is one of:
 * 			* EJECTED
 * 			* NOT_EJECTED
 */
struct ejection_pin_t
{
	void *data;
	eject_status_t (*is_ejected)( ejection_pin_t * );
};

/**
 * @brief
 * 		Initializes an ejection_pin_t structure to use a gpio pin
 * 		on the lpc1769 board as the ejection pin.
 *
 * @details
 * 		Initializes an ejection_pin_t structure to use a gpio pin
 * 		on the lpc1769 board as the ejection pin. When this pin is
 * 		read as high, the satellite is not ejected. When this pin is
 * 		read as low, the satellite is ejected from its pod.
 *
 * @param driver
 * 		A pointer to the ejection_pin_t structure being used.
 *
 * @memberof ejection_pin_t
 */
void initialize_ejection_pin_lpc( ejection_pin_t *driver );

/**
 * @brief
 * 		Initialize an ejection_pin_t structure to spoof an ejection
 * 		pin.
 *
 * @details
 * 		Initialize an ejection_pin_t structure to spoof an ejection
 * 		pin. The method
 * 			@code
 * 			ejection_pin_t::is_ejected
 * 			@endcode
 * 		will return NOT_EJECTED the first two times it is called then
 * 		always return EJECTED everytime after.
 *
 * @param driver
 * 		A pointer to the ejection_pin_t structure being used.
 *
 * @memberof ejection_pin_t
 */
void initialize_ejection_pin_mock_up( ejection_pin_t *driver );


/**
 * @memberof ejection_pin_t
 * @brief destroy the structure
 * @details destroy the structure
 * @param[in] self A pointer to the structure to destroy
 */
void destroy_ejection_pin( ejection_pin_t *self );

#endif /* INCLUDE_EJECTION_PIN_H_ */
