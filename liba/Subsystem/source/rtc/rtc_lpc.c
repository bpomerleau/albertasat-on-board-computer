/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file rtc_lpc.c
 * @author Brendan Bruner
 * @date Jun 24, 2015
 */

#include <time.h>
#include <rtc.h>
#include <chip.h>
#include <portable_types.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define _NOT_INIT 0
#define _IS_INIT 1


/********************************************************************************/
/* Private Variable Defines														*/
/********************************************************************************/
static uint8_t _is_init = _NOT_INIT;
static mutex_t _mutex;
static rtc_t rtc_instance;
#ifdef __LPC17XX__
static int mon_yday[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
#endif

/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static 	void enable( rtc_t *rtc )
{
	DEV_ASSERT( rtc );
	Chip_RTC_Enable(LPC_RTC, ENABLE);
}

static void disable( rtc_t *rtc )
{
	DEV_ASSERT( rtc );
	Chip_RTC_Enable(LPC_RTC, DISABLE);
}

static uint8_t set_time( rtc_t *rtc, rtc_time_t *time )
{
	DEV_ASSERT( rtc );
	DEV_ASSERT( time );

	RTC_TIME_T FullTime;

	if( time->seconds > RTC_SECOND_MAX ||
		time->minutes > RTC_MINUTE_MAX ||
		time->hours > RTC_HOUR_MAX ||
		time->day_of_month > RTC_DAYOFMONTH_MAX ||
		time->month > RTC_MONTH_MAX ||
		time->year >= RTC_YEAR_MAX
		)
	{
		return RTC_TIME_NOT_SET;
	}

	lock_mutex( _mutex, BLOCK_FOREVER );
	FullTime.time[RTC_TIMETYPE_SECOND]  = time->seconds;
	FullTime.time[RTC_TIMETYPE_MINUTE]  = time->minutes;
	FullTime.time[RTC_TIMETYPE_HOUR]    = time->hours;
	FullTime.time[RTC_TIMETYPE_DAYOFMONTH]  = time->day_of_month;
	FullTime.time[RTC_TIMETYPE_MONTH]   = time->month;
	FullTime.time[RTC_TIMETYPE_YEAR]    = time->year;

	Chip_RTC_SetFullTime(LPC_RTC, &FullTime);
	unlock_mutex( _mutex );

	return RTC_TIME_SET;
}

static void get_time( rtc_t *rtc, rtc_time_t *time )
{
	DEV_ASSERT( rtc );
	DEV_ASSERT( time );

	RTC_TIME_T RTCFullTime;

	lock_mutex( _mutex, BLOCK_FOREVER );
	Chip_RTC_GetFullTime(LPC_RTC, &RTCFullTime);
	unlock_mutex( _mutex );

	time->hours = RTCFullTime.time[RTC_TIMETYPE_HOUR];
	time->minutes = RTCFullTime.time[RTC_TIMETYPE_MINUTE];
	time->seconds = RTCFullTime.time[RTC_TIMETYPE_SECOND];
	time->day_of_month = RTCFullTime.time[RTC_TIMETYPE_DAYOFMONTH];
	time->month = RTCFullTime.time[RTC_TIMETYPE_MONTH];
	time->year = RTCFullTime.time[RTC_TIMETYPE_YEAR];
}

static time_t rtc_mktime( struct tm* date )
{
	time_t time;

	/* Source: */
	/* http://stackoverflow.com/questions/9542278/how-do-i-convert-2012-03-02-into-unix-epoch-time-in-c 	Oct. 2015*/
	if( date->tm_mon >= 0 && date->tm_mon <= 11 )
	{
		date->tm_yday = mon_yday[date->tm_mon] + date->tm_mday;
		time = 	date->tm_sec + (date->tm_min*60) + (date->tm_hour*3600) + (date->tm_yday*86400) +
				((date->tm_year-70)*31536000) + (((date->tm_year-69)/4)*86400) -
				(((date->tm_year-1)/100)*86400) + (((date->tm_year+299)/400)*86400);
	}
	else
	{
		time = 0;
	}

	return time;
}


/********************************************************************************/
/* Constructor / Destructor														*/
/********************************************************************************/
rtc_t* get_rtc_lpc( )
{
	rtc_t* self = &rtc_instance;
	RTC_TIME_T FullTime;

	/* Check if singleton instance is already initialized. */
	if( _is_init == _IS_INIT )
	{
		return self;
	}

	extern void initialize_rtc_base( rtc_t* );
	initialize_rtc_base( self );

	/* Bind method pointers. */
	self->enable = enable;
	self->disable = disable;
	self->set_time = set_time;
	self->get_time = get_time;
	self->_.mktime = rtc_mktime;

	/* Create mutex. */
	new_mutex( _mutex );
	if( _mutex == NULL )
	{
		return NULL;
	}

	/* Set up the rtc peripheral chip. */
	Chip_RTC_Init(LPC_RTC);
	Chip_RTC_Enable(LPC_RTC, ENABLE);

	/* default time is 00:00:00PM, 2000-00-00 */
	FullTime.time[RTC_TIMETYPE_SECOND]  = 0;
	FullTime.time[RTC_TIMETYPE_MINUTE]  = 0;
	FullTime.time[RTC_TIMETYPE_HOUR]    = 0;
	FullTime.time[RTC_TIMETYPE_DAYOFMONTH]  = 0;
	FullTime.time[RTC_TIMETYPE_DAYOFWEEK]   = 0;
	FullTime.time[RTC_TIMETYPE_DAYOFYEAR]   = 0;
	FullTime.time[RTC_TIMETYPE_MONTH]   = 0;
	FullTime.time[RTC_TIMETYPE_YEAR]    = 2000;
	Chip_RTC_SetFullTime(LPC_RTC, &FullTime);

	/* Initialization done, */
	_is_init = _IS_INIT;

	return self;
}


