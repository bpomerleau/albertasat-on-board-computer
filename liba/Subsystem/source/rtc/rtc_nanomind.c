/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file rtc_lpc.c
 * @author Brendan Bruner
 * @date Jun 24, 2015
 */

#include "rtc.h"
#include "portable_types.h"
#include <dev/arm/ds1302.h>
#include <printing.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define _NOT_INIT 0
#define _IS_INIT 1


/********************************************************************************/
/* Private Variable Defines														*/
/********************************************************************************/
//static uint8_t _is_init = _NOT_INIT;
static mutex_t _mutex;
struct ds1302_clock *dclock;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static 	void enable( rtc_t *rtc )
{
	DEV_ASSERT( rtc );
	ds1302_clock_resume();
}

static void disable( rtc_t *rtc )
{
	DEV_ASSERT( rtc );
	ds1302_clock_halt();
}

static uint8_t set_time( rtc_t *rtc, rtc_time_t *time )
{
	DEV_ASSERT( rtc );
	DEV_ASSERT( time );

	if( time->seconds > 60 ||
		time->minutes > 60 ||
		time->hours > 23 ||
		time->day_of_month > 31 ||
		time->month > 12 ||
		time->year >= 2100 ||
		time->year < 2000
		)
	{
		return RTC_TIME_NOT_SET;
	}

	struct ds1302_clock date;
	date.seconds 	= time->seconds;
	date.minutes	= time->minutes;
	date.hour 		= time->hours;
	date.date 		= time->day_of_month;
	date.month 		= time->month;
	date.day 		= 0;
	date.year 		= time->year - 2000;
	date.wp			= 0;

	lock_mutex( _mutex, BLOCK_FOREVER );
	ds1302_clock_write_burst( &date );
	unlock_mutex( _mutex );

	return RTC_TIME_SET;
}

static void set_ds1302_time_(struct ds1302_clock *dclock)
{
	lock_mutex( _mutex, BLOCK_FOREVER );
	ds1302_clock_write_burst( dclock );
	unlock_mutex( _mutex );
}

static void get_time( rtc_t *rtc, rtc_time_t *time )
{
	DEV_ASSERT( rtc );
	DEV_ASSERT( time );

	struct ds1302_clock date;

	lock_mutex( _mutex, BLOCK_FOREVER );
	ds1302_clock_read_burst( &date );
	unlock_mutex( _mutex );

	time->seconds = date.seconds;
	time->minutes = date.minutes;
	time->hours = date.hour;
	time->day_of_month = date.date;
	time->month = date.month;
	time->year = date.year + 2000;
}

static time_t get_ds1302_time_( void )
{
	struct ds1302_clock date;
	time_t seconds;

	lock_mutex( _mutex, BLOCK_FOREVER );
	ds1302_clock_read_burst( &date );
	unlock_mutex( _mutex );
	ds1302_clock_to_time(&seconds, &date);
	return seconds;
}

static time_t rtc_mktime( struct tm* date )
{
	DEV_ASSERT( date );
	return mktime( date );
}


/********************************************************************************/
/* Constructor / Destructor														*/
/********************************************************************************/
rtc_t* get_rtc_nanomind( )
{
	static rtc_t* self = NULL;
	static rtc_t self_;

	if( self == NULL )
	{
		self = &self_;

		extern void initialize_rtc_base( rtc_t* );
		initialize_rtc_base( (rtc_t*) self );

		self->disable = disable;
		self->set_time = set_time;
		self->set_ds1302_time = set_ds1302_time_;
		self->get_time = get_time;
		self->get_ds1302_time = get_ds1302_time_;
		self->_.mktime = rtc_mktime;
		self->enable = enable;

		new_mutex( _mutex );
		if( _mutex == NULL )
		{
			return NULL;
		}
	}

	return self;
}


