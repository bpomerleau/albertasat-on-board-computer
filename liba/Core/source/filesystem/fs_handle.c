/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_handle_base.c
 * @author Brendan Bruner
 * @date May 12, 2015
 */

#include <filesystems/fs_handle.h>


/********************************************************************************/
/* Public Methods																*/
/********************************************************************************/
uint8_t fs_handle_get_busy_status( fs_handle_t *self )
{
	DEV_ASSERT( self );

	uint8_t temp_status;

	lock_mutex( self->_busy_mutex_, BLOCK_FOREVER );
	temp_status = self->_busy_status_;
	unlock_mutex( self->_busy_mutex_ );

	return temp_status;
}

void fs_handle_set_busy( fs_handle_t *self )
{
	DEV_ASSERT( self );

	lock_mutex( self->_busy_mutex_, BLOCK_FOREVER );
	self->_busy_status_ = HANDLE_BUSY;
	unlock_mutex( self->_busy_mutex_ );
}

void fs_handle_set_free( fs_handle_t *self )
{
	DEV_ASSERT( self );

	lock_mutex( self->_busy_mutex_, BLOCK_FOREVER );
	self->_busy_status_ = HANDLE_FREE;
	unlock_mutex( self->_busy_mutex_ );
}

void *fs_handle_get_native( fs_handle_t *self )
{
	DEV_ASSERT( self );

	return self->_native_handle_;
}

fs_handle_type_t fs_handle_get_type( fs_handle_t *self )
{
	return self->_type_;
}

void fs_handle_set_name( fs_handle_t *self, char const *name )
{
	uint32_t iter;

	for( iter = 0; iter < FILESYSTEM_MAX_NAME_LENGTH; ++iter )
	{
		self->_name_[iter] = name[iter];

		/* Break if end of string. */
		if( self->_name_[iter] == '\0' )
		{
			break;
		}
	}
	/* Silently truncate string in event the above for loop exceeded */
	/* FILESYSTEM_MAX_NAME_LENGTH iterations. */
	self->_name_[iter] = '\0';
}

char const *fs_handle_get_name( fs_handle_t *self )
{
	DEV_ASSERT( self );

	return (char const *) self->_name_;
}

filesystem_t *_fs_handle_get_fs( fs_handle_t *self )
{
	return self->_fs_;
}

/********************************************************************************/
/* Destructor																	*/
/********************************************************************************/
static void destroy( fs_handle_t *self )
{
	DEV_ASSERT( self );

	delete_mutex( self->_busy_mutex_ );
}


/********************************************************************************/
/* Constructor																	*/
/********************************************************************************/
uint8_t _initialize_fs_handle( fs_handle_t *self, filesystem_t *fs, fs_handle_type_t type )
{
	DEV_ASSERT( self );
	DEV_ASSERT( fs );

	/* Assign virtual destroy method. */
	self->destroy = destroy;

	/* Initialize struct variables. */
	self->_fs_ = fs;
	self->_busy_status_ = HANDLE_NULL;
	self->_type_ = type;
	new_mutex( self->_busy_mutex_ );

	/* Error check mutex initialization. */
	if( self->_busy_mutex_ == NULL )
	{
		return FS_HANDLE_FAILURE;
	}

	return FS_HANDLE_SUCCESS;
}
