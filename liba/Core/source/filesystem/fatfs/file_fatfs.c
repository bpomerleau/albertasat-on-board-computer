/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_handle_lpc_fatfs.c
 * @author Brendan Bruner
 * @date May 12, 2015
 */

#include <filesystems/fatfs/file_fatfs.h>
#include <filesystems/fatfs/fatfs_map.h>
#include <filesystems/filesystem.h>

/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define TRANSFER_BUFFER_SIZE 100
#define ASSERT_FS_OP( fs, handle, err )			do {									\
													if( err != FS_OK ) {				\
														(fs)->close( fs, handle );		\
														return err;						\
													}									\
												} while( 0 )



/********************************************************************************/
/* Virtual Methods																*/
/********************************************************************************/
/* OVERRIDE */
static fs_error_t write( file_t *self, uint8_t *msg, uint32_t size, uint32_t *written )
{
	DEV_ASSERT( self );
	ASSERT_FATFS_HANDLE_RETURN_ERR( self );
	DEV_ASSERT( msg );
	DEV_ASSERT( written );

	FRESULT 	temp_err;
	FIL 		*file;
	uint32_t 	retry;

	/* Write the message to file. */
	file = (FIL *) fs_handle_get_native( (fs_handle_t *) self );
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		temp_err = f_write( file, msg, size, (UINT *) written );
		if( temp_err == FR_OK )
		{
			break;
		}
		/* Give a delay before attempting another write. */
		_DISK_ERR_RECOVERY_DELAY( );
	}

	/* If there was an error writing, do not attempt to sync. */
	/* Just return. */
	if( temp_err != FR_OK )
	{
		return _fatfs_result_map[temp_err];
	}

	return _fatfs_result_map[temp_err];
}

/* OVERRIDE */
static fs_error_t read( file_t *self, uint8_t *msg, uint32_t size, uint32_t *read )
{
	DEV_ASSERT( self );
	ASSERT_FATFS_HANDLE_RETURN_ERR( self );
	DEV_ASSERT( msg );
	DEV_ASSERT( read );

	FRESULT 	temp_err;
	FIL			*native_handle;
	uint32_t	retry;

	/* Attempt to read. */
	native_handle = (FIL *) fs_handle_get_native( (fs_handle_t *) self );
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		temp_err = f_read( native_handle, msg, size, (UINT *) read );
		if( temp_err == FR_OK )
		{
			break;
		}
		/* Give a brief delay before attempting another read. */
		_DISK_ERR_RECOVERY_DELAY( );
	}
	return _fatfs_result_map[temp_err];
}

/* OVERRIDE */
static fs_error_t seek( file_t *self, uint32_t location )
{
	DEV_ASSERT( self );
	ASSERT_FATFS_HANDLE_RETURN_ERR( self );

	FRESULT 	temp_err;
	uint32_t	retry;
	FIL			*file;

	/* Attempt to seek. */
	file = (FIL *) fs_handle_get_native( (fs_handle_t *) self );
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		temp_err = f_lseek( file, location );
		if( temp_err == FR_OK )
		{
			break;
		}
		/* Give a brief delay before attempting to seek again. */
		_DISK_ERR_RECOVERY_DELAY( );
	}
	return _fatfs_result_map[temp_err];
}

/* OVERRIDE */
static uint32_t size( file_t *self, fs_error_t *err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );
	ASSERT_FATFS_HANDLE_RETURN_ZERO( self, *err );

	uint32_t 	temp_size;
	FIL			*native_file;

	/* Size of a file uses cached data, never does IO with memory resource. */
	/* Therefore, this will never fail. */
	native_file = (FIL *) fs_handle_get_native( (fs_handle_t *) self );
	temp_size = f_size( native_file );
	*err = FS_OK;

	return temp_size;
}

/* OVERRIDE */
static uint32_t position( file_t *self, fs_error_t *err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );
	ASSERT_FATFS_HANDLE_RETURN_ZERO( self, *err );

	uint32_t temp_location;
	FIL			*native_file;

	native_file = (FIL *) fs_handle_get_native( (fs_handle_t *) self );

	/* Position in a file uses cached data, never does IO with memory resource. */
	/* Therefore, this will never fail. */
	temp_location = f_tell( native_file );
	*err = FS_OK;
	return temp_location;
}

static fs_error_t truncate( file_t *self )
{
	DEV_ASSERT( self );
	ASSERT_FATFS_HANDLE_RETURN_ERR( self );

	FRESULT temp_err;
	uint32_t retry;
	FIL			*native_file;

	/* Truncate from the current position. */
	native_file = (FIL *) fs_handle_get_native( (fs_handle_t *) self );
	for( retry = 0; retry < _DISK_RETRY_LIMIT; ++retry )
	{
		temp_err = f_truncate( native_file );
		if( temp_err == FR_OK )
		{
			break;
		}
		/* Give a brief delay before trying again. */
		_DISK_ERR_RECOVERY_DELAY( );
	}
	return _fatfs_result_map[temp_err];
}

static fs_error_t kill
(
	file_t *self,
	char const *buffer,
	block_time_t time_out,
	uint32_t begin,
	uint32_t end
)
{
	DEV_ASSERT( self );
	DEV_ASSERT( buffer );
	ASSERT_FATFS_HANDLE_RETURN_ERR( self );

	file_t			*buff_file;
	filesystem_t	*fs;
	fs_error_t		err;
	uint8_t			transfer_buffer[TRANSFER_BUFFER_SIZE];
	uint32_t		iter, read, written, eof;
	char 			name_copy[FILESYSTEM_MAX_NAME_LENGTH+1];
	char const 		*orig_name;

	/* Get the file system managing this file. */
	fs = _fs_handle_get_fs( (fs_handle_t *) self );

	/* Open the buffer file. */
	buff_file = fs->open( fs, &err, buffer, FS_CREATE_ALWAYS, time_out );
	ASSERT_FS_OP( fs, buff_file, err );

	/* Seek to the beginning of this file. */
	err = self->seek( self, BEGINNING_OF_FILE );
	ASSERT_FS_OP( fs, buff_file, err );

	/* Get the size of this file. */
	eof = self->size( self, &err );
	ASSERT_FS_OP( fs, buff_file, err );

	/* Begin copying data into the buffer up to 'begin' - */
	/* in TRANSFER_BUFFER_SIZE chunks. */
	for( iter = 0; iter < begin; iter += TRANSFER_BUFFER_SIZE )
	{
		if( (begin - iter) <= TRANSFER_BUFFER_SIZE )
		{
			/* The transfer buffer is large enough to hold from the current */
			/* position in the file up to 'begin' */
			/* Read data up to begin. */
			err = self->read( self, transfer_buffer, (begin - iter), &read );
			ASSERT_FS_OP( fs, buff_file, err );
			if( read != (begin - iter) )
			{
				/* file ends before 'begin' is reached. */
				fs->close( fs, buff_file );
				return FS_INVALID_PARAMETER;
			}

			/* Write data into the buffer file. */
			err = buff_file->write( buff_file, transfer_buffer, (begin - iter), &written );
			ASSERT_FS_OP( fs, buff_file, err );
			if( written != (begin - iter) )
			{
				/* Not enough memory left to do this. */
				fs->close( fs, buff_file );
				return FS_NO_MEM;
			}
		}
		else
		{
			/* The transfer buffer is smaller than the number of bytes up to 'begin'. */
			/* Read TRANSFER_BUFFER_SIZE bytes. */
			err = self->read( self, transfer_buffer, TRANSFER_BUFFER_SIZE, &read );
			ASSERT_FS_OP( fs, buff_file, err );
			if( read != TRANSFER_BUFFER_SIZE )
			{
				/* file ends before TRANSFER_BUFFER_SIZE bytes are read. */
				fs->close( fs, buff_file );
				return FS_INVALID_PARAMETER;
			}

			/* Write data into the buffer file. */
			err = buff_file->write( buff_file, transfer_buffer, TRANSFER_BUFFER_SIZE, &written );
			ASSERT_FS_OP( fs, buff_file, err );
			if( written < TRANSFER_BUFFER_SIZE )
			{
				/* Not enough memory available to do this. */
				fs->close( fs, buff_file );
				return FS_NO_MEM;
			}
		}
	}

	/* All data up to 'begin' is in buff_file now. Data from here up to 'end' is being deleted. */
	/* Seek ahead to 'end' and begin copying into buff_file until eof is reached. */
	err = self->seek( self, end );
	ASSERT_FS_OP( fs, buff_file, err );

	/* Transfer data from this file into the buffer from 'end' to the end of file - */
	/* in TRANSFER_BUFFER_SIZE chunks. */
	for( iter = end; iter < eof; iter += TRANSFER_BUFFER_SIZE )
	{
		if( (eof - iter) <= TRANSFER_BUFFER_SIZE )
		{
			/* The transfer buffer is large enough to hold data up to the end of file */
			/* Read data to the end of file. */
			err = self->read( self, transfer_buffer, (eof - iter), &read );
			ASSERT_FS_OP( fs, buff_file, err );
			if( read != (eof - iter) )
			{
				/* file ends before the end of file is reached. */
				fs->close( fs, buff_file );
				return FS_INVALID_PARAMETER;
			}

			/* Write data into the buffer file. */
			err = buff_file->write( buff_file, transfer_buffer, (eof - iter), &written );
			ASSERT_FS_OP( fs, buff_file, err );
			if( written != (eof - iter) )
			{
				/* Not enough memory to do this. */
				fs->close( fs, buff_file );
				return FS_NO_MEM;
			}
		}
		else
		{
			/* The transfer buffer is smaller than the number of bytes up to the end of file. */
			/* Read TRANSFER_BUFFER_SIZE bytes. */
			err = self->read( self, transfer_buffer, TRANSFER_BUFFER_SIZE, &read );
			ASSERT_FS_OP( fs, buff_file, err );
			if( read != TRANSFER_BUFFER_SIZE )
			{
				/* file ends before TRANSFER_BUFFER_SIZE bytes are read. */
				fs->close( fs, buff_file );
				return FS_INVALID_PARAMETER;
			}

			/* Write data into the buffer file. */
			err = buff_file->write( buff_file, transfer_buffer, TRANSFER_BUFFER_SIZE, &written );
			ASSERT_FS_OP( fs, buff_file, err );
			if( written < TRANSFER_BUFFER_SIZE )
			{
				/* Not enough memory to do this. */
				fs->close( fs, buff_file );
				return FS_NO_MEM;
			}
		}
	}

	/* The plan here is: */
	/* remap this file to the buff_file, which contains all the desired results, */
	/* and rename the buff file to have the name of this file. */

	/* Close the buff_file */
	err = fs->close( fs, buff_file );
	if( err != FS_OK )
	{
		return FS_FATAL;
	}

	/* Copy name of orignal file since it will be lost with remap. */
	orig_name = fs_handle_get_name( (fs_handle_t *) self );
	for( iter = 0; iter < FILESYSTEM_MAX_NAME_LENGTH+1; ++iter )
	{
		name_copy[iter] = orig_name[iter];
	}

	/* Remap this file handle to use the buffer file. */
	err = fs->remap( fs, self, buffer, FS_OPEN_EXISTING );
	if( err != FS_OK )
	{
		return FS_KILL_SWAP;
	}

	/* Delete the original file, the desired results are all contained in the buffer file. */
	err = fs->delete( fs, name_copy );
	if( err != FS_OK )
	{
		return FS_KILL_SWAP;
	}

	/* Rename the buffer file to have the name of the original file. */
	err = fs->rename_handle( fs, self, name_copy );
	if( err != FS_OK )
	{
		return FS_KILL_SWAP;
	}

	return FS_OK;
}

static void close( file_t* self )
{
	DEV_ASSERT( self );

	fs_error_t err;
	GET_ASSERT_FATFS_HANDLE( self, err );
	if( err != FS_OK ) {
		return;
	}

	((fs_handle_t*) self)->_fs_->close( ((fs_handle_t*)self)->_fs_, self );
}

/********************************************************************************/
/* Initialization Method														*/
/********************************************************************************/
uint8_t initialize_file_fatfs( file_fatfs_t *self, filesystem_t *fs )
{
	DEV_ASSERT( self );
	DEV_ASSERT( fs );

	uint8_t init_err;

	/* Initialize super struct data. */
	init_err = _initialize_file( (file_t *) self, fs, HANDLE_FATFS );
	if( init_err != FS_HANDLE_SUCCESS )
	{
		return init_err;
	}

	/* Assign virtual methods (function pointers) */
	((file_t *) self)->position = &position;
	((file_t *) self)->read = &read;
	((file_t *) self)->seek = &seek;
	((file_t *) self)->size = &size;
	((file_t *) self)->truncate = &truncate;
	((file_t *) self)->write = &write;
	((file_t *) self)->kill = &kill;
	((file_t*) self)->close = close;

	/* Initialize member variables. */
	((fs_handle_t *) self)->_native_handle_ = (void *) &self->_native_handle_;

	return FS_HANDLE_SUCCESS;
}
