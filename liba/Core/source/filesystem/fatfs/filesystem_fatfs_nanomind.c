/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_fatfs_nanomind.c
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */

#include <filesystems/fatfs/filesystem_fatfs_nanomind.h>
#include <filesystems/fatfs/fatfs_map.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
/* OVERRIDE */
static fs_error_t close_dir( filesystem_t *self, dir_t *dir )
{
	DEV_ASSERT( self );
	DEV_ASSERT( dir );
	ASSERT_FATFS_HANDLE_IS_FATFS( dir );

	return FS_UNABLE;
}

static fs_error_t file_exists( filesystem_t *fs, char const *name )
{
	DEV_ASSERT( fs );
	DEV_ASSERT( name );

	file_t* file_to_check;
	fs_error_t file_err;

	task_delay( 50 );
	file_to_check = fs->open( fs, &file_err, name, FS_OPEN_EXISTING, USE_POLLING );
	if( file_err == FS_OK )
	{
		fs->close( fs, file_to_check );
	}
	return file_err;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_filesystem_fatfs_nanomind( filesystem_fatfs_nanomind_t *self )
{
	DEV_ASSERT( self );

	uint8_t init_err;

	/* Initialize super data. */
	init_err = _initialize_filesystem_fatfs( (filesystem_fatfs_t *) self );
	if( init_err != FILESYSTEM_SUCCESS )
	{
		return init_err;
	}

	/* Override virtual functions. */
	((filesystem_t *) self)->close_dir = close_dir;
	((filesystem_t *) self)->file_exists = file_exists;

	return init_err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


