/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dir_null.c
 * @author Brendan Bruner
 * @date Aug 17, 2015
 */

#include <filesystems/dir_null.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_dir_null( dir_null_t *self, filesystem_t *fs )
{
	DEV_ASSERT( self );
	DEV_ASSERT( fs );

	/* Initialize super struct and return error code. */
	return _initialize_dir( (dir_t *) self, fs, HANDLE_NULL );
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


