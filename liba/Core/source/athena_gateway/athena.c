/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file athena.c
 * @author Brendan Bruner
 * @date 2016-06-9
 */

#include "../../include/athena_gateway/athena.h"

#include <fat_sd/ff.h>
#include <athena/arduino/xmodem/main/commands.h>
#include <athena/arduino/xmodem/main/io.h>
#include <core_defines.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>
#include <io/nanopower2.h>

static xQueueHandle athena_command_queue;
static semaphore_t gateway_sem;
static char athena_binary[ATHENA_MAX_BINARY_SIZE];
extern driver_toolkit_nanomind_t drivers;
extern spi_chip_t spi_athena_chip;

static void athena_gateway_task( void* param );

bool_t athena_init( )
{
	athena_command_queue = xQueueCreate(ATHENA_COMMAND_QUEUE_SIZE, sizeof(char));

	if( athena_command_queue == NULL ) {
		return NULL;
	}

	new_semaphore(gateway_sem, BINARY_SEMAPHORE, SEMAPHORE_EMPTY);
	if( gateway_sem == NULL ) {
		return false;
	}

	athena_suspend( );
	if( xTaskCreate(athena_gateway_task, ATHENA_TASK_NAME, ATHENA_TASK_STACK, NULL, ATHENA_TASK_PRIO, NULL) != pdPASS ) {
		return false;
	}

	return true;
}

bool_t athena_push_command( char command )
{
	if( xQueueSendToBack(athena_command_queue, &command, USE_POLLING) == pdTRUE ) {
		return true;
	}
	return false;
}

static void athena_execute_command( char command )
{
	uint32_t crc;
	uint8_t err;

	if( command == COMMAND_UPLOAD ) {
		/* Read in binary.
		 */
		FILE* bin = fopen(ATHENA_BIN_PATH, "r");
		if( bin == NULL ) {
			return;
		}
		size_t bin_size = fread(athena_binary, sizeof(char), ATHENA_MAX_BINARY_SIZE, bin);
		fclose(bin);
		csp_printf_crit("Read in %d bytes.", bin_size);

		/* Upload binary.
		 */
		if( spi_lock_dev(spi_athena_chip.spi_dev) == -1 ) {
			return;
		}
		err = ATHENA_upload((uint8_t*) athena_binary, bin_size);
		spi_unlock_dev(spi_athena_chip.spi_dev);
	}
	else if( command == COMMAND_BOOT ) {
		if( spi_lock_dev(spi_athena_chip.spi_dev) == -1 ) {
			return;
		}
		err = ATHENA_boot( );
		spi_unlock_dev(spi_athena_chip.spi_dev);
	}
	else if( command == COMMAND_VERIFY ) {
		if( spi_lock_dev(spi_athena_chip.spi_dev) == -1 ) {
			return;
		}
		err = ATHENA_verify(&crc);
		spi_unlock_dev(spi_athena_chip.spi_dev);
		csp_printf_crit("ATHENA: crc: 0x%08X", crc);
	}
	else if( command == COMMAND_RESET ) {
		if( spi_lock_dev(spi_athena_chip.spi_dev) == -1 ) {
			return;
		}
		err = ATHENA_reset( );
		spi_unlock_dev(spi_athena_chip.spi_dev);
	}
	else if (command == COMMAND_FLUSH ) {
		if( spi_lock_dev(spi_athena_chip.spi_dev) == -1 ) {
			return;
		}
		err = ATHENA_flush( );
		spi_unlock_dev(spi_athena_chip.spi_dev);
	}

	csp_printf_crit("ATHENA: command error: %d", err);
}

static void athena_extract_payload( )
{
	int i;
	driver_toolkit_t*	kit;
	logger_t* 			athena_logger;
	logger_error_t 		lerr;
	uint32_t 			written;
	fs_error_t			ferr;
	uint8_t				athena_err;
	unsigned char athena_packet[ATHENA_PAYLOAD_PACKET_SIZE];
	int32_t 			time_stamp;

	kit = (driver_toolkit_t*) &drivers;
	time_stamp = kit->rtc->get_ds1302_time( );
	athena_logger = kit->athena_logger;

	/* Get packet from athena. */
	if( spi_lock_dev(spi_athena_chip.spi_dev) == -1 ) {
		return;
	}
	athena_err = ATHENA_download((uint8_t*) athena_packet);
	spi_unlock_dev(spi_athena_chip.spi_dev);
	if( athena_err != ERROR_NOERR ) {
		return;
	}

	/* Write the packet to memory. */
	file_t* log_file;
	log_file = logger_peek_head(athena_logger, &lerr);
	if( lerr == LOGGER_EMPTY ) {
		/* Nothing in logger, insert a blank file to work off of. */
		log_file = logger_insert(athena_logger, &lerr, NULL);
	}
	if( lerr == LOGGER_OK ) {
		uint32_t size = log_file->size(log_file, &ferr);
		if( size >= ATHENA_LOGGER_FILE_SIZE ) {
			/* file contains maximum amount of data. Insert a new blank file into the logger to work with. */
			log_file->close(log_file);
			log_file = logger_insert(athena_logger, &lerr, NULL);
		}
	}
	if( lerr == LOGGER_OK ) {
		/* Write to log_file. */
		log_file->write(log_file, (uint8_t*) &time_stamp, sizeof(time_stamp), &written);
		log_file->write(log_file, (uint8_t*) athena_packet, ATHENA_PAYLOAD_PACKET_SIZE, &written);
		log_file->close(log_file);
	}
}

bool_t athena_suspend( )
{
	take_semaphore(gateway_sem, USE_POLLING);
	return true;
}
bool_t athena_resume( )
{
	/* Only resume if athena has power supplied to it. */
	eps_hk_out_t line_status;
	if( eps_hk_out_get(&line_status) != sizeof(line_status) ) {
		/* Query failed. */
		return false;
	}

	if( line_status.output[ATHENA_5V0_POWER_CHANNEL] == 0 ) {
		/* No power to athena. */
		return false;
	}

	/* Athena has power, turn it on. */
	give_semaphore(gateway_sem);
	return true;
}

bool_t athena_is_suspended( )
{
	if( peek_semaphore(gateway_sem, USE_POLLING) == SEMAPHORE_AVAILABLE) {
		return false;
	}
	return true;
}

static void athena_gateway_task( void* param )
{
	char command;
	uint32_t time_origin;

	time_origin = task_time_elapsed( );
	for( ;; ) {

		if( peek_semaphore(gateway_sem, BLOCK_FOREVER) == SEMAPHORE_AVAILABLE ) {

			/* Check if any telecommands have been sent to athena. */
			if( xQueueReceive(athena_command_queue, &command, USE_POLLING) == pdTRUE ) {
				athena_execute_command(command);
			}


			/* Periodically extract data from Athena. Check if it's time. */
			if( task_time_elapsed( ) - time_origin >= ATHENA_PAYLOAD_EXTRACT_CADENCE ) {
				athena_extract_payload( );
				time_origin = task_time_elapsed( );
			}
		}

		task_delay(100);
	}
}
