/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm_external.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <dfgm/dfgm_external.h>
#include <dfgm.h>
#include <printing.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t power( dfgm_t* self_, eps_t* eps, bool_t state )
{
	bool_t result;
	dfgm_external_t* self = (dfgm_external_t*) self_;
	DEV_ASSERT( self );

	if( state == true )
	{
		DFGM_start( );
		result = 1;
	}
	else
	{
		DFGM_stop( );
		result = 0;
	}
	return result;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void initialize_dfgm_external( dfgm_external_t* self )
{
	DEV_ASSERT( self );

	/* Initialize super class. */
	extern void initialize_dfgm_( dfgm_t* );
	initialize_dfgm_( (dfgm_t*) self );

	self->_.supers_power = ((dfgm_t*) self)->power;
	((dfgm_t*) self)->power = power;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


