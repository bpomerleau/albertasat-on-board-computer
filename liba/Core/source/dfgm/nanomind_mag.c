/*
 * nanomind_mag.c
 *
 *  Created on: Jun 23, 2016
 *      Author: bbruner
 */


#include <dfgm/nanomind_mag.h>
#include <dev/magnetometer.h>
#include <util/error.h>
#include <core_defines.h>

struct nm_mag_t
{
	semaphore_t mag_enabled_sem;
	FILE* data_file;
};
static struct nm_mag_t nm_mag;

static void nm_mag_task( void* );

bool_t initialize_nanomind_mag( )
{
	new_semaphore(nm_mag.mag_enabled_sem, BINARY_SEMAPHORE, SEMAPHORE_EMPTY);
	if( nm_mag.mag_enabled_sem == NULL ) {
		return false;
	}

	if( xTaskCreate(nm_mag_task, NM_MAG_TASK_NAME, NM_MAG_TASK_STACK, NULL, NM_MAG_TASK_PRIO, NULL) != pdPASS ) {
		return false;
	}

	if( mag_init( ) != E_NO_ERR ) {
		return false;
	}

	return true;
}


bool_t enable_nanomind_mag( )
{
	/* Enable mag. */
	if( mag_set_mode(MAG_MODE_CONTINUOUS) != E_NO_ERR ) {
		return false;
	}

	/* Enable logging task. */
	post_semaphore(nm_mag.mag_enabled_sem);

	return true;
}

bool_t disable_nanomind_mag( )
{
	/* Disable logging task. */
	if( take_semaphore(nm_mag.mag_enabled_sem, NM_MAG_DISABLE_TIMEOUT) == SEMAPHORE_ACQUIRED ) {
		/* Disable mag. */
		mag_set_mode(MAG_MODE_IDLE);
		return true;
	}
	return false;
}

static void nm_mag_task( void* inputs )
{
	FILE* data_file;
	struct mag_data_s16 mag_data;

	for( ;; ) {
		if( peek_semaphore(nm_mag.mag_enabled_sem, BLOCK_FOREVER) == SEMAPHORE_AVAILABLE ) {
			/* Task is enabled. */

			/* Open log file. */
			data_file = fopen(NM_MAG_LOG_FILE_PATH, "a+");
			if( data_file != NULL ) {

				/* Get mag data. */
				if( mag_read_s16(&mag_data) == E_NO_ERR ) {

					/* Log data. */
					fwrite(&mag_data, 1, sizeof(mag_data), data_file);
				}

				fclose(data_file);
			}
		}

		/* Wait before logging again. */
		task_delay(NM_MAG_LOG_PERIOD);
	}
}
