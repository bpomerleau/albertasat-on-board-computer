/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_log_wod.c
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#include <telecommands/telemetry_alteration/telecommand_log_wod.h>
#include <packets/packet_base.h>
#include <csp/csp.h>
#include <io/nanomind.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
/* Array indexs. */
#define MODE_INDEX 0
#define VBATT_INDEX 1
#define CBATT_INDEX 2
#define CBUS3V3_INDEX 3
#define CBUS5V0_INDEX 4
#define CTEMP_INDEX 5
#define ETEMP_INDEX 6
#define BTEMP_INDEX 7

/* Possible satellite modes. */
#define PACKET_WOD_SCIENCE_MODE 1
#define PACKET_WOD_SAFE_MODE 0

/* These defines are used to process the data to meet QB50 requirements. */
#define _WP_MAX_ONE_BYTE 255
#define _WP_MIN( a, b) ((a) < (b) ? (a) : (b))
#define _WP_MAX( a, b) ((a) > (b) ? (a) : (b))
#define EVAL_WOD( expr ) (uint8_t) (expr)


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
/**
 * @memberof telecommand_log_wod_t
 * @private
 * @brief
 * 		Log an array of wod into non volatile memory.
 * @details
 * 		Log an array of wod into non volatile memory.
 * @param raw_wod[in]
 * 		The array of wod to log. It must be at least <b>PACKET_RAW_WOD_SIZE</b>
 * 		bytes long. Only the first <b>PACKET_RAW_WOD_SIZE</b> bytes of the array
 * 		are logged to non volatile memory.
 */
static void telecommand_commit_wod( telecommand_t* self, uint8_t* raw_wod )
{
	DEV_ASSERT( self );
	DEV_ASSERT( raw_wod );

	/* API to write the WOD into non volatile memory. */
	logger_t*			logger;
	logger_error_t		logger_err;
	file_t*				wod_log_file;
	fs_error_t			file_err;
	uint32_t			bytes_size;
	uint32_t			pre_processed_entry_time;
	uint8_t				entry_time[sizeof(pre_processed_entry_time)];
	rtc_t* 				rtc;
	uint32_t			written;


	/* Get a file to write the WOD to. */
	logger = (logger_t *) self->_kit->wod_logger;
	wod_log_file = logger_peek_head( logger, &logger_err );
	if( logger_err == LOGGER_EMPTY ) {
		/* No files are in the logger, add new one to it. */
		wod_log_file->close(wod_log_file);
		wod_log_file = logger_insert(logger, &logger_err, NULL);
	}
	if( logger_err == LOGGER_NVMEM_ERR )
	{
		/* Failure in the logger's file system back bone. */
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		wod_log_file->close( wod_log_file );
		return;
	}

	/* Find out of if the file is full. If it is, get a new file to write to. */
	bytes_size = wod_log_file->size( wod_log_file, &file_err );
	if( file_err != FS_OK )
	{
		/* Error querying size of file. */
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		wod_log_file->close( wod_log_file );
		return;
	}
	if( bytes_size >= PACKET_TYPE_WOD_SIZE )
	{
		/* Log file has reached its maximum. Insert a new file into the logger. */
		wod_log_file->close( wod_log_file );
		wod_log_file = logger_insert( logger, &logger_err, NULL );
		if( logger_err != LOGGER_OK )
		{
			/* Error pushing the packet. */
			wod_log_file->close(wod_log_file);
			_telecommand_set_status( self, CMND_NVMEM_ERR );
			return;
		}
	}

	/* Find out of if the file is empty. If it is, put a time stamp on it. */
	/* The reason we do this EVERY time is to ensure there is always a time stamp. */
	/* For example, if the last command failed to insert the time stamp, the next time */
	/* this command is run, the time stamp will be inserted. */
	bytes_size = wod_log_file->size( wod_log_file, &file_err );
	if( file_err != FS_OK )
	{
		/* Error querying size of file. */
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		wod_log_file->close( wod_log_file );
		return;
	}
	if( bytes_size == 0 ) {
		/* Put a time stamp on the new file. */
		/* Calculate time stamp. Time since qb50 epoch converted to little endian, 4 bytes. */
		rtc = self->_kit->rtc;
		pre_processed_entry_time = rtc->get_ds1302_time( );
		to_little_endian_from32( &pre_processed_entry_time, entry_time );

		/* Insert time stamp. */
		file_err = wod_log_file->write( wod_log_file, entry_time, sizeof(pre_processed_entry_time), &written );
		if( file_err != FS_OK ) {
			/* Error time stamping file or out of memory. */
			_telecommand_set_status( self, CMND_NVMEM_ERR );
			wod_log_file->close( wod_log_file );
			return;
		}
		if( written < sizeof(pre_processed_entry_time) ) {
			/* Out of memory. */
			_telecommand_set_status( self, CMND_MEM_FULL );
			wod_log_file->close( wod_log_file );
			return;
		}
	}

	/* Write the WOD to non volatile memory. */
	file_err = wod_log_file->write( wod_log_file, raw_wod, PACKET_RAW_WOD_SIZE, &bytes_size );
	wod_log_file->close( wod_log_file );
	if( file_err != FS_OK )
	{
		/* Error writing the data to file. */
		_telecommand_set_status( self, CMND_NVMEM_ERR );
		return;
	}
	if( bytes_size < PACKET_RAW_WOD_SIZE )
	{
		/* Out of memory in file system. */
		_telecommand_set_status( self, CMND_MEM_FULL );
		return;
	}
}

/**
 * @memberof telecommand_log_wod_t
 * @private
 * @brief
 * 		Takes one round of wod and packetizes it according to QB50 requirements.
 * @details
 * 		Takes one round of wod and packetizes it according to QB50 requirements. This does not
 * 		add the required time stamp, it simply takes the required WOD and puts it into a uint8_t
 * 		array in the correct order required by QB50. Use this method when logging wod to non volatile memory.
 * @attention
 * 		This method is not static so it can be used in testing. It should never be used
 * 		outside of its source file though.
 * @param packet
 * 		A pointer to the uint8_t array where the data will be copied to. This array must be
 * 		at least the size of <b>PACKET_RAW_WOD_SIZE</b>. Shorter arrays will result in segmentation
 * 		faults and/or stack stomping.
 * @param mode
 * 		<b>PACKET_WOD_SCIENCE_MODE</b> if in science state, <b>PACKET_WOD_SAFE_MODE</b> otherwise.
 * @param vbatt
 * 		Raw battery voltage of EPS.
 * @param cbatt
 * 		Raw battery current of EPS.
 * @param cbus3v0
 * 		Raw bus current of 3v3 bus on EPS.
 * @param cbus5v0
 * 		Raw bus current of 5v0 bus on EPS.
 * @param comm_temp
 * 		Temperature of COMM board.
 * @param eps_temp
 * 		Temperature of EPS board.
 * @param eps_batt_temp
 * 		Temperature of battery on EPS board.
 */
void packetize_raw_wod( uint8_t *packet, uint8_t mode, uint16_t vbatt, uint16_t cbatt, uint16_t cbus3v3,
								uint16_t cbus5v0, int16_t comm_temp, int16_t eps_temp, int16_t eps_batt_temp )
{
	DEV_ASSERT( packet );

	packet[MODE_INDEX] = mode;
	packet[VBATT_INDEX] = EVAL_WOD(20*(vbatt/1000) - 60);
	packet[CBATT_INDEX] = EVAL_WOD(127*(cbatt/1000) + 127);
	packet[CBUS3V3_INDEX] = EVAL_WOD(40*(cbus3v3/1000));
	packet[CBUS5V0_INDEX] = EVAL_WOD(40*(cbus5v0/1000));
	packet[CTEMP_INDEX] = EVAL_WOD(4*comm_temp + 60);
	packet[ETEMP_INDEX] = EVAL_WOD(4*eps_temp + 60);
	packet[BTEMP_INDEX] = EVAL_WOD(4*eps_batt_temp + 60);
}

/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	/* Systems where WOD will be drawn from. */
	state_relay_t*		relay;
	eps_hk_t			eps_hk;

	/* Temporary variables to hold collect WOD.
	uint16_t			vbatt, cbatt, cbus3v3, cbus5v0;
	int16_t				ctemp, etemp, btemp;
	uint8_t				mode;*/
	int16_t				ctemp;
	uint8_t				mode;
	uint8_t				raw_wod[sizeof(eps_hk_t)+6+sizeof(int16_t)+sizeof(uint8_t)];

	relay = ((telecommand_log_wod_t *) self)->_relay_;

#ifdef HARDWARE_DRIVERS_LOADED
	ctemp = comm_get_pcb_temp(self->_kit->comm);
#else
	ctemp = 0;
#endif
	if( relay_state_id( relay ) == STATE_SCIENCE_ID ) {
		mode = PACKET_WOD_SCIENCE_MODE;
	}
	else {
		mode = PACKET_WOD_SAFE_MODE;
	}

	/*
	vbatt = 5000;

	// Packetize WOD into an easy to manage uint8_t array.
	packetize_raw_wod( raw_wod, mode, vbatt, cbatt, cbus3v3,
					   cbus5v0, ctemp, etemp, btemp ); */

	eps_hk_get(&eps_hk);

	memcpy(raw_wod, &eps_hk, PACKET_RAW_WOD_SIZE);
	raw_wod[PACKET_RAW_WOD_SIZE] = mode;
	raw_wod[PACKET_RAW_WOD_SIZE+1] = (uint8_t) ( (ctemp & 0xFF00) >> 8 );
	raw_wod[PACKET_RAW_WOD_SIZE+2] = (uint8_t) ( ctemp & 0x00FF );
	raw_wod[PACKET_RAW_WOD_SIZE+3] = 0x4F; // ASCII for ON03CA - callsign is included as last 6 bytes.
	raw_wod[PACKET_RAW_WOD_SIZE+4] = 0x4E;
	raw_wod[PACKET_RAW_WOD_SIZE+5] = 0x30;
	raw_wod[PACKET_RAW_WOD_SIZE+6] = 0x33;
	raw_wod[PACKET_RAW_WOD_SIZE+7] = 0x43;
	raw_wod[PACKET_RAW_WOD_SIZE+8] = 0x41;

	csp_transaction(CSP_PRIO_NORM, 10, WOD_BEACON_PORT, 1000, &raw_wod, PACKET_RAW_WOD_SIZE+9, NULL, 0);

	/* Commit the WOD to non volatile memory. */
	telecommand_commit_wod( self, (uint8_t*) &eps_hk );
	csp_printf("log wod error code: %d", telecommand_status(self));
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_telecommand_log_wod( telecommand_log_wod_t *self, state_relay_t *relay )
{
	DEV_ASSERT( self );
	DEV_ASSERT( relay );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) self, relay->drivers );

	((telecommand_t *) self)->_execute = execute;
	_telecommand_set_type( (telecommand_t *) self, TELECOMMAND_TYPE_LOG_WOD );
	self->_relay_ = relay;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
