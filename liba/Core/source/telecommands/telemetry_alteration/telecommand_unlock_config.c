/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_unlock_state_config.c
 * @author Brendan Bruner
 * @date Aug 24, 2015
 */
#include <states/state.h>
#include <telecommands/telemetry_alteration/telecommand_unlock_config.h>
#include <telemetry_priority/telemetry_priority.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	state_relay_t *relay;

	relay = ((telecommand_unlock_config_t *) self)->_relay_;

	/* Unlock and refresh state configurations. */
	state_unlock_config_globally( );
	state_refresh( (state_t *) &relay->alignment, self->_kit );
	state_refresh( (state_t *) &relay->bring_up, self->_kit );
	state_refresh( (state_t *) &relay->low_power, self->_kit );
	state_refresh( (state_t *) &relay->science, self->_kit );
	/* refresh all other states. */

	/* Unlock and refresh telemetry priority configuration. */
	telemetry_priority_unlock_config_globally( );
	telemetry_priority_refresh_configuration( &self->_kit->priority );
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_telecommand_unlock_config( telecommand_unlock_config_t *self, state_relay_t *relay )
{
	DEV_ASSERT( self );
	DEV_ASSERT( relay );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) self, relay->drivers );

	self->_relay_ = relay;
	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_UNLOCK_CONFIG;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
