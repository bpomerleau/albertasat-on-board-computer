/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_deploy_boom.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/subsystem_alteration/telecommand_deploy_boom.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	dfgm_t* dfgm;
	hub_t* hub;
	bool_t status;

	dfgm = self->_kit->dfgm;
	hub = self->_kit->hub;

	hub->power( hub, self->_kit->eps, true );
	status = dfgm->deploy( dfgm, self->_kit->hub );
	if( status == false )
	{
		/* Failed to deploy. */
		_telecommand_set_status( self, CMND_FAILED );
	}
	status = hub->power( hub, self->_kit->eps, false );
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_deploy_boom_t* clone;
	bool_t err;

	clone = (telecommand_deploy_boom_t*) OBCMalloc( sizeof(telecommand_deploy_boom_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_deploy_boom( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_deploy_boom( telecommand_deploy_boom_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DEPLOY_BOOM;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
