/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_configure_eps.c
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#include <telecommands/subsystem_alteration/telecommand_configure_eps.h>
#include <io/nanopower2.h>
#include <csp_internal.h>
#include <string.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );
	char delay_str[TELECOMMAND_MAX_ARGUMENT_LENGTH+1];
	int16_t delay = 0;

	uint8_t channel = self->_argument[0] - '0';
	uint8_t mode = self->_argument[1] - '0';
	strncpy(delay_str, &self->_argument[2], self->_argument_length-2);
	delay_str[self->_argument_length-1] = '\0';
	delay = atoi(delay_str);

	if( !(mode == 0 || mode == 1) ) {
		csp_printf_crit("EPS conf: Invalid state %d", mode);
		_telecommand_set_status(self, CMND_FAILED);
		return;
	}

	if( channel > 9 ) {
		csp_printf_crit("EPS conf: Invalid channel %d", channel);
		_telecommand_set_status(self, CMND_FAILED);
		return;
	}

	if( delay < 0 ) {
		csp_printf_crit("EPS conf: Invalid delay %d", delay);
		_telecommand_set_status(self, CMND_FAILED);
		return;
	}

	csp_printf_crit("eps output channel %d to state %d in %d seconds", channel, mode, delay);
	int err = eps_output_set_single(channel, mode, delay);
	if( err <= 0 ) {
		csp_printf_crit("EPS conf: output set error: %d", err);
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_configure_eps_t* clone;
	bool_t err;

	clone = (telecommand_configure_eps_t*) OBCMalloc( sizeof(telecommand_configure_eps_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_configure_eps( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_telecommand_configure_eps( telecommand_configure_eps_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	_telecommand_set_type( (telecommand_t *) self, TELECOMMAND_TYPE_CONFIGURE_EPS );

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
