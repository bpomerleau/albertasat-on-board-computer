/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_dosimeter.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_dosimeter.h>
#include <string.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );
	teledyne_t* dos;

	dos = self->_kit->teledyne;

	if( strncmp(TELECOMMAND_DOS_SUSP_SYM0, self->_argument, TELECOMMAND_DOS_SUSP_SYM0_LEN) == 0 ||
		strncmp(TELECOMMAND_DOS_SUSP_SYM1, self->_argument, TELECOMMAND_DOS_SUSP_SYM1_LEN) == 0 ) {
		if( dosimeter_suspend(dos) == true ) {
			csp_printf("DOSIMETER: suspended");
		}
		else {
			csp_printf("DOSIMETER: failed to suspend");
		}
	}
	else if( strncmp(TELECOMMAND_DOS_RES_SYM0, self->_argument, TELECOMMAND_DOS_RES_SYM0_LEN) == 0 ||
			 strncmp(TELECOMMAND_DOS_RES_SYM1, self->_argument, TELECOMMAND_DOS_RES_SYM1_LEN) == 0 ) {
		dosimeter_resume(dos);
		csp_printf("DOSIMETER: resumed");
	}
	else if( strncmp(TELECOMMAND_DOS_DIAG_SYM0, self->_argument, TELECOMMAND_DOS_DIAG_SYM0_LEN) == 0 ) {
		csp_printf("DOSIMETER: no diagnostics available, now or ever.");
	}
	else {
		csp_printf("DOSIMETER: Unkown command");
	}


}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_dosimeter_t* clone;
	bool_t err;

	clone = (telecommand_dosimeter_t*) OBCMalloc( sizeof(telecommand_dosimeter_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_dosimeter( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_dosimeter( telecommand_dosimeter_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DOSIMETER;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
