/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_prioritize_downlink.c
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#include "../../../include/telecommands/nanomind/mnlp_reinit.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );
	self->_kit->mnlp->start_stop(self->_kit->mnlp, true);
}

static telecommand_t* clone( telecommand_t* self )
{
	telecommand_mnlp_reinit_t* clone;

	clone = malloc(sizeof(*clone));
	if( clone == NULL ) {
		return NULL;
	}
	initialize_telecommand_mnlp_reinit(clone, self->_kit);
	return (telecommand_t*) clone;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_telecommand_mnlp_reinit( telecommand_mnlp_reinit_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	_telecommand_set_type( (telecommand_t *) self, TELECOMMAND_TYPE_MNLP_REINIT );

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
