/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_adcs.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_adcs.h>
#include <string.h>
#include <eps/power_lines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	char arg[ADCS_TELECOMMAND_BUFFER_LENGTH];

	strncpy(arg, self->_argument, self->_argument_length);
	arg[self->_argument_length] = '\0';


	if( self->_argument_length > 0 ) {
		if(strncmp(self->_argument, ":", 1) == 0) {
			FILE* buffer = fopen(&arg[1], "r");
			if( buffer == NULL ) {
				csp_printf_crit("Failed to open file %s", &arg[1]);
				_telecommand_set_status(self, CMND_FAILED);
				return;
			}
			else {
				csp_printf_crit("Opened file %s", &arg[1]);
			}

			fseek(buffer, 0, SEEK_END);
			long buffer_length = ftell(buffer) - 1;
			fseek(buffer, 0, SEEK_SET);
			if( buffer_length > ADCS_TELECOMMAND_BUFFER_LENGTH || buffer_length <= 0 ) {
				csp_printf_crit("File to large, contains %d bytes", buffer_length);
				_telecommand_set_status(self, CMND_FAILED);
				fclose(buffer);
				return;
			}

			/* Skip first byte for compatability with Collin's
			 * ADCS command generator.
			 */
			/* TODO */
			fseek(buffer, 1, SEEK_SET);

			if( buffer_length != (long) fread(arg, 1, (size_t) buffer_length, buffer) ) {
				csp_printf_crit("File IO error");
				fclose(buffer);
				_telecommand_set_status(self, CMND_FAILED);
				return;
			}

			fclose(buffer);
			csp_printf_crit("ADCS command: %d with length %d", arg[0], buffer_length);
			ADCS_callback((uint8_t) buffer_length, (uint8_t*) arg);
		}
		else {
			arg[0] = (uint8_t) atoi(arg);
			csp_printf_crit("ADCS command: %d", arg[0]);
			ADCS_callback(1, (uint8_t*) arg);
		}
	}
	else {
		csp_printf_crit("\n(\"adcs\", \"buffer\");\nor\n(\"adcs\", \":buffer.bin\");");
		csp_printf_crit("\nFirst: the string \"buffer\" is converted to uint8 and sent to the ADCS\n"
						"Second: the contents of \"buffer.bin\" is sent to the ADCS");
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_adcs_t* clone;
	bool_t err;

	clone = (telecommand_adcs_t*) OBCMalloc( sizeof(telecommand_adcs_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_adcs( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_adcs( telecommand_adcs_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_ADCS;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
