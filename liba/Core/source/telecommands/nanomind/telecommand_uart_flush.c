/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_uart_flush.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_uart_flush.h>
#include <string.h>
#include <athena_gateway/athena.h>
#include <dev/usart.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );
	char num[TELECOMMAND_MAX_ARGUMENT_LENGTH+1];
	int i;

	strncpy(num, self->_argument, self->_argument_length);
	num[self->_argument_length] = '\0';
	i = atoi(num);

	int mes_wait = usart_messages_waiting(ADCS_USART_HANDLE);
	csp_printf_crit("Waiting: %d", mes_wait);

	for( ; i > 0; i-- ) {
		portBASE_TYPE status;
		usart_getc_blocking(ADCS_USART_HANDLE, USE_POLLING, &status);
	}

	mes_wait = usart_messages_waiting(ADCS_USART_HANDLE);
	csp_printf_crit("Waiting: %d", mes_wait);
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_uart_flush_t* clone;
	bool_t err;

	clone = (telecommand_uart_flush_t*) OBCMalloc( sizeof(telecommand_uart_flush_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_uart_flush( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_uart_flush( telecommand_uart_flush_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_UART_FLUSH;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
