/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_downlink.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_downlink.h>
#include <string.h>
#include <inttypes.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static void telecommand_downlink_print_diag( char* src, file_t* log_file )
{
	fs_error_t ferr;
	uint32_t bytes_read;
	const char* file_name;
	uint32_t time_stamp;

	ferr = log_file->read(log_file, (uint8_t*) &time_stamp, sizeof(time_stamp), &bytes_read);
	file_name = fs_handle_get_name((fs_handle_t*) log_file);
	log_file->close(log_file);

	csp_printf("downlink: diag:\n\t%s: %.*s\n\ttime stamped with: %" PRIu32, src, FILESYSTEM_MAX_NAME_LENGTH, file_name, (ferr == FS_OK ? time_stamp : 0));
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	logger_t* dfgm_firA_log;
	logger_t* dfgm_firB_log;
	logger_t* dfgm_raw_log;
	logger_t* dfgm_hk_log;
	logger_t* athena_log;
	logger_t* wod_log;
	logger_error_t lerr;
	file_t* log_file;

	dfgm_firA_log = self->_kit->dfgm_filt1_logger;
	dfgm_firB_log = self->_kit->dfgm_filt2_logger;
	dfgm_raw_log = self->_kit->dfgm_raw_logger;
	dfgm_hk_log = self->_kit->dfgm_hk_logger;
	wod_log = self->_kit->wod_logger;
	athena_log = self->_kit->athena_logger;

	/* Look at all fir stream A. */
	log_file = logger_peek_tail(dfgm_firA_log, &lerr);
	if( lerr == LOGGER_OK ) {
		/* Get time stamp on file. */
		telecommand_downlink_print_diag("FIR stream A", log_file);
	}
	else {
		csp_printf("downlink: diag:\n\tFIR stream A empty");
	}

	/* Look at all fir stream B. */
	log_file = logger_peek_tail(dfgm_firB_log, &lerr);
	if( lerr == LOGGER_OK ) {
		/* Get time stamp on file. */
		telecommand_downlink_print_diag("FIR stream B", log_file);
	}
	else {
		csp_printf("downlink: diag:\n\tFIR stream B empty");
	}

	/* Look at dfgm raw stream. */
	log_file = logger_peek_tail(dfgm_raw_log, &lerr);
	if( lerr == LOGGER_OK ) {
		/* Get time stamp on file. */
		telecommand_downlink_print_diag("DFGM raw stream", log_file);
	}
	else {
		csp_printf("downlink: diag:\n\tDFGM raw stream empty");
	}

	/* Look at dfgm hk log. */
	log_file = logger_peek_tail(dfgm_hk_log, &lerr);
	if( lerr == LOGGER_OK ) {
		/* Get time stamp on file. */
		telecommand_downlink_print_diag("DFGM hk stream", log_file);
	}
	else {
		csp_printf("downlink: diag:\n\tDFGM hk stream empty");
	}

	/* Look at wod log. */
	log_file = logger_peek_tail(wod_log, &lerr);
	if( lerr == LOGGER_OK ) {
		/* Get time stamp on file. */
		telecommand_downlink_print_diag("WOD stream", log_file);
	}
	else {
		csp_printf("downlink: diag:\n\tWOD stream empty");
	}

	/* Look at athena log. */
	log_file = logger_peek_tail(athena_log, &lerr);
	if( lerr == LOGGER_OK ) {
		/* Get time stamp on file. */
		telecommand_downlink_print_diag("Athena stream", log_file);
	}
	else {
		csp_printf("downlink: diag:\n\tAthena stream empty");
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_downlink_t* clone;
	bool_t err;

	clone = (telecommand_downlink_t*) OBCMalloc( sizeof(telecommand_downlink_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_downlink( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_downlink( telecommand_downlink_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_DOWNLINK;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
