/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_scv_config.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_scv_config.h>
#include <string.h>
#include <scv_config.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define TCLC_FLUSH			"flush"
#define TCLC_COMMIT			"commit"
#define TCLC_CURRENT		"current"
#define TCLC_ATHENA			"athena"
#define TCLC_DFGM_FILT1		"dfgm filt1"
#define TCLC_DFGM_FILT2		"dfgm filt2"
#define TCLC_DFGM_RAW		"dfgm raw"
#define TCLC_ADCS			"adcs hk"
#define TCLC_DFGM_HK		"dfgm hk"
#define TCLC_WOD			"wod"
#define TCLC_WOD_CAD		"cadwod"
#define TCLC_UDOS			"udos"
#define TCLC_UDOS_CAD		"cadudos"


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static size_t getNum( const char* arg, size_t arg_length )
{
	char num[TELECOMMAND_MAX_ARGUMENT_LENGTH+1];

	strncpy(num, arg, arg_length);
	num[arg_length] = '\0';

	return (size_t) atoi(num);

}

static size_t getFirstNum( const char* arg, size_t arg_length )
{
	size_t i;

	for( i = 0; i < arg_length; ++i ) {
		if( arg[i] == ':' ) {
			if( i + 1 >= arg_length ) {
				return 0;
			}
			return getNum(arg+i+1, arg_length-i-1);
		}
	}
	return 0;
}

static size_t getSecondNum( const char* arg, size_t arg_length )
{
	size_t i;
	int colon_count = 0;

	for( i = 0; i < arg_length; ++i ) {
		if( arg[i] == ':' ) {
			++colon_count;
			if( colon_count == 2 ) {
				if( i + 1 >= arg_length ) {
					return 0;
				}
				return getNum(arg+i+1, arg_length-i-1);
			}
		}
	}
	return 0;
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
#define DBG() csp_printf("dbg %d", __LINE__);
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	size_t capacity;
	size_t file_size;
	struct scv_config_t config;

	scv_config_get_current(&config);


	if( self->_argument_length == 0 ) {
		/* Print help message.
		 */
		csp_printf_crit("\nSCV Config: Help:\n"
				"%s\t\tcalls scv_config_flush\n"
				"%s\t\tcalls scv_config_commit\n"
				"%s\t\tget current running configuration",
				TCLC_FLUSH, TCLC_COMMIT, TCLC_CURRENT);
		csp_printf_crit("\n%s:x:y\tsets athena capacity to x, file size to y\n"
				"%s:x:y\tsets dfgm filt1 capacity to x, file size to y\n"
				"%s:x:y\tsets dfgm filt2 capacity to x, file size to y",
				TCLC_ATHENA, TCLC_DFGM_FILT1, TCLC_DFGM_FILT2);
		csp_printf_crit("\n%s:x:y\tsets dfgm raw capacity to x, file size to y\n"
				"%s:x:y\tsets adcs hk capacity to x, file size to y\n"
				"%s:x:y\tsets dfgm hk capacity to x, file size to y\n"
				"%s:x:y\t\tsets wod capacity to x, file size to y",
				TCLC_DFGM_RAW, TCLC_ADCS, TCLC_DFGM_HK, TCLC_WOD);
		csp_printf_crit("\n%s:x\tsets the wod logging cadence in ms to x\n"
				"%s:x:y\tsets the udos capacity to x, file size to y\n"
				"%s:x\tsets the udos logging cadence in ms to x",
				TCLC_WOD_CAD, TCLC_UDOS, TCLC_UDOS_CAD);
		return;
	}

	if( strncmp(self->_argument, TCLC_FLUSH, strlen(TCLC_FLUSH)) == 0 ) {
		int err = scv_config_flush( );
		if( err == 0 ) {
			csp_printf_crit("SCV Config: flush successful");
		}
		else {
			csp_printf_crit("SCV Config: flush error: %d", err);
			_telecommand_set_status(self, CMND_FAILED);
		}
		return;
	}

	if( strncmp(self->_argument, TCLC_COMMIT, strlen(TCLC_COMMIT)) == 0 ) {
		int err = scv_config_commit( );
		if( err == 0 ) {
			csp_printf_crit("SCV Config: commit successful");
		}
		else {
			csp_printf_crit("SCV Config: commit error: %d", err);
			_telecommand_set_status(self, CMND_FAILED);
		}
		return;
	}

	if( strncmp(self->_argument, TCLC_CURRENT, strlen(TCLC_CURRENT)) == 0 ) {
		csp_printf_crit("\nSCV Config: Current configuration:\n"
				"athena\t\tcapacity=%d, file size=%d\n"
				"dfgm filt1\tcapacity=%d, file size=%d\n"
				"dfgm filt2\tcapacity=%d, file size=%d",
				config.athena_logger_capacity, config.athena_logger_file_size,
				config.dfgm_filt1_capacity, config.dfgm_filt_file_size,
				config.dfgm_filt2_capacity, config.dfgm_filt_file_size);
		csp_printf_crit("\ndfgm raw\tcapacity=%d, file size=%d\n"
				"adcs hk\t\tcapacity=%d, file size=%d\n"
				"dfgm hk\t\tcapacity=%d, file size=%d\n"
				"wod\t\tcapacity=%d, file size=%d, cadence[ms]=%d\n",
				config.dfgm_raw_capacity, config.dfgm_raw_file_size,
				config.adcs_locale_capacity, config.adcs_locale_file_size,
				config.dfgm_hk_capacity, config.dfgm_hk_file_size,
				config.wod_capacity, config.wod_file_size, config.wod_cadence);
		csp_printf_crit("\nudos\t\tcapacity=%d, file size=%d, cadence[ms]=%d",
				config.udos_capacity, config.udos_file_size, config.udos_cadence);
		return;
	}

	capacity = getFirstNum(self->_argument, self->_argument_length);
	file_size = getSecondNum(self->_argument, self->_argument_length);

	if( capacity == 0 ) {
		csp_printf("SCV Config: Invalid argument");
		_telecommand_set_status(self, CMND_SYNTX_ERR);
		return;
	}
	else {
		csp_printf_crit("SCV Config: received capacity=%d, file size=%d", capacity, file_size);
	}

	if( strncmp(self->_argument, TCLC_ATHENA, strlen(TCLC_ATHENA)) == 0 ) {
		config.athena_logger_capacity = capacity;
		config.athena_logger_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_DFGM_FILT1, strlen(TCLC_DFGM_FILT1)) == 0 ) {
		config.dfgm_filt1_capacity = capacity;
		config.dfgm_filt_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_DFGM_FILT2, strlen(TCLC_DFGM_FILT2)) == 0 ) {
		config.dfgm_filt2_capacity = capacity;
		config.dfgm_filt_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_DFGM_RAW, strlen(TCLC_DFGM_RAW)) == 0 ) {
		config.dfgm_raw_capacity = capacity;
		config.dfgm_raw_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_ADCS, strlen(TCLC_ADCS)) == 0 ) {
		config.adcs_locale_capacity = capacity;
		config.adcs_locale_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_DFGM_HK, strlen(TCLC_DFGM_HK)) == 0 ) {
		config.dfgm_hk_capacity = capacity;
		config.dfgm_hk_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_WOD, strlen(TCLC_WOD)) == 0 ) {
		config.wod_capacity = capacity;
		config.wod_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_WOD_CAD, strlen(TCLC_WOD_CAD)) == 0 ) {
		config.wod_cadence = capacity;
	}
	else if( strncmp(self->_argument, TCLC_UDOS, strlen(TCLC_UDOS)) == 0 ) {
		config.udos_capacity = capacity;
		config.udos_file_size = file_size;
	}
	else if( strncmp(self->_argument, TCLC_UDOS_CAD, strlen(TCLC_UDOS_CAD)) == 0 ) {
		config.udos_cadence = capacity;
	}
	else {
		csp_printf_crit("SCV Config: No SCV variable by that name");
		_telecommand_set_status(self, CMND_SYNTX_ERR);
		return;
	}

	scv_config_set_current(&config);
	csp_printf_crit("SCV Config: update accepted and ready for flush");
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_scv_config_t* clone;
	bool_t err;

	clone = (telecommand_scv_config_t*) OBCMalloc( sizeof(telecommand_scv_config_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_scv_config( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_scv_config( telecommand_scv_config_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_SCV_CONFIG;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
