/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_telemetry_cleanup.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_telemetry_cleanup.h>
#include <string.h>
#include <telemetry_cleanup.h>
#include <inttypes.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static int32_t uint32pow10( int32_t exp )
{
	int32_t i;
	int32_t result = 1;

	for( i = 0; i < exp; ++i ) {
		result = result * 10;
	}

	return result;
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	const char* epoch_string = self->_argument;
	uint32_t epoch = 0;
	uint32_t inter_num;
	uint32_t i;

	for( i = 0; i < self->_argument_length; ++i ) {
		/* Convert ascii byte to int. */
		if( epoch_string[i] >= '0' && epoch_string[i] <= '9' ) {
			inter_num = epoch_string[i] - '0';
		}
		else {
			csp_printf("telemetry cleanup: error: invalid epoch format");
			return;
		}
		epoch += inter_num * uint32pow10(self->_argument_length - i - 1);
	}

	if( self->_argument_length == 0 ) {
		csp_printf("telemetry cleanup: usage\n\tDelete all telemetry from before the epoch\n\t\"epoch\"\t time since UNIX epoch in second.");
	}
	else {
		if( telemetry_cleanup_all(epoch) == true ) {
			csp_printf("telemetry cleanup: cleanup queued for epoch %" PRIu32, epoch);
		}
		else {
			csp_printf("telemetry cleanup: error: failed to queue cleanup");
		}
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_telemetry_cleanup_t* clone;
	bool_t err;

	clone = (telecommand_telemetry_cleanup_t*) OBCMalloc( sizeof(telecommand_telemetry_cleanup_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_telemetry_cleanup( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_telemetry_cleanup( telecommand_telemetry_cleanup_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_TELEMETRY_CLEANUP;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
