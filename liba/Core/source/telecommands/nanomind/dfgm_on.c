/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_prioritize_downlink.c
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#include <telecommands/nanomind/dfgm_on.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );

	/* TODO: Write prioritize downlink command */

	PORT_PRINT( "prioritizing downlink\n" );
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_telecommand_dfgm_on( telecommand_dfgm_on_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	uint8_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	_telecommand_set_type( (telecommand_t *) self, TELECOMMAND_TYPE_DFGM_ON );

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
