/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_athena.c
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#include <telecommands/nanomind/telecommand_athena.h>
#include <string.h>

#include "../../../include/athena_gateway/athena.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define HEX_BASE_NUMBER 16


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static unsigned int athena_command_powerof( unsigned int base, unsigned int exp )
{
  unsigned int i;
  unsigned int num = 1;
  for( i = 0; i < exp; ++i ) {
    num = num * base;
  }
  return num;
}

static unsigned char athena_command_atouc( const char* str, size_t len, int base )
{
  size_t i;
  unsigned int num = 0;
  unsigned int inter_num = 0;


  for( i = 0; i < len; ++i ) {
    /* Convert ascii byte to int. */
    if( str[i] >= '0' && str[i] <= '9' ) {
      inter_num = str[i] - '0';
    }
    else if( str[i] >= 'a' && str[i] <= 'z' ) {
      inter_num = str[i] - 'a' + 10;
    }
    else if( str[i] >= 'A' && str[i] <= 'Z' ) {
      inter_num = str[i] - 'A' + 10;
    }
    else {
      return 0;
    }

    /* accumulate. */
    num += inter_num * athena_command_powerof(base, len - i - 1);
  }

  return (unsigned char) num;
}


/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void execute( telecommand_t *self )
{
	DEV_ASSERT( self );
	if( self->_argument_length == 2 ) {
		/* Convert argument from human readable to machine readable. */
		char command = (char) athena_command_atouc(self->_argument, self->_argument_length, HEX_BASE_NUMBER);

		if( athena_push_command(command) == false ) {
			csp_printf("athena: diag: command queue full");
		}
	}
	else if( strncmp(TELECOMMAND_ATHENA_RESUME_SYM, self->_argument, TELECOMMAND_ATHENA_RESUME_SYM_LEN) == 0 ) {
		if( athena_resume( ) == true ) {
			csp_printf("athena: diag: data collection and commanding enabled");
		}
		else {
			csp_printf("athena: diag: failed to enable data collection and commanding");
		}
	}
	else if( strncmp(TELECOMMAND_ATHENA_SUSPEND_SYM, self->_argument, TELECOMMAND_ATHENA_SUSPEND_SYM_LEN) == 0 ) {
		if( athena_suspend( ) == true ) {
			csp_printf("athena: diag: data collection and commanding disabled");
		}
		else {
			csp_printf("athena: diag: failed to disabled data collection and commanding");
		}
	}
	else {
		csp_printf("\nathena: usage:\n\t%s\t\tenable athena data collection and commanding\n\t%s\t\tdisable athena data collection and commanding\n\t\"c\"\t\t"
				"two character commands are routed to athena"
				"\nathena: diag: running = %d", TELECOMMAND_ATHENA_RESUME_SYM, TELECOMMAND_ATHENA_SUSPEND_SYM, !athena_is_suspended( ));
	}
}

static telecommand_t* clone( telecommand_t* self )
{
	DEV_ASSERT( self );

	telecommand_athena_t* clone;
	bool_t err;

	clone = (telecommand_athena_t*) OBCMalloc( sizeof(telecommand_athena_t) );
	if( clone == NULL ){ return NULL; }

	err = initialize_telecommand_athena( clone, self->_kit );
	if( err == false )
	{
		OBCFree( (void*) clone );
		return NULL;
	}

	return (telecommand_t*) clone;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_telecommand_athena( telecommand_athena_t *self, driver_toolkit_t *kit )
{
	DEV_ASSERT( self );
	DEV_ASSERT( kit );

	bool_t err;

	err = initialize_telecommand( (telecommand_t *) self, kit );

	((telecommand_t *) self)->_execute = execute;
	((telecommand_t *) self)->clone = clone;
	((telecommand_t *) self)->_type_ = TELECOMMAND_TYPE_ATHENA;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
