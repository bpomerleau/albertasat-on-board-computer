/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_antenna_deployment.c
 * @author Brendan Bruner
 * @date Oct 7, 2015
 */
#include <states/state.h>
#include <states/state_relay.h>
#include <string.h>
#include <states/leop/state_antenna_deployment.h>
#include <core_defines.h>
#include <hub/deployment_lines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_ENABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_DISABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_DISABLES_EXECUTION
#define DEFAULT_DFGM		STATE_DISABLES_EXECUTION
#define DEFAULT_MNLP		STATE_DISABLES_EXECUTION


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_antenna_deployment_enter( state_t *state, driver_toolkit_t *kit )
{
	state_antenna_deployment_t* self = (state_antenna_deployment_t*) state;
	DEV_ASSERT( state );
	DEV_ASSERT( kit );

	non_volatile_variable_t* leop_flag;
	uint8_t leop_flag_value;

	leop_flag = self->_.leop_flag;
	leop_flag_value = LEOP_FLAG_FINISHED;

	//What if antenna doesn't deploy?
	kit->hub->deploy(kit->hub, ANTENNA_DEPLOYMENT_LINE);

	leop_flag->set( leop_flag, (void*) &leop_flag_value );

	lock_mutex( *state->_mutex_, BLOCK_FOREVER );
	self->_.is_deployment_done = true;
	unlock_mutex( *state->_mutex_ );
}

static void state_antenna_deployment_exit( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	state_antenna_deployment_t* self = (state_antenna_deployment_t*) state;
	DEV_ASSERT( state );
	DEV_ASSERT( relay );

	bool_t is_deployment_done;

	lock_mutex( *state->_mutex_, BLOCK_FOREVER );
	is_deployment_done = self->_.is_deployment_done;
	unlock_mutex( *state->_mutex_ );

	if( is_deployment_done )
	{
		/* Finished deploying. */
		return (state_t*) &relay->bring_up;
	}
	else
	{
		return NULL;
	}
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_antenna_deployment
(
	state_antenna_deployment_t *state,
	filesystem_t *fs,
	non_volatile_variable_t* leop_flag
)
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	state_config_t	config;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, STATE_ANTENNA_DEPLOYMENT_LOG, fs, &config );

	((state_t *) state)->_id_ = 		STATE_ANTENNA_DEPLOYMENT_ID;
	((state_t *) state)->enter_state = 	state_antenna_deployment_enter;
	((state_t *) state)->exit_state = 	state_antenna_deployment_exit;
	((state_t *) state)->next_state = 	next_state;

	state->_.leop_flag = leop_flag;
	state->_.is_deployment_done = false;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
