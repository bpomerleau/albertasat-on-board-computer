/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_boom_deployment.c
 * @author Brendan Bruner
 * @date Oct 7, 2015
 */
#include <states/state.h>
#include <states/state_relay.h>
#include <states/leop/state_boom_deployment.h>
#include <core_defines.h>
#include <printing.h>
#include <hub/deployment_lines.h>
#include <dfgm.h>
#include <dfgm/nanomind_mag.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_ENABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_DISABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_DISABLES_EXECUTION
#define DEFAULT_DFGM		STATE_DISABLES_EXECUTION
#define DEFAULT_MNLP		STATE_DISABLES_EXECUTION


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_boom_deployment_enter( state_t *state, driver_toolkit_t *kit )
{
	state_boom_deployment_t* self = (state_boom_deployment_t*) state;
	DEV_ASSERT( state );
	DEV_ASSERT( kit );

	/* Enable nanomind mag - collect mag data in parallel with fluxgate. */
	enable_nanomind_mag( );

	/* Enable raw dfgm data collection during deployment */
	DFGM_unlock();
	dfgm_config_set_stream(&kit->dfgm->dfgm_config, RAW_ENABLE);
	kit->dfgm->power(kit->dfgm, kit->eps, true);

	/* Collect data before deployment */
	/* DFGM is on, so data is collected in another task */
	task_delay(PRE_DEPLOY_TIMEOUT_S*ONE_SECOND);

	// TODO: Sync rtc with gps?
	// TODO take photo with adcs?

	/* Deploy fluxgate's boom. */
	kit->hub->deploy(kit->hub, DFGM_BOOM_DEPLOYMENT_LINE); // TODO fix this pointer

	/* Continue data collection after deployment. */
	task_delay(POST_DEPLOY_TIMEOUT_S*ONE_SECOND);

	/* Restore dfgm state. */
	kit->dfgm->power(kit->dfgm, kit->eps, false);
	dfgm_config_set_stream(&kit->dfgm->dfgm_config, FILTER_ENABLE);
	DFGM_lock( );

	/* Disable nanomind mag. */
	disable_nanomind_mag( );

	/* Set variable indicating deployment is finished */
	lock_mutex( *state->_mutex_, BLOCK_FOREVER );
	self->_.is_deployment_done = true;
	unlock_mutex( *state->_mutex_ );
}

static void state_boom_deployment_exit( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );
	//DFGM_lock();
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	state_boom_deployment_t* self = (state_boom_deployment_t*) state;
	DEV_ASSERT( state );
	DEV_ASSERT( relay );

	bool_t is_deployment_done;

	lock_mutex( *state->_mutex_, BLOCK_FOREVER );
	is_deployment_done = self->_.is_deployment_done;
	unlock_mutex( *state->_mutex_ );

	if( is_deployment_done )
	{
		/* Finished deploying. */
		return (state_t*) &relay->antenna_deploy;
	}
	else
	{
		return NULL;
	}
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_boom_deployment( state_boom_deployment_t *state, filesystem_t *fs )
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	state_config_t	config;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, STATE_BOOM_DEPLOYMENT_LOG, fs, &config );

	((state_t *) state)->_id_ = 		STATE_BOOM_DEPLOYMENT_ID;
	((state_t *) state)->enter_state = 	state_boom_deployment_enter;
	((state_t *) state)->exit_state = 	state_boom_deployment_exit;
	((state_t *) state)->next_state = 	next_state;

	state->_.is_deployment_done = false;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
