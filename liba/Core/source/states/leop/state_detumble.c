/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_detumble.c
 * @author Brendan Bruner
 * @date Oct 7, 2015
 */
#include <states/state.h>
#include <states/state_relay.h>
#include <states/leop/state_detumble.h>
#include <adcs/adcs.h>
#include <eps/eps.h>
#include <core_defines.h>
#include <printing.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define DEFAULT_HK			STATE_ENABLES_EXECUTION
#define DEFAULT_TRANSMIT	STATE_DISABLES_EXECUTION
#define DEFAULT_RESPONSE	STATE_DISABLES_EXECUTION
#define DEFAULT_DFGM		STATE_DISABLES_EXECUTION
#define DEFAULT_MNLP		STATE_DISABLES_EXECUTION


/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/
static char const * const config_log = STATE_DETUMBLE_LOG;


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void state_detumble_enter( state_t *state, driver_toolkit_t *kit )
{
	//state_detumble_t* self = (state_detumble_t*) state;
	DEV_ASSERT( state );
	DEV_ASSERT( kit );
}

static void state_detumble_exit( state_t *state, driver_toolkit_t *kit )
{
	DEV_ASSERT( state );
	DEV_ASSERT( kit );
}

static state_t *next_state(	state_t *state,	state_relay_t *relay )
{
	state_detumble_t* self = (state_detumble_t*) state;
	DEV_ASSERT( self );
	DEV_ASSERT( relay );

	eps_t* eps;

	eps = relay->drivers->eps;
	if( eps->mode( eps ) == EPS_MODE_POWER_SAFE || eps->mode( eps ) == EPS_MODE_CRITICAL )
	{
		// Not enough power to keep detumbling, charge batteries for a bit.
		return (state_t*) &relay->power_charge;
	}
	else
	{
		// Can't do anything, so go to science.
		return (state_t*) &relay->science;
	}

	return NULL;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy( state_t* state )
{
	state_detumble_t* self = (state_detumble_t*) state;
	DEV_ASSERT( self );

	self->priv_.timer.destroy( &self->priv_.timer );
	self->priv_.supers_destroy( (state_t*) self );
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
uint8_t initialize_state_detumble( state_detumble_t *state, filesystem_t *fs, uint32_t time_out, char const* log_file )
{
	DEV_ASSERT( state );
	DEV_ASSERT( fs );

	uint8_t 		err;
	state_config_t	config;
	bool_t			timer_err;

	state_config_set_hk( &config, DEFAULT_HK );
	state_config_set_transmit( &config, DEFAULT_TRANSMIT );
	state_config_set_response( &config, DEFAULT_RESPONSE );
	state_config_set_mnlp( &config, DEFAULT_MNLP );
	state_config_set_dfgm( &config, DEFAULT_DFGM );

	err = initialize_state( (state_t *) state, config_log, fs, &config );

	// TODO decide if we are timing.
	timer_err = initialize_ptimer( &state->priv_.timer, time_out, LEOP_DETUMBLE_RESOLUTION, fs, log_file );
	if( !timer_err )
	{
		// Error initializing timer.
		return STATE_FAILURE;
	}

	((state_t *) state)->_id_ = 		STATE_DETUMBLE_ID;
	((state_t *) state)->enter_state = 	state_detumble_enter;
	((state_t *) state)->exit_state = 	state_detumble_exit;
	((state_t *) state)->next_state = 	next_state;
	state->priv_.supers_destroy = ((state_t*) state)->destroy;
	((state_t*) state)->destroy = destroy;

	return err;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
