/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file system_state_relay
 * @author Brendan Bruner
 * @date Jan 29, 2015
 */

#include <states/state_relay.h>
#include <telecommands/telecommand.h>
#include "core_defines.h"
#include "portable_types.h"
#include <stdio.h>
#include <printing.h>

#ifndef NULL
#define NULL (void *) 0
#endif

/************************************************************************/
/* Private 																*/
/************************************************************************/
static mutex_t state_relay_communal_mutex;
static bool_t state_relay_is_init = false;

#define ASSERT_STATE( err )	\
	do {										\
		if( (err) == STATE_FAILURE ) {			\
			return STATE_RELAY_FAILURE;			\
		}										\
	} while( 0 )

#define generic_relay( relay_p, command_p, handle_name )				\
				do {													\
					state_t *state;										\
																		\
					if( command_p != NULL )								\
					{													\
						lock_mutex( relay_p->_state_mutex_, BLOCK_FOREVER );\
						state = relay_p->_current_state_;					\
						unlock_mutex( relay_p->_state_mutex_ );			\
																		\
						handle_name ( state, command_p );				\
					}													\
				} while( 0 )


/************************************************************************/
/* State Manipulation Methods											*/
/************************************************************************/
static state_t * get_current_state( state_relay_t *relay )
{
	state_t *current_state;

	lock_mutex( relay->_state_mutex_, BLOCK_FOREVER );
	current_state = relay->_current_state_;
	unlock_mutex( relay->_state_mutex_ );

	return current_state;
}

static void set_current_state( state_relay_t *relay, state_t *state )
{
	lock_mutex( relay->_state_mutex_, BLOCK_FOREVER );
	relay->_current_state_ = state;
	unlock_mutex( relay->_state_mutex_ );
}

static void log_state_change( state_relay_t *relay, state_t *next_state )
{
	rtc_t 				*rtc;
	uint8_t 			state_id;
	uint32_t 			time;
	FILE* fid;

	rtc = relay->drivers->rtc;
	state_id = query_state_id( next_state );
	time = rtc->get_ds1302_time();

	/* If there is an error logging, nothing can be done to correct it. Therefore */
	/* disregard returned error code. */
	fid = fopen("/boot/state_log.bin","a");
	fwrite(&state_id, 1, 1, fid);
	fwrite(&time, 4, 1, fid);
	fclose(fid);
}

void relay_next_state( state_relay_t *relay )
{
	state_t *next_state, *current_state;

	/* Get current state. */
	current_state = get_current_state( relay );

	/* Query next state from current state. */
	next_state = current_state->next_state( current_state, relay );

	if ( next_state == NULL )
	{
		/* No next state, return. */
		return;
	}
	else
	{
		/* Set the next state as the current state. */
		set_current_state( relay, next_state );

		/* Call exit method of previous state. */
		state_exit( current_state, relay->drivers );
		/* Call entry method of new state. */
		//DEMO_PRINTF( "Transitioning into state: %s\n", state_to_name_map[query_state_id( next_state )] );
		csp_printf( "Transitioning into state: %s\n", state_to_name_map[query_state_id( next_state )] );
		state_enter( next_state, relay->drivers );

		/* Log the change in states. */
		log_state_change( relay, next_state );
	}
}

void relay_force_change_state( 	state_relay_t *relay, state_t *next )
{
	DEV_ASSERT( relay );
	DEV_ASSERT( next );

	state_t *current_state;

	/* Get current state and call its exit method. */
	current_state = get_current_state( relay );
	state_exit( current_state, relay->drivers );

	/* Set the new state. */
	set_current_state( relay, next );

	/* Call the new states entry method. */
	state_enter( next, relay->drivers );

	/* Log the change in states. */
	log_state_change( relay, next );
}

/************************************************************************/
/* Methods to relay commands from application to state.					*/
/************************************************************************/
void relay_command_collect_housekeeping( state_relay_t *relay, telecommand_t *command )
{
	DEV_ASSERT( relay );
	generic_relay( relay, command, state_execution_hk );
}

void relay_command_respond_to_request( state_relay_t *relay, telecommand_t *command )
{
	DEV_ASSERT( relay );
	generic_relay( relay, command, state_execution_response );
}

void relay_command_diagnostics( state_relay_t *relay, telecommand_t *command )
{
	DEV_ASSERT( relay );
	generic_relay( relay, command, state_execution_diagnostics );
}

void relay_command_transmit_data( state_relay_t *relay, telecommand_t *command )
{
	DEV_ASSERT( relay );
	generic_relay( relay, command, state_execution_transmit );
}

void relay_command_handle_MnLP(	state_relay_t *relay, telecommand_t *command )
{
	DEV_ASSERT( relay );
	generic_relay( relay, command, state_execution_mnlp );
}

void relay_command_handle_DFGM(	state_relay_t *relay, telecommand_t *command )
{
	DEV_ASSERT( relay );
	generic_relay( relay, command, state_execution_dfgm );
}


/************************************************************************/
/* Methods to relay data from state back to application.				*/
/************************************************************************/
uint8_t relay_state_id( state_relay_t *relay )
{
	DEV_ASSERT( relay );

	state_t *current_state;

	current_state = get_current_state( relay );
	return query_state_id( current_state );
}

/************************************************************************/
/* Initialization Methods												*/
/************************************************************************/
uint8_t initialize_state_relay_simple( state_relay_t* relay, driver_toolkit_t* drivers )
{
	return initialize_state_relay
			(
				relay,
				drivers,
				LEOP_FLAG_FILE,
				BLACK_OUT_TIME_OUT,
				BLACK_OUT_TIME_OUT_LOG,
				DETUMBLE_TIME_OUT,
				DETUMBLE_TIME_OUT_LOG
			);
}

uint8_t initialize_state_relay
(
	state_relay_t *relay,
	driver_toolkit_t *drivers,
	char const* leop_log_file,
	uint32_t black_out_time_out,
	char const* black_out_log_file,
	uint32_t detumble_time_out,
	char const* detumble_log_file
)
{
	DEV_ASSERT( relay );
	DEV_ASSERT( drivers );
	DEV_ASSERT( leop_log_file );
	DEV_ASSERT( black_out_log_file );
	DEV_ASSERT( detumble_log_file );

	uint8_t state_err;

	/* Initialize singleton mutex. */
	if( state_relay_is_init == false )
	{
		new_mutex( state_relay_communal_mutex );
		if( state_relay_communal_mutex == NULL )
		{
			return STATE_RELAY_FAILURE;
		}
		//unlock_mutex( state_relay_communal_mutex );
		state_relay_is_init = true;
	}

	state_err = initialize_state_bring_up( &relay->bring_up, drivers->fs );
	ASSERT_STATE( state_err );
	state_err = initialize_state_low_power( &relay->low_power, drivers->fs );
	ASSERT_STATE( state_err );
	state_err = initialize_state_science( &relay->science, drivers->fs );
	ASSERT_STATE( state_err );

	/* Note, the variable state_relay_communal_mutex is a singleton variable used by all */
	/* state relay objects. Search 'singleton' in google. */
	relay->_state_mutex_ = state_relay_communal_mutex;
	relay->drivers = drivers;

	relay->_current_state_ = (state_t*) &relay->bring_up;
	relay->_current_state_->enter_state( relay->_current_state_, relay->drivers );

	return STATE_RELAY_SUCCESS;
}

void destroy_state_relay( state_relay_t *relay )
{
	DEV_ASSERT( relay );

	((state_t *) &relay->bring_up)->destroy( (state_t *) &relay->bring_up );
	((state_t *) &relay->low_power)->destroy( (state_t *) &relay->low_power );
	((state_t *) &relay->science)->destroy( (state_t *) &relay->science );
}
