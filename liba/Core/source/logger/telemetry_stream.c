/*
 *
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telemetry_stream
 * @author Brendan Bruner
 * @date Oct. 2, 2017
 */

#include <logger/telemetry_stream.h>


#define OPEN_NEXT_FILE 1
#define OPEN_CURRENT_FILE 0

static int telemetry_stream_next_bucket( struct telemetry_stream_t* self, file_t* file )
{
	uint32_t size;
	fs_error_t ferr;

	size = file->size(file, &ferr);
	if( ferr == FS_OK && size >= self->max_bucket_size ) {
		return 1;
	}
	return -1;
}

static file_t* telemetry_stream_insert_file( struct telemetry_stream_t* self, struct logger_t* logger, int* err )
{
	logger_error_t lerr;

	self->data_file = logger_insert(logger, &lerr, NULL);
	if( lerr != LOGGER_OK) {
		*err = TELEMETRY_STREAM_FAIL;
		self->data_file = NULL;
		return NULL;
	}
	else {
		*err = TELEMETRY_STREAM_NEW_FILE;
		return self->data_file;
	}
}

static file_t* telemetry_stream_apply_user_constraint( struct telemetry_stream_t* self,
														struct logger_t* logger,
														int* err )
{
	/* Log file is open, check if it meets the critera to be
	 * closed, and a new file opened.
	 */
	int do_next = self->next(self, self->data_file);
	if( do_next > 0 ) {
		/* Time to close the file and open a new one.
		 */
		self->data_file->close(self->data_file);
		return telemetry_stream_insert_file(self, logger, err);
	}

	/* Keep this file, no need to close it.
	 */
	*err = TELEMETRY_STREAM_OK;
	return self->data_file;
}

/********************************************************************************/
/* Functions																	*/
/********************************************************************************/
/*
 * Cyclomatic complexity graph
 * 1(if[args bound]) +--> exit <-------------------------------+---------------+
 * 					 |										   |			   |
 * 					 +--> 2(if[re-open]) +--> 3(if[open next]) +---------------|
 * 					 					 |                                     |
 * 					 					 +--> 4(if[empty]) +-------------------+
 * 					 					 				   | 				   |
 * 					 					 				   +--> 5(if[err]) +---+
 * 					 					 				                   |   |
 * 					 					 				                   +---+
 *
 * Nodes = 6
 * Edges = 10
 * Complexity = E - N + 2 = 6
 */
file_t* telemetry_stream_get_file( struct telemetry_stream_t* stream, int* err )
{
	struct logger_t* logger;
	logger_error_t lerr;

	if( stream == NULL || err == NULL ) {
		return NULL;
	}

	logger = stream->data_bank;

	/* Ensure a log file is open.
	 */
	if( stream->data_file == NULL && stream->do_next_file == OPEN_CURRENT_FILE ) {

		stream->data_file = logger_peek_head(logger, &lerr);
		if( lerr == LOGGER_EMPTY ) {
			/* There are no file in the ring buffer. Insert a file.
			 */
			return telemetry_stream_insert_file(stream, logger, err);
		}
		else if( lerr != LOGGER_OK ){
			/* Failure getting a data file
			 */
			*err = TELEMETRY_STREAM_FAIL;
			stream->data_file = NULL;
			return NULL;
		}
		else {
			/* No error, apply constraints.
			 */
			return telemetry_stream_apply_user_constraint(stream, logger, err);
		}

	}
	else if( stream->data_file == NULL && stream->do_next_file == OPEN_NEXT_FILE ) {
		return telemetry_stream_insert_file(stream, logger, err);
	}

	return telemetry_stream_apply_user_constraint(stream, logger, err);
}

int telemetry_stream_next_file( struct telemetry_stream_t* stream )
{
	if( stream == NULL ) {
		return TELEMETRY_STREAM_FAIL;
	}

	if( stream->data_file != NULL ) {
		stream->data_file->close(stream->data_file);
	}

	/* Let telemetry_stream_get_file( )
	 * open a new file when called by the application.
	 */
	stream->data_file = NULL;
	stream->do_next_file = OPEN_NEXT_FILE;
	return TELEMETRY_STREAM_OK;
}

int telemetry_stream_close_file( struct telemetry_stream_t* stream )
{
	int err;
	err = telemetry_stream_next_file(stream);
	if( err == TELEMETRY_STREAM_OK ) {
		stream->do_next_file = OPEN_CURRENT_FILE;
	}
	return err;
}

int telemetry_stream_init_custom( struct telemetry_stream_t* stream, struct logger_t* logger, telemetry_stream_next_f next, int strat )
{
	if( stream == NULL || logger == NULL || next == NULL ) {
		return TELEMETRY_STREAM_FAIL;
	}
	stream->data_bank = logger;
	stream->next = next;
	stream->data_file = NULL;
	if( strat == 0 ) {
		stream->do_next_file = OPEN_NEXT_FILE;
	}
	else {
		stream->do_next_file = OPEN_CURRENT_FILE;
	}
	return TELEMETRY_STREAM_OK;
}

int telemetry_stream_init( struct telemetry_stream_t* stream, struct logger_t* logger, size_t max_bucket_size, int strat )
{
	stream->max_bucket_size = max_bucket_size;
	return telemetry_stream_init_custom(stream, logger, telemetry_stream_next_bucket, strat);
}
