/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file logger.c
 * @author Brendan Bruner
 * @date May 15, 2015
 *
 * Looking at this in 2018 I feel it's the most magnificent piece of code i've written
 * and at the same time it's garbage.
 * The good stuff:
 * 		* Implements a pretty robust ring buffer using file descriptors for buckets
 * 		* Can handle a decent amount of corruption to it's meta data
 * 		* Persists through unexpected power cycles
 * The bad stuff:
 * 		* So much duplicate code. It's amazing how many times I wrote the same 4 lines of
 * 		  code all over the place
 * 		* Functions are sooooooo long. It's hard for me to understand the algorithm and I
 * 		  wrote the damn thing
 * 		* Don't like the class' name
 * 		* Goes full retard if the bucket at the head of ring buffer or the meta data file
 * 		  is asynchronously deleted.
 * 		* Tries to hard to recover lost data resulting in significant slow downs
 * 		* Does too much for one class. A lot of functionality can be abstracted into helper
 * 		  classes
 */

#include <filesystems/fs_handle.h>
#include <logger/logger.h>
#include <string.h>
#include <printing.h>

/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define LOGGER_META_HEAD_START 0
#define	LOGGER_META_TAIL_START (FILESYSTEM_MAX_NAME_LENGTH+1)
#define LOGGER_CONTROL_DATA_LENGTH ((2*(FILESYSTEM_MAX_NAME_LENGTH+1))+3+4+7+2)
#define LOGGER_META_TEM_START ((2*(FILESYSTEM_MAX_NAME_LENGTH+1))+3+4)
#define LOGGER_META_TEM_LENGTH 7

/********************************************************************************/
/* Singletons																	*/
/********************************************************************************/
/* All logger instance share the same mutex. */
static mutex_t logger_sync_mutex;
static bool_t logger_is_init = false;

/********************************************************************************/
/* Private Method Definitions													*/
/********************************************************************************/
static unsigned int logger_powerof( unsigned int base, unsigned int exp )
{
  unsigned int i;
  unsigned int num = 1;
  for( i = 0; i < exp; ++i ) {
    num = num * base;
  }
  return num;
}

static void logger_uitoa( unsigned int num, char* str, size_t len, int base )
{
  size_t i;
  unsigned int rem;
  char ascii_num;

  /* Zero fill string. */
  for( i = 0; i < len; ++i ) {
    str[i] = '0';
  }

  /* Check for zero. */
  if( num == 0 ) {
    return;
  }

  for( i = 0; i < len; ++i ) {
    rem = num % base;
    ascii_num = (rem > 9) ? ((rem-10) + 'a') : (rem + '0');
    str[len-i-1] = ascii_num;
    num = (num - rem) / base;
  }
}
void ocp_uito( unsigned int num, char* str, size_t len, int base )
{
	logger_uitoa(num, str, len, base);
}

static unsigned int logger_atoui( const char* str, size_t len, int base )
{
  size_t i;
  unsigned int num = 0;
  unsigned int inter_num = 0;


  for( i = 0; i < len; ++i ) {
    /* Convert ascii byte to int. */
    if( str[i] >= '0' && str[i] <= '9' ) {
      inter_num = str[i] - '0';
    }
    else if( str[i] >= 'a' && str[i] <= 'z' ) {
      inter_num = str[i] - 'a' + 10;
    }
    else if( str[i] >= 'A' && str[i] <= 'Z' ) {
      inter_num = str[i] - 'A' + 10;
    }
    else {
      return 0;
    }

    /* accumulate. */
    num += inter_num * logger_powerof(base, len - i - 1);
  }

  return num;
}
unsigned int ocp_atoui( const char* str, size_t len, int base )
{
	return logger_atoui(str, len, base);
}

/**
 * @memberof logger_t @private
 * @brief
 * 		Get the next file name.
 * @details
 * 		Given the current file name, <b>name</b>, compute the next name of
 * 		a file. <br>For example, the next name, with a max capacity of 40, of 039G2304
 * 		is 000G2305.
 * 		<br>With a max capacity of 120, the next name would of 103Y0032 would be
 * 		104Y0033.
 * @param current_name[in/out]
 * 		When this function is called, this is the current name of the file
 * 		pointed to by HEAD, when the function returns, this will now be the
 * 		name of the new file to be the new HEAD.
 */
static void logger_next_name( logger_t* self, char* name )
{
	DEV_ASSERT(self);
	DEV_ASSERT(name);

	unsigned int next_seq = 0;
	unsigned int next_tem = 0;

	/* Increment temporal and sequence number, then roll over if necessary.
	 * We can't use MOD for next_seq because logger_t::max_capacity can change dynamically. If it were
	 * to suddenly decrease and next_seq became greater than  logger_t::max_capacity, the MOD would not
	 * return the desired value.
	 */
	next_seq = (logger_atoui(name + LOGGER_SEQUENCE_START, LOGGER_TOTAL_SEQUENCE_BYTES, LOGGER_SEQUENCE_BASE) + 1);
	if( next_seq >= self->max_capacity ) {
		next_seq = 0;
	}
	next_tem = (((name[4]-'0')*1000) + ((name[5]-'0')*100) + ((name[6]-'0')*10) + (name[7]-'0') + 1) % LOGGER_MAX_TEMPORAL_POINTS;

	/* Convert numbers back to ascii and put in name string. */
	logger_uitoa(next_seq, name + LOGGER_SEQUENCE_START, LOGGER_TOTAL_SEQUENCE_BYTES, LOGGER_SEQUENCE_BASE);
//	name[2] = (char) ((next_seq % 20) + 'a') & 0xFF;
//	next_seq = (next_seq - (next_seq % 20)) / 20; // basically a base ten logical shift right.
//	name[1] = (char) ((next_seq % 20) + 'a') & 0xFF;
//	next_seq = (next_seq - (next_seq % 10)) / 10; // basically a base ten logical shift right.
//	name[0] = (char) ((next_seq % 20) + 'a') & 0xFF;

	name[7] = (char) ((next_tem % 10) + '0') & 0xFF;
	next_tem = (next_tem - (next_tem % 10)) / 10; // basically a base ten logical shift right.
	name[6] = (char) ((next_tem % 10) + '0') & 0xFF;
	next_tem = (next_tem - (next_tem % 10)) / 10; // basically a base ten logical shift right.
	name[5] = (char) ((next_tem % 10) + '0') & 0xFF;
	next_tem = (next_tem - (next_tem % 10)) / 10; // basically a base ten logical shift right.
	name[4] = (char) ((next_tem % 10) + '0') & 0xFF;

	//snprintf(name, LOGGER_TOTAL_SEQUENCE_BYTES, "%d", next_seq);
	//snprintf(name+LOGGER_TEMPORAL_START, LOGGER_TOTAL_TEMPORAL_BYTES, "%d", next_tem);
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Creates a new control data file.
 * @details
 * 		Creates a new control data file. All data is wiped from the existing file (if one
 * 		exists).
 * 		| HEAD (LOGGER_MAX_FILE_NAME_LENGTH+1 bytes) | TAIL (LOGGER_MAX_FILE_NAME_LENGTH+1 bytes) |
 * 		| HEAD sequence data (3 bytes) | HEAD temporal data (4 bytes) | popped temporal data (7 bytes) | reserved (2 bytes) |
 */
static logger_error_t logger_create_control_file( logger_t* self )
{
	DEV_ASSERT(self);

	file_t* 	control_file_handle;
	fs_error_t	ferr;
	char 		control_string[LOGGER_CONTROL_DATA_LENGTH];
	uint32_t	bytes_written;

	/* Write to a variable the initial set of control data. */
	snprintf(control_string, LOGGER_CONTROL_DATA_LENGTH, "000%c0000.bin%c000%c0000.bin%c0000000000000000", self->element_file_name, '\0', self->element_file_name, '\0');

	/* Open control file. */
	control_file_handle = self->fs->open(self->fs, &ferr, self->control_file_name, FS_CREATE_ALWAYS, USE_POLLING);
	if( ferr != FS_OK ) {
		/* File system failure. */
		return LOGGER_NVMEM_ERR;
	}

	/* Write control data into control file. */
	ferr = control_file_handle->write(control_file_handle, (uint8_t*) control_string, LOGGER_CONTROL_DATA_LENGTH, &bytes_written);
	control_file_handle->close(control_file_handle);
	if( ferr != FS_OK ) {
		/* File system failure. */
		return LOGGER_NVMEM_ERR;
	} else if( bytes_written != LOGGER_CONTROL_DATA_LENGTH ) {
		/* Out of memory :( */
		return LOGGER_NVMEM_FULL;
	}
	return LOGGER_OK;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Cache control.
 * @details
 * 		Caches control data from the control data file. If no control data file exists, it creates one.
 */
static logger_error_t logger_cache_control_data( logger_t* self )
{
	DEV_ASSERT(self);

	file_t*		control_file_handle;
	fs_error_t	ferr;
	uint32_t	bytes_read;
	logger_error_t lerr;


	control_file_handle = self->fs->open(self->fs, &ferr, self->control_file_name, FS_OPEN_EXISTING, USE_POLLING);
	if( ferr == FS_NO_FILE ) {
		/* Need to create the control file, it doesn't exist. */
		lerr = logger_create_control_file(self);
		if( lerr == LOGGER_OK ) {
			control_file_handle = self->fs->open(self->fs, &ferr, self->control_file_name, FS_OPEN_EXISTING, USE_POLLING);
		} else {
			return lerr;
		}
	}
	if ( ferr != FS_OK ) {
		/* Error, return. */
		return LOGGER_NVMEM_ERR;
	}

	/* Cache HEAD. */
	ferr = control_file_handle->read(control_file_handle, (uint8_t*) self->head_file_name, FILESYSTEM_MAX_NAME_LENGTH+1, &bytes_read);
	if( ferr != FS_OK ) {
		/* Failed to read head into memory. */
		control_file_handle->close(control_file_handle);
		return LOGGER_NVMEM_ERR;
	} else if( bytes_read != (FILESYSTEM_MAX_NAME_LENGTH+1) ) {
		/* File is too small, wipe it and create new one. */
		control_file_handle->close(control_file_handle);
		return logger_create_control_file(self);
	}
	self->head_file_name[FILESYSTEM_MAX_NAME_LENGTH] = '\0';

	/* Cache TAIL. */
	ferr = control_file_handle->read(control_file_handle, (uint8_t*) self->tail_file_name, FILESYSTEM_MAX_NAME_LENGTH+1, &bytes_read);
	control_file_handle->close(control_file_handle);
	if( ferr != FS_OK ) {
		/* Failed to read head into memory. */
		return LOGGER_NVMEM_ERR;
	} else if( bytes_read != (FILESYSTEM_MAX_NAME_LENGTH+1) ) {
		/* File is too small, wipe it and create new one. */
		return logger_create_control_file(self);
	}
	self->tail_file_name[FILESYSTEM_MAX_NAME_LENGTH] = '\0';

	return LOGGER_OK;
}

/**
 * @memberof logger_t @private
 * @brief
 * 		Get the file name of the HEAD.
 * @details
 * 		Get the file name of the HEAD. This method just calls
 * 		logger_cache_control_data then returns logger_t::head_file_name.
 */
static char* logger_get_head( logger_t* self, logger_error_t* err )
{
	DEV_ASSERT(self);
	DEV_ASSERT(err);

	*err = logger_cache_control_data(self);
	return self->head_file_name;
}

/**
 * @memberof logger_t @private
 * @brief
 * 		Set the file name of the HEAD.
 * @details
 * 		Set the file name of the HEAD.
 * @param head [in]
 * 		Must point to a string of at least FILESYSTEM_MAX_NAME_LENGTH bytes.
 */
static logger_error_t logger_set_head( logger_t* self, char const* head )
{
	DEV_ASSERT(self);
	DEV_ASSERT(head);

	file_t*		control_file_handle;
	fs_error_t	ferr;
	uint32_t	bytes_written;

	control_file_handle = self->fs->open(self->fs, &ferr, self->control_file_name, FS_OPEN_ALWAYS, USE_POLLING);
	if( ferr != FS_OK ) {
		return LOGGER_NVMEM_ERR;
	}

	ferr = control_file_handle->write(control_file_handle, (uint8_t*) head, FILESYSTEM_MAX_NAME_LENGTH, &bytes_written );
	control_file_handle->close(control_file_handle);
	if( ferr != FS_OK ) {
		return LOGGER_NVMEM_ERR;
	}
	if( bytes_written < FILESYSTEM_MAX_NAME_LENGTH ) {
		return LOGGER_NVMEM_FULL;
	}
	return LOGGER_OK;
}

/**
 * @memberof logger_t @private
 * @brief
 * 		Get the file name of the TAIL.
 * @details
 * 		Get the file name of the TAIL. This method just calls
 * 		logger_cache_control_data then returns logger_t::tail_file_name.
 */
static char* logger_get_tail( logger_t* self, logger_error_t* err )
{
	DEV_ASSERT(self);
	DEV_ASSERT(err);

	*err = logger_cache_control_data(self);
	return self->tail_file_name;
}

/**
 * @memberof logger_t @private
 * @brief
 * 		Set the file name of the TAIL.
 * @details
 * 		Set the file name of the TAIL.
 */
static logger_error_t logger_set_tail( logger_t* self, char const* tail )
{
	DEV_ASSERT(self);
	DEV_ASSERT(tail);

	file_t*		control_file_handle;
	fs_error_t	ferr;
	uint32_t	bytes_written;

	control_file_handle = self->fs->open(self->fs, &ferr, self->control_file_name, FS_OPEN_ALWAYS, USE_POLLING);
	if( ferr != FS_OK ) {
		return LOGGER_NVMEM_ERR;
	}

	ferr = control_file_handle->seek(control_file_handle, LOGGER_META_TAIL_START);
	if( ferr != FS_OK ) {
		control_file_handle->close(control_file_handle);
		return LOGGER_NVMEM_ERR;
	}

	ferr = control_file_handle->write(control_file_handle, (uint8_t*) tail, FILESYSTEM_MAX_NAME_LENGTH, &bytes_written );
	control_file_handle->close(control_file_handle);
	if( ferr != FS_OK ) {
		return LOGGER_NVMEM_ERR;
	}
	if( bytes_written < FILESYSTEM_MAX_NAME_LENGTH ) {
		return LOGGER_NVMEM_FULL;
	}
	return LOGGER_OK;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Rename a file to remove it from the ring buffer's tracking.
 * @details
 * 		Rename a file to remove it from the ring buffer's tracking.
 * 		This will give the file the naming convention:
 * 		<br><b>Xaaaaaaa.bin</b>
 * 		<br>As defined in by logger_t documentation.
 */
static logger_error_t logger_untrack_file( logger_t* self, char* file_name )
{
	DEV_ASSERT(self);
	DEV_ASSERT(file_name);

	char 		new_name[FILESYSTEM_MAX_NAME_LENGTH+1];
	file_t*		control_file_handle;
	fs_error_t	ferr;
	int			temporal_point;
	uint32_t	bytes_read;

	/* Open control file. */
	control_file_handle = self->fs->open(self->fs, &ferr, self->control_file_name, FS_OPEN_ALWAYS, USE_POLLING);
	if( ferr != FS_OK ) {
		return LOGGER_NVMEM_ERR;
	}

	/* Seek to temporal data. */
	ferr = control_file_handle->seek(control_file_handle, LOGGER_META_TEM_START);
	if( ferr != FS_OK ) {
		control_file_handle->close(control_file_handle);
		return LOGGER_NVMEM_ERR;
	}

	/* Get temporal point. */
	ferr = control_file_handle->read(control_file_handle, (uint8_t*) new_name+1, LOGGER_META_TEM_LENGTH, &bytes_read);
	if( ferr != FS_OK || bytes_read != LOGGER_META_TEM_LENGTH ) {
		control_file_handle->close(control_file_handle);
		return LOGGER_NVMEM_ERR;
	}

	/* Finish off the name. */
	new_name[0] = self->element_file_name;
	new_name[8] = '.';
	new_name[9] = 'b';
	new_name[10] = 'i';
	new_name[11] = 'n';
	new_name[12] = '\0';

	/* Get next temporal point. */
	temporal_point = (((new_name[0]-'0')*1000000) + ((new_name[1]-'0')*100000) + ((new_name[2]-'0')*10000) + ((new_name[3]-'0')*1000) + ((new_name[4]-'0')*100) + ((new_name[5]-'0')*10) + ((new_name[6]-'0')*1) + 1) % (10*10*10*10*10*10*10);

	/* Write the new point back to string. */
	new_name[7] = (char) ((temporal_point % 10) + '0') & 0xFF;
	temporal_point = (temporal_point - (temporal_point% 10)) / 10; // basically a base ten logical shift right.
	new_name[6] = (char) ((temporal_point % 10) + '0') & 0xFF;
	temporal_point = (temporal_point - (temporal_point% 10)) / 10; // basically a base ten logical shift right.
	new_name[5] = (char) ((temporal_point % 10) + '0') & 0xFF;
	temporal_point = (temporal_point - (temporal_point% 10)) / 10; // basically a base ten logical shift right.
	new_name[4] = (char) ((temporal_point % 10) + '0') & 0xFF;
	temporal_point = (temporal_point - (temporal_point% 10)) / 10; // basically a base ten logical shift right.
	new_name[3] = (char) ((temporal_point % 10) + '0') & 0xFF;
	temporal_point = (temporal_point - (temporal_point% 10)) / 10; // basically a base ten logical shift right.
	new_name[2] = (char) ((temporal_point % 10) + '0') & 0xFF;
	temporal_point = (temporal_point - (temporal_point% 10)) / 10; // basically a base ten logical shift right.
	new_name[1] = (char) ((temporal_point % 10) + '0') & 0xFF;

	/* Update new temporal point in file. */
	/* Seek to temporal data. */
	ferr = control_file_handle->seek(control_file_handle, LOGGER_META_TEM_START);
	if( ferr != FS_OK ) {
		control_file_handle->close(control_file_handle);
		return LOGGER_NVMEM_ERR;
	}
	/* Set temporal point. */
	ferr = control_file_handle->write(control_file_handle, (uint8_t*) (new_name+1), LOGGER_META_TEM_LENGTH, &bytes_read);
	control_file_handle->close(control_file_handle);
	if( ferr != FS_OK || bytes_read != LOGGER_META_TEM_LENGTH ) {
		return LOGGER_NVMEM_ERR;
	}

	/* Rename the file, first, check if a file with this name already exist. If it does, delete it. */
	ferr = self->fs->file_exists(self->fs, new_name);
	if( ferr == FS_OK ) {
		self->fs->delete(self->fs, new_name);
	}
	ferr = self->fs->rename(self->fs, file_name, new_name);
	if( ferr != FS_OK ) {
		return LOGGER_NVMEM_ERR;
	}

	strncpy(file_name, new_name, FILESYSTEM_MAX_NAME_LENGTH);

	return LOGGER_OK;
}

/**
 * @memberof logger_t
 * @private
 * @brief
 * 		Update tail position.
 * @details
 * 		Updates the position of the tail in the ring buffer. This is useful when asynchronous
 * 		file removals have rendered the tail position corrupt (ie, pointing to a non existant file).
 * @returns
 * 		Error code
 */
static logger_error_t logger_update_tail( logger_t* self )
{
	DEV_ASSERT(self);

	char*			tail_file_name;
	char*			head_file_name;
	logger_error_t	lerr;
	unsigned int				i;
	fs_error_t		ferr;
	bool_t			do_update = false;

	tail_file_name = logger_get_tail(self, &lerr);
	if( lerr != LOGGER_OK ) {
		return lerr;
	}

	head_file_name = logger_get_head(self, &lerr);
	if( lerr != LOGGER_OK ) {
		return lerr;
	}

	/* From the current tail, there is a maximum of logger_t::max_capacity elements to search. */
	for( i = 0; i < self->max_capacity; ++i ) {
		/* Check if this tail file exists (ie, check if it has been asynchronously removed. */
		ferr = self->fs->file_exists(self->fs, tail_file_name);
		if( ferr == FS_NO_FILE ) {
			/* The file doesn't exist. See if this is also the HEAD file. */
			/* If HEAD == TAIL and this file doesn't exist then the buffer has */
			/* been asynchronously emptied (all files deleted). */
			if( 0 == strncmp(tail_file_name, head_file_name, FILESYSTEM_MAX_NAME_LENGTH) ) {
				return LOGGER_EMPTY;
			}

			/* No file, and HEAD != TAIL, keep searching for the current TAIL. */
			logger_next_name(self, tail_file_name);
		} else if ( ferr == FS_OK ) {
			/* Element has a file. */
			do_update = true;
			break;
		} else {
			/* Some file system error. */
			return LOGGER_NVMEM_ERR;
		}
	}

	if( do_update ) {
		/* Update the tail meta data. */
		return logger_set_tail(self, tail_file_name);
	}
	return LOGGER_OK;
}


/********************************************************************************/
/* Public Method Definitions													*/
/********************************************************************************/
file_t* logger_peek_head( logger_t* self, logger_error_t* err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );

	filesystem_t* 	fs;
	file_t*			head_file_handle;
	fs_error_t		file_err;
	logger_error_t	logger_err;
	char const*		head_file_name;
	uint32_t		eof;

	lock_mutex( *self->sync_mutex, BLOCK_FOREVER );

	/* Set up some local variables to simplify code readability. */
	fs = self->fs;

	/* Get name of file at HEAD. */
	head_file_name = logger_get_head(self, &logger_err);
	if( logger_err != LOGGER_OK ) {
		unlock_mutex( *self->sync_mutex );
		return _filesystem_get_null_file(fs);
	}

	/* Open the file. */
	head_file_handle = fs->open(fs, &file_err, head_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER);
	unlock_mutex( *self->sync_mutex );
	if( file_err == FS_NO_FILE ) {
		*err = LOGGER_EMPTY;
		return _filesystem_get_null_file(fs);
	}
	if( file_err != FS_OK ) {
		*err = LOGGER_NVMEM_ERR;
		return _filesystem_get_null_file(fs);
	}

	/* Get size of the file so we can seek to the end of it. */
	eof = head_file_handle->size(head_file_handle, &file_err);
	if( file_err != FS_OK ) {
		*err = LOGGER_NVMEM_ERR;
		head_file_handle->close(head_file_handle);
		return _filesystem_get_null_file(fs);
	}

	/* Seek to the end of the file. */
	file_err = head_file_handle->seek(head_file_handle, eof);
	if( file_err != FS_OK ) {
		*err = LOGGER_NVMEM_ERR;
		head_file_handle->close(head_file_handle);
		return _filesystem_get_null_file(fs);
	}

	*err = LOGGER_OK;
	return head_file_handle;
}

file_t* logger_insert( logger_t* self, logger_error_t* err, char const* file_to_insert_name )
{
	DEV_ASSERT(self);
	DEV_ASSERT(err);

	logger_error_t 	lerr;
	fs_error_t		fs_err;
	char* 			head_file_name;
	char*			tail_file_name;
	char			new_head_file_name[FILESYSTEM_MAX_NAME_LENGTH+1];
	file_t*			head_file_handle;

	lock_mutex(*self->sync_mutex, BLOCK_FOREVER);

	/* Get the name (position) of the HEAD and TAIL. */
	head_file_name = logger_get_head(self, &lerr);
	if( lerr != LOGGER_OK ) {
		unlock_mutex(*self->sync_mutex);
		*err = lerr;
		return _filesystem_get_null_file(self->fs);
	}
	if( self->fs->file_exists(self->fs, head_file_name) == FS_NO_FILE ) {
		/* The HEAD file doesn't exist => logger emptied via asynchronous file removal. */
		lerr = logger_create_control_file(self); /* FIXME: only reset head/tail pointers - not temporal data too */
		if( lerr != LOGGER_OK ) {
			unlock_mutex(*self->sync_mutex);
			*err = lerr;
			return _filesystem_get_null_file(self->fs);
		}
		head_file_name = logger_get_head(self, &lerr);
		if( lerr != LOGGER_OK ) {
			unlock_mutex(*self->sync_mutex);
			*err = lerr;
			return _filesystem_get_null_file(self->fs);
		}
	}

	tail_file_name = logger_get_tail(self, &lerr);
	if( lerr != LOGGER_OK ) {
		unlock_mutex(*self->sync_mutex);
		*err = lerr;
		return _filesystem_get_null_file(self->fs);
	}

	/* Increment HEAD to next element. */
	logger_next_name(self, head_file_name);

	/* Check if HEAD == TAIL. To do this, we only need to look at the sequencing bytes (first three bytes). */
	if( strncmp(tail_file_name, head_file_name, LOGGER_TOTAL_SEQUENCE_BYTES) == 0 ) {
		/* HEAD and TAIL are overlapping. */
		/* Since asynchronous file removals can happen, lets first update position of the TAIL and then see if */
		/* the HEAD and TAIL still overlap. */
		lerr = logger_update_tail(self);
		if( lerr != LOGGER_OK ) {
			unlock_mutex(*self->sync_mutex);
			*err = lerr;
			return _filesystem_get_null_file(self->fs);
		}

		/* Get new TAIL. */
		tail_file_name = logger_get_tail(self, &lerr);
		if( lerr != LOGGER_OK ) {
			unlock_mutex(*self->sync_mutex);
			*err = lerr;
			return _filesystem_get_null_file(self->fs);
		}

		/* Since get tail method refreshes cache - the cached head name is also refreshed so must recall next name method. */
		logger_next_name(self, head_file_name);

		if( strncmp(tail_file_name, head_file_name, LOGGER_TOTAL_SEQUENCE_BYTES) == 0 ) {
			/* HEAD and TAIL still overlap. Remove the TAIL so it can be replaced. */
			self->fs->delete(self->fs, tail_file_name);
			logger_next_name(self, tail_file_name);
			lerr = logger_set_tail(self, tail_file_name);
			if( lerr != LOGGER_OK ) {
				/* Failed to increment TAIL. */
				unlock_mutex(*self->sync_mutex);
				*err = lerr;
				return _filesystem_get_null_file(self->fs);
			}
		}
	}

	/* Insert at HEAD. */
	/* First check if we are inserting an empty file. */
	if( file_to_insert_name == NULL ) {
		/* Inserting an empty file, lets create it. */
		head_file_handle = self->fs->open(self->fs, &fs_err, head_file_name, FS_CREATE_ALWAYS, USE_POLLING);
	} else {
		/* Inserting the file given as a function argument. Lets process that string to avoid some errors. */
		strncpy(new_head_file_name, file_to_insert_name, FILESYSTEM_MAX_NAME_LENGTH);
		new_head_file_name[FILESYSTEM_MAX_NAME_LENGTH] = '\0';

		/* Now rename it so that the ring buffer can track it. */
		/* Due to corruption, a file by this name may exist already, remove it if one does. */
		if( self->fs->file_exists(self->fs, head_file_name) == FS_OK ) {
			self->fs->delete(self->fs, head_file_name);
		}
		fs_err = self->fs->rename(self->fs, new_head_file_name, head_file_name);
		if( fs_err != FS_OK ) {
			/* Failed to rename it, all we can do is abort. */
			unlock_mutex(*self->sync_mutex);
			*err = LOGGER_NVMEM_ERR;
			return _filesystem_get_null_file(self->fs);
		}

		/* Finally, lets open it. */
		head_file_handle = self->fs->open(self->fs, &fs_err, head_file_name, FS_OPEN_ALWAYS, USE_POLLING);
	}

	/* Check we opened the file without errors. */
	if( fs_err != FS_OK ) {
		/* Failed to open the file, all we can do is abort. */
		unlock_mutex(*self->sync_mutex);
		*err = LOGGER_NVMEM_ERR;
		return _filesystem_get_null_file(self->fs);
	}

	/* The file is open and named such that it can be the HEAD, so, lets make it so. */
	lerr = logger_set_head(self, head_file_name);
	if( lerr != LOGGER_OK ) {
		/* Failed to set HEAD. */
		head_file_handle->close(head_file_handle);
		unlock_mutex(*self->sync_mutex);
		*err = lerr;
		return _filesystem_get_null_file(self->fs);
	}

	/* Insert successful.. */
	unlock_mutex( *self->sync_mutex );
	*err = LOGGER_OK;
	return head_file_handle;
}

file_t* logger_peek_tail( logger_t* self, logger_error_t* err )
{
	DEV_ASSERT( self );
	DEV_ASSERT( err );

	file_t* 	tail_file_handle;
	char const* tail_file_name;
	fs_error_t	fs_err;

	lock_mutex( *self->sync_mutex, BLOCK_FOREVER );

	/* Get name of tail. */
	tail_file_name = logger_get_tail(self, err);
	if( *err != LOGGER_OK ) {
		unlock_mutex(*self->sync_mutex);
		return _filesystem_get_null_file(self->fs);
	}

	/* Open tail file. */
	tail_file_handle = self->fs->open(self->fs, &fs_err, tail_file_name, FS_OPEN_EXISTING, USE_POLLING);
	if( fs_err == FS_NO_FILE ) {
		/* Tail needs to be updated. */
		*err = logger_update_tail(self);
		if( *err != LOGGER_OK ) {
			unlock_mutex(*self->sync_mutex);
			return _filesystem_get_null_file(self->fs);
		}

		/* Updated tail, try opening the file again. */
		tail_file_name = logger_get_tail(self, err);
		if( *err != LOGGER_OK ) {
			unlock_mutex(*self->sync_mutex);
			return _filesystem_get_null_file(self->fs);
		}
		tail_file_handle = self->fs->open(self->fs, &fs_err, tail_file_name, FS_OPEN_ALWAYS, USE_POLLING);
	}
	if( fs_err != FS_OK ) {
		*err = LOGGER_NVMEM_ERR;
		unlock_mutex(*self->sync_mutex);
		return _filesystem_get_null_file(self->fs);
	}

	*err = LOGGER_OK;
	unlock_mutex( *self->sync_mutex );
	return tail_file_handle;
}

logger_error_t logger_pop( logger_t* self, char* popped_file_name )
{
	DEV_ASSERT( self );

	logger_error_t 	lerr;
	char*			tail_file_name;
	char const* 	head_file_name;
	fs_error_t		fs_err;

	lock_mutex( *self->sync_mutex, BLOCK_FOREVER );

	/* Get the TAIL file. */
	tail_file_name = logger_get_tail(self, &lerr);
	if( lerr != LOGGER_OK ) {
		unlock_mutex( *self->sync_mutex );
		return lerr;
	}

	/* Check if this file exists, if not, we have to update the TAIL. */
	fs_err = self->fs->file_exists(self->fs, tail_file_name);
	if( fs_err == FS_NO_FILE ) {
		/* File doesn't exist, so update TAIL. */
		lerr = logger_update_tail(self);
		if( lerr != LOGGER_OK ) {
			unlock_mutex( *self->sync_mutex );
			return lerr;
		}
		/* Get name of TAIL. */
		tail_file_name = logger_get_tail(self, &lerr);
		if( lerr != LOGGER_OK ) {
			unlock_mutex( *self->sync_mutex );
			return lerr;
		}
	} else if( fs_err != FS_OK ) {
		/* Failed to check for file existance. */
		unlock_mutex( *self->sync_mutex );
		return LOGGER_NVMEM_ERR;
	}

	/* Check if this is the HEAD file. If it is, don't touch it. */
	head_file_name = logger_get_head(self, &lerr);
	if( lerr != LOGGER_OK ) {
		unlock_mutex( *self->sync_mutex );
		return lerr;
	}
	if( strncmp(head_file_name, tail_file_name, FILESYSTEM_MAX_NAME_LENGTH) == 0 ) {
		/* HEAD == TAIL, don't untrack head.. */
		unlock_mutex( *self->sync_mutex );
		return LOGGER_EMPTY;
	}

	/* We're removing this file from the ring buffer tracking, so untrack the file. */
	/* This operation just renames it. */
	lerr = logger_untrack_file(self, tail_file_name);
	if( lerr != LOGGER_OK ) {
		/* Failed to untrack the file. */
		unlock_mutex( *self->sync_mutex );
		return lerr;
	}

	/* Got the file that is going to be removed, copy it into input buffer. */
	if( popped_file_name != NULL ) {
		/* Copy name of popped file into here. */
		strncpy(popped_file_name, tail_file_name, FILESYSTEM_MAX_NAME_LENGTH);
		popped_file_name[FILESYSTEM_MAX_NAME_LENGTH] = '\0';
	}

	/* Update the TAIL. */
	lerr = logger_update_tail(self);

	unlock_mutex( *self->sync_mutex );
	return lerr;
}


/********************************************************************************/
/* Constructor / Destructor Define												*/
/********************************************************************************/
static void destroy( logger_t *self )
{
	DEV_ASSERT( self );
}

logger_error_t initialize_logger
(
	logger_t *self,
	filesystem_t *filesystem,
	char const *control_file_name,
	char element_file_name,
	size_t max_capacity
)
{
	DEV_ASSERT( self );
	DEV_ASSERT( filesystem );
	DEV_ASSERT( control_file_name );

	/* Link virtual methods. */
	self->destroy = destroy;

	/* Initialize singleton mutex. */
	if( logger_is_init == false ) {
		new_mutex( logger_sync_mutex );
		if( logger_sync_mutex == NULL ) {
			return LOGGER_MUTEX_ERR;
		}
		logger_is_init = true;
		unlock_mutex( logger_sync_mutex );
	}

	/* Setup Member data. */
	self->fs = filesystem;
	self->sync_mutex = &logger_sync_mutex;
	self->element_file_name = element_file_name;
	if( max_capacity > LOGGER_MAX_CAPACITY || max_capacity < LOGGER_MIN_CAPCITY ) {
		return LOGGER_INV_CAP;
	}
	self->max_capacity = max_capacity;

	/* Copy control file name into logger instance. */
	strncpy( self->control_file_name, control_file_name, FILESYSTEM_MAX_NAME_LENGTH );
	self->control_file_name[FILESYSTEM_MAX_NAME_LENGTH] = '\0'; /* Fail safe. */

	/* Cache control data within the control data file. */
	return logger_cache_control_data(self);
}
