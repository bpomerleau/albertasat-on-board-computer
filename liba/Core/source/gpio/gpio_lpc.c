/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file gpio_lpc.c
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */

#include <gpio/gpio_lpc.h>
#include <chip.h>
#include <portable_types.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static void set_state( gpio_t* self_, gpio_state_t state )
{
	gpio_lpc_t* self = (gpio_lpc_t*) self_;
	DEV_ASSERT( self );

	/* Convert gpio_t state to the lpc's representation of state. */
	if( state == GPIO_LOW )
	{
		Chip_GPIO_SetPinState( LPC_GPIO, self->_.port, self->_.pin, false );
	}
	else if( state == GPIO_HIGH )
	{
		Chip_GPIO_SetPinState( LPC_GPIO, self->_.port, self->_.pin, true );
	}
}

static gpio_state_t get_state( gpio_t* self_ )
{
	gpio_lpc_t* self = (gpio_lpc_t*) self_;
	DEV_ASSERT( self );

	bool lpc_state;

	lpc_state = Chip_GPIO_GetPinState( LPC_GPIO, self->_.port, self->_.pin );

	/* Convert lpc's state to gpio_t state. */
	if( lpc_state == true )
	{
		return GPIO_HIGH;
	}
	return GPIO_LOW;
}

static void set_direction( gpio_t* self_, gpio_direction_t dir )
{
	gpio_lpc_t* self = (gpio_lpc_t*) self_;
	DEV_ASSERT( self );

	/* Convert from gpio_t direction to lpc's representation of direction. */
	if( dir == GPIO_OUTPUT )
	{
		Chip_GPIO_SetPinDIROutput( LPC_GPIO, self->_.port, self->_.pin );
	}
	else if( dir == GPIO_INPUT )
	{
		Chip_GPIO_SetPinDIRInput( LPC_GPIO, self->_.port, self->_.pin );
	}
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void initialize_gpio_lpc( gpio_lpc_t* self, uint8_t port, uint8_t pin, gpio_wire_t wire )
{
	DEV_ASSERT( self );
	static bool_t is_init = false;
	uint32_t lpc_wire;

	extern void initialize_gpio_( gpio_t* );
	initialize_gpio_( (gpio_t*) self );

	((gpio_t*) self)->set_state = set_state;
	((gpio_t*) self)->set_direction = set_direction;
	((gpio_t*) self)->get_state = get_state;

	/* Initialize GPIO hardware only once. */
	if( is_init == false )
	{
		Chip_GPIO_Init( LPC_GPIO );
		is_init = true;
	}

	self->_.port = port;
	self->_.pin = pin;

	/* Convert to lpc's represention of gpio_wire_t. */
	if		( wire == GPIO_PULLUP )		{ lpc_wire = IOCON_MODE_PULLUP; }
	else if	( wire == GPIO_PULLDOWN )	{ lpc_wire = IOCON_MODE_PULLDOWN; }
	else if	( wire == GPIO_REPEATER )	{ lpc_wire = IOCON_MODE_REPEATER; }
	else								{ lpc_wire = IOCON_MODE_INACT; }

	Chip_IOCON_PinMuxSet( LPC_IOCON, port, pin, lpc_wire | IOCON_FUNC0 );
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


