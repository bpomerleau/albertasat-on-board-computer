/*
 * mnlp.c
 *
 *  Created on: 22-Jun-2015
 *      Author: Atri Bhattacharyya
 */

#ifdef LPC1769

#include "LPC17xx.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"

#include <stdio.h>

/****************************************************************/
/*  mNLP defines												*/
/****************************************************************/

#ifdef DEBUG
	#define INIT_WARNING( script_index , ...)	{ printf(__VA_ARGS__); script_slot[script_index].status.script_error = 1; continue;}
	#define INIT_ERROR( error_code, ...)	{ printf(__VA_ARGS__);  goto init_end;}
	#define PRINT_DEBUG_STATEMENT(...)				 printf(__VA_ARGS__);
#else
	#define INIT_WARNING( warning_print_statement , script_index )	{ script_slot[script_index].status.script_error = 1; continue;}
	#define INIT_ERROR( error_code, error_print_statement)	{  goto init_end;}
	#define PRINT_DEBUG_STATEMENT(...)
#endif

#define INITIALIZE_FILESYSTEM(x) 	initialize_filesystem_lpc_fatfs(x)

#define USART_PUTSTR(...)			usart_putstr(__VA_ARGS__)

/****************************************************************/
/*  Support functions for LPC1769								*/
/****************************************************************/
//Remove from here. These should be done elsewhere, along with other initializations


void usart_callback();
void usart_putstr(uint8_t *buf, uint8_t len);

/*
 * UART Stuff
 * Only uses UART3. Have to modify to allow all UART ports
 */

uint8_t buffer[1024];

void LPC_UART3_Init()
{

	UART_CFG_Type UART3_cfg;
	PINSEL_CFG_Type pincfg;

	//Initialize UART module
	UART_ConfigStructInit(&UART3_cfg);
	UART_Init(LPC_UART3, &UART3_cfg);

	//Set pin modes
	pincfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pincfg.Pinmode = PINSEL_PINMODE_PULLUP;
	pincfg.Portnum = PINSEL_PORT_0;
	pincfg.Pinnum = PINSEL_PIN_0;
	pincfg.Funcnum = PINSEL_FUNC_2;
	PINSEL_ConfigPin(&pincfg);
	pincfg.Pinnum++;
	PINSEL_ConfigPin(&pincfg);

	//Configure interrupts
	UART_IntConfig(LPC_UART3,UART_INTCFG_RBR, ENABLE);
	UART_IntConfig(LPC_UART3, UART_INTCFG_THRE, DISABLE);
	UART_IntConfig(LPC_UART3, UART_INTCFG_RLS, ENABLE);

	//Setup NVIC
	NVIC_SetPriority(UART3_IRQn, configMAX_SYSCALL_INTERRUPT_PRIORITY + 1);
	NVIC_EnableIRQ(UART3_IRQn);

	//Setup fifo
	UART_FIFO_CFG_Type FIFOCfg;
	FIFOCfg.FIFO_ResetRxBuf = ENABLE;
	FIFOCfg.FIFO_ResetTxBuf = ENABLE;
	FIFOCfg.FIFO_DMAMode = DISABLE;
	FIFOCfg.FIFO_Level = UART_FIFO_TRGLEV1;
	UART_FIFOConfig(LPC_UART3, &FIFOCfg);

	//Enable tx
	UART_TxCmd(LPC_UART3, ENABLE);
}

void UART3_IRQHandler(void)
{
	uint32_t IIRValue, LSRValue;
	uint32_t Dummy = Dummy;

	IIRValue = UART_GetIntId(LPC_UART3);

	if ( !(IIRValue & UART_IIR_INTSTAT_PEND) ) /* Receive Line Status */
	{
		if( (IIRValue & UART_IIR_INTID_MASK) == UART_IIR_INTID_RLS)
		{
			LSRValue = UART_GetLineStatus(LPC_UART3);
			/* Receive Line Status */
			if ( LSRValue & (UART_LSR_OE|UART_LSR_PE|UART_LSR_FE|UART_LSR_RXFE|UART_LSR_BI) )
			{
				/* There are errors or break interrupt */
				/* Ignore RBR characters - transport layer need not be reliable */
				return;
			}
			if ( LSRValue & UART_LSR_RDR ) /* Receive Data Ready */
			{
				/* If no error on RLS, normal ready, save into the data buffer. */
				/* Note: read RBR will clear the interrupt */
				usart_callback();
			}
		}
		else if ( (IIRValue & UART_IIR_INTID_MASK) == UART_IIR_INTID_RDA ) /* Receive Data Available */
		{
			//add FIFO to buffer
			usart_callback();
		}
		else if (  (IIRValue & UART_IIR_INTID_MASK) == UART_IIR_INTID_CTI ) /* Character timeout indicator */
		{
			/* Character Time-out indicator */
			//Not applicable for FIFO trigger of 1
			//Still read char from RBR in case FIFO trigger is changed
			usart_callback();
		}
	}
}

void usart_callback()
{
	int i=0;
	while(UART_GetLineStatus(LPC_UART3) & UART_LSR_RDR)
	{
		buffer[i++] = UART_ReceiveByte(LPC_UART3);
	}
	mnlp_usart_callback(buffer, i, NULL);
}

void usart_putstr(uint8_t *buf, uint8_t len)
{
	int i;
	for(i = 0; i < len; i++)
	{
		while(!(UART_GetLineStatus(LPC_UART3) & UART_LSR_THRE));		//Wait for FIFO to have empty spot before writing next character
		UART_SendByte(LPC_UART3, buf[i]);
	}
}
/******************************************************************************/

#endif
