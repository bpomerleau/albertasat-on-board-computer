/*
 * mnlp_nanomind.c
 *
 *  Created on: 09-Jul-2015
 *      Author: Atri Bhattacharyya
 */

#ifndef LPC1769

#include "dev/usart.h"
#include "core_defines.h"

/****************************************************************/
/*  mNLP defines												*/
/****************************************************************/

//#ifdef DEBUG
	#define INIT_WARNING( script_index , ...)	{ csp_printf_crit(__VA_ARGS__); script_slot[script_index].status.script_error = 1; continue;}
	#define INIT_ERROR( error_code, ...)	{ csp_printf_crit(__VA_ARGS__);  goto init_end;}
	#define PRINT_DEBUG_STATEMENT(...)				 csp_printf(__VA_ARGS__);
/*#else
	#define INIT_WARNING( script_index , ...)	{ script_slot[script_index].status.script_error = 1; continue;}
	#define INIT_ERROR( error_print_statement , ...)	{  goto init_end;}
	#define PRINT_DEBUG_STATEMENT(...)
#endif*/

#define INITIALIZE_FILESYSTEM(x) 	initialize_filesystem_nanomind_fatfs(x)

#define USART_PUTSTR(...)			usart_putstr(MNLP_USART_HANDLE, __VA_ARGS__)

#endif
