/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file mnlp_external.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <mnlp/mnlp_external.h>
#include <mnlp.h>
#include <printing.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t power( mnlp_t* self_, hub_t* eps, bool_t state )
{
	mnlp_external_t* self = (mnlp_external_t*) self_;
	DEV_ASSERT( self );
	bool_t result;

	result = self->_.supers_power( (mnlp_t*) self, eps, state );
	if( !result )
	{
		// Failed to change power.
		return result;
	}
	return result;
}

static bool_t start_stop(mnlp_t *self_, bool_t state)
{
	mnlp_external_t* self = (mnlp_external_t*) self_;
	DEV_ASSERT( self );
	bool_t result;

	result = self->_.supers_start_stop( (mnlp_t*) self, state);
	return result;
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
void initialize_mnlp_external( mnlp_external_t* self )
{
	DEV_ASSERT( self );

	/* Initialize super class. */
	extern void initialize_mnlp_( mnlp_t* );
	initialize_mnlp_( (mnlp_t*) self );

	self->_.supers_power = ((mnlp_t*) self)->power;
	self->_.supers_start_stop = ((mnlp_t*) self)->start_stop;
	((mnlp_t*) self)->power = power;
	((mnlp_t*) self)->start_stop = start_stop;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


