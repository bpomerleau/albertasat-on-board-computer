/*
 * mnlp_common.c
 *
 *  Created on: 15-Jul-2015
 *      Author: Atri Bhattacharyya
 */

#include "mnlp.h"
#include <string.h>
#include "rtc.h"
#include "driver_toolkit/driver_toolkit_nanomind.h"
#include <portable_types.h>
#include <printing.h>

#ifdef LPC1769
#include "mnlp_lpc.h"
#else
#include "mnlp_nanomind.h"
#include "dev/usart.h"
#include "dev/arm/ds1302.h"
#include "dev/arm/at91sam7.h"
#endif

/****************************************************************/
/*  Private Function prototypes									*/
/****************************************************************/

uint8_t mnlp_best_script( void);
int mnlp_best_seq( void);
void mnlp_rx_data();
void mnlp_exec_cmd(mnlp_seq_cmd_t cmd, uint8_t *param);
void mnlp_task_wait(uint8_t min, uint8_t sec);
void mnlpTask( void *pvParameters);
uint32_t mnlp_gettime();
int mnlp_is_eot(mnlp_times_table_t entry);
void fletcher_checksum_update(uint8_t *buf, uint32_t len);
int fletcher_checksum_correct();
void create_output_filename(char *name, uint8_t rsp_id, uint32_t time);
void create_script_filename(char *name, int script_num);
void create_seq_filename(char *name, int script_num, int seq_num);

/****************************************************************/
/*  mNLP globals												*/
/****************************************************************/

mnlp_script_t script_slot[7];				//Several slots for scripts - QB50 specs
filesystem_t *filesys;						//File system var.
file_t *data_file;						//File for output of mNLP data
task_t cmd_tx_task, pkt_rx_task;			//Task handles for mnlpTask and mnlp_rx_data tasks
volatile uint8_t mnlp_error_detected = 0;	//When mNLP responds with SU_ERR packet, this variable is set
volatile uint8_t mnlp_is_on = 0;			//mNLP status - knows when mNLP is on or off based on mNLP power commands sent
uint8_t run_script = 0;						//Identified currently running script
uint16_t run_tt = 0;						//Identified currently running times table entry
static uint16_t mnlp_timeout = 0;

uint16_t c0_int;							//Variable used to checksum script
uint16_t c1_int;							//Variable used to checksum script
uint16_t xsum_w;							//Variable used to checksum script



/****************************************************************/
/*  Local defines												*/
/****************************************************************/
//Units for angular values for ADCS and MNLP data
#define ADCS_ANGLE_UNIT		0.01	//degrees
#define MNLP_ANGLE_UNIT 	2		//degrees
//Units for position values for ADCS and MNLP data
#define ADCS_POSITION_UNIT	0.25	//kilometers
#define MNLP_POSITION_UNIT	5		//kilometers

#define SCRIPT_ENTRY_IS_VALID(x) 	((x.script_index == SCPT_SEQ_FIVE || x.script_index == SCPT_SEQ_FOUR || x.script_index == SCPT_SEQ_THREE || x.script_index == SCPT_SEQ_TWO || x.script_index == SCPT_SEQ_ONE || x.script_index == END_OF_TABLE) && x.time_seconds < 60 && x.time_minutes < 60 && x.time_hours < 60)
#define CMD_HEADER_IS_VALID(x)		(x.delSec < 60 && x.delMin < 60 && 						\
(	x.CMD_ID == OBC_SU_ON 	|| x.CMD_ID == OBC_SU_OFF 	|| x.CMD_ID == SU_RESET 	|| x.CMD_ID == SU_LDP || x.CMD_ID == SU_HC		\
|| 	x.CMD_ID == SU_CAL 		|| x.CMD_ID == SU_SCI		|| x.CMD_ID == SU_HK 		|| x.CMD_ID == SU_STM							\
|| 	x.CMD_ID == SU_DUMP 	|| x.CMD_ID == SU_BIAS_ON 	|| x.CMD_ID == SU_BIAS_OFF 	|| x.CMD_ID == SU_MTEE_ON						\
|| 	x.CMD_ID == SU_MTEE_OFF	|| x.CMD_ID == SU_ERR 		|| x.CMD_ID == OBC_SU_ERR 	|| x.CMD_ID == OBC_EOT))

/****************************************************************/
/*  External variables											*/
/****************************************************************/
extern driver_toolkit_nanomind_t drivers;
static driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;

xSemaphoreHandle mnlp_go;
xSemaphoreHandle mnlp_nogo;


/****************************************************************/
/*  mNLP Public functions										*/
/****************************************************************/
bool_t mnlp_start(void)
{
	if(take_semaphore( mnlp_go, 0) == SEMAPHORE_ACQUIRED)
	{
		xTaskCreate(mnlp_init,(const signed char *) "mnlp init", 1024, NULL, 1, NULL);
		return true;
	}
	else
	{
		csp_printf("MNLP_go semaphore unavailable");
		return false;
	}
	return false;
}

bool_t mnlp_stop(void)
{
	if(take_semaphore(mnlp_nogo, 0) == SEMAPHORE_ACQUIRED)
	{
		xTaskCreate(mnlp_deinit,(const signed char *) "mnlp deinit", 1024, NULL, 1, NULL);
		return true;
	}
	else
	{
		csp_printf("MNLP_stop semaphore unavailable");
		return false;
	}
	return false;
}

void mnlp_preinit(void)
{
	vSemaphoreCreateBinary(mnlp_go);
	vSemaphoreCreateBinary(mnlp_nogo);
	give_semaphore(mnlp_go);
	if(take_semaphore(mnlp_nogo ,0) != SEMAPHORE_ACQUIRED)
	{
		csp_printf_crit("MNLP_nogo semaphore creation failure.");
	}
}

/****************************************************************/
/*  mNLP Private functions										*/
/****************************************************************/
/**
 * @brief
 * 	Initialize and setup function for mNLP
 *
 * @details
 * 	Reads the scripts, stores headers.
 * 	It stores the first times-table entry and points the file pointer to the next entry.
 * 	Stores the script sequences for all scripts in unique files
 * 	Creates the script running task and data storing task before deleting itself
 */
void mnlp_init(void *pvParameters) {

	kit = (driver_toolkit_t*) &drivers;
	filesys = kit->fs;

	int i;						//Loop variable
	uint32_t br;				//Bytes read - used multiple times in different reads
	uint32_t bw;				//Bytes written - used multiple times in different writes
	uint8_t num_seq;			//Number of different script sequences
	fs_error_t error;
	file_t *seq_file;		//File handle used to store sequences to different files

	//Check for filesystem init errors, and uncomment following code if error
	//INITIALIZE_FILESYSTEM(filesys);
//**************************FILESYSTEM INIT ERROR CODE*************************
//	{
//		//All scripts are erroneous
//		for(i=0;i<7;i++)
//			script_slot[i].status.script_error = 1;
//		INIT_ERROR(FILESYSTEM_NOT_OPENED, "Error mounting file system");
//	}
//******************************************************************************

	for(i=0;i<7;i++)
	{
		//Fletcher-16 var inits
		c0_int = 0x0000;
		c1_int = 0x0000;
		xsum_w = 0xFFFF;

		//Create filename
		create_script_filename(script_slot[i].script_file_name, i);

		//File opening
		script_slot[i].fp = filesys->open( filesys, &error, script_slot[i].script_file_name, FS_OPEN_EXISTING, BLOCK_FOREVER);
		if(error != FS_OK)
		{
			INIT_WARNING(i, "Opening file for script %d failed", i);
			script_slot[i].status.script_error = 1;
		}
		else
		{
			//If file opened successfully, read the header
			if(script_slot[i].fp->read(script_slot[i].fp, (void *)&script_slot[i].raw_header, 12, &br) != FS_OK || br != 12)
			{
				filesys->close(filesys, script_slot[i].fp);
				INIT_WARNING(i, "Unable to read header for script %d", i);
			}
			fletcher_checksum_update((void *)(&script_slot[i].raw_header), br);
		}
		// Read correct length and start time into header struct
		script_slot[i].script_hdr.script_length = (script_slot[i].raw_header[1] << 8) + script_slot[i].raw_header[0];
		script_slot[i].script_hdr.start_time = (script_slot[i].raw_header[5]<<24) + (script_slot[i].raw_header[4] <<16) + (script_slot[i].raw_header[3] <<8) + script_slot[i].raw_header[2];
		script_slot[i].status.done_today = 0;

		//csp_printf("Start time: %x, %x", script_slot[i].script_hdr.start_time, script_slot[i].script_hdr.script_length);

		if(script_slot[i].script_hdr.script_length != script_slot[i].fp->size(script_slot[i].fp, &error))
		{
			filesys->close(filesys, script_slot[i].fp);
			INIT_WARNING(i, "Size error for script %d",i);
		}

		//Load first times table entry into script struct
		mnlp_times_table_t tt_entry;
		uint8_t num_tt_entries = 0;
		if(script_slot[i].fp->read(script_slot[i].fp, (void *)&tt_entry, sizeof(mnlp_times_table_t), &br) != FS_OK || br != sizeof(mnlp_times_table_t))
		{
			filesys->close(filesys, script_slot[i].fp);
			INIT_WARNING(i, "Error reading first times table entry for script %d", i);
		}
		fletcher_checksum_update((uint8_t *)&tt_entry, br);
		if(mnlp_is_eot(tt_entry))
		{
			filesys->close(filesys, script_slot[i].fp);
			INIT_WARNING(i, "Error: first time table entry in script %d is EOT", i);
		}
		//Check and parse through remaining times table entries
		else
		{
			script_slot[i].next_seq[num_tt_entries] = tt_entry;
			num_tt_entries++;

			//csp_printf("TT entry: %x, %x, %x, %x",tt_entry.time_seconds,tt_entry.time_minutes,tt_entry.time_hours,tt_entry.script_index);

			do
			{
				if(script_slot[i].fp->read(script_slot[i].fp, (void *)&tt_entry, sizeof(mnlp_times_table_t), &br) != FS_OK || br != sizeof(mnlp_times_table_t))	//Parse over the remaining table
				{
					filesys->close(filesys, script_slot[i].fp);
					INIT_WARNING(i, "Error in reading times table for script %d", i);
				}
				if(!SCRIPT_ENTRY_IS_VALID(tt_entry))
				{
					filesys->close(filesys, script_slot[i].fp);
					INIT_WARNING(i, "Invalid script index in script %d", i);
				}
				fletcher_checksum_update((uint8_t *)&tt_entry, br);
				//Save TT entry in script struct and add one to number of entries
				script_slot[i].next_seq[num_tt_entries] = tt_entry;
				num_tt_entries++;

				//csp_printf("TT entry: %x, %x, %x, %x",tt_entry.time_seconds,tt_entry.time_minutes,tt_entry.time_hours,tt_entry.script_index);

			}while(!mnlp_is_eot(tt_entry) && !script_slot[i].status.script_error);
			if(script_slot[i].status.script_error)
				continue;

			script_slot[i].info.num_tt_entries = num_tt_entries;
		}

		//Store script sequences in different file - s1seq1.bin, s1seq2.bin, s2seq1.bin, etc.
		num_seq = 0;
		mnlp_seq_cmd_t ss_cmd;
		uint8_t *buf;
		char script_seq_filename[13];

		do
		{
			uint8_t seq_cnt = 1;	// Command sequence starts at 1, not 0.
			//Read each script sequence and keep storing it in appropriate files
			create_seq_filename(script_seq_filename, i, num_seq);
			seq_file = filesys->open(filesys, &error, script_seq_filename , FS_CREATE_ALWAYS, BLOCK_FOREVER);
			if(error != FS_OK)
			{
				filesys->close(filesys, seq_file);
				filesys->close(filesys, script_slot[i].fp);
				INIT_WARNING(i, "Unable to open sequence file for script %d sequence %d", i, num_seq);
			}
			else
			{
				do
				{
					//Read command header
					if(script_slot[i].fp->read(script_slot[i].fp, (void *)&ss_cmd, sizeof(mnlp_seq_cmd_t), &br) != FS_OK || br != sizeof(mnlp_seq_cmd_t))
					{
						filesys->close(filesys, seq_file);
						filesys->close(filesys, script_slot[i].fp);
						INIT_WARNING(i, "Size error in command sequence for script %d", i);
					}

					fletcher_checksum_update((uint8_t *)&ss_cmd, br);
					if((ss_cmd.CMD_ID == 0xFE) && (ss_cmd.SEQ_CNT != seq_cnt))
					{
						csp_printf("Empty sequence found.");
					}
					else if((ss_cmd.CMD_ID == 0xF2) && (ss_cmd.SEQ_CNT != seq_cnt))
					{
						csp_printf("Incorrect sequence at end.");
					}
					else if(ss_cmd.SEQ_CNT != seq_cnt)
					{
						filesys->close(filesys, seq_file);
						filesys->close(filesys, script_slot[i].fp);
						INIT_WARNING(i, "Error in sequence command count at count %d in sequence %d in script %d", seq_cnt, num_seq, i);
					}
					if(!CMD_HEADER_IS_VALID(ss_cmd))
					{
						filesys->close(filesys, seq_file);
						filesys->close(filesys, script_slot[i].fp);
						INIT_WARNING(i, "Error in header parameters in script %d sequence %d", i, num_seq);
					}

					if(seq_file->write(seq_file, (void *)&ss_cmd, sizeof(mnlp_seq_cmd_t), &bw) != FS_OK || bw != sizeof(mnlp_seq_cmd_t))
					{
						filesys->close(filesys, seq_file);
						filesys->close(filesys, script_slot[i].fp);
						INIT_WARNING(i, "File write failed or volume full for script %d sequence %d", i, num_seq);
					}
					//If parameters exist
					if(ss_cmd.LEN > 1)
					{
						buf = pvPortMalloc(ss_cmd.LEN - 1);
						//Read parameters
						if(script_slot[i].fp->read(script_slot[i].fp, buf, ss_cmd.LEN - 1, &br) != FS_OK || br != (ss_cmd.LEN - 1))
						{
							filesys->close(filesys, seq_file);
							filesys->close(filesys, script_slot[i].fp);
							INIT_WARNING(i, "File ends before parameters end for script %d", i);
						}
						fletcher_checksum_update(buf, br);

						if(seq_file->write(seq_file, buf, ss_cmd.LEN - 1, &bw) != FS_OK || bw != (ss_cmd.LEN - 1))
						{
							filesys->close(filesys, seq_file);
							filesys->close(filesys, script_slot[i].fp);
							INIT_ERROR(VOLUME_FULL, "Parameter write failed or volume full");
						}
						vPortFree(buf);
					}
					seq_cnt++;

					//csp_printf("CMD: %x, %x, %x, %x",ss_cmd.delSec,ss_cmd.delMin,ss_cmd.CMD_ID,ss_cmd.SEQ_CNT);

				}while(ss_cmd.CMD_ID != OBC_EOT && !script_slot[i].status.script_error);
				filesys->close(filesys, seq_file);
				if(script_slot[i].status.script_error)
				{
					filesys->close(filesys, seq_file);
					filesys->close(filesys, script_slot[i].fp);
					continue;
				}
			}
			num_seq++;

		}while( script_slot[i].fp->position(script_slot[i].fp, &error) != (script_slot[i].script_hdr.script_length - sizeof(script_slot[i].xsum) ) && !script_slot[i].status.script_error); //Continue until end of file minus size of xsum
		if(script_slot[i].status.script_error)
		{
			filesys->close(filesys, script_slot[i].fp);
			continue;
		}

		script_slot[i].info.num_seq = num_seq;
		if(script_slot[i].fp->read(script_slot[i].fp,  (void *)&(script_slot[i].xsum), sizeof(script_slot[i].xsum), &br) != FS_OK || br != sizeof(script_slot[i].xsum))
		{
			filesys->close(filesys, script_slot[i].fp);
			INIT_WARNING(i, "Unable to read XSUM for script %d", i);
		}
		fletcher_checksum_update((uint8_t *)(&script_slot[i].xsum), br);

		if(!fletcher_checksum_correct())
		{
			filesys->close(filesys, script_slot[i].fp);
			INIT_WARNING(i, "Checksum error for script %d", i);
		}
		filesys->close(filesys, script_slot[i].fp);
	}

init_end:
PRINT_DEBUG_STATEMENT("Init finished - generating tasks");
	//Create the task that checks and runs scripts at appropriate times
	create_task(mnlpTask, "mNLP task", 1024, NULL, BASE_PRIORITY + 1, &cmd_tx_task );
	create_task(mnlp_rx_data, "mnlp Rx task", 2*1024, NULL, BASE_PRIORITY + 2, &pkt_rx_task);	//Processing task should have high priority
	give_semaphore(mnlp_nogo);
	delete_task(NO_HANDLE);

}

/**
 * @brief
 * 	De-initializes the mNLP and all associated tasks
 *
 * @details
 * 	Kills all running mNLP tasks and switches off the mNLP.
 * 	Also clears all allocated memory.
 */
void mnlp_deinit( void *pvParameters)
{
	int i;

	//Delete running mNLP tasks
	delete_task(cmd_tx_task);
	delete_task(pkt_rx_task);

	//Close all open files
	//if(filesys->close_all(filesys) != FS_OK)
	//	PRINT_DEBUG_STATEMENT("Closing files failed in de-init"); //OMG - memory leak with open file handles.

	for(i = 0; i < 7; i++)
	{
		//Set status bits appropriately
		script_slot[i].status.done_today = 0;
		script_slot[i].status.script_error = 0;
	}

	//Switch off mNLP
	mnlp_seq_cmd_t shut_down_cmd;
	shut_down_cmd.CMD_ID = OBC_SU_OFF;
	shut_down_cmd.LEN = 1;
	shut_down_cmd.SEQ_CNT = 0;
	shut_down_cmd.delMin = 0;
	shut_down_cmd.delSec = 0;
	mnlp_exec_cmd(shut_down_cmd, NULL);

	PRINT_DEBUG_STATEMENT("mNLP Deinitialized");
	//De init done. Now task deletes itself
	give_semaphore(mnlp_go);
	delete_task( NO_HANDLE );
}

/**
* @brief
*	Used to handle mNLP errors
*	Basically switch mNLP off and wait 60 seconds
*/
void handle_mnlp_error()
{
	//Switch off mNLP
	mnlp_seq_cmd_t shut_down_cmd;
	shut_down_cmd.CMD_ID = OBC_SU_OFF;
	shut_down_cmd.LEN = 1;
	shut_down_cmd.SEQ_CNT = 0;
	shut_down_cmd.delMin = 0;
	shut_down_cmd.delSec = 0;
	mnlp_exec_cmd(shut_down_cmd, NULL);

	//Wait 60 seconds
	mnlp_task_wait(1, 0);
}


/**
* @brief
*	Generates OBC error packet according to QB50 specs
*
* @param response_pkt
* 	Response packet to be filled with data
*/
void generate_obc_su_err_pkt(mnlp_response_pkt_t *response_pkt)
{
	int i;
	mnlp_obc_err_pkt_data *err_pkt;

	memset(response_pkt, 0, DATA_PACKET_SIZE);

	response_pkt->rsp_id = OBC_SU_ERR;
	err_pkt = (mnlp_obc_err_pkt_data *) response_pkt->data;
	err_pkt->rsp_err_code = RX_TIMEOUT_ERR;

	//Running script details
	err_pkt->XSUM_R = script_slot[run_script].xsum;
	memcpy(&(err_pkt->hdr_R), &(script_slot[run_script].script_hdr.start_time), sizeof(err_pkt->hdr_R));

	//All script details
	for(i=0;i<7;i++)
	{
		err_pkt->script_info[i].XSUM_n = script_slot[i].xsum;
		memcpy(&(err_pkt->script_info[i].hdr_n), &(script_slot[i].script_hdr.start_time), sizeof(err_pkt->script_info[i].hdr_n));
	}
}


/**
* @brief
*	Computes incremental Fletcher checksum for the next few bytes of the script
*
* @param buf
* 	Pointer to latest bytes read from script file
*
* @param len
* 	Number of bytes read from script file
*/
void fletcher_checksum_update(uint8_t *buf, uint32_t len)
{
	unsigned int i;
	for(i = 0; i < len; i++ )
	{
		c0_int += buf[i];
		c1_int += c0_int;
		c0_int %= 0xFF;
		c1_int %= 0xFF;
	}
}

/**
* @brief
*	Finally checks for checksum correctness for the script
*
* @return
* 	If script has correct checksum returns 1, else 0
*/
int fletcher_checksum_correct()
{
	xsum_w = c0_int + (c1_int << 8);
	return (xsum_w == 0);
}

/**
* @brief
*	Checks whether the last read times-table entry was the end-of-table
*
* @param entry
* 	Times table entry from the mNLP script
*
* @return
* 	If entry is EOT entry, returns 1, else 0
*/
int mnlp_is_eot(mnlp_times_table_t entry)
{
	//EOT entry now has seconds, minutes, hours fields
	//Return result of the equality
	return (entry.script_index == END_OF_TABLE);
}

/**
* @brief
* 	Decides which script should run. Updates run_script global variable.
*
* @details
* 	Checks start time of all scripts and selects the most
* 	recent start time but not greater than the current time.
*
* @return
* 	Returns the script slot number to run.
*/

uint8_t mnlp_best_script()
{
	uint32_t most_recent = 7; // 7 is an invalid index. Handler will do nothing with this.
	uint32_t time_now = mnlp_gettime();
	uint32_t best_time = 31536000; // Best script starts at a year old.
	for(uint8_t i=0;i<7;i++)
	{
		//csp_printf("Script %x, Start: %x Now: %x",i,script_slot[i].script_hdr.start_time,time_now);
		if(script_slot[i].script_hdr.start_time <= time_now && script_slot[i].status.script_error != 1)
		{
			if((time_now - script_slot[i].script_hdr.start_time) <= best_time)
			{
				best_time = time_now - script_slot[i].script_hdr.start_time;
				//csp_printf("Best Time: %d", best_time);
				most_recent = i;
			}
		}
	}
	if(most_recent == 7) // If for loop completes without finding one, either none are ready yet or all have
	{						// run and should be reset.
		script_slot[0].status.done_today = 0;
		script_slot[1].status.done_today = 0;
		script_slot[2].status.done_today = 0;
		script_slot[3].status.done_today = 0;
		script_slot[4].status.done_today = 0;
		script_slot[5].status.done_today = 0;
		script_slot[6].status.done_today = 0;
	}
	run_script = most_recent;
	return most_recent;
}

/**
* @brief
* 	Decides the best sequence that should run out of the current script times table.
*
* @details
* 	Iterates through all times table entries until entry with time greater than current time is found.
* 	The entry before that entry will run.
*
* @return
* 	Returns index of sequence for times table.
*/
int mnlp_best_seq()
{
	uint32_t time_now = mnlp_gettime() % 86400;
	for(uint16_t i=0;i<1024;i++) // Max number of times table entries is 1024.
	{
		uint32_t start_time = 3600 * script_slot[run_script].next_seq[i].time_hours + 60 * script_slot[run_script].next_seq[i].time_minutes + script_slot[run_script].next_seq[i].time_seconds;

		if(script_slot[run_script].next_seq[i].script_index == END_OF_TABLE)
		{
			script_slot[run_script].status.done_today = 1;
			//csp_printf("Script %d is done today.", run_script);
			run_tt = i;
			return run_tt;
		}

		if((time_now >= start_time) && ((time_now - start_time) < 10))
		{
			run_tt = i;
			return run_tt;
		}
		else
		{

		}
	}
	return -1; // Return first entry if loop fails
}

/**
* @brief
* 	Task delay for the mNLP
*
* @param min
* 	Minutes to wait
*
* @param sec
* 	Seconds to wait
*/
void mnlp_task_wait(uint8_t min, uint8_t sec)
{
	task_delay(min*ONE_MINUTE + sec*ONE_SECOND);
	//task_delay(min*2*ONE_SECOND + sec*ONE_SECOND);
}



/**
* @brief
* 	Process and store received data packets from mNLP
*
* @details
* 	This task processes data packets received from the mNLP
* 	It must add the relevant headers from ADCS and time data
* 	and save packets to date-tagged files on mass memory.
*/
void mnlp_rx_data()
{
	FILE* fid;
	FILE* fid2;
	mnlp_science_hdr_t data_hdr;					//Data packet header - to be attached before all packets
	mnlp_response_pkt_t *response_pkt;				//Packet containing received data
	char mnlp_raw[DATA_PACKET_SIZE];
	uint8_t res_seq_cnt[9] = {0,0,0,0,0,0,0,0,0};	//Sequence count for each type of packet
	char data_file_name[13];						//File name for data write to file
	char sd_file_name[17];
	char uffs_file_name[19];
	mnlp_timeout = 0;

	while(1)
	{
		//Check if mNLP is on
		if(mnlp_is_on == 1)
		{
			/*if(usart_messages_waiting(MNLP_USART_HANDLE) > 0)
			{
				csp_printf("Waiting: %d", usart_messages_waiting(MNLP_USART_HANDLE));
			}*/
			mnlp_timeout++;
			if(mnlp_timeout >= (400*10)) // should have defines but it doesn't. Task runs every 100 ms.
			{
				//Generate OBC_error packet
				response_pkt = (mnlp_response_pkt_t *)pvPortMalloc(sizeof(mnlp_response_pkt_t));

				//Put error data
				generate_obc_su_err_pkt(response_pkt);
			}
			else if(usart_messages_waiting(MNLP_USART_HANDLE) >= DATA_PACKET_SIZE)
			{
				csp_printf("\nMnLP messages waiting: %d\nMnLP packets waiting: %d", usart_messages_waiting(MNLP_USART_HANDLE), usart_messages_waiting(MNLP_USART_HANDLE)/DATA_PACKET_SIZE);
				memset(mnlp_raw, 0, DATA_PACKET_SIZE);
				for(int i=0;i<DATA_PACKET_SIZE;i++)
				{
					mnlp_raw[i] = usart_getc(MNLP_USART_HANDLE);
				}
				response_pkt = (mnlp_response_pkt_t *)pvPortMalloc(sizeof(mnlp_response_pkt_t));
				memcpy(response_pkt, mnlp_raw, DATA_PACKET_SIZE);
			}
			else
			{
				vTaskDelay(100);
				continue;
			}

			switch(response_pkt->rsp_id)
			{
				case SU_LDP_PKT	:	response_pkt->seq_cnt = res_seq_cnt[0]++;	break;
				case SU_HC_PKT	:	response_pkt->seq_cnt = res_seq_cnt[1]++;	break;
				case SU_CAL_PKT	:	response_pkt->seq_cnt = res_seq_cnt[2]++;	break;
				case SU_SCI_PKT	:	response_pkt->seq_cnt = res_seq_cnt[3]++;	break;
				case SU_HK_PKT	:	response_pkt->seq_cnt = res_seq_cnt[4]++;	break;
				case SU_STM_PKT	:	response_pkt->seq_cnt = res_seq_cnt[5]++;	break;
				case SU_DUMP_PKT:	response_pkt->seq_cnt = res_seq_cnt[6]++;	break;
				case SU_ERR_PKT	:	response_pkt->seq_cnt = res_seq_cnt[7]++;	break;
				case OBC_SU_ERR	:	response_pkt->seq_cnt = res_seq_cnt[8]++;	break;
				default			:	PRINT_DEBUG_STATEMENT("Illegal response ID start");
			}

			PRINT_DEBUG_STATEMENT("\tData packet logged with RSP_ID %d and sequence no  %d", response_pkt->rsp_id, response_pkt->seq_cnt);


			kit->adcs.getframe_adcs_state(&kit->adcs); // TODO check that these are correct
			data_hdr.utc = mnlp_gettime();
			data_hdr.roll = kit->adcs.adcs_state.estimated_roll_angle * (ADCS_ANGLE_UNIT / MNLP_ANGLE_UNIT);
			data_hdr.pitch = kit->adcs.adcs_state.estimated_pitch_angle * (ADCS_ANGLE_UNIT / MNLP_ANGLE_UNIT);
			data_hdr.yaw = kit->adcs.adcs_state.estimated_yaw_angle * (ADCS_ANGLE_UNIT / MNLP_ANGLE_UNIT);
			data_hdr.rolldot = kit->adcs.adcs_state.estimated_X_angular_rate;
			data_hdr.pitchdot = kit->adcs.adcs_state.estimated_Y_angular_rate;
			data_hdr.yawdot = kit->adcs.adcs_state.estimated_Z_angular_rate;
			data_hdr.x_eci = kit->adcs.adcs_state.X_position * (ADCS_POSITION_UNIT / MNLP_POSITION_UNIT);
			data_hdr.y_eci = kit->adcs.adcs_state.Y_position * (ADCS_POSITION_UNIT / MNLP_POSITION_UNIT);
			data_hdr.z_eci = kit->adcs.adcs_state.Z_position * (ADCS_POSITION_UNIT / MNLP_POSITION_UNIT);

			/*
			data_hdr.utc = 8;
			data_hdr.roll = 6;
			data_hdr.pitch = 7;
			data_hdr.yaw = 8;
			data_hdr.rolldot = 6;
			data_hdr.pitchdot = 7;
			data_hdr.yawdot = 8;
			data_hdr.x_eci = 6;
			data_hdr.y_eci = 7;
			data_hdr.z_eci = 8;
			*/

			//Write to date-stamped data file in SD card
			create_output_filename(data_file_name, response_pkt->rsp_id, data_hdr.utc);

			snprintf(sd_file_name, 13, "%s", data_file_name);
			fs_error_t ferr;
			file_t* file = kit->fs->open(kit->fs, &ferr, sd_file_name, FS_OPEN_ALWAYS, 0);
			//file_t* file = kit->fs->open(kit->fs, &ferr, sd_file_name, FS_OPEN_EXISTING, 0);
			//fid = file->_super_._native_handle_;
			//fid = fopen(sd_file_name, "a");
			if(ferr == FS_OK)
			{
				uint32_t written;

				file->seek(file, file->size(file, &ferr));
				file->write(file, &data_hdr, sizeof(mnlp_science_hdr_t), &written);
				//fwrite(&data_hdr, sizeof(mnlp_science_hdr_t), 1, fid);
				file->write(file, response_pkt, DATA_PACKET_SIZE, &written);
				//fwrite(response_pkt, DATA_PACKET_SIZE, 1, fid);
				file->close(file);
			}
			else
			{
				csp_printf("SD open failed, err: %d", ferr);
			}
			//fclose(fid);

			// Save to UFFS as well
			sprintf(uffs_file_name, "/boot/%s", data_file_name);
			fid2 = fopen(uffs_file_name, "a");
			if(fid2 != NULL)
			{
				fwrite(&data_hdr, sizeof(mnlp_science_hdr_t), 1, fid2);
				fwrite(response_pkt, DATA_PACKET_SIZE, 1, fid2);
			}
			else
			{
				csp_printf("UFFS write failed");
			}
			fclose(fid2);

			if(response_pkt->rsp_id == SU_ERR_PKT || response_pkt->rsp_id == OBC_SU_ERR)
			{
				mnlp_error_detected = 1;
				//PRINT_DEBUG_STATEMENT("ERROR PACKET LOGGED");
				//Error handling
				handle_mnlp_error();
			}

			//free allocated memory
			vPortFree(response_pkt);
			mnlp_timeout = 0;
		}
		vTaskDelay(100);
	}

}

/**
* @brief
*	Executes a command
*
* @details
* 	Sends SU commands to the mNLP via UART and
* 	processes OBC commands
*
* @param cmd
* 	Command to be executed
*
* @param param
* 	Parameters for the command
*/
void mnlp_exec_cmd(mnlp_seq_cmd_t cmd, uint8_t *param)
{
	PRINT_DEBUG_STATEMENT("Execute command %x", cmd.CMD_ID);
	char garbage;

	if((cmd.CMD_ID & 0xF0) != 0xF0)
	{
		//Send command to mNLP
		USART_PUTSTR(&(cmd.CMD_ID), 3);	//Write 3 bytes - CMD_ID, LEN, SEQ_CNT
		USART_PUTSTR(param, cmd.LEN - 1);	//Write remaining parameters
	}
	else
	{
		if(cmd.CMD_ID == OBC_SU_ON)
		{
			kit->mnlp->power(kit->mnlp, kit->hub, true);
			mnlp_is_on = 1;
			vTaskDelay(1000);
			while(usart_messages_waiting(MNLP_USART_HANDLE) > 0)
			{
				garbage = usart_getc(MNLP_USART_HANDLE);
			}
		}
		else if(cmd.CMD_ID == OBC_SU_OFF)
		{
			kit->mnlp->power(kit->mnlp, kit->hub, false);
			mnlp_is_on = 0;
			vTaskDelay(1000);
			while(usart_messages_waiting(MNLP_USART_HANDLE) > 0)
			{
				garbage = usart_getc(MNLP_USART_HANDLE);
			}
		}
		else
		{

		}
	}
}


/**
 * @brief
 *	This task executes the scripts
 *
 * @details
 * 	This task iterates through scripts and script sequences
 * 	When ready, it starts script sequences, and sends the commands
 * 	to be appropriately executed.
 */
void mnlpTask( void *pvParameters)
{
	csp_printf_crit("MNLP task start");

	uint32_t br;					//Used to count bytes read
	uint8_t seq_num;				//Identifies sequence running
	mnlp_seq_cmd_t cmd;				//Current command to be run
	fs_error_t status;				//Common var to store error returns
	file_t *run_seq_file;			//File handle for sequence currently running
	uint32_t sequence_cmd_cntr;		// Tracks sequence file position to allow closing of fp.
	char script_seq_filename[13];
	for(;;)
	{
		mnlp_best_script();
		//1. Check next script UTC time for start
		if(!script_slot[run_script].status.script_error && (mnlp_best_script() < 7))
		{
			//2. Check current times table for readiness. -1 indicates not ready yet.
			if(mnlp_best_seq() != -1)
			{
				switch(script_slot[run_script].next_seq[run_tt].script_index)
				{
					case SCPT_SEQ_ONE 	:	seq_num = 0 ; break;
					case SCPT_SEQ_TWO 	:	seq_num = 1 ; break;
					case SCPT_SEQ_THREE :	seq_num = 2 ; break;
					case SCPT_SEQ_FOUR 	:	seq_num = 3 ; break;
					case SCPT_SEQ_FIVE 	:	seq_num = 4 ; break;
					default: vTaskDelay(1000); continue;
				}

				create_seq_filename(script_seq_filename, run_script, seq_num);
				sequence_cmd_cntr = 0;
				csp_printf_crit("Running script %d sequence S%d", run_script, seq_num + 1);
				do
				{
					uint8_t *buf = NULL;

					run_seq_file = filesys->open(filesys, &status, script_seq_filename, FS_OPEN_EXISTING, BLOCK_FOREVER);
					if(status != FS_OK)
					{	csp_printf_crit("Unable to open commands file for script %d sequence %d", run_script, seq_num);	break;}

					if(run_seq_file->seek(run_seq_file, sequence_cmd_cntr) != FS_OK)
					{
						csp_printf_crit("Error seeking to current command in script %d sequence %d", run_script, seq_num);
					}
					//3. Load next command header
					if(run_seq_file->read(run_seq_file, (void *)&cmd, sizeof(mnlp_seq_cmd_t), &br) != FS_OK || br != sizeof(mnlp_seq_cmd_t))
					{
						csp_printf_crit("Error reading command header from script %d sequence %d at runtime", run_script, seq_num);
					}
					sequence_cmd_cntr = sequence_cmd_cntr + br;
					//4. Load next command parameters
					if(cmd.LEN > 1)
					{
						buf = pvPortMalloc(cmd.LEN - 1);
						//Read parameters
						if(run_seq_file->read(run_seq_file, (void *)buf, cmd.LEN - 1, &br) != FS_OK || br != (cmd.LEN - 1))
						{
							csp_printf_crit("Error reading parameters from script %d sequence %d at runtime", run_script, seq_num);
						}
						sequence_cmd_cntr = sequence_cmd_cntr + br;
					}
					filesys->close(filesys, run_seq_file);

					//5. Execute command and wait
					mnlp_exec_cmd(cmd, buf);
					//If mNLP returns error packet, then break out of current script sequence execution
					if(mnlp_error_detected)
					{
						//Wait a minute, to allow mnlp reset
						mnlp_task_wait(1,0);
						mnlp_error_detected = 0;
						break;
					}
					if(cmd.CMD_ID == (OBC_SU_ON || OBC_SU_OFF))
					{
						mnlp_task_wait(cmd.delMin, cmd.delSec - 1); // accounts for the delay settling the UART lines
					}
					else
					{
					mnlp_task_wait(cmd.delMin, cmd.delSec);
					}

					//6. Deallocate command memory
					if(cmd.LEN > 1)
						vPortFree(buf);

					//Make sure no other scripts should be running now instead
					uint8_t script_change = run_script;
					uint8_t seq_change = run_tt;
					//if((script_change != mnlp_best_script()) || (seq_change != mnlp_best_seq()))
					if(script_change != mnlp_best_script())
					{
						break;
					}
				}while(cmd.CMD_ID !=  OBC_EOT); 	//7. while command is not OBC_EOT */

				//filesys->close(filesys, run_seq_file);
			}

		}
		task_delay(1000);
	}
}

/**
 * @brief
 * 	Get current time
 *
 * @return
 * 	Seconds since QB50 epoch
 */
uint32_t mnlp_gettime()
{
	driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;
	time_t seconds;
	rtc_t *rtc;
	rtc = kit->rtc;
	seconds = rtc->get_ds1302_time();

	return seconds - QB50_EPOCH_OFFSET_FROM_UNIX_EPOCH; // Converts to QB50 epoch.
}

/**
 * @brief
 * 	Checks if a year is a leap year
 *
 * @param year
 * 	Year to be checked
 */
inline int isleap(int year)
{
	return ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0));
}

void mnlp_getdate( int* yearp, int* monthp, int* dayp, uint32_t time )
{
	int year, month, days;
	int days_in_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	int days_in_month_leap[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	days = time / 86400;		//Remove seconds

	//Extract year, month, date from time
	year = 2000;
	while(( days -= (isleap(year)?366:365)) > 0)
		year++;
	days += (isleap(year))?366:365;
	month = 1;
	if(isleap(year))
	{
		while((days -= days_in_month_leap[month - 1]) > 0)
			month++;
		days += days_in_month_leap[month - 1];
	}
	else
	{
		while((days -= days_in_month[month - 1]) > 0)
			month++;
		days += days_in_month[month - 1];
	}

	*yearp = year;
	*monthp = month;
	*dayp = days;
}

/**
 * @brief
 * 	Create the filename string for output file
 *
 * @param name
 * 	Pointer to char array to write filename to.
 *
 * @param rsp_id
 * 	Response ID of packet to be written
 *
 * @param time
 * 	Current QB50 epoch time
 */
void create_output_filename(char *name, uint8_t rsp_id, uint32_t time)
{
	int year, month, days;
	mnlp_getdate(&year, &month, &days, time);

	//Create file name
	switch(rsp_id)
	{
		case SU_LDP_PKT	:
		case SU_HC_PKT	:
		case SU_CAL_PKT	:
		case SU_HK_PKT	:
		case SU_STM_PKT	:	snprintf(name, 13, "M%02d%02d%02dH.bin", year % 100, month, days);	break;
		case SU_SCI_PKT	:
		case SU_DUMP_PKT:	snprintf(name, 13, "M%02d%02d%02dS.bin", year % 100, month, days);	break;
		case SU_ERR_PKT	:
		case OBC_SU_ERR	:	snprintf(name, 13, "M%02d%02d%02dE.bin", year % 100, month, days);	break;
		default			:	snprintf(name, 13, "M%02d%02d%02dD.bin", year % 100, month, days);	PRINT_DEBUG_STATEMENT("Illegal response ID");
	}
}

/**
 * @brief
 * 	Create the filename string for script file
 *
 * @param name
 * 	Pointer to char array to write filename to.
 *
 * @param script_num
 * 	Script index
 */
void create_script_filename(char *name, int script_num)
{
	if(script_num < 0 || script_num > 6)
	{
		PRINT_DEBUG_STATEMENT("Illegal script number in create filename function");
		return;
	}
	sprintf(name, "MNLP_%d.bin", script_num);
}

/**
 * @brief
 * 	Create the filename string for script sequence file
 *
 * @param name
 * 	Pointer to char array to write filename to.
 *
 * @param script_num
 * 	Script index
 *
 * @param seq_num
 * 	Sequence index
 */
void create_seq_filename(char *name, int script_num, int seq_num)
{
	if(script_num < 0 || script_num > 6)
	{
		PRINT_DEBUG_STATEMENT("Illegal script number in create filename function");
		return;
	}
	if(seq_num < 0 || seq_num > 4)
	{
		PRINT_DEBUG_STATEMENT("Illegal sequence number in create filename function");
		return;
	}
	sprintf(name, "s%dseq%d.bin", script_num, seq_num);
}

/****************************************************************/
/*  End of mNLP driver											*/
/****************************************************************/

