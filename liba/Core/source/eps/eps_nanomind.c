/*
 * Copyright (C) 2015  Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_lpc_local.c
 * @author Brendan Bruner
 * @author Stefan Damkjar
 * @date Feb 2, 2015
 */

#include <eps/eps_nanomind.h>
#include <portable_types.h>
#include "csp/csp.h"

/********************************************************************/
/* Virtual Method Implementations									*/
/********************************************************************/
/**
 * @memberof eps_nanomind_t
 * @brief
 *		Calls EPS CSP level drivers to refresh hk table
 * @details
 * 		This function makes a call to the hardware level transaction driver for
 * 		the EPS to update its housekeeping table.
 *	@param eps
 * 		A pointer to the eps_t structure whose hk structure should be refreshed.
 *	@return
 *		<b>true</b> on success, <b>false</b> otherwise.
 */
static bool_t hk_refresh( eps_t *eps )
{
	DEV_ASSERT( eps );
	int status;

	status=eps_hk_get( &eps->_.hk );
	if( status == CSP_ERR_NONE ){ return true; }
	return false;
}


/********************************************************************/
/* Method Implementations											*/
/********************************************************************/
void initialize_eps_nanomind( eps_nanomind_t *eps )
{
	DEV_ASSERT( eps );

	extern void initialize_eps_( eps_t* );
	initialize_eps_( (eps_t*) eps );

	/* Initialize function pointers to their corresponding functions */
	//((eps_t*) eps)->power_line = &power_line;
	((eps_t*) eps)->hk_refresh = &hk_refresh;

	/* Initialize Telemetery Struct */
	((eps_t*) eps)->_.hk = (eps_hk_t) {	.vboost = {0, 0, 0},
								.vbatt = 0,
								.curin = {0, 0, 0},
								.cursun = 0,
								.cursys = 0,
								.reserved = 0,
								.curout = {0, 0, 0, 0, 0, 0},
								.output = {0, 0, 0, 0, 0, 0, 0, 0},
								.output_on_delta = {0, 0, 0, 0, 0, 0, 0, 0},
								.output_off_delta = {0, 0, 0, 0, 0, 0, 0, 0},
								.latchup = {0, 0, 0, 0, 0, 0},
								.wdt_i2c_time_left = 0,
								.wdt_gnd_time_left = 0,
								.wdt_csp_pings_left = {0, 0},
								.counter_wdt_i2c = 0,
								.counter_wdt_gnd = 0,
								.counter_wdt_csp = {0, 0},
								.counter_boot = 0,
								.temp = {0, 0, 0, 0, 0, 0},
								.bootcause = 0,
								.battmode = 0,
								.pptmode = 0,
								.reserved2 = 0 };

}
