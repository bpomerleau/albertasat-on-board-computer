/*
 * Copyright (C) 2015  Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_lpc_local.c
 * @author Brendan Bruner
 * @author Stefan Damkjar
 * @date Feb 2, 2015
 */

#include <eps/eps_local.h>
#include <portable_types.h>

static unsigned int v_counter = 0;
static unsigned int i_counter = 0;
#define SIZE_OF_VOLTAGES 65
static uint16_t voltages[] = 			{
										 17000, 17000, 17000, 17000, 16500,
										 16500, 16450, 16450, 16300, 16300,
										 16250, 16250, 16200, 16200, 16200,
										 16100, 16100, 16100, 16100, 16100,
										 16100, 16100, 16000, 16000, 16000,
										 15950, 15950, 15950, 15900, 15900,
										 15850, 15850, 15850, 15850, 15850,
										 15850, 15800, 15800, 15800, 15600,
										 15600, 15600, 15700, 15700, 15750,
										 15750, 15800, 15900, 15900, 15900,
										 15950, 16000, 16000, 16300, 16350,
										 16400, 16600, 16600, 16700, 16900,
										 16900, 16950, 16950, 16950, 16950
										};
static uint16_t currents[] = {800, 880, 789, 689, 900, 990, 123, 14, 465};
#define SIZE_OF_CURRENTS (sizeof(currents)/sizeof(uint16_t))


/********************************************************************/
/* Virtual Method Implementations									*/
/********************************************************************/
static bool_t hk_refresh( eps_t *eps )
{
	DEV_ASSERT( eps );

	eps->_.hk.vbatt    = (uint16_t) (voltages[v_counter++ % SIZE_OF_VOLTAGES] - (unsigned int) 1800);
	eps->_.hk.curin[0] = currents[i_counter++ % SIZE_OF_CURRENTS];
	eps->_.hk.curin[1] = currents[i_counter++ % SIZE_OF_CURRENTS];
	eps->_.hk.curin[2] = currents[i_counter++ % SIZE_OF_CURRENTS];

	return true;
}

static bool_t power_line( eps_t* eps_, eps_power_line_enum_t line, eps_power_line_state_enum_t state )
{
	eps_local_t* eps = (eps_local_t*) eps_;
	DEV_ASSERT( eps );

	gpio_t* gpio;

	if( eps->_.using_power_lines == false )
	{
		return true;
	}

	gpio = eps->_.power_line_gpios[(uint8_t) line];
	if( state == EPS_POWER_LINE_ACTIVE )
	{
		gpio->set_state( gpio, GPIO_HIGH );
	}
	else
	{
		gpio->set_state( gpio, GPIO_LOW );
	}

	return true;
}

static eps_mode_t mode( eps_t *eps_ )
{
	eps_local_t* eps = (eps_local_t*) eps_;
	DEV_ASSERT( eps );

	eps_mode_t current_mode;
	gpio_t** gpios;

	/* Invoke super.mode( ). */
	current_mode = eps->_.s_mode( (eps_t*) eps );

	/* If using state GPIOs, toggle them based on the current state. */
	if( eps->_.using_state_gpios == false )
	{
		return current_mode;
	}
	gpios = eps->_.state_gpios;

	/* Set the correct LED. */
	switch( current_mode )
	{
		case EPS_MODE_POWER_SAFE:
			gpios[0]->set_state( gpios[0], GPIO_HIGH );
			gpios[1]->set_state( gpios[1], GPIO_LOW );
			gpios[2]->set_state( gpios[2], GPIO_LOW );
			break;
		case EPS_MODE_NORMAL:
			gpios[0]->set_state( gpios[0], GPIO_LOW );
			gpios[1]->set_state( gpios[1], GPIO_HIGH );
			gpios[2]->set_state( gpios[2], GPIO_LOW );
			break;
		case EPS_MODE_OPTIMAL:
			gpios[0]->set_state( gpios[0], GPIO_LOW );
			gpios[1]->set_state( gpios[1], GPIO_LOW );
			gpios[2]->set_state( gpios[2], GPIO_HIGH );
			break;
		default:
			gpios[0]->set_state( gpios[0], GPIO_LOW );
			gpios[1]->set_state( gpios[1], GPIO_LOW );
			gpios[2]->set_state( gpios[2], GPIO_LOW );
			break;
	}

	return current_mode;
}


/********************************************************************/
/* Method Implementations											*/
/********************************************************************/
void initialize_eps_local( eps_local_t *eps, gpio_t** power_line_gpios, gpio_t** state_gpios )
{
	DEV_ASSERT( eps );
	uint32_t iter;

	extern void initialize_eps_( eps_t* );
	initialize_eps_( (eps_t*) eps );

	/* Initialize function pointers to their corresponding functions */
	((eps_t*) eps)->power_line = &power_line;
	((eps_t*) eps)->hk_refresh = &hk_refresh;
	eps->_.s_mode = ((eps_t*) eps)->mode;
	((eps_t*) eps)->mode = &mode;

	/* Initialize Telemetery Struct */
	((eps_t*) eps)->_.hk = (eps_hk_t) {	.vboost = {0, 0, 0},
								.vbatt = 0,
								.curin = {0, 0, 0},
								.cursun = 0,
								.cursys = 0,
								.reserved = 0,
								.curout = {0, 0, 0, 0, 0, 0},
								.output = {0, 0, 0, 0, 0, 0, 0, 0},
								.output_on_delta = {0, 0, 0, 0, 0, 0, 0, 0},
								.output_off_delta = {0, 0, 0, 0, 0, 0, 0, 0},
								.latchup = {0, 0, 0, 0, 0, 0},
								.wdt_i2c_time_left = 0,
								.wdt_gnd_time_left = 0,
								.wdt_csp_pings_left = {0, 0},
								.counter_wdt_i2c = 0,
								.counter_wdt_gnd = 0,
								.counter_wdt_csp = {0, 0},
								.counter_boot = 0,
								.temp = {0, 0, 0, 0, 0, 0},
								.bootcause = 0,
								.battmode = 0,
								.pptmode = 0,
								.reserved2 = 0 };

	/* Assigned power line GPIOs. */
	if( power_line_gpios == NULL )
	{
		eps->_.using_power_lines = false;
	}
	else
	{
		eps->_.using_power_lines = true;
		for( iter = 0; iter < EPS_TOTAL_POWER_LINES; ++iter )
		{
			power_line_gpios[iter]->set_direction( power_line_gpios[iter], GPIO_OUTPUT );
			eps->_.power_line_gpios[iter] = power_line_gpios[iter];
		}
	}

	/* Assign state GPIOs. */
	if( state_gpios == NULL )
	{
		eps->_.using_state_gpios = false;
	}
	else
	{
		eps->_.using_state_gpios = true;
		for( iter = 0; iter < EPS_LOCAL_STATE_GPIO_COUNT; ++iter )
		{
			state_gpios[iter]->set_direction( state_gpios[iter], GPIO_OUTPUT );
			eps->_.state_gpios[iter] = state_gpios[iter];
		}
	}
}
