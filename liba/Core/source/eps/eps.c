/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_generic
 * @author Brendan Bruner
 * @date Mar 12, 2015
 */

#include <eps/eps.h>
#include <io/nanohub.h>
#include <portable_types.h>
#include <core_defines.h>

/********************************************************************/
/* Look up tables													*/
/********************************************************************/
static uint8_t const curout_3v3_hash[] = { 3, 4, 2, 5 };
static uint8_t const curout_5v0_hash[] = { 0, 1 };
static uint8_t const pcb_temp_hash[] = { 0, 1, 2, 3 };
static uint8_t const batt_temp_hash[] = { 4, 5 };


/********************************************************************/
/* Virtual Methods													*/
/********************************************************************/
/**
 * @memberof eps_t
 * @brief
 * 		Refreshes cached housekeeping data.
 * @details
 * 		Refreshes cached housekeeping data with the most up to date data
 * 		from the electronic power supply.
 */
static bool_t hk_refresh( eps_t *eps )
{
	DEV_ASSERT( eps );
	return false;
}

/**
 * @memberof eps_t
 * @brief
 * 		Set the state of a power line.
 * @details
 * 		Set the state of a power line.
 * 		Redundant calls have no effect (ie, calling the function two or more
 * 		times in a row with the same parameters).
 * @param line
 * 		The power line to set the state of.
 * @param state
 * 		The state the power line will be set to.
 * @returns
 * 		<b>true</b> on successful setting of state, <b>false</b> otherwise.
 */
static bool_t power_line( eps_t* self, eps_power_line_enum_t line, eps_power_line_state_enum_t state )
{
	DEV_ASSERT( self );
	int status = 0;
	status = eps_output_set_single(line, state, 0);
	if(status == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

/**
 * @memberof eps_t
 * @brief
 * 		Retrieves a specific housekeeping item from EPS housekeeping struct
 * @details
 * 		Puts a pointer to a certain piece of information into a handle. If
 * 		the handle has a size of zero, the information is invalid.
 *
 *            Macro             |Value|       Description
 * 		------------------------|-----|-------------------------
 * 		EPS_VBOOST              |  0  |   Voltage of boost converters [mV] [PV1, PV2, PV3]
 * 		EPS_VBATT               |  1  |   Voltage of battery [mV]
 * 		EPS_CURIN               |  2  |   Current in [mA]
 * 		EPS_CURSUN              |  3  |	  Current from boost converters [mA]
 * 		EPS_CURSYS              |  4  |   Current out of battery [mA]
 * 		EPS_RESERVED            |  5  |   Reserved for future use
 * 		EPS_CUROUT              |  6  |   Current out (switchable outputs) [mA]
 * 		EPS_OUTPUT              |  7  |   Status of outputs ([6] is BP4 heater, [7] is BP4 switch)
 * 		EPS_OUTPUT_ON_DELTA     |  8  |   Time till power on ([6] is BP4 heater, [7] is BP4 switch)
 * 		EPS_OUTPUT_OFF_DELTA    |  9  |   Time till power off ([6] is BP4 heater, [7] is BP4 switch)
 * 		EPS_LATCHUP             | 10  |   Number of latch-ups
 * 		EPS_WDT_I2C_TIME_LEFT   | 11  |   Time left on I2C wdt [s]
 * 		EPS_WDT_GND_TIME_LEFT   | 12  |   Time left on I2C wdt [s]
 * 		EPS_WDT_CSP_PINGS_LEFT  | 13  |   Pings left on CSP wdt
 * 		EPS_COUNTER_WDT_I2C     | 14  |   Number of WDT I2C reboots
 * 		EPS_COUNTER_WDT_GND     | 15  |   Number of WDT GND reboots
 * 		EPS_COUNTER_WDT_CSP     | 16  |   Number of WDT CSP reboots
 * 		EPS_COUNTER_BOOT        | 17  |   Number of EPS reboots
 * 		EPS_TEMP                | 18  |   Temperatures [degC] [0 = TEMP1,TEMP2,TEMP3,TEMP4,BP4a, BP4b]
 * 		EPS_BOOTCAUSE           | 19  |   Cause of last EPS reboot
 * 		EPS_BATTMODE            | 20  |   Mode for battery [0 = initial,1 = undervoltage,2 = nominal, 3 = batteryfull]
 * 		EPS_PPTMODE             | 21  |   Mode of PPT tracker [1=MPPT, 2=FIXED
 * 		EPS_RESERVED2           | 22  |   Reserved for future use
 * 	@attention
 * 		Must call eps_t::hk_refresh( ) to refresh memory with up to date values. This is
 * 		just a look up function.
 *	@param eps
 * 		A pointer to the eps_t structure whose hk structure is being accessed.
 *  @param tlm_id
 *		A macro corresponding to the desired housekeeping value
 *  @return
 *  	Returns a struct with the size (in bytes) and address of the requested housekeeping item
 *
 */
static hk_handler_t hk_get( eps_t *eps, eps_tlm_id_t tlm_id )
{
	DEV_ASSERT( eps );

	/* Initialize structure for housekeeping item and pad with zeros */
	hk_handler_t hk_itm = {0};

	/* Retrieve the size of, and a pointer to the request housekeeping item */
	switch(tlm_id) {
	case EPS_VBOOST:
			hk_itm.size = sizeof(eps->_.hk.vboost);
			hk_itm.ptr  = &eps->_.hk.vboost;
			break;
	case EPS_VBATT:
			hk_itm.size = sizeof(eps->_.hk.vbatt);
			hk_itm.ptr  = &eps->_.hk.vbatt;
			break;
	case EPS_CURIN:
			hk_itm.size = sizeof(eps->_.hk.curin);
			hk_itm.ptr  = &eps->_.hk.curin;
			break;
	case EPS_CURSUN:
			hk_itm.size = sizeof(eps->_.hk.cursun);
			hk_itm.ptr  = &eps->_.hk.cursun;
			break;
	case EPS_CURSYS:
			hk_itm.size = sizeof(eps->_.hk.cursys);
			hk_itm.ptr  = &eps->_.hk.cursys;
			break;
	case EPS_RESERVED:
			hk_itm.size = sizeof(eps->_.hk.reserved);
			hk_itm.ptr  = &eps->_.hk.reserved;
			break;
	case EPS_CUROUT:
			hk_itm.size = sizeof(eps->_.hk.curout);
			hk_itm.ptr  = &eps->_.hk.curout;
			break;
	case EPS_OUTPUT:
			hk_itm.size = sizeof(eps->_.hk.output);
			hk_itm.ptr  = &eps->_.hk.output;
			break;
	case EPS_OUTPUT_ON_DELTA:
			hk_itm.size = sizeof(eps->_.hk.output_on_delta);
			hk_itm.ptr  = &eps->_.hk.output_on_delta;
			break;
	case EPS_OUTPUT_OFF_DELTA:
			hk_itm.size = sizeof(eps->_.hk.output_off_delta);
			hk_itm.ptr  = &eps->_.hk.output_off_delta;
			break;
	case EPS_LATCHUP:
			hk_itm.size = sizeof(eps->_.hk.latchup);
			hk_itm.ptr  = &eps->_.hk.latchup;
			break;
	case EPS_WDT_I2C_TIME_LEFT:
			hk_itm.size = sizeof(eps->_.hk.wdt_i2c_time_left);
			hk_itm.ptr  = &eps->_.hk.wdt_i2c_time_left;
			break;
	case EPS_WDT_GND_TIME_LEFT:
			hk_itm.size = sizeof(eps->_.hk.wdt_gnd_time_left);
			hk_itm.ptr  = &eps->_.hk.wdt_gnd_time_left;
			break;
	case EPS_WDT_CSP_PINGS_LEFT:
			hk_itm.size = sizeof(eps->_.hk.wdt_csp_pings_left);
			hk_itm.ptr  = &eps->_.hk.wdt_csp_pings_left;
			break;
	case EPS_COUNTER_WDT_I2C:
			hk_itm.size = sizeof(eps->_.hk.counter_wdt_i2c);
			hk_itm.ptr  = &eps->_.hk.counter_wdt_i2c;
			break;
	case EPS_COUNTER_WDT_GND:
			hk_itm.size = sizeof(eps->_.hk.counter_wdt_gnd);
			hk_itm.ptr  = &eps->_.hk.counter_wdt_gnd;
			break;
	case EPS_COUNTER_WDT_CSP:
			hk_itm.size = sizeof(eps->_.hk.counter_wdt_csp);
			hk_itm.ptr  = &eps->_.hk.counter_wdt_csp;
			break;
	case EPS_COUNTER_BOOT:
			hk_itm.size = sizeof(eps->_.hk.counter_boot);
			hk_itm.ptr  = &eps->_.hk.counter_boot;
			break;
	case EPS_TEMP:
			hk_itm.size = sizeof(eps->_.hk.temp);
			hk_itm.ptr  = &eps->_.hk.temp;
			break;
	case EPS_BOOTCAUSE:
			hk_itm.size = sizeof(eps->_.hk.bootcause);
			hk_itm.ptr  = &eps->_.hk.bootcause;
			break;
	case EPS_BATTMODE:
			hk_itm.size = sizeof(eps->_.hk.battmode);
			hk_itm.ptr  = &eps->_.hk.battmode;
			break;
	case EPS_PPTMODE:
			hk_itm.size = sizeof(eps->_.hk.pptmode);
			hk_itm.ptr  = &eps->_.hk.pptmode;
			break;
	case EPS_RESERVED2:
			hk_itm.size = sizeof(eps->_.hk.reserved2);
			hk_itm.ptr  = &eps->_.hk.reserved2;
			break;
	default:
			return TLM_ERR_INVAL;
	}

	return hk_itm;
}

/**
 * @memberof eps_t
 * @brief
 * 		Get the current cached battery voltage.
 * @details
 * 		Get the current cached battery voltage.
 * @returns
 * 		Cached battery voltage.
 */
static eps_vbatt_t get_vbatt( eps_t *eps )
{
	DEV_ASSERT( eps );

	return eps->_.hk.vbatt;
}

/**
 * @memberof eps_t
 * @brief
 * 		Gets the current state of the EPS.
 * @details
 * 		Gets the current state of the EPS. The cached housekeeping data
 * 		is always refreshed before the current state of the eps is determined
 * 		and returned.
 */
static eps_mode_t mode( eps_t *eps )
{
	DEV_ASSERT( eps );

	eps_vbatt_t current_voltage;
	int status;


	//eps->hk_refresh( eps );
	//current_voltage = *(eps_vbatt_t*)eps->hk_get(eps,EPS_VBATT).ptr;

	eps_hk_vi_t hk;
	status = eps_hk_vi_get(&hk);
	current_voltage = hk.vbatt;
	if(status != sizeof(eps_hk_vi_t))
	{
		return EPS_MODE_OPTIMAL;
	}


	if( current_voltage >= OPTIMAL_LOWER_THRESHOLD )
	{
		return EPS_MODE_OPTIMAL;
	}
	else if( current_voltage < OPTIMAL_LOWER_THRESHOLD &&
			 current_voltage >= NORMAL_LOWER_THRESHOLD )
	{
		return EPS_MODE_NORMAL;
	}
	else if( current_voltage < NORMAL_LOWER_THRESHOLD &&
			 current_voltage >= POWER_SAFE_LOWER_THRESHOLD )
	{
		return EPS_MODE_POWER_SAFE;
	}
	else
	{
		return EPS_MODE_CRITICAL;
	}
}


/********************************************************************/
/* Constructor														*/
/********************************************************************/
static void destroy( eps_t* self )
{
	DEV_ASSERT( self );
}

/**
 * @memberof eps_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_eps_( eps_t* self )
{
	DEV_ASSERT( self );

	self->get_vbatt = get_vbatt;
	self->hk_get = hk_get;
	self->hk_refresh = hk_refresh;
	self->mode = mode;
	self->power_line = power_line;
	self->destroy = destroy;
}

/********************************************************************/
/* Public Non Virtual Methods										*/
/********************************************************************/
uint16_t eps_get_vbatt( eps_t *eps )
{
	DEV_ASSERT( eps );

	eps_hk_vi_t hk;
	eps_hk_vi_get(&hk);

	return hk.vbatt;
}

uint16_t eps_get_batt_current( eps_t *eps )
{
	DEV_ASSERT( eps );

	eps_hk_vi_t hk;
	eps_hk_vi_get(&hk);

	return hk.cursys;
}

uint16_t eps_get_3v3_current( eps_t *eps )
{
	DEV_ASSERT( eps );

	uint16_t	 current;
	eps_hk_out_t hk;

	eps_hk_out_get(&hk);
	current = hk.curout[2] + hk.curout[3] + hk.curout[4] + hk.curout[5];

	return current/CUROUT_3V3_NUM;
}

uint16_t eps_get_5v0_current( eps_t *eps )
{
	DEV_ASSERT( eps );

	uint16_t	 current;
	eps_hk_out_t hk;

	eps_hk_out_get(&hk);
	current = hk.curout[0] + hk.curout[1];

	return current/CUROUT_5V0_NUM;
}

int16_t	 eps_get_pcb_temp( eps_t *eps )
{
	DEV_ASSERT( eps );

	//hk_handler_t handle;
	//int16_t 	 *temp_array;
	int16_t 	temp;
	//uint8_t		 iter;

	/*handle = eps->hk_get( eps, EPS_TEMP );
	if( handle.size == 0 ) return 0;
	temp_array = ((int16_t *) handle.ptr);

	temp = 0;
	for( iter = 0; iter < PCB_TEMP_NUM; ++iter )
	{
		temp = (int16_t) (temp + temp_array[PCB_TEMP_INDEX[iter]]);
	}*/

	eps_hk_basic_t hk;
	eps_hk_basic_get(&hk);

	temp = hk.temp[0] + hk.temp[1] + hk.temp[2] + hk.temp[3];

	return (temp/PCB_TEMP_NUM);
}

int16_t  eps_get_batt_temp( eps_t *eps )
{
	DEV_ASSERT( eps );

	//hk_handler_t handle;
	//int16_t 	 *temp_array;
	int16_t		temp;
	//uint8_t		 iter;

	/*handle = eps->hk_get( eps, EPS_TEMP );
	if( handle.size == 0 ) return 0;
	temp_array = ((int16_t *) handle.ptr);

	temp = 0;
	for( iter = 0; iter < BATT_TEMP_NUM; ++iter )
	{
		temp = (int16_t) (temp + temp_array[BATT_TEMP_INDEX[iter]]);
	}*/

	eps_hk_basic_t hk;
	eps_hk_basic_get(&hk);

	temp = hk.temp[4] + hk.temp[5];


	return (temp/BATT_TEMP_NUM);
}
