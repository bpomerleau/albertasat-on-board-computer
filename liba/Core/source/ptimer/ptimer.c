/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ptimer.c
 * @author Brendan Bruner
 * @date Sep 22, 2015
 */
#include <ptimer/ptimer.h>
#include <core_defines.h>
#include <string.h>
#include <printing.h>
#include <ptimer/ptimer_controller.h>
#include <csp_internal.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
/* Initial time to log when timer is created for first time. */
#define PERSISTENT_TIMER_EXPIRED 1
#define PERSISTENT_TIMER_NOT_EXPIRED 0

#define PERSISTENT_TIMER_RESET_TIME 0



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
/**
 * @memberof ptimer_t
 * @private
 * @brief
 * 		Initialize mutexs and software timers.
 * @details
 * 		Initialize mutexs and software timers. Only call in constructor.
 * 		On failure, memory is cleaned up within the function.
 * @returns
 * 		true if successful, false otherwise.
 */
static bool_t _presistent_timer_create_os_components( ptimer_t* self )
{
	DEV_ASSERT( self );

	new_semaphore( self->_.timer_completion_guard, 1, 0 );
	if( self->_.timer_completion_guard == NULL )
	{
		/* Failed to create semaphore. */
		return false;
	}

	return true;
}

/**
 * @memberof ptimer_t
 * @private
 * @brief
 * 		Create a new log file for the timer.
 * @details
 * 		Create a new log file for the timer. If a log file already exists, it is
 * 		overwritten with a new one. The log file is initialized to indicate the
 * 		timer has been running for zero seconds.
 */
static bool_t _ptimer_create_new_log_file_( ptimer_t* self, file_t* log_file )
{
	DEV_ASSERT( self );

	uint32_t	initial_state = PERSISTENT_TIMER_RESET_TIME;
	uint32_t	written;
	fs_error_t	file_err;

	file_err = log_file->seek( log_file, BEGINNING_OF_FILE );
	if( file_err != FS_OK )
	{
		/* Error writing to file or out of memory, abort. */
		return false;
	}

	file_err = log_file->write( log_file, (uint8_t*) &initial_state, sizeof( initial_state ), &written );
	if( file_err != FS_OK || written != sizeof( initial_state ) )
	{
		/* Error writing to file or out of memory, abort. */
		return false;
	}
	return true;
}

/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
static bool_t is_expired( ptimer_t *self, block_time_t block_time )
{
	DEV_ASSERT( self );

	if( peek_semaphore( self->_.timer_completion_guard, block_time ) == SEMAPHORE_AVAILABLE )
	{
		return true;
	}
	else
	{
		/* Timer not expired. */
		return false;
	}
}

static bool_t start( ptimer_t* self )
{
	DEV_ASSERT( self );

	if( peek_semaphore( self->_.timer_completion_guard, USE_POLLING ) == SEMAPHORE_AVAILABLE )
	{
		/* Timer should not be started again. */
		return false;
	}
	else
	{
		ptimer_controller_t* controller = initialize_ptimer_controller( );
		if( controller == NULL )
		{
			return false;
		}
		ptimer_controller_register( controller, self );

		return true;
	}
}


/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy( ptimer_t *self )
{
	DEV_ASSERT( self );
	ptimer_controller_t* controller;

	controller = initialize_ptimer_controller( );
	ptimer_controller_deregister( controller, self );
	delete_semaphore( self->_.timer_completion_guard );
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_ptimer
(
	ptimer_t *self,
	uint32_t duration,
	uint32_t resolution,
	filesystem_t *fs,
	char const * timer_log_file
)
{
	DEV_ASSERT( self );
	DEV_ASSERT( fs );
	DEV_ASSERT( timer_log_file );

	fs_error_t 	file_err;
	file_t* 	log_file;
	uint32_t 	read;

	/* Link virtual functions. */
	self->is_expired = is_expired;
	self->start = start;
	self->destroy = destroy;

	/* Make a copy of the log file name. */
	strncpy( self->_.log_file, timer_log_file, FILESYSTEM_MAX_NAME_LENGTH );
	self->_.log_file[FILESYSTEM_MAX_NAME_LENGTH] = '\0';

	/* Setup more member variables. */
	self->_.duration = 1800; // TODO hard coded for blackout
	self->_.seconds_elpased = 0;
	self->_.resolution = 5; // TODO hard coded for blackout
	self->_.fs = fs;
	/* Create mutexs and software timer. */
	if( !_presistent_timer_create_os_components( self ) )
	{
		return false;
	}
	/* Check if the log file exists yet. */
	file_err = self->_.fs->file_exists( self->_.fs, self->_.log_file );
	if( file_err == FS_NO_FILE )
	{
		/* File doesn't exist yet, this is first invocation of timer. */
		/* Create the file and initialize the timer to zero. */
		log_file = self->_.fs->open( self->_.fs, &file_err, self->_.log_file, FS_CREATE_ALWAYS, BLOCK_FOREVER );
		if( file_err != FS_OK )
		{
			/* Failed to open log file. */
			return false;
		}
		if( !_ptimer_create_new_log_file_( self, log_file ) )
		{
			log_file->close( log_file );
			/* Failed to initialize timer to zero. */
			return false;
		}
		log_file->close( log_file );
	}
	else
	{
		/* The file exists, check to see if the timer is expired. */
		uint32_t is_timer_done;

		log_file = self->_.fs->open( self->_.fs, &file_err, self->_.log_file, FS_OPEN_EXISTING, BLOCK_FOREVER );
		if( file_err != FS_OK)
		{
			/* Error opening file, abort. */
			return false;
		}

		file_err = log_file->read( log_file, (uint8_t*) &is_timer_done, sizeof( is_timer_done ), &read );
		if( file_err != FS_OK )
		{
			/* Error reading out of memory, abort. */
			self->_.fs->close( self->_.fs, log_file );
			return false;
		}
		if( read != sizeof( is_timer_done ) )
		{
			/* The file is smaller then it ought to be. It is corrupt. */
			/* Repair procedure is to restart the timer from zero. */
			if( !_ptimer_create_new_log_file_( self, log_file ) )
			{
				/* Failed to initialize timer to zero. */
				self->_.fs->close( self->_.fs, log_file );
				return false;
			}
		}
		self->_.fs->close( self->_.fs, log_file );

		/* Got the current time of the log file, check if it is expired. */
		if( is_timer_done >= self->_.duration )
		{
			/* Timer is expired, unlock the timer lock. */
			post_semaphore( self->_.timer_completion_guard );
		}
	}

	return true;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
void ptimer_update( ptimer_t* self, uint32_t ms_elapsed )
{
	DEV_ASSERT( self );

	file_t*		log_file;
	fs_error_t	file_err;
	uint32_t	total_time;
	uint32_t	bytes_used;

	if( self->is_expired( self, USE_POLLING ) )
	{
		/* timer is expired, no need to update. */
		return;
	}

	self->_.seconds_elpased += (ms_elapsed / 1000);
	if( self->_.seconds_elpased < self->_.resolution )
	{
		/* Not enough time has passed to update non volatile memory. */
		return;
	}

	/* Time to update non volatile memory. */
	log_file = self->_.fs->open( self->_.fs, &file_err, self->_.log_file, FS_OPEN_ALWAYS, USE_POLLING );
	if( file_err != FS_OK )
	{
		return;
	}

	/* Get total time elapsed since last update. */
	total_time = 0;
	file_err = log_file->read( log_file, (uint8_t*) &total_time, sizeof(total_time), &bytes_used );
	if( file_err != FS_OK )
	{
		log_file->close( log_file );
		return;
	}
	if( bytes_used != sizeof(total_time) )
	{
		total_time = 0;
	}

	/* Update with new time elapsed. */
	total_time += self->_.seconds_elpased;
	file_err = log_file->seek( log_file, BEGINNING_OF_FILE );
	if( file_err != FS_OK )
	{
		log_file->close( log_file );
		return;
	}
	file_err = log_file->write( log_file, (uint8_t*) &total_time, sizeof(total_time), &bytes_used );
	log_file->close( log_file );
	if( file_err == FS_OK )
	{
		self->_.seconds_elpased = 0;
	}

	/* Unlock completion guard if done. */
	if( total_time >= self->_.duration )
	{
		ptimer_controller_t* controller;

		post_semaphore( self->_.timer_completion_guard );
		/* Deregister. */
		controller = initialize_ptimer_controller( );
		ptimer_controller_deregister( controller, self );
	}
}

