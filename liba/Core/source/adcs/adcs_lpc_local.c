/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca

*
 * @file adcs_lpc_local
 * @author Brendan Bruner
 * @date April 7, 2015
*/

#include <adcs/adcs.h>
#include <portable_types.h>
/*
#ifndef __LPC17XX__
static void getframe_identification(adcs_t *adcsadcs)
{
	return;
}
static void getframe_communication_status(adcs_t *adcs)
		{
	return;
		}
static void getframe_telecommand_acknowledge(adcs_t *adcs)
		{
	return;
		}
static void getframe_reset_cause(adcs_t *adcs)
		{
	return;
		}
static void getframe_actuator_commands(adcs_t *adcs)
		{
	return;
		}
static void getframe_acp_execution_state(adcs_t *adcs)
		{
	return;
		}
static void getframe_acp_execution_times(adcs_t *adcs)
		{
	return;
		}
static void getframe_edac_and_latchup_counters(adcs_t *adcs)
		{
	return;
		}
static void getframe_start_up_mode(adcs_t *adcs)
		{
	return;
		}
static void getframe_file_information(adcs_t *adcs)
		{
	return;
		}
static void getframe_file_block_crc(adcs_t *adcs)
		{
	return;
		}
static void getframe_file_data_block(adcs_t *adcs)
		{
	return;
		}
static void getframe_power_control_selection(adcs_t *adcs)
		{
	return;
		}
static void getframe_power_and_temperature_measurements(adcs_t *adcs)
		{
	return;
		}
#endif
static void getframe_adcs_state(adcs_t *adcs)
{
	static uint32_t count = 0;

	if( count < 2 )
	{
		adcs->adcs_state.adcs_modes &= 0xF8;
	}
	else if (count < 4 )
	{
		adcs->adcs_state.adcs_modes &= 0xF8;
		adcs->adcs_state.adcs_modes |= 0x02;
	}
	else if (count < 7 )
	{
		adcs->adcs_state.adcs_modes &= 0xF8;
		adcs->adcs_state.adcs_modes |= 0x03;
	}
	else if ( count < 20 )
	{
		adcs->adcs_state.adcs_modes &= 0xF8;
		adcs->adcs_state.adcs_modes |= 0x04;
	}
	else
	{
		count = 0;
	}
	++count;
	return;
}

 *
 * @memberof adcs_t
 * @brief
 * 		Command over adcs' detumbling unit.
 * @details
 * 		Command over adcs' detumbling unit.
 * @param command
 * 		The command to perform.
 * @returns
 * 		<b>true</b> on successful delivery of command to adcs.
 * 		<br><b>false</b> on failure to send command to adcs.
 *
static bool_t detumble_command( adcs_t* self, adcs_detumbe_command_enum_t command )
{
	DEV_ASSERT( self );
	return true;
}

#ifndef __LPC17XX__
static void getframe_adcs_measurements(adcs_t *adcs)
		{
	return;
		}
static void getframe_current_time(adcs_t *adcs)
		{
	return;
		}
static void getframe_current_state(adcs_t *adcs)
		{
	return;
		}
static void getframe_estimated_attitude_angles(adcs_t *adcs)
		{
	return;
		}
static void getframe_estimated_angular_rates(adcs_t *adcs)
		{
	return;
		}
static void getframe_satellite_position_llh(adcs_t *adcs)
		{
	return;
		}
static void getframe_satellite_velocity_eci(adcs_t *adcs)
		{
	return;
		}
static void getframe_magnetic_field_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_coarse_sun_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_fine_sun_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_nadir_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_rate_sensor_rates(adcs_t *adcs)
		{
	return;
		}
static void getframe_wheel_speed(adcs_t *adcs)
		{
	return;
		}
static void getframe_cubesense_current(adcs_t *adcs)
		{
	return;
		}
static void getframe_cubecontrol_current(adcs_t *adcs)
		{
	return;
		}
static void getframe_peripheral_current_and_temperature_measurements(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_sensor_measurements(adcs_t *adcs)
		{
	return;
		}
static void getframe_angular_rate_covariance(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_nadir_sensor(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_sun_sensor(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_css(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_magnetometer(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_gps_status(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_gps_time(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_gps_x(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_gps_y(adcs_t *adcs)
		{
	return;
		}
static void getframe_raw_gps_z(adcs_t *adcs)
		{
	return;
		}
static void getframe_estimation_data(adcs_t *adcs)
		{
	return;
		}
static void getframe_igrf_modelled_magnetic_field_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_modelled_sun_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_estimated_gyro_bias(adcs_t *adcs)
		{
	return;
		}
static void getframe_estimation_innovation_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_quaternion_error_vector(adcs_t *adcs)
		{
	return;
		}
static void getframe_quaternion_covariance(adcs_t *adcs)
		{
	return;
		}
static void getframe_magnetorquer_command(adcs_t *adcs)
		{
	return;
		}
static void getframe_wheel_speed_commands(adcs_t *adcs)
		{
	return;
		}
static void getframe_sgp4_orbit_parameters(adcs_t *adcs)
		{
	return;
		}
static void getframe_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_status_of_image_capture_and_save_operation(adcs_t *adcs)
		{
	return;
		}
static void getframe_uploaded_program_status(adcs_t *adcs)
		{
	return;
		}
static void getframe_get_flash_program_list(adcs_t *adcs)
		{
	return;
		}
static void getframe_reset(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_unix_time(adcs_t *adcs)
		{
	return;
		}
static void getframe_adcs_run_mode(adcs_t *adcs)
		{
	return;
		}
static void getframe_selected_logged_data(adcs_t *adcs)
		{
	return;
		}
static void getframe_power_control(adcs_t *adcs)
		{
	return;
		}
static void getframe_deploy_magnetometer_boom(adcs_t *adcs)
		{
	return;
		}
static void getframe_trigger_adcs_loop(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_attitude_estimation_mode(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_attitude_control_mode(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_commanded_attitude_angles(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_wheel(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_magnetorquer_output(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_start_up_mode(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_sgp4_orbit_parameters(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_magnetorquer(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_wheel_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_css_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_sun_sensor_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_nadir_sensor_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_magnetometer_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_rate_sensor_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_detumbling_control_parameters(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_y_momentum_control_parameters(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_moment_of_inertia(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_estimation_parameters(adcs_t *adcs)
		{
	return;
		}
static void getframe_save_configuration(adcs_t *adcs)
		{
	return;
		}
static void getframe_save_orbit_parameters(adcs_t *adcs)
		{
	return;
		}
static void getframe_capture_and_save_image(adcs_t *adcs)
		{
	return;
		}
static void getframe_reset_file_list(adcs_t *adcs)
		{
	return;
		}
static void getframe_advance_file_list_index(adcs_t *adcs)
		{
	return;
		}
static void getframe_initialize_file_download(adcs_t *adcs)
		{
	return;
		}
static void getframe_advance_file_read_pointer(adcs_t *adcs)
		{
	return;
		}
static void getframe_erase_file(adcs_t *adcs)
		{
	return;
		}
static void getframe_erase_all_files(adcs_t *adcs)
		{
	return;
		}
static void getframe_set_boot_index(adcs_t *adcs)
		{
	return;
		}
static void getframe_erase_program(adcs_t *adcs)
		{
	return;
		}
static void getframe_upload_program_block(adcs_t *adcs)
		{
	return;
		}
static void getframe_finalize_program_upload(adcs_t *adcs)
		{
	return;
		}
static void setframe_identification(adcs_t *adcs, adcs_identification_t *arg )
		{
	return;
		}
static void setframe_communication_status(adcs_t *adcs, adcs_communication_status_t *arg )
		{
	return;
		}
static void setframe_telecommand_acknowledge(adcs_t *adcs, adcs_telecommand_acknowledge_t *arg )
		{
	return;
		}
static void setframe_reset_cause(adcs_t *adcs, adcs_reset_cause_t *arg )
		{
	return;
		}*
static void setframe_actuator_commands(adcs_t *adcs, adcs_actuator_commands_t *arg )
		{
	return;
		}
static void setframe_acp_execution_state(adcs_t *adcs, adcs_acp_execution_state_t *arg )
		{
	return;
		}
static void setframe_acp_execution_times(adcs_t *adcs, adcs_acp_execution_times_t *arg )
		{
	return;
		}
static void setframe_edac_and_latchup_counters(adcs_t *adcs, adcs_edac_and_latchup_counters_t *arg )
		{
	return;
		}
static void setframe_start_up_mode(adcs_t *adcs, adcs_start_up_mode_t *arg )
		{
	return;
		}
static void setframe_file_information(adcs_t *adcs, adcs_file_information_t *arg )
		{
	return;
		}
static void setframe_file_block_crc(adcs_t *adcs, adcs_file_block_crc_t *arg )
		{
	return;
		}
static void setframe_file_data_block(adcs_t *adcs, adcs_file_data_block_t *arg )
		{
	return;
		}
static void setframe_power_control_selection(adcs_t *adcs, adcs_power_control_selection_t *arg )
		{
	return;
		}
static void setframe_power_and_temperature_measurements(adcs_t *adcs, adcs_power_and_temperature_measurements_t *arg )
		{
	return;
		}
static void setframe_adcs_state(adcs_t *adcs, adcs_adcs_state_t *arg )
		{
	return;
		}
static void setframe_adcs_measurements(adcs_t *adcs, adcs_adcs_measurements_t *arg )
		{
	return;
		}
static void setframe_current_time(adcs_t *adcs, adcs_current_time_t *arg )
		{
	return;
		}
static void setframe_current_state(adcs_t *adcs, adcs_current_state_t *arg )
		{
	return;
		}
static void setframe_estimated_attitude_angles(adcs_t *adcs, adcs_estimated_attitude_angles_t *arg )
		{
	return;
		}
static void setframe_estimated_angular_rates(adcs_t *adcs, adcs_estimated_angular_rates_t *arg )
		{
	return;
		}
static void setframe_satellite_position_llh(adcs_t *adcs, adcs_satellite_position_llh_t *arg )
		{
	return;
		}
static void setframe_satellite_velocity_eci(adcs_t *adcs, adcs_satellite_velocity_eci_t *arg )
		{
	return;
		}
static void setframe_magnetic_field_vector(adcs_t *adcs, adcs_magnetic_field_vector_t *arg )
		{
	return;
		}
static void setframe_coarse_sun_vector(adcs_t *adcs, adcs_coarse_sun_vector_t *arg )
		{
	return;
		}
static void setframe_fine_sun_vector(adcs_t *adcs, adcs_fine_sun_vector_t *arg )
		{
	return;
		}
static void setframe_nadir_vector(adcs_t *adcs, adcs_nadir_vector_t *arg )
		{
	return;
		}
static void setframe_rate_sensor_rates(adcs_t *adcs, adcs_rate_sensor_rates_t *arg )
		{
	return;
		}
static void setframe_wheel_speed(adcs_t *adcs, adcs_wheel_speed_t *arg )
		{
	return;
		}
static void setframe_cubesense_current(adcs_t *adcs, adcs_cubesense_current_t *arg )
		{
	return;
		}
static void setframe_cubecontrol_current(adcs_t *adcs, adcs_cubecontrol_current_t *arg )
		{
	return;
		}
static void setframe_peripheral_current_and_temperature_measurements(adcs_t *adcs, adcs_peripheral_current_and_temperature_measurements_t *arg )
		{
	return;
		}
static void setframe_raw_sensor_measurements(adcs_t *adcs, adcs_raw_sensor_measurements_t *arg )
		{
	return;
		}
static void setframe_angular_rate_covariance(adcs_t *adcs, adcs_angular_rate_covariance_t *arg )
		{
	return;
		}
static void setframe_raw_nadir_sensor(adcs_t *adcs, adcs_raw_nadir_sensor_t *arg )
		{
	return;
		}
static void setframe_raw_sun_sensor(adcs_t *adcs, adcs_raw_sun_sensor_t *arg )
		{
	return;
		}
static void setframe_raw_css(adcs_t *adcs, adcs_raw_css_t *arg )
		{
	return;
		}
static void setframe_raw_magnetometer(adcs_t *adcs, adcs_raw_magnetometer_t *arg )
		{
	return;
		}
static void setframe_raw_gps_status(adcs_t *adcs, adcs_raw_gps_status_t *arg )
		{
	return;
		}
static void setframe_raw_gps_time(adcs_t *adcs, adcs_raw_gps_time_t *arg )
		{
	return;
		}
static void setframe_raw_gps_x(adcs_t *adcs, adcs_raw_gps_x_t *arg )
		{
	return;
		}
static void setframe_raw_gps_y(adcs_t *adcs, adcs_raw_gps_y_t *arg )
		{
	return;
		}
static void setframe_raw_gps_z(adcs_t *adcs, adcs_raw_gps_z_t *arg )
		{
	return;
		}
static void setframe_estimation_data(adcs_t *adcs, adcs_estimation_data_t *arg )
		{
	return;
		}
static void setframe_igrf_modelled_magnetic_field_vector(adcs_t *adcs, adcs_igrf_modelled_magnetic_field_vector_t *arg )
		{
	return;
		}
static void setframe_modelled_sun_vector(adcs_t *adcs, adcs_modelled_sun_vector_t *arg )
		{
	return;
		}
static void setframe_estimated_gyro_bias(adcs_t *adcs, adcs_estimated_gyro_bias_t *arg )
		{
	return;
		}
static void setframe_estimation_innovation_vector(adcs_t *adcs, adcs_estimation_innovation_vector_t *arg )
		{
	return;
		}
static void setframe_quaternion_error_vector(adcs_t *adcs, adcs_quaternion_error_vector_t *arg )
		{
	return;
		}
static void setframe_quaternion_covariance(adcs_t *adcs, adcs_quaternion_covariance_t *arg )
		{
	return;
		}
static void setframe_magnetorquer_command(adcs_t *adcs, adcs_magnetorquer_command_t *arg )
		{
	return;
		}
static void setframe_wheel_speed_commands(adcs_t *adcs, adcs_wheel_speed_commands_t *arg )
		{
	return;
		}
static void setframe_sgp4_orbit_parameters(adcs_t *adcs, adcs_sgp4_orbit_parameters_t *arg )
		{
	return;
		}
static void setframe_configuration(adcs_t *adcs, adcs_configuration_t *arg )
		{
	return;
		}
static void setframe_status_of_image_capture_and_save_operation(adcs_t *adcs, adcs_status_of_image_capture_and_save_operation_t *arg )
		{
	return;
		}
static void setframe_uploaded_program_status(adcs_t *adcs, adcs_uploaded_program_status_t *arg )
		{
	return;
		}
static void setframe_get_flash_program_list(adcs_t *adcs, adcs_get_flash_program_list_t *arg )
		{
	return;
		}
static void setframe_reset(adcs_t *adcs, adcs_reset_t *arg )
		{
	return;
		}
static void setframe_set_unix_time(adcs_t *adcs, adcs_set_unix_time_t *arg )
		{
	return;
		}
static void setframe_adcs_run_mode(adcs_t *adcs, adcs_adcs_run_mode_t *arg )
		{
	return;
		}
static void setframe_selected_logged_data(adcs_t *adcs, adcs_selected_logged_data_t *arg )
		{
	return;
		}
static void setframe_power_control(adcs_t *adcs, adcs_power_control_t *arg )
		{
	return;
		}
static void setframe_deploy_magnetometer_boom(adcs_t *adcs, adcs_deploy_magnetometer_boom_t *arg )
		{
	return;
		}
static void setframe_trigger_adcs_loop(adcs_t *adcs, adcs_trigger_adcs_loop_t *arg )
		{
	return;
		}
static void setframe_set_attitude_estimation_mode(adcs_t *adcs, adcs_set_attitude_estimation_mode_t *arg )
		{
	return;
		}
static void setframe_set_attitude_control_mode(adcs_t *adcs, adcs_set_attitude_control_mode_t *arg )
		{
	return;
		}
static void setframe_set_commanded_attitude_angles(adcs_t *adcs, adcs_set_commanded_attitude_angles_t *arg )
		{
	return;
		}
static void setframe_set_wheel(adcs_t *adcs, adcs_set_wheel_t *arg )
		{
	return;
		}
static void setframe_set_magnetorquer_output(adcs_t *adcs, adcs_set_magnetorquer_output_t *arg )
		{
	return;
		}
static void setframe_set_start_up_mode(adcs_t *adcs, adcs_set_start_up_mode_t *arg )
		{
	return;
		}
static void setframe_set_sgp4_orbit_parameters(adcs_t *adcs, adcs_set_sgp4_orbit_parameters_t *arg )
		{
	return;
		}
static void setframe_set_configuration(adcs_t *adcs, adcs_set_configuration_t *arg )
		{
	return;
		}
static void setframe_set_magnetorquer(adcs_t *adcs, adcs_set_magnetorquer_t *arg )
		{
	return;
		}
static void setframe_set_wheel_configuration(adcs_t *adcs, adcs_set_wheel_configuration_t *arg )
		{
	return;
		}
static void setframe_set_css_configuration(adcs_t *adcs, adcs_set_css_configuration_t *arg )
		{
	return;
		}
static void setframe_set_sun_sensor_configuration(adcs_t *adcs, adcs_set_sun_sensor_configuration_t *arg )
		{
	return;
		}
static void setframe_set_nadir_sensor_configuration(adcs_t *adcs, adcs_set_nadir_sensor_configuration_t *arg )
		{
	return;
		}
static void setframe_set_magnetometer_configuration(adcs_t *adcs, adcs_set_magnetometer_configuration_t *arg )
		{
	return;
		}
static void setframe_set_rate_sensor_configuration(adcs_t *adcs, adcs_set_rate_sensor_configuration_t *arg )
		{
	return;
		}
static void setframe_set_detumbling_control_parameters(adcs_t *adcs, adcs_set_detumbling_control_parameters_t *arg )
		{
	return;
		}
static void setframe_set_y_momentum_control_parameters(adcs_t *adcs, adcs_set_y_momentum_control_parameters_t *arg )
		{
	return;
		}
static void setframe_set_moment_of_inertia(adcs_t *adcs, adcs_set_moment_of_inertia_t *arg )
		{
	return;
		}
static void setframe_set_estimation_parameters(adcs_t *adcs, adcs_set_estimation_parameters_t *arg )
		{
	return;
		}
static void setframe_save_configuration(adcs_t *adcs, adcs_save_configuration_t *arg )
		{
	return;
		}
static void setframe_save_orbit_parameters(adcs_t *adcs, adcs_save_orbit_parameters_t *arg )
		{
	return;
		}
static void setframe_capture_and_save_image(adcs_t *adcs, adcs_capture_and_save_image_t *arg )
		{
	return;
		}
static void setframe_reset_file_list(adcs_t *adcs, adcs_reset_file_list_t *arg )
		{
	return;
		}
static void setframe_advance_file_list_index(adcs_t *adcs, adcs_advance_file_list_index_t *arg )
		{
	return;
		}
static void setframe_initialize_file_download(adcs_t *adcs, adcs_initialize_file_download_t *arg )
		{
	return;
		}
static void setframe_advance_file_read_pointer(adcs_t *adcs, adcs_advance_file_read_pointer_t *arg )
		{
	return;
		}
static void setframe_erase_file(adcs_t *adcs, adcs_erase_file_t *arg )
		{
	return;
		}
static void setframe_erase_all_files(adcs_t *adcs, adcs_erase_all_files_t *arg )
		{
	return;
		}
static void setframe_set_boot_index(adcs_t *adcs, adcs_set_boot_index_t *arg )
		{
	return;
		}
static void setframe_erase_program(adcs_t *adcs, adcs_erase_program_t *arg )
		{
	return;
		}
static void setframe_upload_program_block(adcs_t *adcs, adcs_upload_program_block_t *arg )
		{
	return;
		}
static void setframe_finalize_program_upload(adcs_t *adcs, adcs_finalize_program_upload_t *arg )
		{
	return;
		}
#endif
***********************************************************************
 Initialization
***********************************************************************
void initialize_adcs_lpc_local( adcs_t *adcs )
{
	DEV_ASSERT( adcs );

	extern void initialize_adcs_( adcs_t* );
	initialize_adcs_( adcs );

#ifndef __LPC17XX__
	adcs->getframe_identification = getframe_identification;
	adcs->getframe_communication_status = getframe_communication_status;
	adcs->getframe_telecommand_acknowledge = getframe_telecommand_acknowledge;
	//adcs->getframe_reset_cause = getframe_reset_cause;
	adcs->getframe_actuator_commands = getframe_actuator_commands;
	adcs->getframe_acp_execution_state = getframe_acp_execution_state;
	adcs->getframe_acp_execution_times = getframe_acp_execution_times;
	adcs->getframe_edac_and_latchup_counters = getframe_edac_and_latchup_counters;
	adcs->getframe_start_up_mode = getframe_start_up_mode;
	adcs->getframe_file_information = getframe_file_information;
	adcs->getframe_file_block_crc = getframe_file_block_crc;
	adcs->getframe_file_data_block = getframe_file_data_block;
	adcs->getframe_power_control_selection = getframe_power_control_selection;
	adcs->getframe_power_and_temperature_measurements = getframe_power_and_temperature_measurements;
#endif
	adcs->getframe_adcs_state = getframe_adcs_state;
	adcs->detumble_command = &detumble_command;
#ifndef __LPC17XX__
	adcs->getframe_adcs_measurements = getframe_adcs_measurements;
	adcs->getframe_current_time = getframe_current_time;
	adcs->getframe_current_state = getframe_current_state;
	adcs->getframe_estimated_attitude_angles = getframe_estimated_attitude_angles;
	adcs->getframe_estimated_angular_rates = getframe_estimated_angular_rates;
	adcs->getframe_satellite_position_llh = getframe_satellite_position_llh;
	adcs->getframe_satellite_velocity_eci = getframe_satellite_velocity_eci;
	adcs->getframe_magnetic_field_vector = getframe_magnetic_field_vector;
	adcs->getframe_coarse_sun_vector = getframe_coarse_sun_vector;
	adcs->getframe_fine_sun_vector = getframe_fine_sun_vector;
	adcs->getframe_nadir_vector = getframe_nadir_vector;
	adcs->getframe_rate_sensor_rates = getframe_rate_sensor_rates;
	adcs->getframe_wheel_speed = getframe_wheel_speed;
	adcs->getframe_cubesense_current = getframe_cubesense_current;
	adcs->getframe_cubecontrol_current = getframe_cubecontrol_current;
	adcs->getframe_peripheral_current_and_temperature_measurements = getframe_peripheral_current_and_temperature_measurements;
	adcs->getframe_raw_sensor_measurements = getframe_raw_sensor_measurements;
	adcs->getframe_angular_rate_covariance = getframe_angular_rate_covariance;
	adcs->getframe_raw_nadir_sensor = getframe_raw_nadir_sensor;
	adcs->getframe_raw_sun_sensor = getframe_raw_sun_sensor;
	adcs->getframe_raw_css = getframe_raw_css;
	adcs->getframe_raw_magnetometer = getframe_raw_magnetometer;
	adcs->getframe_raw_gps_status = getframe_raw_gps_status;
	adcs->getframe_raw_gps_time = getframe_raw_gps_time;
	adcs->getframe_raw_gps_x = getframe_raw_gps_x;
	adcs->getframe_raw_gps_y = getframe_raw_gps_y;
	adcs->getframe_raw_gps_z = getframe_raw_gps_z;
	adcs->getframe_estimation_data = getframe_estimation_data;
	adcs->getframe_igrf_modelled_magnetic_field_vector = getframe_igrf_modelled_magnetic_field_vector;
	adcs->getframe_modelled_sun_vector = getframe_modelled_sun_vector;
	adcs->getframe_estimated_gyro_bias = getframe_estimated_gyro_bias;
	adcs->getframe_estimation_innovation_vector = getframe_estimation_innovation_vector;
	adcs->getframe_quaternion_error_vector = getframe_quaternion_error_vector;
	adcs->getframe_quaternion_covariance = getframe_quaternion_covariance;
	adcs->getframe_magnetorquer_command = getframe_magnetorquer_command;
	adcs->getframe_wheel_speed_commands = getframe_wheel_speed_commands;
	adcs->getframe_sgp4_orbit_parameters = getframe_sgp4_orbit_parameters;
	adcs->getframe_configuration = getframe_configuration;
	adcs->getframe_status_of_image_capture_and_save_operation = getframe_status_of_image_capture_and_save_operation;
	adcs->getframe_uploaded_program_status = getframe_uploaded_program_status;
	adcs->getframe_get_flash_program_list = getframe_get_flash_program_list;
	*adcs->getframe_reset = getframe_reset;
	adcs->getframe_set_unix_time = getframe_set_unix_time;
	adcs->getframe_adcs_run_mode = getframe_adcs_run_mode;
	adcs->getframe_selected_logged_data = getframe_selected_logged_data;
	adcs->getframe_power_control = getframe_power_control;
	adcs->getframe_deploy_magnetometer_boom = getframe_deploy_magnetometer_boom;
	adcs->getframe_trigger_adcs_loop = getframe_trigger_adcs_loop;
	adcs->getframe_set_attitude_estimation_mode = getframe_set_attitude_estimation_mode;
	adcs->getframe_set_attitude_control_mode = getframe_set_attitude_control_mode;
	adcs->getframe_set_commanded_attitude_angles = getframe_set_commanded_attitude_angles;
	adcs->getframe_set_wheel = getframe_set_wheel;
	adcs->getframe_set_magnetorquer_output = getframe_set_magnetorquer_output;
	adcs->getframe_set_start_up_mode = getframe_set_start_up_mode;
	adcs->getframe_set_sgp4_orbit_parameters = getframe_set_sgp4_orbit_parameters;
	adcs->getframe_set_configuration = getframe_set_configuration;
	adcs->getframe_set_magnetorquer = getframe_set_magnetorquer;
	adcs->getframe_set_wheel_configuration = getframe_set_wheel_configuration;
	adcs->getframe_set_css_configuration = getframe_set_css_configuration;
	adcs->getframe_set_sun_sensor_configuration = getframe_set_sun_sensor_configuration;
	adcs->getframe_set_nadir_sensor_configuration = getframe_set_nadir_sensor_configuration;
	adcs->getframe_set_magnetometer_configuration = getframe_set_magnetometer_configuration;
	adcs->getframe_set_rate_sensor_configuration = getframe_set_rate_sensor_configuration;
	adcs->getframe_set_detumbling_control_parameters = getframe_set_detumbling_control_parameters;
	adcs->getframe_set_y_momentum_control_parameters = getframe_set_y_momentum_control_parameters;
	adcs->getframe_set_moment_of_inertia = getframe_set_moment_of_inertia;
	adcs->getframe_set_estimation_parameters = getframe_set_estimation_parameters;
	adcs->getframe_save_configuration = getframe_save_configuration;
	adcs->getframe_save_orbit_parameters = getframe_save_orbit_parameters;
	adcs->getframe_capture_and_save_image = getframe_capture_and_save_image;
	adcs->getframe_reset_file_list = getframe_reset_file_list;
	adcs->getframe_advance_file_list_index = getframe_advance_file_list_index;
	adcs->getframe_initialize_file_download = getframe_initialize_file_download;
	adcs->getframe_advance_file_read_pointer = getframe_advance_file_read_pointer;
	adcs->getframe_erase_file = getframe_erase_file;
	adcs->getframe_erase_all_files = getframe_erase_all_files;
	adcs->getframe_set_boot_index = getframe_set_boot_index;
	adcs->getframe_erase_program = getframe_erase_program;
	adcs->getframe_upload_program_block = getframe_upload_program_block;
	adcs->getframe_finalize_program_upload = getframe_finalize_program_upload;

	adcs->setframe_identification = setframe_identification;
	adcs->setframe_communication_status = setframe_communication_status;
	adcs->setframe_telecommand_acknowledge = setframe_telecommand_acknowledge;
	//adcs->setframe_reset_cause = setframe_reset_cause;
	adcs->setframe_actuator_commands = setframe_actuator_commands;
	adcs->setframe_acp_execution_state = setframe_acp_execution_state;
	adcs->setframe_acp_execution_times = setframe_acp_execution_times;
	adcs->setframe_edac_and_latchup_counters = setframe_edac_and_latchup_counters;
	adcs->setframe_start_up_mode = setframe_start_up_mode;
	adcs->setframe_file_information = setframe_file_information;
	adcs->setframe_file_block_crc = setframe_file_block_crc;
	adcs->setframe_file_data_block = setframe_file_data_block;
	adcs->setframe_power_control_selection = setframe_power_control_selection;
	adcs->setframe_power_and_temperature_measurements = setframe_power_and_temperature_measurements;
	adcs->setframe_adcs_state = setframe_adcs_state;
	adcs->setframe_adcs_measurements = setframe_adcs_measurements;
	adcs->setframe_current_time = setframe_current_time;
	adcs->setframe_current_state = setframe_current_state;
	adcs->setframe_estimated_attitude_angles = setframe_estimated_attitude_angles;
	adcs->setframe_estimated_angular_rates = setframe_estimated_angular_rates;
	adcs->setframe_satellite_position_llh = setframe_satellite_position_llh;
	adcs->setframe_satellite_velocity_eci = setframe_satellite_velocity_eci;
	adcs->setframe_magnetic_field_vector = setframe_magnetic_field_vector;
	adcs->setframe_coarse_sun_vector = setframe_coarse_sun_vector;
	adcs->setframe_fine_sun_vector = setframe_fine_sun_vector;
	adcs->setframe_nadir_vector = setframe_nadir_vector;
	adcs->setframe_rate_sensor_rates = setframe_rate_sensor_rates;
	adcs->setframe_wheel_speed = setframe_wheel_speed;
	adcs->setframe_cubesense_current = setframe_cubesense_current;
	adcs->setframe_cubecontrol_current = setframe_cubecontrol_current;
	adcs->setframe_peripheral_current_and_temperature_measurements = setframe_peripheral_current_and_temperature_measurements;
	adcs->setframe_raw_sensor_measurements = setframe_raw_sensor_measurements;
	adcs->setframe_angular_rate_covariance = setframe_angular_rate_covariance;
	adcs->setframe_raw_nadir_sensor = setframe_raw_nadir_sensor;
	adcs->setframe_raw_sun_sensor = setframe_raw_sun_sensor;
	adcs->setframe_raw_css = setframe_raw_css;
	adcs->setframe_raw_magnetometer = setframe_raw_magnetometer;
	adcs->setframe_raw_gps_status = setframe_raw_gps_status;
	adcs->setframe_raw_gps_time = setframe_raw_gps_time;
	adcs->setframe_raw_gps_x = setframe_raw_gps_x;
	adcs->setframe_raw_gps_y = setframe_raw_gps_y;
	adcs->setframe_raw_gps_z = setframe_raw_gps_z;
	adcs->setframe_estimation_data = setframe_estimation_data;
	adcs->setframe_igrf_modelled_magnetic_field_vector = setframe_igrf_modelled_magnetic_field_vector;
	adcs->setframe_modelled_sun_vector = setframe_modelled_sun_vector;
	adcs->setframe_estimated_gyro_bias = setframe_estimated_gyro_bias;
	adcs->setframe_estimation_innovation_vector = setframe_estimation_innovation_vector;
	adcs->setframe_quaternion_error_vector = setframe_quaternion_error_vector;
	adcs->setframe_quaternion_covariance = setframe_quaternion_covariance;
	adcs->setframe_magnetorquer_command = setframe_magnetorquer_command;
	adcs->setframe_wheel_speed_commands = setframe_wheel_speed_commands;
	adcs->setframe_sgp4_orbit_parameters = setframe_sgp4_orbit_parameters;
	adcs->setframe_configuration = setframe_configuration;
	adcs->setframe_status_of_image_capture_and_save_operation = setframe_status_of_image_capture_and_save_operation;
	adcs->setframe_uploaded_program_status = setframe_uploaded_program_status;
	adcs->setframe_get_flash_program_list = setframe_get_flash_program_list;*
	adcs->setframe_reset = setframe_reset;
	adcs->setframe_set_unix_time = setframe_set_unix_time;
	adcs->setframe_adcs_run_mode = setframe_adcs_run_mode;
	adcs->setframe_selected_logged_data = setframe_selected_logged_data;
	adcs->setframe_power_control = setframe_power_control;
	adcs->setframe_deploy_magnetometer_boom = setframe_deploy_magnetometer_boom;
	adcs->setframe_trigger_adcs_loop = setframe_trigger_adcs_loop;

	adcs->setframe_set_attitude_estimation_mode = setframe_set_attitude_estimation_mode;
	adcs->setframe_set_attitude_control_mode = setframe_set_attitude_control_mode;
	adcs->setframe_set_commanded_attitude_angles = setframe_set_commanded_attitude_angles;
	adcs->setframe_set_wheel = setframe_set_wheel;
	adcs->setframe_set_magnetorquer_output = setframe_set_magnetorquer_output;
	adcs->setframe_set_start_up_mode = setframe_set_start_up_mode;
	adcs->setframe_set_sgp4_orbit_parameters = setframe_set_sgp4_orbit_parameters;
	adcs->setframe_set_configuration = setframe_set_configuration;
	adcs->setframe_set_magnetorquer = setframe_set_magnetorquer;
	adcs->setframe_set_wheel_configuration = setframe_set_wheel_configuration;
	adcs->setframe_set_css_configuration = setframe_set_css_configuration;
	adcs->setframe_set_sun_sensor_configuration = setframe_set_sun_sensor_configuration;
	adcs->setframe_set_nadir_sensor_configuration = setframe_set_nadir_sensor_configuration;
	adcs->setframe_set_magnetometer_configuration = setframe_set_magnetometer_configuration;
	adcs->setframe_set_rate_sensor_configuration = setframe_set_rate_sensor_configuration;
	adcs->setframe_set_detumbling_control_parameters = setframe_set_detumbling_control_parameters;
	adcs->setframe_set_y_momentum_control_parameters = setframe_set_y_momentum_control_parameters;
	adcs->setframe_set_moment_of_inertia = setframe_set_moment_of_inertia;
	adcs->setframe_set_estimation_parameters = setframe_set_estimation_parameters;
	adcs->setframe_save_configuration = setframe_save_configuration;
	adcs->setframe_save_orbit_parameters = setframe_save_orbit_parameters;
	adcs->setframe_capture_and_save_image = setframe_capture_and_save_image;
	adcs->setframe_reset_file_list = setframe_reset_file_list;
	adcs->setframe_advance_file_list_index = setframe_advance_file_list_index;
	adcs->setframe_initialize_file_download = setframe_initialize_file_download;
	adcs->setframe_advance_file_read_pointer = setframe_advance_file_read_pointer;
	adcs->setframe_erase_file = setframe_erase_file;
	adcs->setframe_erase_all_files = setframe_erase_all_files;
	adcs->setframe_set_boot_index = setframe_set_boot_index;
	adcs->setframe_erase_program = setframe_erase_program;
	adcs->setframe_upload_program_block = setframe_upload_program_block;
	adcs->setframe_finalize_program_upload = setframe_finalize_program_upload;
#endif
}*/
