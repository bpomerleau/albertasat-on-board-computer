/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file adcs_nanomind_local
 * @author Alex Hamilton
 * @date Sept 1, 2015
 */

#ifndef LPC1769

#include <adcs/adcs.h>
#include <math.h>
#include <dev/arm/ds1302.h>
#include <portable_types.h>
#include <time.h>
#include <string.h>
#include <printing.h>



#define ADCS_START_X_RATE 0 //initial tip off in degrees per second
#define ADCS_START_Y_RATE 10 //initial tip off in degrees per second
#define ADCS_START_Z_RATE 0 //initial tip off in degrees per second

#define ADCS_ROTATION_POWER 0.01 //adcs control rate in degrees per second

double lon,lat;
double xrot=ADCS_START_X_RATE;
double yrot=ADCS_START_Y_RATE;
double zrot=ADCS_START_Z_RATE;

uint8_t adcs_modes;

//Tempororary functions for MNLP:
//*******************************************************************************************
int is_china(double lat,double lon){
	const double lata =30.747500833466347;
	const double longa=111.181640625;
	const double rada=1500;
	const double latb=38.54129088479034;
	const double longb =88.41796875;
	const double radb=1350;
	const double latc=46.313549654089094;
	const double longc=123.92578125;
	const double radc=1000;

	const double earth_radius=6371;

	double siga,sigb,sigc;

	siga=earth_radius*acos(sin(lat*M_PI/180)*sin(lata*M_PI/180)+cos(lat*M_PI/180)*cos(lata*M_PI/180)*cos(fabs((lon-longa)*M_PI/180)));
	sigb=earth_radius*acos(sin(lat*M_PI/180)*sin(latb*M_PI/180)+cos(lat*M_PI/180)*cos(latb*M_PI/180)*cos(fabs((lon-longb)*M_PI/180)));
	sigc=earth_radius*acos(sin(lat*M_PI/180)*sin(latc*M_PI/180)+cos(lat*M_PI/180)*cos(latc*M_PI/180)*cos(fabs((lon-longc)*M_PI/180)));

	if(siga<=rada||sigb<=radb||sigc<=radc) return 1;
	else return 0;

}
/*
int adcs_getframe_adcs_state (adcs_t *adcs)
{
	int stat=0;
	if(adcs_modes==4){

	}
	else if(adcs_modes==3 && adcs->adcs_state.adcs_modes==3){
		adcs->adcs_state.adcs_modes=4; //stable
		adcs_modes=4;
	}
	else{
		adcs->adcs_state.adcs_modes=adcs_modes; //detumbling
	}
	return stat;
}

int adcs_getframe_adcs_measurements (adcs_t *adcs)
{
	int stat=0;
	uint8_t buff[ADCS_ADCS_MEASUREMENTS_SIZE];
	memset(buff,0,ADCS_ADCS_MEASUREMENTS_SIZE);
	uint8_t cmd = ADCS_ADCS_MEASUREMENTS_ID;
	stat=adcs_get(cmd,buff,ADCS_ADCS_MEASUREMENTS_SIZE);
	if(stat==0) memcpy(&adcs->adcs_measurements,buff,ADCS_ADCS_MEASUREMENTS_SIZE);
	return stat;
}
int adcs_getframe_current_time(adcs_t *adcs)
{
	int stat =0;
	//Put fake times here
	static int i = 0;
	switch(i)
	{
		case 0:	adcs->current_time.unix_time = 0xd0d61c02;	break;		//Clear 0
		case 4: adcs->current_time.unix_time = 0xd0d4ca84;	break;		//Not run 5
		case 5:
		case 6: adcs->current_time.unix_time = 0xd0d55721;	break;		//Run 0.1
		case 10: adcs->current_time.unix_time = 0xd0d55740;	break;		//Not run 5
		case 11: adcs->current_time.unix_time = 0xd0d61c02;	break;		//Clear 0
		case 15: adcs->current_time.unix_time = 0xd0d76d84;	break;		//Clear 5
		case 16: adcs->current_time.unix_time = 0xd0d6a1b9;	break;		//Not run 0
		case 20:
		case 21: adcs->current_time.unix_time = 0xd0d6a1bb;	break;		//Run 5.1
		case 22: adcs->current_time.unix_time = 0xd0d6a1d6;	break;		//Not run 0
		case 26:
		case 27: adcs->current_time.unix_time = 0xd0d6a1d8;	break;  	//Run 5.2
		case 28: adcs->current_time.unix_time = 0xd0d6a212;	break;		//Not run 0
		case 32:
		case 33: adcs->current_time.unix_time = 0xd0d6a214;	break; 		//Run 5.3
		case 34:
		case 35: adcs->current_time.unix_time = 0xd0d6a33e;	break;		//Run 0.1
		case 39: adcs->current_time.unix_time = 0xd0d6a340;	break;		//Not run 5
		default: adcs->current_time.unix_time = 0x0;
	}
	adcs->current_time.unix_time += 946728000;
	adcs->adcs_state.current_unix_time = adcs->current_time.unix_time;
	i++;
	return stat;
}
*/
//*******************************************************************************************


void vTaskOrbitalProp(void *pvParameters){

	double sgn(double val);

	double cphi,sphi,ctheta,stheta;
	double theta, phi;
	const double incl = 79*M_PI/180;
	const double cincl =cos(incl);
	const double sincl =sin(incl);
	const double earth_radius = 6371; //earth radius in km
	const double alt = 400; //altitude in km
	const double mu =  	398600.4418; //standard gravitation parameter in km^3 s^-2
	const double orbital_period = 2*M_PI*sqrt(pow((earth_radius+alt),3)/mu);
	const double earth_period = 86164.0905; //length of a sidereal day in seconds
	int china=0;

	struct ds1302_clock clock;
	time_t utime;

	while(1){

		ds1302_clock_read_burst(&clock);
		ds1302_clock_to_time(&utime, &clock);
		utime= utime-973273967; //seconds from mission start (epoch adjusted)
		//utime= utime-MISSION_START_DATE-973273967;

		theta= (2*M_PI*(double)utime)/orbital_period;
		phi = (2*M_PI*(double)utime)/earth_period;

		cphi=cos(phi);
		sphi=sin(phi);
		ctheta=cos(theta);
		stheta=sin(theta);

		lon=atan((ctheta*sphi + cincl*cphi*stheta)/(ctheta*cphi - cincl*stheta*sphi))*180/M_PI;
		lat=((M_PI/2) - acos(sincl*stheta))*180/M_PI;

		if(fabs(xrot)>0||fabs(yrot)>0||fabs(zrot)>0){
			adcs_modes=2;
		}

		if(adcs_modes<=2){
			if(fabs(xrot)>fabs((double)utime*ADCS_ROTATION_POWER)){
				xrot=sgn(xrot)*(fabs(xrot)-fabs((double)utime*ADCS_ROTATION_POWER));
			}
			else{
				xrot=0;
				if(yrot==0 && zrot==0 && adcs_modes!=4) adcs_modes=3; //detumbling done
			}

			if(fabs(yrot)>fabs((double)utime*ADCS_ROTATION_POWER)){
				yrot=sgn(yrot)*(fabs(yrot)-fabs((double)utime*ADCS_ROTATION_POWER));
			}
			else{
				yrot=0;
				if(xrot==0 && zrot==0 && adcs_modes!=4) adcs_modes=3; //detumbling done
			}

			if(fabs(zrot)>fabs((double)utime*ADCS_ROTATION_POWER)){
				zrot=sgn(zrot)*(fabs(zrot)-fabs((double)utime*ADCS_ROTATION_POWER));
			}
			else{
				zrot=0;
				if(xrot==0 && yrot==0 && adcs_modes!=4) adcs_modes=3; //detumbling done
			}
		}

		//DEMO_PRINTF("mission time: %d",utime);
		//DEMO_PRINTF("lat: %f\n long: %f",lat,lon);
		//DEMO_PRINTF("xrot: %f\n yrot: %f\n zrot: %f\n",xrot,yrot,zrot);
		int is_china(double lat,double lon);

		if(is_china(lat,lon)!=china){
			if(china){
				DEMO_PRINTF("Exiting Chinese airspace");
				china=0;
			}
			else{
				DEMO_PRINTF("Entering Chinese airspace");
				china=1;
			}
		}
		vTaskDelay(1000/portTICK_RATE_MS);
	}
}

double sgn(double val){
	if(val>=0)
		return 1;
	else
		return -1;
}


#endif //LPC1769
