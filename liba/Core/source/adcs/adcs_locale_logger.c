/*
 * adcs_locale_logger.c
 *
 *  Created on: Jun 16, 2016
 *      Author: bbruner
 */

#include <adcs/adcs_locale_logger.h>
#include <logger/logger.h>
#include <core_defines.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>
#include <io/nanopower2.h>
#include <eps/power_lines.h>
#include <non_volatile/file_variable.h>

#define ADCS_LOCALE_LOGGER_COMMISSIONED		1
#define ADCS_LOCALE_LOGGER_DECOMMISSIONED	0

struct adcs_locale_logger_t
{
	uint32_t 		cadence;
	mutex_t 		cadence_lock;
	logger_t 		locale_logger;
	semaphore_t		task_enable;
	file_variable_t	commission_status;
};

struct __attribute__((packed)) adcs_locale_t
{
	uint32_t						time_stamp;
	adcs_satellite_position_llh_t 	llh_position;
	adcs_nadir_vector_t				nadir_vector;
};

static struct adcs_locale_logger_t adcs_locale_logger;
extern driver_toolkit_nanomind_t  drivers;

static void adcs_locale_logger_task( void* );

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Initialize the ADCS locale logger. The locale logger will be disabled after initialization
 * 		and can only be started with a call to adcs_locale_logger_enable( ).
 *
 * 		In addition, the locale logger is decommissioned by default. Attempts to enable it with
 * 		adcs_locale_logger_enable( ) will fail while it is decommissioned.
 * 		The locale logger must be commissioned before it can be used. Do this by calling
 * 		adcs_locale_logger_commission( ).
 *
 * 		The commissioned status of the locale logger persists through power cycles. The enable status
 * 		does not.
 *
 * 		The locale logger should be commissioned by a telecommand, commissioning should not be hard coded in the
 * 		on board computers initialization.
 * @return
 * 		<b>TRUE</b>: on successful initialization
 * 		<br><b>FALSE</b>: otherwise.
 */
bool_t init_adcs_locale_logger( )
{
	static bool_t is_init = false;

	/* Allow only one call to this function. */
	if( is_init == true ) {
		return true;
	}

	/* Set default cadence. */
	adcs_locale_logger.cadence = ADCS_LOCALE_LOGGER_DEFAULT_CADENCE;

	/* Create mutex to protect cadence variable. */
	new_mutex(adcs_locale_logger.cadence_lock);
	if( adcs_locale_logger.cadence_lock == NULL ) {
		return false;
	}

	/* Create semaphore to enable/disable locale logging task. */
	new_semaphore(adcs_locale_logger.task_enable, BINARY_SEMAPHORE, SEMAPHORE_EMPTY);
	if( adcs_locale_logger.task_enable == NULL ) {
		return false;
	}

	/* Initialize the logger for storing locale information. */
	logger_error_t lerr;
	lerr = initialize_logger(
								&adcs_locale_logger.locale_logger,
								((driver_toolkit_t*) &drivers)->fs,
								ADCS_LOCALE_LOGGER_CONTROL_FILE_PATH,
								ADCS_LOCALE_LOGGER_NAME_PATH,
								ADCS_LOCALE_LOGGER_CAPACITY
							);
	if( lerr != LOGGER_OK ) {
		return false;
	}

	/* Initialize file variable which indicates enable status of previous boot. */
	bool_t fv_err = initialize_file_variable
	(
		&adcs_locale_logger.commission_status,
		((driver_toolkit_t*) &drivers)->fs,
		ADCS_LOCALE_LOGGER_CONFIG_PATH,
		sizeof(char)
	);
	if( fv_err == false ) {
		return false;
	}

	if( xTaskCreate(adcs_locale_logger_task, ADCS_LOCALE_LOGGER_TASK_NAME, ADCS_LOCALE_LOGGER_TASK_STACK, NULL, ADCS_LOCALE_LOGGER_TASK_PRIO, NULL) != pdPASS ) {
		return false;
	}

	is_init = true;
	return true;
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Set the cadence at which ADCS locale information is logger.
 * @param
 * 		Cadence, in ms, to log ADCS locale information.
 * @return
 * 		The actual cadence, in ms, which will be used to log locale information.
 */
uint32_t adcs_locale_logger_set_cadence( uint32_t cadence )
{
	lock_mutex(adcs_locale_logger.cadence_lock, BLOCK_FOREVER);
	adcs_locale_logger.cadence = cadence;
	unlock_mutex(adcs_locale_logger.cadence_lock);
	return cadence;
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Enables the locale logging task to run. By default, the task is disabled.
 * 		This function does not turn on/off any power lines. The ADCS must be running
 * 		for meaningful data to be logged.
 *
 * 		In addition, the locale logger can only run after its been commissioned.
 * 		Use adcs_locale_logger_commission( ) to commission it.
 * @return
 * 		<b>TRUE</b>: locale logger enabled.
 * 		<br><b>FALSE</b>: otherwise. ADCS is off or locale logger is decommissioned.
 */
bool_t adcs_locale_logger_enable( )
{
	eps_hk_out_t	line_status;
	uint8_t 		adcs_3v3_status;
	uint8_t			adcs_5v0_status;
	bool_t			fv_err;
	char			commission_status;

	/* Check if locale logger has been commissioned yet. */
	fv_err = non_volatile_variable_get(&adcs_locale_logger.commission_status.s_, &commission_status);
	if( fv_err == false ) {
		/* Error in non volatile memory. */
		csp_printf("Locale logger: diag: filesystem error");
		return false;
	}
	if( commission_status == ADCS_LOCALE_LOGGER_DECOMMISSIONED ) {
		/* Locale logger is not commissioned for service, don't enable. */
		return false;
	}

	/* Get status of all power lines. */
	if( eps_hk_out_get(&line_status) != sizeof(line_status) ) {
		/* Failed to get power line status. */
		csp_printf("Locale logger: diag: TWI error");
		return false;
	}

	/* Cache power status in easier to read variables. */
	adcs_3v3_status = line_status.output[ADCS_3V3_POWER_CHANNEL];
	adcs_5v0_status = line_status.output[ADCS_5V0_POWER_CHANNEL];

	/* Check if ADCS is on. */
	if( adcs_3v3_status == POWER_CHANNEL_ON && adcs_5v0_status == POWER_CHANNEL_ON ) {
		post_semaphore(adcs_locale_logger.task_enable);
		return true;
	}

	/* ADCS is off, don't enable locale logger. */
	csp_printf("Locale logger: diag: no power");
	return false;
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Disables the locale logging task from running. This does not turn on/off any
 * 		power lines. If power lines are being turned off, then the locale logger must
 * 		be disabled before turning of the lines.
 * @return
 * 		<b>TRUE</b>: locale logger disabled.
 * 		<br><b>FALSE</b>: Otherwise. This can happen when power lines to ADCS are turned off
 * 		before disabling the locale logger.
 */
bool_t adcs_locale_logger_disable( )
{
	if(take_semaphore(adcs_locale_logger.task_enable, ADCS_LOCALE_LOGGER_DISABLE_TIMEOUT) == SEMAPHORE_ACQUIRED ) {
		return true;
	}
	return false;
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Query if locale logger is enabled.
 * @return
 * 		<b>true</b>: when enabled
 * 		<b>false</b>: otherwise
 */
bool_t adcs_locale_logger_is_enabled( )
{
	if( peek_semaphore(adcs_locale_logger.task_enable, USE_POLLING) == SEMAPHORE_AVAILABLE ) {
		return true;
	}
	return false;
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Used to commission the locale logger. Calls to adcs_locale_logger_enable( ) will fail
 * 		if the locale logger hasn't been commissioned. By default, the locale logger is decommissioned.
 * 		The commission status of the locale logger persists through power cycles.
 * @return
 * 		<b>true</b>: when successfully commissioned.
 * 		<br><b>false</b>: otherwise, error setting commission status flag in non volatile memory.
 */
bool_t adcs_locale_logger_commission( )
{
	char commission_status = ADCS_LOCALE_LOGGER_COMMISSIONED;
	return non_volatile_variable_set(&adcs_locale_logger.commission_status.s_, &commission_status);
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Used to decommission and disable the locale logger. Calls to adcs_locale_logger_enable( ) will
 * 		failed while the locale logger is decommissioned. Commission status persists through power cycles.
 * @return
 * 		<b>true</b> when successfully decommissioned
 * 		<br><b>false</b>: otherwise, error setting commission status flag in non volatile memory.
 */
bool_t adcs_locale_logger_decommission( )
{
	char commission_status = ADCS_LOCALE_LOGGER_DECOMMISSIONED;
	bool_t status = non_volatile_variable_set(&adcs_locale_logger.commission_status.s_, &commission_status);
	adcs_locale_logger_disable( );
	return status;
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		Query if the locale logger is commissioned.
 * @return
 * 		<b>true</b>: when commissioned
 * 		<b>false</b>: when decommissioned
 */
bool_t adcs_locale_logger_is_commissioned( )
{
	bool_t			fv_err;
	char			commission_status;

	/* Check if locale logger has been commissioned. */
	fv_err = non_volatile_variable_get(&adcs_locale_logger.commission_status.s_, &commission_status);
	if( fv_err == false ) {
		/* Error in non volatile memory. */
		return false;
	}
	if( commission_status == ADCS_LOCALE_LOGGER_DECOMMISSIONED ) {
		/* Locale logger is not commissioned . */
		return false;
	}
	return true;
}

/**
 * @memberof struct adcs_locale_logger_t
 * @details
 * 		This is the task which logs locale information to permenant storage.
 * @param param
 */
static void adcs_locale_logger_task( void* param )
{
	rtc_t*							rtc = ((driver_toolkit_t*) &drivers)->rtc;
	uint32_t 						time_origin = task_time_elapsed( );
	struct adcs_locale_t			locale;
	struct adcs_location_vectors_t	vecs;
	file_t*							log_file;
	logger_error_t 					lerr;
	uint32_t 						written;
	fs_error_t						ferr;
	uint32_t						cadence;

	for( ;; ) {

		/* Wait until the local logging task is enabled before continuing. */
		if( peek_semaphore(adcs_locale_logger.task_enable, BLOCK_FOREVER) == SEMAPHORE_AVAILABLE ) {

			/* Get time stamp. */
			locale.time_stamp = rtc->seconds_since_epoch(rtc, UNIX_EPOCH);

			/* Get LLH and Nadir. */
			adcs_get_location_vectors(&vecs);
			locale.llh_position = vecs.llh_position;
			locale.nadir_vector = vecs.nadir_vector;
//			/* Get LLH position. */
//			adcs->getframe_satellite_position_llh(adcs);
//			locale.llh_position = adcs->satellite_position_llh;
//
//			/* Get Nadir vector. */
//			adcs->getframe_nadir_vector(adcs);
//			locale.nadir_vector = adcs->nadir_vector;

			/* Now we have our locale information, log it to memory. */
			log_file = logger_peek_head(&adcs_locale_logger.locale_logger, &lerr);
			if( lerr == LOGGER_EMPTY ) {
				/* Nothing in logger, insert a blank file to work off of. */
				log_file = logger_insert(&adcs_locale_logger.locale_logger, &lerr, NULL);
			}
			if( lerr == LOGGER_OK ) {
				uint32_t size = log_file->size(log_file, &ferr);
				if( size >= ADCS_LOCALE_LOGGER_MAX_FILE_SIZE ) {
					/* file contains maximum amount of data. Insert a new blank file into the logger to work with. */
					log_file->close(log_file);
					log_file = logger_insert(&adcs_locale_logger.locale_logger, &lerr, NULL);
				}
			}
			if( lerr == LOGGER_OK ) {
				/* Write to log_file. */
				log_file->write(log_file, (uint8_t*) &locale, sizeof(locale), &written);
				log_file->close(log_file);
			}
		}

		/* Wait the required cadence before logging again. */
		lock_mutex(adcs_locale_logger.cadence_lock, BLOCK_FOREVER);
		cadence = adcs_locale_logger.cadence;
		unlock_mutex(adcs_locale_logger.cadence_lock);
		task_delay_until(time_origin, cadence);
		time_origin = task_time_elapsed( );
	}
}
