/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file adcs_generic
 * @author Brendan Bruner
 * @date Mar 12, 2015
 */

#include <adcs/adcs.h>
#include <portable_types.h>
#include <core_defines.h>

/************************************************************************/
/* Global Defines														*/
/************************************************************************/
char const * const adcs_mode_to_name[] = { "stable\n", "detumble\n", "idle\n" };

adcs_state_enum_t adcs_get_detumbling_state( adcs_t* self )
{
	return ADCS_Y_STEADY;
}

/**
 * @memberof adcs_t
 * @brief
 *		Turn the adcs' power supply on/off
 * @details
 * 		Turn the adcs' power supply on/off. Note, this does not turn
 * 		the adcs on/off, it turned the power running to it on/off. Therefore,
 * 		this will hard power down the adcs if used to turn it off.
 * 		Redundant calls have no effect (ie, calling this function with the
 * 		same parameters two or more times ).
 * @param eps
 * 		The eps_t object which manages the eps board.
 * @param state
 * 		<b>true</b> to turn the power on, <b>false</b> to turn the power off.
 * @returns
 * 		<b>true</b> if power was successfully turned on/off.
 * 		<br><b>false</b> if power was not successfully turned on/off.
 */
static bool_t power( adcs_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );
	bool_t status1 = 0;
	bool_t status2 = 0;

	if( state == 1)
	{
		status1 = eps->power_line( eps, ADCS_3V3_POWER_CHANNEL, EPS_POWER_LINE_ACTIVE );
		status2 = eps->power_line( eps, ADCS_5V0_POWER_CHANNEL, EPS_POWER_LINE_ACTIVE );
		if(status1 == 1 && status2 == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		status2 = eps->power_line( eps, ADCS_5V0_POWER_CHANNEL, EPS_POWER_LINE_INACTIVE );
		status1 = eps->power_line( eps, ADCS_3V3_POWER_CHANNEL, EPS_POWER_LINE_INACTIVE );
		if(status1 == 1 && status2 == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

static bool_t gps_power( adcs_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );

	if( state == 1)
	{
		return eps->power_line( eps, GPS_3V3_POWER_CHANNEL, EPS_POWER_LINE_ACTIVE );
	}
	else
	{
		return eps->power_line( eps, GPS_3V3_POWER_CHANNEL, EPS_POWER_LINE_INACTIVE );
	}
}

/********************************************************************************/
/* Destructor 																	*/
/********************************************************************************/
void initialize_adcs_( adcs_t* self )
{
	DEV_ASSERT( self );
	self->power = power;
	self->gps_power = gps_power;
}

void destroy_adcs( adcs_t *adcs )
{
	DEV_ASSERT( adcs );
}


