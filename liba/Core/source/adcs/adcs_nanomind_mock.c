/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file adcs_lpc_local
 * @author Alex Hamilton
 * @author Brendan Bruner
 * @date Feb 11, 2015
 */

#include "adcs/adcs.h"
#ifndef LPC1769
//#include "dev/i2c.h"
#include "dev/usart.h"
#include <stdio.h>
#include <string.h>
#include "csp_internal.h"
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include <driver_toolkit/driver_toolkit_nanomind.h>
#include <csp/csp.h>
#include <io/nanomind.h>
#endif

xSemaphoreHandle adcs_go;
uint8_t adcs_length;
uint8_t adcs_buf[200];
uint8_t adcs_file[256];
uint8_t adcs_conf[240];

static uint8_t adcs_blocking;
static xTimerHandle timer;
#define ADCS_BUFF_SIZE 600
static uint8_t buff[ADCS_BUFF_SIZE]; // Very important!

int adcs_get(uint8_t cmd, uint8_t *input, uint32_t size){
	(void) cmd;
	(void) input;
	(void) size;
	csp_printf("adcs_get(cmd = %" PRIu8 ");", cmd);
	return 0;
}

int adcs_getc(uint8_t* value){
	//adcs_blocking =0;
	//xTimerReset(timer,0);
	//xTimerStart(timer,0);

	uint32_t time_origin = task_time_elapsed( );

	while( (time_origin+5000) > task_time_elapsed( ) ){
		if(usart_messages_waiting(ADCS_USART_HANDLE)){
			*value=usart_getc(ADCS_USART_HANDLE);
			//xTimerStop(timer,0);
			return 0;
		}
	}
	//xTimerStop(timer,0);
	csp_printf("ADCS timeout");
	return -1;
}

void adcs_get_timeout(xTimerHandle xTimer){
	adcs_blocking = 1;
	csp_printf("ADCS timeout");
}

int adcs_set(uint8_t *output, uint32_t size){
	(void) output;
	(void) size;
	csp_printf("adcs_set(cmd = %" PRIu8 ");", output[0]);
	return 0;
}



int adcs_getframe_identification (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_IDENTIFICATION_ID;
	status=adcs_get(cmd,buff,ADCS_IDENTIFICATION_SIZE);
	if(status==0) memcpy(&adcs->identification,buff,ADCS_IDENTIFICATION_SIZE);
	return status;
}

int adcs_getframe_extended_identification (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_EXTENDED_IDENTIFICATION_ID;
	status=adcs_get(cmd,buff,ADCS_EXTENDED_IDENTIFICATION_SIZE);
	if(status==0) memcpy(&adcs->extended_identification,buff,ADCS_EXTENDED_IDENTIFICATION_SIZE);
	return status;
}

int adcs_getframe_communication_status (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_COMMUNICATION_STATUS_ID;
	status=adcs_get(cmd,buff,ADCS_COMMUNICATION_STATUS_SIZE);
	if(status==0) memcpy(&adcs->communication_status,buff,ADCS_COMMUNICATION_STATUS_SIZE);
	return status;
}

int adcs_getframe_telecommand_acknowledge (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_TELECOMMAND_ACKNOWLEDGE_ID;
	status=adcs_get(cmd,buff,ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE);
	if(status==0) memcpy(&adcs->telecommand_acknowledge,buff,ADCS_TELECOMMAND_ACKNOWLEDGE_SIZE);
	return status;
}

int adcs_getframe_actuator_commands (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ACTUATOR_COMMANDS_ID;
	status=adcs_get(cmd,buff,ADCS_ACTUATOR_COMMANDS_SIZE);
	if(status==0) memcpy(&adcs->actuator_commands,buff,ADCS_ACTUATOR_COMMANDS_SIZE);
	return status;
}

int adcs_getframe_acp_execution_state (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ACP_EXECUTION_STATE_ID;
	status=adcs_get(cmd,buff,ADCS_ACP_EXECUTION_STATE_SIZE);
	if(status==0) memcpy(&adcs->acp_execution_state,buff,ADCS_ACP_EXECUTION_STATE_SIZE);
	return status;
}

int adcs_getframe_acp_execution_times (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ACP_EXECUTION_TIMES_ID;
	status=adcs_get(cmd,buff,ADCS_ACP_EXECUTION_TIMES_SIZE);
	if(status==0) memcpy(&adcs->acp_execution_times,buff,ADCS_ACP_EXECUTION_TIMES_SIZE);
	return status;
}

int adcs_getframe_edac_and_latchup_counters (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_EDAC_AND_LATCHUP_COUNTERS_ID;
	status=adcs_get(cmd,buff,ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE);
	if(status==0) memcpy(&adcs->edac_and_latchup_counters,buff,ADCS_EDAC_AND_LATCHUP_COUNTERS_SIZE);
	return status;
}

int adcs_getframe_start_up_mode (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_START_UP_MODE_ID;
	status=adcs_get(cmd,buff,ADCS_START_UP_MODE_SIZE);
	if(status==0) memcpy(&adcs->start_up_mode,buff,ADCS_START_UP_MODE_SIZE);
	return status;
}

int adcs_getframe_file_information (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_FILE_INFORMATION_ID;
	status=adcs_get(cmd,buff,ADCS_FILE_INFORMATION_SIZE);
	if(status==0) memcpy(&adcs->file_information,buff,ADCS_FILE_INFORMATION_SIZE);
	return status;
}

int adcs_getframe_file_block_crc (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_FILE_BLOCK_CRC_ID;
	status=adcs_get(cmd,buff,ADCS_FILE_BLOCK_CRC_SIZE);
	if(status==0) memcpy(&adcs->file_block_crc,buff,ADCS_FILE_BLOCK_CRC_SIZE);
	return status;
}

int adcs_getframe_file_data_block (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_FILE_DATA_BLOCK_ID;
	status=adcs_get(cmd,buff,ADCS_FILE_DATA_BLOCK_SIZE);
	if(status==0) memcpy(&adcs->file_data_block,buff,ADCS_FILE_DATA_BLOCK_SIZE);
	return status;
}

int adcs_getframe_power_control_selection (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_POWER_CONTROL_SELECTION_ID;
	status=adcs_get(cmd,buff,ADCS_POWER_CONTROL_SELECTION_SIZE);
	if(status==0) memcpy(&adcs->power_control_selection,buff,ADCS_POWER_CONTROL_SELECTION_SIZE);
	return status;
}

int adcs_getframe_power_and_temperature_measurements (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_ID;
	status=adcs_get(cmd,buff,ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	if(status==0) memcpy(&adcs->power_and_temperature_measurements,buff,ADCS_POWER_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	return status;
}

/**
 * @memberof adcs_t
 * @brief
 * 		Command over adcs' detumbling unit.
 * @details
 * 		Command over adcs' detumbling unit.
 * @attention
 * 		No other setup has to be done. and adcs struct can be used at any point
 * 		to command the adcs' detumbling unit. Regardless of what the adcs is/was
 * 		doing and what initialization has been done to it thus far.
 * @param command
 * 		The command to perform.
 * @returns
 * 		<b>true</b> on successful delivery of command to adcs.
 * 		<br><b>false</b> on failure to send command to adcs.
 */
static bool_t detumble_command( adcs_t* self, adcs_detumbe_command_enum_t command )
{
	DEV_ASSERT( self );

	return false;
}

//#ifndef ADCS_LOCAL
int adcs_getframe_adcs_state (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ADCS_STATE_ID;
	status=adcs_get(cmd,buff,ADCS_ADCS_STATE_SIZE);
	if(status==0) memcpy(&adcs->adcs_state,buff,ADCS_ADCS_STATE_SIZE);
	return status;
}

int adcs_getframe_adcs_measurements (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ADCS_MEASUREMENTS_ID;
	status=adcs_get(cmd,buff,ADCS_ADCS_MEASUREMENTS_SIZE);
	if(status==0) memcpy(&adcs->adcs_measurements,buff,ADCS_ADCS_MEASUREMENTS_SIZE);
	return status;
}

int adcs_getframe_current_time (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_CURRENT_TIME_ID;
	status=adcs_get(cmd,buff,ADCS_CURRENT_TIME_SIZE);
	if(status==0) memcpy(&adcs->current_time,buff,ADCS_CURRENT_TIME_SIZE);
	return status;
}

//#endif /*ADCS_LOCAL*/

int adcs_getframe_current_state (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_CURRENT_STATE_ID;
	status=adcs_get(cmd,buff,ADCS_CURRENT_STATE_SIZE);
	if(status==0) memcpy(&adcs->current_state,buff,ADCS_CURRENT_STATE_SIZE);
	return status;
}

int adcs_getframe_estimated_attitude_angles (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ESTIMATED_ATTITUDE_ANGLES_ID;
	status=adcs_get(cmd,buff,ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE);
	if(status==0) memcpy(&adcs->estimated_attitude_angles,buff,ADCS_ESTIMATED_ATTITUDE_ANGLES_SIZE);
	return status;
}

int adcs_getframe_estimated_angular_rates (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ESTIMATED_ANGULAR_RATES_ID;
	status=adcs_get(cmd,buff,ADCS_ESTIMATED_ANGULAR_RATES_SIZE);
	if(status==0) memcpy(&adcs->estimated_angular_rates,buff,ADCS_ESTIMATED_ANGULAR_RATES_SIZE);
	return status;
}

int adcs_getframe_satellite_position_llh (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_SATELLITE_POSITION_LLH_ID;
	status=adcs_get(cmd,buff,ADCS_SATELLITE_POSITION_LLH_SIZE);
	if(status==0) memcpy(&adcs->satellite_position_llh,buff,ADCS_SATELLITE_POSITION_LLH_SIZE);
	return status;
}

int adcs_getframe_satellite_velocity_eci (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_SATELLITE_VELOCITY_ECI_ID;
	status=adcs_get(cmd,buff,ADCS_SATELLITE_VELOCITY_ECI_SIZE);
	if(status==0) memcpy(&adcs->satellite_velocity_eci,buff,ADCS_SATELLITE_VELOCITY_ECI_SIZE);
	return status;
}

int adcs_getframe_magnetic_field_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_MAGNETIC_FIELD_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_MAGNETIC_FIELD_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->magnetic_field_vector,buff,ADCS_MAGNETIC_FIELD_VECTOR_SIZE);
	return status;
}

int adcs_getframe_coarse_sun_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_COARSE_SUN_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_COARSE_SUN_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->coarse_sun_vector,buff,ADCS_COARSE_SUN_VECTOR_SIZE);
	return status;
}

int adcs_getframe_fine_sun_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_FINE_SUN_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_FINE_SUN_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->fine_sun_vector,buff,ADCS_FINE_SUN_VECTOR_SIZE);
	return status;
}

int adcs_getframe_nadir_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_NADIR_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_NADIR_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->nadir_vector,buff,ADCS_NADIR_VECTOR_SIZE);
	return status;
}

int adcs_getframe_rate_sensor_rates (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RATE_SENSOR_RATES_ID;
	status=adcs_get(cmd,buff,ADCS_RATE_SENSOR_RATES_SIZE);
	if(status==0) memcpy(&adcs->rate_sensor_rates,buff,ADCS_RATE_SENSOR_RATES_SIZE);
	return status;
}

int adcs_getframe_wheel_speed (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_WHEEL_SPEED_ID;
	status=adcs_get(cmd,buff,ADCS_WHEEL_SPEED_SIZE);
	if(status==0) memcpy(&adcs->wheel_speed,buff,ADCS_WHEEL_SPEED_SIZE);
	return status;
}

int adcs_getframe_cubesense_current (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_CUBESENSE_CURRENT_ID;
	status=adcs_get(cmd,buff,ADCS_CUBESENSE_CURRENT_SIZE);
	if(status==0) memcpy(&adcs->cubesense_current,buff,ADCS_CUBESENSE_CURRENT_SIZE);
	return status;
}

int adcs_getframe_cubecontrol_current (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_CUBECONTROL_CURRENT_ID;
	status=adcs_get(cmd,buff,ADCS_CUBECONTROL_CURRENT_SIZE);
	if(status==0) memcpy(&adcs->cubecontrol_current,buff,ADCS_CUBECONTROL_CURRENT_SIZE);
	return status;
}

int adcs_getframe_peripheral_current_and_temperature_measurements (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_ID;
	status=adcs_get(cmd,buff,ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	if(status==0) memcpy(&adcs->peripheral_current_and_temperature_measurements,buff,ADCS_PERIPHERAL_CURRENT_AND_TEMPERATURE_MEASUREMENTS_SIZE);
	return status;
}

int adcs_getframe_raw_sensor_measurements (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_SENSOR_MEASUREMENTS_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_SENSOR_MEASUREMENTS_SIZE);
	if(status==0) memcpy(&adcs->raw_sensor_measurements,buff,ADCS_RAW_SENSOR_MEASUREMENTS_SIZE);
	return status;
}

int adcs_getframe_angular_rate_covariance (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ANGULAR_RATE_COVARIANCE_ID;
	status=adcs_get(cmd,buff,ADCS_ANGULAR_RATE_COVARIANCE_SIZE);
	if(status==0) memcpy(&adcs->angular_rate_covariance,buff,ADCS_ANGULAR_RATE_COVARIANCE_SIZE);
	return status;
}

int adcs_getframe_raw_nadir_sensor (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_NADIR_SENSOR_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_NADIR_SENSOR_SIZE);
	if(status==0) memcpy(&adcs->raw_nadir_sensor,buff,ADCS_RAW_NADIR_SENSOR_SIZE);
	return status;
}

int adcs_getframe_raw_sun_sensor (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_SUN_SENSOR_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_SUN_SENSOR_SIZE);
	if(status==0) memcpy(&adcs->raw_sun_sensor,buff,ADCS_RAW_SUN_SENSOR_SIZE);
	return status;
}

int adcs_getframe_raw_css (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_CSS_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_CSS_SIZE);
	if(status==0) memcpy(&adcs->raw_css,buff,ADCS_RAW_CSS_SIZE);
	return status;
}

int adcs_getframe_raw_magnetometer (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_MAGNETOMETER_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_MAGNETOMETER_SIZE);
	if(status==0) memcpy(&adcs->raw_magnetometer,buff,ADCS_RAW_MAGNETOMETER_SIZE);
	return status;
}

int adcs_getframe_raw_gps_status (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_GPS_STATUS_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_GPS_STATUS_SIZE);
	if(status==0) memcpy(&adcs->raw_gps_status,buff,ADCS_RAW_GPS_STATUS_SIZE);
	return status;
}

int adcs_getframe_raw_gps_time (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_GPS_TIME_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_GPS_TIME_SIZE);
	if(status==0) memcpy(&adcs->raw_gps_time,buff,ADCS_RAW_GPS_TIME_SIZE);
	return status;
}

int adcs_getframe_raw_gps_x (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_GPS_X_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_GPS_X_SIZE);
	if(status==0) memcpy(&adcs->raw_gps_x,buff,ADCS_RAW_GPS_X_SIZE);
	return status;
}

int adcs_getframe_raw_gps_y (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_GPS_Y_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_GPS_Y_SIZE);
	if(status==0) memcpy(&adcs->raw_gps_y,buff,ADCS_RAW_GPS_Y_SIZE);
	return status;
}

int adcs_getframe_raw_gps_z (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_RAW_GPS_Z_ID;
	status=adcs_get(cmd,buff,ADCS_RAW_GPS_Z_SIZE);
	if(status==0) memcpy(&adcs->raw_gps_z,buff,ADCS_RAW_GPS_Z_SIZE);
	return status;
}

int adcs_getframe_estimation_data (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ESTIMATION_DATA_ID;
	status=adcs_get(cmd,buff,ADCS_ESTIMATION_DATA_SIZE);
	if(status==0) memcpy(&adcs->estimation_data,buff,ADCS_ESTIMATION_DATA_SIZE);
	return status;
}

int adcs_getframe_igrf_modelled_magnetic_field_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->igrf_modelled_magnetic_field_vector,buff,ADCS_IGRF_MODELLED_MAGNETIC_FIELD_VECTOR_SIZE);
	return status;
}

int adcs_getframe_modelled_sun_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_MODELLED_SUN_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_MODELLED_SUN_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->modelled_sun_vector,buff,ADCS_MODELLED_SUN_VECTOR_SIZE);
	return status;
}

int adcs_getframe_estimated_gyro_bias (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ESTIMATED_GYRO_BIAS_ID;
	status=adcs_get(cmd,buff,ADCS_ESTIMATED_GYRO_BIAS_SIZE);
	if(status==0) memcpy(&adcs->estimated_gyro_bias,buff,ADCS_ESTIMATED_GYRO_BIAS_SIZE);
	return status;
}

int adcs_getframe_estimation_innovation_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_ESTIMATION_INNOVATION_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->estimation_innovation_vector,buff,ADCS_ESTIMATION_INNOVATION_VECTOR_SIZE);
	return status;
}

int adcs_getframe_quaternion_error_vector (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_QUATERNION_ERROR_VECTOR_ID;
	status=adcs_get(cmd,buff,ADCS_QUATERNION_ERROR_VECTOR_SIZE);
	if(status==0) memcpy(&adcs->quaternion_error_vector,buff,ADCS_QUATERNION_ERROR_VECTOR_SIZE);
	return status;
}

int adcs_getframe_quaternion_covariance (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_QUATERNION_COVARIANCE_ID;
	status=adcs_get(cmd,buff,ADCS_QUATERNION_COVARIANCE_SIZE);
	if(status==0) memcpy(&adcs->quaternion_covariance,buff,ADCS_QUATERNION_COVARIANCE_SIZE);
	return status;
}

int adcs_getframe_magnetorquer_command (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_MAGNETORQUER_COMMAND_ID;
	status=adcs_get(cmd,buff,ADCS_MAGNETORQUER_COMMAND_SIZE);
	if(status==0) memcpy(&adcs->magnetorquer_command,buff,ADCS_MAGNETORQUER_COMMAND_SIZE);
	return status;
}

int adcs_getframe_wheel_speed_commands (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_WHEEL_SPEED_COMMANDS_ID;
	status=adcs_get(cmd,buff,ADCS_WHEEL_SPEED_COMMANDS_SIZE);
	if(status==0) memcpy(&adcs->wheel_speed_commands,buff,ADCS_WHEEL_SPEED_COMMANDS_SIZE);
	return status;
}

int adcs_getframe_sgp4_orbit_parameters (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_SGP4_ORBIT_PARAMETERS_ID;
	status=adcs_get(cmd,buff,ADCS_SGP4_ORBIT_PARAMETERS_SIZE);
	if(status==0) memcpy(&adcs->sgp4_orbit_parameters,buff,ADCS_SGP4_ORBIT_PARAMETERS_SIZE);
	return status;
}

int adcs_getframe_configuration (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_CONFIGURATION_ID;
	status=adcs_get(cmd,buff,ADCS_CONFIGURATION_SIZE);
	if(status==0) memcpy(&adcs->configuration,buff,ADCS_CONFIGURATION_SIZE);
	return status;
}

int adcs_getframe_status_of_image_capture_and_save_operation (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_ID;
	status=adcs_get(cmd,buff,ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE);
	if(status==0) memcpy(&adcs->status_of_image_capture_and_save_operation,buff,ADCS_STATUS_OF_IMAGE_CAPTURE_AND_SAVE_OPERATION_SIZE);
	return status;
}

int adcs_getframe_uploaded_program_status (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_UPLOADED_PROGRAM_STATUS_ID;
	status=adcs_get(cmd,buff,ADCS_UPLOADED_PROGRAM_STATUS_SIZE);
	if(status==0) memcpy(&adcs->uploaded_program_status,buff,ADCS_UPLOADED_PROGRAM_STATUS_SIZE);
	return status;
}

int adcs_getframe_get_flash_program_list (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_GET_FLASH_PROGRAM_LIST_ID;
	status=adcs_get(cmd,buff,ADCS_GET_FLASH_PROGRAM_LIST_SIZE);
	if(status==0) memcpy(&adcs->get_flash_program_list,buff,ADCS_GET_FLASH_PROGRAM_LIST_SIZE);
	return status;
}

int adcs_getframe_boot_index (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	uint8_t cmd = ADCS_BOOT_INDEX_ID;
	status=adcs_get(cmd,buff,ADCS_BOOT_INDEX_SIZE);
	if(status==0) memcpy(&adcs->boot_index,buff,ADCS_BOOT_INDEX_SIZE);
	return status;
}
int adcs_setframe_reset (adcs_t *adcs, adcs_reset_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_RESET_ID;
	memcpy(&buff[1],frame,ADCS_RESET_SIZE);
	status=adcs_set(buff,ADCS_RESET_SIZE+1);
	return status;
}
int adcs_setframe_set_unix_time (adcs_t *adcs, adcs_set_unix_time_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_UNIX_TIME_ID;
	memcpy(&buff[1],frame,ADCS_SET_UNIX_TIME_SIZE);
	status=adcs_set(buff,ADCS_SET_UNIX_TIME_SIZE+1);
	return status;
}
int adcs_setframe_adcs_run_mode (adcs_t *adcs, adcs_adcs_run_mode_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_ADCS_RUN_MODE_ID;
	memcpy(&buff[1],frame,ADCS_ADCS_RUN_MODE_SIZE);
	status=adcs_set(buff,ADCS_ADCS_RUN_MODE_SIZE+1);
	return status;
}
int adcs_setframe_selected_logged_data (adcs_t *adcs, adcs_selected_logged_data_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SELECTED_LOGGED_DATA_ID;
	memcpy(&buff[1],frame,ADCS_SELECTED_LOGGED_DATA_SIZE);
	status=adcs_set(buff,ADCS_SELECTED_LOGGED_DATA_SIZE+1);
	return status;
}
int adcs_setframe_power_control (adcs_t *adcs, adcs_power_control_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_POWER_CONTROL_ID;
	memcpy(&buff[1],frame,ADCS_POWER_CONTROL_SIZE);
	status=adcs_set(buff,ADCS_POWER_CONTROL_SIZE+1);
	return status;
}
int adcs_setframe_deploy_magnetometer_boom (adcs_t *adcs, adcs_deploy_magnetometer_boom_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_DEPLOY_MAGNETOMETER_BOOM_ID;
	memcpy(&buff[1],frame,ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE);
	status=adcs_set(buff,ADCS_DEPLOY_MAGNETOMETER_BOOM_SIZE+1);
	return status;
}
int adcs_setframe_trigger_adcs_loop (adcs_t *adcs, adcs_trigger_adcs_loop_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_TRIGGER_ADCS_LOOP_ID;
	memcpy(&buff[1],frame,ADCS_TRIGGER_ADCS_LOOP_SIZE);
	status=adcs_set(buff,ADCS_TRIGGER_ADCS_LOOP_SIZE+1);
	return status;
}
int adcs_setframe_set_attitude_estimation_mode (adcs_t *adcs, adcs_set_attitude_estimation_mode_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_ATTITUDE_ESTIMATION_MODE_ID;
	memcpy(&buff[1],frame,ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE);
	status=adcs_set(buff,ADCS_SET_ATTITUDE_ESTIMATION_MODE_SIZE+1);
	return status;
}
int adcs_setframe_set_attitude_control_mode (adcs_t *adcs, adcs_set_attitude_control_mode_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_ATTITUDE_CONTROL_MODE_ID;
	memcpy(&buff[1],frame,ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE);
	status=adcs_set(buff,ADCS_SET_ATTITUDE_CONTROL_MODE_SIZE+1);
	return status;
}
int adcs_setframe_set_commanded_attitude_angles (adcs_t *adcs, adcs_set_commanded_attitude_angles_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_COMMANDED_ATTITUDE_ANGLES_ID;
	memcpy(&buff[1],frame,ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE);
	status=adcs_set(buff,ADCS_SET_COMMANDED_ATTITUDE_ANGLES_SIZE+1);
	return status;
}
int adcs_setframe_set_wheel (adcs_t *adcs, adcs_set_wheel_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_WHEEL_ID;
	memcpy(&buff[1],frame,ADCS_SET_WHEEL_SIZE);
	status=adcs_set(buff,ADCS_SET_WHEEL_SIZE+1);
	return status;
}
int adcs_setframe_set_magnetorquer_output (adcs_t *adcs, adcs_set_magnetorquer_output_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_MAGNETORQUER_OUTPUT_ID;
	memcpy(&buff[1],frame,ADCS_SET_MAGNETORQUER_OUTPUT_SIZE);
	status=adcs_set(buff,ADCS_SET_MAGNETORQUER_OUTPUT_SIZE+1);
	return status;
}
int adcs_setframe_set_start_up_mode (adcs_t *adcs, adcs_set_start_up_mode_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_START_UP_MODE_ID;
	memcpy(&buff[1],frame,ADCS_SET_START_UP_MODE_SIZE);
	status=adcs_set(buff,ADCS_SET_START_UP_MODE_SIZE+1);
	return status;
}
int adcs_setframe_set_sgp4_orbit_parameters (adcs_t *adcs, adcs_set_sgp4_orbit_parameters_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_SGP4_ORBIT_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE);
	status=adcs_set(buff,ADCS_SET_SGP4_ORBIT_PARAMETERS_SIZE+1);
	return status;
}
int adcs_setframe_set_configuration (adcs_t *adcs, adcs_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_set_magnetorquer (adcs_t *adcs, adcs_set_magnetorquer_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_MAGNETORQUER_ID;
	memcpy(&buff[1],frame,ADCS_SET_MAGNETORQUER_SIZE);
	status=adcs_set(buff,ADCS_SET_MAGNETORQUER_SIZE+1);
	return status;
}
int adcs_setframe_set_wheel_configuration (adcs_t *adcs, adcs_set_wheel_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_WHEEL_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_WHEEL_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_SET_WHEEL_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_set_css_configuration (adcs_t *adcs, adcs_set_css_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_CSS_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_CSS_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_SET_CSS_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_set_sun_sensor_configuration (adcs_t *adcs, adcs_set_sun_sensor_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_SUN_SENSOR_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_SET_SUN_SENSOR_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_set_nadir_sensor_configuration (adcs_t *adcs, adcs_set_nadir_sensor_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_NADIR_SENSOR_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_SET_NADIR_SENSOR_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_set_magnetometer_configuration (adcs_t *adcs, adcs_set_magnetometer_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_MAGNETOMETER_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_SET_MAGNETOMETER_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_set_rate_sensor_configuration (adcs_t *adcs, adcs_set_rate_sensor_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_RATE_SENSOR_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_SET_RATE_SENSOR_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_set_detumbling_control_parameters (adcs_t *adcs, adcs_set_detumbling_control_parameters_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE);
	status=adcs_set(buff,ADCS_SET_DETUMBLING_CONTROL_PARAMETERS_SIZE+1);
	return status;
}
int adcs_setframe_set_y_momentum_control_parameters (adcs_t *adcs, adcs_set_y_momentum_control_parameters_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE);
	status=adcs_set(buff,ADCS_SET_Y_MOMENTUM_CONTROL_PARAMETERS_SIZE+1);
	return status;
}
int adcs_setframe_set_moment_of_inertia (adcs_t *adcs, adcs_set_moment_of_inertia_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_MOMENT_OF_INERTIA_ID;
	memcpy(&buff[1],frame,ADCS_SET_MOMENT_OF_INERTIA_SIZE);
	status=adcs_set(buff,ADCS_SET_MOMENT_OF_INERTIA_SIZE+1);
	return status;
}
int adcs_setframe_set_estimation_parameters (adcs_t *adcs, adcs_set_estimation_parameters_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_ESTIMATION_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SET_ESTIMATION_PARAMETERS_SIZE);
	status=adcs_set(buff,ADCS_SET_ESTIMATION_PARAMETERS_SIZE+1);
	return status;
}
int adcs_setframe_save_configuration (adcs_t *adcs, adcs_save_configuration_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SAVE_CONFIGURATION_ID;
	memcpy(&buff[1],frame,ADCS_SAVE_CONFIGURATION_SIZE);
	status=adcs_set(buff,ADCS_SAVE_CONFIGURATION_SIZE+1);
	return status;
}
int adcs_setframe_save_orbit_parameters (adcs_t *adcs, adcs_save_orbit_parameters_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SAVE_ORBIT_PARAMETERS_ID;
	memcpy(&buff[1],frame,ADCS_SAVE_ORBIT_PARAMETERS_SIZE);
	status=adcs_set(buff,ADCS_SAVE_ORBIT_PARAMETERS_SIZE+1);
	return status;
}
int adcs_setframe_capture_and_save_image (adcs_t *adcs, adcs_capture_and_save_image_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_CAPTURE_AND_SAVE_IMAGE_ID;
	memcpy(&buff[1],frame,ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE);
	status=adcs_set(buff,ADCS_CAPTURE_AND_SAVE_IMAGE_SIZE+1);
	return status;
}
int adcs_setframe_reset_file_list (adcs_t *adcs, adcs_reset_file_list_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_RESET_FILE_LIST_ID;
	memcpy(&buff[1],frame,ADCS_RESET_FILE_LIST_SIZE);
	status=adcs_set(buff,ADCS_RESET_FILE_LIST_SIZE+1);
	return status;
}
int adcs_setframe_advance_file_list_index (adcs_t *adcs, adcs_advance_file_list_index_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_ADVANCE_FILE_LIST_INDEX_ID;
	memcpy(&buff[1],frame,ADCS_ADVANCE_FILE_LIST_INDEX_SIZE);
	status=adcs_set(buff,ADCS_ADVANCE_FILE_LIST_INDEX_SIZE+1);
	return status;
}
int adcs_setframe_initialize_file_download (adcs_t *adcs, adcs_initialize_file_download_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_INITIALIZE_FILE_DOWNLOAD_ID;
	memcpy(&buff[1],frame,ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE);
	status=adcs_set(buff,ADCS_INITIALIZE_FILE_DOWNLOAD_SIZE+1);
	return status;
}
int adcs_setframe_advance_file_read_pointer (adcs_t *adcs, adcs_advance_file_read_pointer_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_ADVANCE_FILE_READ_POINTER_ID;
	memcpy(&buff[1],frame,ADCS_ADVANCE_FILE_READ_POINTER_SIZE);
	status=adcs_set(buff,ADCS_ADVANCE_FILE_READ_POINTER_SIZE+1);
	return status;
}
int adcs_setframe_erase_file (adcs_t *adcs, adcs_erase_file_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_ERASE_FILE_ID;
	memcpy(&buff[1],frame,ADCS_ERASE_FILE_SIZE);
	status=adcs_set(buff,ADCS_ERASE_FILE_SIZE+1);
	return status;
}
int adcs_setframe_erase_all_files (adcs_t *adcs, adcs_erase_all_files_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_ERASE_ALL_FILES_ID;
	memcpy(&buff[1],frame,ADCS_ERASE_ALL_FILES_SIZE);
	status=adcs_set(buff,ADCS_ERASE_ALL_FILES_SIZE+1);
	return status;
}
int adcs_setframe_set_boot_index (adcs_t *adcs, adcs_set_boot_index_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_SET_BOOT_INDEX_ID;
	memcpy(&buff[1],frame,ADCS_SET_BOOT_INDEX_SIZE);
	status=adcs_set(buff,ADCS_SET_BOOT_INDEX_SIZE+1);
	return status;
}
int adcs_setframe_erase_program (adcs_t *adcs, adcs_erase_program_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_ERASE_PROGRAM_ID;
	memcpy(&buff[1],frame,ADCS_ERASE_PROGRAM_SIZE);
	status=adcs_set(buff,ADCS_ERASE_PROGRAM_SIZE+1);
	return status;
}
int adcs_setframe_upload_program_block (adcs_t *adcs, adcs_upload_program_block_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_UPLOAD_PROGRAM_BLOCK_ID;
	memcpy(&buff[1],frame,ADCS_UPLOAD_PROGRAM_BLOCK_SIZE);
	status=adcs_set(buff,ADCS_UPLOAD_PROGRAM_BLOCK_SIZE+1);
	return status;
}
int adcs_setframe_finalize_program_upload (adcs_t *adcs, adcs_finalize_program_upload_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_FINALIZE_PROGRAM_UPLOAD_ID;
	memcpy(&buff[1],frame,ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE);
	status=adcs_set(buff,ADCS_FINALIZE_PROGRAM_UPLOAD_SIZE+1);
	return status;
}

int adcs_setframe_reset_mcu (adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_RESET_MCU_ID;
	//memcpy(&buff[1],frame,ADCS_RESET_MCU_SIZE);
	status=adcs_set(buff,ADCS_RESET_MCU_SIZE+1);
	return status;
}

int adcs_setframe_run_program_now(adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_RUN_PROGRAM_NOW_ID;
	//memcpy(&buff[1],frame,ADCS_RUN_PROGRAM_NOW_SIZE);
	status=adcs_set(buff,ADCS_RUN_PROGRAM_NOW_SIZE+1);
	return status;
}

int adcs_setframe_copy_to_internal_flash(adcs_t *adcs, adcs_copy_to_internal_flash_t *frame)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_COPY_TO_INTERNAL_FLASH_ID;
	memcpy(&buff[1],frame,ADCS_COPY_TO_INTERNAL_FLASH_SIZE);
	status=adcs_set(buff,ADCS_COPY_TO_INTERNAL_FLASH_SIZE+1);
	return status;
}

int adcs_setframe_reset_boot_statistics(adcs_t *adcs)
{
	int status=0;
	memset(buff, 0, ADCS_BUFF_SIZE); // clear the static buffer
	buff[0]=ADCS_RESET_BOOT_STATISTICS_ID;
	//memcpy(&buff[1],frame,ADCS_RESET_BOOT_STATISTICS_SIZE);
	status=adcs_set(buff,ADCS_RESET_BOOT_STATISTICS_SIZE+1);
	return status;
}

void ADCS_callback(uint8_t length, uint8_t* data)
{
	memset(adcs_buf, 0, 200);
	memcpy(adcs_buf, data, length);
	adcs_length = length;
	csp_printf("CMD: %d", adcs_buf[0]);
	xSemaphoreGive(adcs_go);
}

static semaphore_t fetch_location_vectors;
static queue_t location_vectors_queue;

void adcs_get_location_vectors( struct adcs_location_vectors_t* vecs )
{
	post_semaphore(fetch_location_vectors);
	xQueueReceive(location_vectors_queue, vecs, BLOCK_FOREVER);
}

void ADCS_server(void* args)
{
	new_semaphore(fetch_location_vectors, BINARY_SEMAPHORE, SEMAPHORE_EMPTY);
	struct adcs_location_vectors_t vecs;
	location_vectors_queue = xQueueCreate(3, sizeof(struct adcs_location_vectors_t));
	vSemaphoreCreateBinary(adcs_go);
	take_semaphore( adcs_go, BLOCK_FOREVER );
	driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;
	uint16_t crc_result;
	int error;
	csp_printf("ADCS server started.");
	for(;;)
	{
		/* 150 ms cadence. */
		task_delay(150);

		/* DER get location vectors. */
		if( take_semaphore(fetch_location_vectors, USE_POLLING) == SEMAPHORE_ACQUIRED ) {
			/* Get LLH position. */
			kit->adcs.getframe_satellite_position_llh(&kit->adcs);
			vecs.llh_position = kit->adcs.satellite_position_llh;

			/* Get Nadir vector. */
			kit->adcs.getframe_nadir_vector(&kit->adcs);
			vecs.nadir_vector = kit->adcs.nadir_vector;

			xQueueSendToBack(location_vectors_queue, &vecs, BLOCK_FOREVER);
		}

		if(xSemaphoreTake( adcs_go, 0 ) == SEMAPHORE_ACQUIRED)
		{
			switch (adcs_buf[0])
			{
				case 128:
				{
					kit->adcs.getframe_identification(&kit->adcs);
					csp_printf("Runtime: %x", kit->adcs.identification.runtime_seconds);
					csp_printf("Node: %d", kit->adcs.identification.node_type);
					csp_printf("Firmware 1: %d", kit->adcs.identification.firmware_version_major);
					csp_printf("Firmware 2: %d", kit->adcs.identification.firmware_version_minor);
					csp_printf("Interface: %d", kit->adcs.identification.interface_version);
					csp_printf("Milliseconds: %d", kit->adcs.identification.runtime_millis);
					break;
				}
				case 129:
				{
					kit->adcs.getframe_extended_identification(&kit->adcs);
					csp_printf("Boot Cause: %x", kit->adcs.extended_identification.boot_cause);
					csp_printf("Boot Count: %d", kit->adcs.extended_identification.boot_count);
					csp_printf("Boot Index: %d", kit->adcs.extended_identification.boot_index);
					csp_printf("Firmware Major: %x", kit->adcs.extended_identification.firmware_major);
					csp_printf("Firmware Minor: %x", kit->adcs.extended_identification.firmware_minor);
					break;
				}
				case 130:
				{
					kit->adcs.getframe_communication_status(&kit->adcs);
					csp_printf("TeleCMD Count: %d", kit->adcs.communication_status.telecommand_counter);
					csp_printf("Telemetry Count: %d", kit->adcs.communication_status.telemetry_request_counter);
					csp_printf("Com error: %x", kit->adcs.communication_status.comm_status_booleans);
					break;
				}
				case 131:
				{
					kit->adcs.getframe_telecommand_acknowledge(&kit->adcs);
					csp_printf("Cmd: %d", kit->adcs.telecommand_acknowledge.last_telecommand_id);
					csp_printf("Cmd Processed: %d", kit->adcs.telecommand_acknowledge.processed);
					csp_printf("Cmd Error: %d", kit->adcs.telecommand_acknowledge.telecommand_error_status);
					csp_printf("Cmd Param Flag: %d", kit->adcs.telecommand_acknowledge.telecommand_parameter_error_index);
					break;
				}
				case 134:
				{
					kit->adcs.getframe_power_control_selection(&kit->adcs);
					csp_printf("Control Signal: %x", kit->adcs.power_control_selection.cube_control_signal_power_selection);
					csp_printf("Control Motor: %x", kit->adcs.power_control_selection.cube_control_motor_power_selection);
					csp_printf("Sense: %x", kit->adcs.power_control_selection.cube_sense_power_selection);
					csp_printf("Motor Driver: %x", kit->adcs.power_control_selection.cube_motor_power_selection);
					csp_printf("GPS LNA: %x", kit->adcs.power_control_selection.gps_lna_power_selection);
					break;
				}
				case 135:
				{
					kit->adcs.getframe_power_and_temperature_measurements(&kit->adcs);
					csp_printf("Sense 3V3: %d", kit->adcs.power_and_temperature_measurements.cubesense_3V3_current);
					csp_printf("Nadir: %d", kit->adcs.power_and_temperature_measurements.cubesense_nadir_sram_current);
					csp_printf("Sun: %d", kit->adcs.power_and_temperature_measurements.cubesense_sun_sram_current);
					csp_printf("CPU Temp: %d", kit->adcs.power_and_temperature_measurements.ARM_CPU_temperature);
					csp_printf("Control 3V3: %d", kit->adcs.power_and_temperature_measurements.cubecontrol_3V3_current);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Control 5V0: %d", kit->adcs.power_and_temperature_measurements.cubecontrol_5V_current);
					csp_printf("Control 8V0: %d", kit->adcs.power_and_temperature_measurements.cubecontrol_Vbat_current);
					csp_printf("Torquer: %d", kit->adcs.power_and_temperature_measurements.magnetorquer_current);
					csp_printf("Wheel: %d", kit->adcs.power_and_temperature_measurements.momentum_wheel_current);
					csp_printf("Rate Temp: %d", kit->adcs.power_and_temperature_measurements.rate_sensor_temperature);
					csp_printf("Mag Temp: %d", kit->adcs.power_and_temperature_measurements.magnetometer_temperature);
					break;
				}
				case 136:
				{
					kit->adcs.getframe_adcs_state(&kit->adcs);
					csp_printf("Current time: %d", kit->adcs.adcs_state.current_unix_time);
					csp_printf("Current millis: %d", kit->adcs.adcs_state.millis);
					csp_printf("Run Mode: %x", kit->adcs.adcs_state.adcs_modes);
					csp_printf("Current time: %d", kit->adcs.adcs_state.current_unix_time);
					csp_printf("Flags 1: %x", kit->adcs.adcs_state.adcs_state_bool_flags_set1);
					csp_printf("Flags 2: %x", kit->adcs.adcs_state.adcs_state_bool_flags_set2);
					csp_printf("Flags 3: %x", kit->adcs.adcs_state.adcs_state_bool_flags_set3);
					csp_printf("Flags 4: %x", kit->adcs.adcs_state.adcs_state_bool_flags_set4);
					csp_printf("Flags 5: %x", kit->adcs.adcs_state.adcs_state_bool_flags_set5);
					csp_printf("Set Roll: %d", kit->adcs.adcs_state.commanded_roll_angle);
					csp_printf("Set Pitch: %d", kit->adcs.adcs_state.commanded_pitch_angle);
					csp_printf("Set Yaw: %d", kit->adcs.adcs_state.commanded_yaw_angle);
					csp_printf("Read Roll: %d", kit->adcs.adcs_state.estimated_roll_angle);
					csp_printf("Read Pitch: %d", kit->adcs.adcs_state.estimated_pitch_angle);
					csp_printf("Read Yaw: %d", kit->adcs.adcs_state.estimated_yaw_angle);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Read X spin: %d", kit->adcs.adcs_state.estimated_X_angular_rate);
					csp_printf("Read Y spin: %d", kit->adcs.adcs_state.estimated_Y_angular_rate);
					csp_printf("Read Z spin: %d", kit->adcs.adcs_state.estimated_Z_angular_rate);
					csp_printf("Read X pos: %d", kit->adcs.adcs_state.X_position);
					csp_printf("Read Y pos: %d", kit->adcs.adcs_state.Y_position);
					csp_printf("Read Z pos: %d", kit->adcs.adcs_state.Z_position);
					csp_printf("Read X vel: %d", kit->adcs.adcs_state.X_velocity);
					csp_printf("Read Y vel: %d", kit->adcs.adcs_state.Y_velocity);
					csp_printf("Read Z vel: %d", kit->adcs.adcs_state.Z_velocity);
					csp_printf("Read Lat: %d", kit->adcs.adcs_state.latitude);
					csp_printf("Read Long: %d", kit->adcs.adcs_state.longitude);
					csp_printf("Read Alt: %d", kit->adcs.adcs_state.altitude);
					break;
				}
				case 137:
				{
					kit->adcs.getframe_adcs_measurements(&kit->adcs);
					csp_printf("Mag X: %d", kit->adcs.adcs_measurements.magnetic_field_x);
					csp_printf("Mag Y: %d", kit->adcs.adcs_measurements.magnetic_field_y);
					csp_printf("Mag Z: %d", kit->adcs.adcs_measurements.magnetic_field_z);
					csp_printf("CSS X: %d", kit->adcs.adcs_measurements.coarse_sun_x);
					csp_printf("CSS Y: %d", kit->adcs.adcs_measurements.coarse_sun_y);
					csp_printf("CSS Z: %d", kit->adcs.adcs_measurements.coarse_sun_z);
					csp_printf("Sun X: %d", kit->adcs.adcs_measurements.sun_x);
					csp_printf("Sun Y: %d", kit->adcs.adcs_measurements.sun_y);
					csp_printf("Sun Z: %d", kit->adcs.adcs_measurements.sun_z);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Nadir X: %d", kit->adcs.adcs_measurements.nadir_x);
					csp_printf("Nadir Y: %d", kit->adcs.adcs_measurements.nadir_y);
					csp_printf("Nadir Z: %d", kit->adcs.adcs_measurements.nadir_z);
					csp_printf("X Rate: %d", kit->adcs.adcs_measurements.angular_rate_x);
					csp_printf("Y Rate: %d", kit->adcs.adcs_measurements.angular_rate_y);
					csp_printf("Z Rate: %d", kit->adcs.adcs_measurements.angular_rate_z);
					csp_printf("X Wheel: %d", kit->adcs.adcs_measurements.wheel_speed_x);
					csp_printf("Y Wheel: %d", kit->adcs.adcs_measurements.wheel_speed_y);
					csp_printf("Z Wheel: %d", kit->adcs.adcs_measurements.wheel_speed_z);
					break;
				}
				case 138:
				{
					kit->adcs.getframe_actuator_commands(&kit->adcs);
					csp_printf("Mag X: %u", kit->adcs.actuator_commands.magnetorquer_command_x);
					csp_printf("Mag Y: %u", kit->adcs.actuator_commands.magnetorquer_command_y);
					csp_printf("Mag Z: %u", kit->adcs.actuator_commands.magnetorquer_command_z);
					csp_printf("Wheel X: %u", kit->adcs.actuator_commands.wheel_speed_x);
					csp_printf("Wheel Y: %u", kit->adcs.actuator_commands.wheel_speed_y);
					csp_printf("Wheel Z: %u", kit->adcs.actuator_commands.wheel_speed_z);
					break;
				}
				case 139:
				{
					kit->adcs.getframe_raw_sensor_measurements(&kit->adcs);
					csp_printf("Nadir X Cent: %d", kit->adcs.raw_sensor_measurements.nadir_centroid_x);
					csp_printf("Nadir Y Cent: %d", kit->adcs.raw_sensor_measurements.nadir_centroid_y);
					csp_printf("Nadir Busy: %d", kit->adcs.raw_sensor_measurements.nadir_busy_status);
					csp_printf("Nadir Detect: %d", kit->adcs.raw_sensor_measurements.nadir_detection_result);
					csp_printf("Sun X Cent: %d", kit->adcs.raw_sensor_measurements.sun_centroid_x);
					csp_printf("Sun Y Cent: %d", kit->adcs.raw_sensor_measurements.sun_centroid_y);
					csp_printf("Sun Busy: %d", kit->adcs.raw_sensor_measurements.sun_busy_status);
					csp_printf("Sun Detect: %d", kit->adcs.raw_sensor_measurements.sun_detection_result);
					csp_printf("CSS1: %d", kit->adcs.raw_sensor_measurements.css1);
					csp_printf("CSS2: %d", kit->adcs.raw_sensor_measurements.css2);
					csp_printf("CSS3: %d", kit->adcs.raw_sensor_measurements.css3);
					csp_printf("CSS4: %d", kit->adcs.raw_sensor_measurements.css4);
					csp_printf("CSS5: %d", kit->adcs.raw_sensor_measurements.css5);
					csp_printf("CSS6: %d", kit->adcs.raw_sensor_measurements.css6);
					csp_printf("Mag X: %d", kit->adcs.raw_sensor_measurements.magx);
					csp_printf("Mag Y: %d", kit->adcs.raw_sensor_measurements.magy);
					csp_printf("Mag Z: %d", kit->adcs.raw_sensor_measurements.magz);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("GPS Solution: %d", kit->adcs.raw_sensor_measurements.GPS_solution_status);
					csp_printf("GPS Sats: %d", kit->adcs.raw_sensor_measurements.number_of_tracked_GPS_satellites);
					csp_printf("GPS Sats Used: %d", kit->adcs.raw_sensor_measurements.number_of_GPS_satellites_used_in_solution);
					csp_printf("GPS XYZ: %d", kit->adcs.raw_sensor_measurements.counter_for_XYZ_log_from_GPS);
					csp_printf("GPS Range: %d", kit->adcs.raw_sensor_measurements.counter_for_RANGE_log_from_GPS);
					csp_printf("GPS Resp: %d", kit->adcs.raw_sensor_measurements.response_message_for_GPS_log_setup);
					csp_printf("GPS Week: %d", kit->adcs.raw_sensor_measurements.GPS_reference_week);
					csp_printf("GPS Millis: %d", kit->adcs.raw_sensor_measurements.GPS_time_millis);
					csp_printf("X Pos: %d", kit->adcs.raw_sensor_measurements.ECEF_position_x);
					csp_printf("Y Pos: %d", kit->adcs.raw_sensor_measurements.ECEF_position_y);
					csp_printf("Z Pos: %d", kit->adcs.raw_sensor_measurements.ECEF_position_z);
					csp_printf("X Vel: %d", kit->adcs.raw_sensor_measurements.ECEF_velocity_x);
					csp_printf("Y Vel: %d", kit->adcs.raw_sensor_measurements.ECEF_velocity_y);
					csp_printf("Z Vel: %d", kit->adcs.raw_sensor_measurements.ECEF_velocity_z);
					csp_printf("X pos dev: %d", kit->adcs.raw_sensor_measurements.x_pos_stddev);
					csp_printf("Y pos dev: %d", kit->adcs.raw_sensor_measurements.y_pos_stddev);
					csp_printf("Z pos dev: %d", kit->adcs.raw_sensor_measurements.z_pos_stddev);
					csp_printf("X vel dev: %d", kit->adcs.raw_sensor_measurements.ECEF_velocity_x);
					csp_printf("Y vel dev: %d", kit->adcs.raw_sensor_measurements.ECEF_velocity_y);
					csp_printf("Z vel dev: %d", kit->adcs.raw_sensor_measurements.ECEF_velocity_z);
					break;
				}
				case 140:
				{
					kit->adcs.getframe_estimation_data(&kit->adcs);
					csp_printf("IGRF X: %d", kit->adcs.estimation_data.IGRF_modelled_mag_field_x);
					csp_printf("IGRF Y: %d", kit->adcs.estimation_data.IGRF_modelled_mag_field_y);
					csp_printf("IGRF Z: %d", kit->adcs.estimation_data.IGRF_modelled_mag_field_z);
					csp_printf("Sun X: %d", kit->adcs.estimation_data.modelled_sun_vector_x);
					csp_printf("Sun Y: %d", kit->adcs.estimation_data.modelled_sun_vector_y);
					csp_printf("Sun Z: %d", kit->adcs.estimation_data.modelled_sun_vector_z);
					csp_printf("Gyro Bias X: %d", kit->adcs.estimation_data.estimated_x_gyro_bias);
					csp_printf("Gyro Bias Y: %d", kit->adcs.estimation_data.estimated_y_gyro_bias);
					csp_printf("Gyro Bias Z: %d", kit->adcs.estimation_data.estimated_z_gyro_bias);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Innovation X: %d", kit->adcs.estimation_data.innovation_vector_x);
					csp_printf("Innovation Y: %d", kit->adcs.estimation_data.innovation_vector_y);
					csp_printf("Innovation Z: %d", kit->adcs.estimation_data.innovation_vector_z);
					csp_printf("Quat Err 1: %d", kit->adcs.estimation_data.quaternion1_error);
					csp_printf("Quat Err 2: %d", kit->adcs.estimation_data.quaternion2_error);
					csp_printf("Quat Err 3: %d", kit->adcs.estimation_data.quaternion3_error);
					csp_printf("Quat CoV 1: %d", kit->adcs.estimation_data.quaternion1_rms_covariance);
					csp_printf("Quat CoV 2: %d", kit->adcs.estimation_data.quaternion2_rms_covariance);
					csp_printf("Quat CoV 3: %d", kit->adcs.estimation_data.quaternion3_rms_covariance);
					csp_printf("Rate CoV X: %d", kit->adcs.estimation_data.angular_rate_covariance_x);
					csp_printf("Rate CoV Y: %d", kit->adcs.estimation_data.angular_rate_covariance_y);
					csp_printf("Rate CoV Z: %d", kit->adcs.estimation_data.angular_rate_covariance_z);
					break;
				}
				case 143:
				{
					kit->adcs.getframe_current_time(&kit->adcs);
					csp_printf("Current time: %d", kit->adcs.current_time.unix_time);
					csp_printf("Current millis: %d", kit->adcs.current_time.millis);
					break;
				}
				case 144:
				{
					kit->adcs.getframe_current_state(&kit->adcs);
					csp_printf("Run Mode: %x", kit->adcs.current_state.adcs_modes);
					csp_printf("Flags 1: %x", kit->adcs.current_state.adcs_state_bool_flags_set1);
					csp_printf("Flags 2: %x", kit->adcs.current_state.adcs_state_bool_flags_set2);
					csp_printf("Flags 3: %x", kit->adcs.current_state.adcs_state_bool_flags_set3);
					csp_printf("Flags 4: %x", kit->adcs.current_state.adcs_state_bool_flags_set4);
					csp_printf("Flags 5: %x", kit->adcs.current_state.adcs_state_bool_flags_set5);
					break;
				}
				case 145:
				{
					kit->adcs.getframe_estimated_attitude_angles(&kit->adcs);
					csp_printf("Roll: %d", kit->adcs.estimated_attitude_angles.estimated_roll_angle);
					csp_printf("Pitch: %d", kit->adcs.estimated_attitude_angles.estimated_pitch_angle);
					csp_printf("Yaw: %d", kit->adcs.estimated_attitude_angles.estimated_yaw_angle);
					break;
				}
				case 146:
				{
					kit->adcs.getframe_estimated_angular_rates(&kit->adcs);
					csp_printf("X: %d", kit->adcs.estimated_angular_rates.estimated_X_angular_rate);
					csp_printf("Y: %d", kit->adcs.estimated_angular_rates.estimated_Y_angular_rate);
					csp_printf("Z: %d", kit->adcs.estimated_angular_rates.estimated_Z_angular_rate);
					break;
				}
				case 147:
				{
					kit->adcs.getframe_satellite_position_llh(&kit->adcs);
					csp_printf("Lat: %d", kit->adcs.satellite_position_llh.latitude);
					csp_printf("Long: %d", kit->adcs.satellite_position_llh.longitude);
					csp_printf("Alt: %d", kit->adcs.satellite_position_llh.altitude);
					break;
				}
				case 148:
				{
					kit->adcs.getframe_satellite_velocity_eci(&kit->adcs);
					csp_printf("X Vel: %d", kit->adcs.satellite_velocity_eci.X_velocity);
					csp_printf("Y Vel: %d", kit->adcs.satellite_velocity_eci.Y_velocity);
					csp_printf("Z Vel: %d", kit->adcs.satellite_velocity_eci.Z_velocity);
					break;
				}
				case 149:
				{
					kit->adcs.getframe_magnetic_field_vector(&kit->adcs);
					csp_printf("Mag X: %d", kit->adcs.magnetic_field_vector.magnetic_field_x);
					csp_printf("Mag Y: %d", kit->adcs.magnetic_field_vector.magnetic_field_y);
					csp_printf("Mag Z: %d", kit->adcs.magnetic_field_vector.magnetic_field_z);
					break;
				}
				case 150:
				{
					kit->adcs.getframe_coarse_sun_vector(&kit->adcs);
					csp_printf("CSS X: %d", kit->adcs.coarse_sun_vector.coarse_sun_x);
					csp_printf("CSS Y: %d", kit->adcs.coarse_sun_vector.coarse_sun_y);
					csp_printf("CSS Z: %d", kit->adcs.coarse_sun_vector.coarse_sun_z);
					break;
				}
				case 151:
				{
					kit->adcs.getframe_fine_sun_vector(&kit->adcs);
					csp_printf("Sun X: %d", kit->adcs.fine_sun_vector.sun_x);
					csp_printf("Sun Y: %d", kit->adcs.fine_sun_vector.sun_y);
					csp_printf("Sun Z: %d", kit->adcs.fine_sun_vector.sun_z);
					break;
				}
				case 152:
				{
					kit->adcs.getframe_nadir_vector(&kit->adcs);
					csp_printf("Nadir X: %d", kit->adcs.nadir_vector.nadir_x);
					csp_printf("Nadir Y: %d", kit->adcs.nadir_vector.nadir_y);
					csp_printf("Nadir Z: %d", kit->adcs.nadir_vector.nadir_z);
					break;
				}
				case 153:
				{
					kit->adcs.getframe_rate_sensor_rates(&kit->adcs);
					csp_printf("Rate X: %d", kit->adcs.rate_sensor_rates.angular_rate_x);
					csp_printf("Rate Y: %d", kit->adcs.rate_sensor_rates.angular_rate_y);
					csp_printf("Rate Z: %d", kit->adcs.rate_sensor_rates.angular_rate_z);
					break;
				}
				case 154:
				{
					kit->adcs.getframe_wheel_speed(&kit->adcs);
					csp_printf("Wheel X: %d", kit->adcs.wheel_speed.wheel_speed_x);
					csp_printf("Wheel Y: %d", kit->adcs.wheel_speed.wheel_speed_y);
					csp_printf("Wheel Z: %d", kit->adcs.wheel_speed.wheel_speed_z);
					break;
				}
				case 155:
				{
					kit->adcs.getframe_magnetorquer_command(&kit->adcs);
					csp_printf("Torq X: %d", kit->adcs.magnetorquer_command.magnetorquer_command_x);
					csp_printf("Torq Y: %d", kit->adcs.magnetorquer_command.magnetorquer_command_y);
					csp_printf("Torq Z: %d", kit->adcs.magnetorquer_command.magnetorquer_command_z);
					break;
				}
				case 156:
				{
					kit->adcs.getframe_wheel_speed_commands(&kit->adcs);
					csp_printf("Wheel CMD X: %d", kit->adcs.wheel_speed_commands.wheel_speed_x);
					csp_printf("Wheel CMD Y: %d", kit->adcs.wheel_speed_commands.wheel_speed_y);
					csp_printf("Wheel CMD Z: %d", kit->adcs.wheel_speed_commands.wheel_speed_z);
					break;
				}
				case 157:
				{
					kit->adcs.getframe_igrf_modelled_magnetic_field_vector(&kit->adcs);
					csp_printf("Modelled Mag X: %d", kit->adcs.igrf_modelled_magnetic_field_vector.IGRF_modelled_mag_field_x);
					csp_printf("Modelled Mag Y: %d", kit->adcs.igrf_modelled_magnetic_field_vector.IGRF_modelled_mag_field_y);
					csp_printf("Modelled Mag Z: %d", kit->adcs.igrf_modelled_magnetic_field_vector.IGRF_modelled_mag_field_z);
					break;
				}
				case 158:
				{
					kit->adcs.getframe_modelled_sun_vector(&kit->adcs);
					csp_printf("Modelled Sun X: %d", kit->adcs.modelled_sun_vector.modelled_sun_vector_x);
					csp_printf("Modelled Sun Y: %d", kit->adcs.modelled_sun_vector.modelled_sun_vector_y);
					csp_printf("Modelled Sun Z: %d", kit->adcs.modelled_sun_vector.modelled_sun_vector_z);
					break;
				}
				case 159:
				{
					kit->adcs.getframe_estimated_gyro_bias(&kit->adcs);
					csp_printf("Gyro Bias X: %d", kit->adcs.estimated_gyro_bias.estimated_x_gyro_bias);
					csp_printf("Gyro Bias Y: %d", kit->adcs.estimated_gyro_bias.estimated_y_gyro_bias);
					csp_printf("Gyro Bias Z: %d", kit->adcs.estimated_gyro_bias.estimated_z_gyro_bias);
					break;
				}
				case 160:
				{
					kit->adcs.getframe_estimation_innovation_vector(&kit->adcs);
					csp_printf("Innovation X: %d", kit->adcs.estimation_innovation_vector.innovation_vector_x);
					csp_printf("Innovation Y: %d", kit->adcs.estimation_innovation_vector.innovation_vector_y);
					csp_printf("Innovation Z: %d", kit->adcs.estimation_innovation_vector.innovation_vector_z);
					break;
				}
				case 161:
				{
					kit->adcs.getframe_quaternion_error_vector(&kit->adcs);
					csp_printf("Quat Err 1: %d", kit->adcs.quaternion_error_vector.quaternion1_error);
					csp_printf("Quat Err 2: %d", kit->adcs.quaternion_error_vector.quaternion2_error);
					csp_printf("Quat Err 3: %d", kit->adcs.quaternion_error_vector.quaternion3_error);
					break;
				}
				case 162:
				{
					kit->adcs.getframe_quaternion_covariance(&kit->adcs);
					csp_printf("Quat CoV 1: %d", kit->adcs.quaternion_covariance.quaternion1_rms_covariance);
					csp_printf("Quat CoV 2: %d", kit->adcs.quaternion_covariance.quaternion2_rms_covariance);
					csp_printf("Quat CoV 3: %d", kit->adcs.quaternion_covariance.quaternion3_rms_covariance);
					break;
				}
				case 163:
				{
					kit->adcs.getframe_angular_rate_covariance(&kit->adcs);
					csp_printf("Rate CoV X: %d", kit->adcs.angular_rate_covariance.angular_rate_covariance_x);
					csp_printf("Rate CoV Y: %d", kit->adcs.angular_rate_covariance.angular_rate_covariance_y);
					csp_printf("Rate CoV Z: %d", kit->adcs.angular_rate_covariance.angular_rate_covariance_z);
					break;
				}
				case 164:
				{kit->adcs.getframe_raw_nadir_sensor(&kit->adcs);
					csp_printf("Raw Nadir X: %d", kit->adcs.raw_nadir_sensor.nadir_centroid_x);
					csp_printf("Raw Nadir Y: %d", kit->adcs.raw_nadir_sensor.nadir_centroid_y);
					csp_printf("Nadir Busy: %d", kit->adcs.raw_nadir_sensor.nadir_busy_status);
					csp_printf("Nadir Detect: %d", kit->adcs.raw_nadir_sensor.nadir_detection_result);
					break;
				}
				case 165:
				{
					kit->adcs.getframe_raw_sun_sensor(&kit->adcs);
					csp_printf("Raw Sun X: %d", kit->adcs.raw_sun_sensor.sun_centroid_x);
					csp_printf("Raw Sun Y: %d", kit->adcs.raw_sun_sensor.sun_centroid_y);
					csp_printf("Sun Busy: %d", kit->adcs.raw_sun_sensor.sun_busy_status);
					csp_printf("Sun Detect: %d", kit->adcs.raw_sun_sensor.sun_detection_result);
					break;
				}
				case 166:
				{
					kit->adcs.getframe_raw_css(&kit->adcs);
					csp_printf("Raw CSS1: %d", kit->adcs.raw_css.css1);
					csp_printf("Raw CSS2: %d", kit->adcs.raw_css.css2);
					csp_printf("Raw CSS3: %d", kit->adcs.raw_css.css3);
					csp_printf("Raw CSS4: %d", kit->adcs.raw_css.css4);
					csp_printf("Raw CSS5: %d", kit->adcs.raw_css.css5);
					csp_printf("Raw CSS6: %d", kit->adcs.raw_css.css6);
					break;
				}
				case 167:
				{
					kit->adcs.getframe_raw_magnetometer(&kit->adcs);
					csp_printf("Raw Mag X: %d", kit->adcs.raw_magnetometer.magx);
					csp_printf("Raw Mag Y: %d", kit->adcs.raw_magnetometer.magy);
					csp_printf("Raw Mag Z: %d", kit->adcs.raw_magnetometer.magz);
					break;
				}
				case 168:
				{
					kit->adcs.getframe_raw_gps_status(&kit->adcs);
					csp_printf("Solution Status: %d", kit->adcs.raw_gps_status.GPS_solution_status);
					csp_printf("Sats Tracked: %d", kit->adcs.raw_gps_status.number_of_tracked_GPS_satellites);
					csp_printf("Sats Used: %d", kit->adcs.raw_gps_status.number_of_GPS_satellites_used_in_solution);
					csp_printf("XYZ Counter: %d", kit->adcs.raw_gps_status.counter_for_XYZ_log_from_GPS);
					csp_printf("RANGE Counter: %d", kit->adcs.raw_gps_status.counter_for_RANGE_log_from_GPS);
					csp_printf("Response: %d", kit->adcs.raw_gps_status.response_message_for_GPS_log_setup);
					break;
				}
				case 169:
				{
					kit->adcs.getframe_raw_gps_time(&kit->adcs);
					csp_printf("Week Ref: %d", kit->adcs.raw_gps_time.GPS_reference_week);
					csp_printf("Milliseconds: %d", kit->adcs.raw_gps_time.GPS_time_millis);
					break;
				}
				case 170:
				{
					kit->adcs.getframe_raw_gps_x(&kit->adcs);
					csp_printf("Pos X: %d", kit->adcs.raw_gps_x.ECEF_position_x);
					csp_printf("Vel X: %d", kit->adcs.raw_gps_x.ECEF_velocity_x);
					break;
				}
				case 171:
				{
					kit->adcs.getframe_raw_gps_y(&kit->adcs);
					csp_printf("Pos Y: %d", kit->adcs.raw_gps_y.ECEF_position_y);
					csp_printf("Vel Y: %d", kit->adcs.raw_gps_y.ECEF_velocity_y);
					break;
				}
				case 172:
				{
					kit->adcs.getframe_raw_gps_z(&kit->adcs);
					csp_printf("Pos Z: %d", kit->adcs.raw_gps_z.ECEF_position_z);
					csp_printf("Vel Z: %d", kit->adcs.raw_gps_z.ECEF_velocity_z);
					break;
				}
				case 173:
				{
					kit->adcs.getframe_cubesense_current(&kit->adcs);
					csp_printf("3V3 Curr: %d", kit->adcs.cubesense_current.cubesense_3V3_current);
					csp_printf("Nadir Curr: %d", kit->adcs.cubesense_current.cubesense_nadir_sram_current);
					csp_printf("Sun Curr: %d", kit->adcs.cubesense_current.cubesense_sun_sram_current);
					csp_printf("CPU Temp: %d", kit->adcs.cubesense_current.cpu_temperature);
					break;
				}
				case 174:
				{
					kit->adcs.getframe_cubecontrol_current(&kit->adcs);
					csp_printf("3V3 Curr: %d", kit->adcs.cubecontrol_current.cubecontrol_3V3_current);
					csp_printf("5V0 Curr: %d", kit->adcs.cubecontrol_current.cubecontrol_5V_current);
					csp_printf("8V0 Curr: %d", kit->adcs.cubecontrol_current.cubecontrol_Vbat_current);
					break;
				}
				case 175:
				{
					kit->adcs.getframe_peripheral_current_and_temperature_measurements(&kit->adcs);
					csp_printf("Torq Curr: %d", kit->adcs.peripheral_current_and_temperature_measurements.magnetorquer_current);
					csp_printf("Wheel Curr: %d", kit->adcs.peripheral_current_and_temperature_measurements.momentum_wheel_current);
					csp_printf("Rate Temp: %d", kit->adcs.peripheral_current_and_temperature_measurements.rate_sensor_temperature);
					csp_printf("Mag Temp: %d", kit->adcs.peripheral_current_and_temperature_measurements.magnetometer_temperature);
					break;
				}
				case 189:
				{
					kit->adcs.getframe_acp_execution_state(&kit->adcs);
					csp_printf("Start Millis: %d", kit->adcs.acp_execution_state.time_since_iteration_start);
					csp_printf("Doing: %d", kit->adcs.acp_execution_state.current_execution_point);
					break;
				}
				case 190:
				{
					kit->adcs.getframe_acp_execution_times(&kit->adcs);
					csp_printf("ADCS Update: %d", kit->adcs.acp_execution_times.time_to_perform_ADCS_update);
					csp_printf("Sensors: %d", kit->adcs.acp_execution_times.time_to_perform_sensor_or_actuator_comms);
					csp_printf("SGP4: %d", kit->adcs.acp_execution_times.time_to_execute_SGP4_propagator);
					csp_printf("IGRF: %d", kit->adcs.acp_execution_times.time_to_execute_IGRF_model);
					break;
				}
				case 191:
				{
					kit->adcs.getframe_sgp4_orbit_parameters(&kit->adcs);
					csp_printf("Incl: %f", kit->adcs.sgp4_orbit_parameters.inclination);
					csp_printf("Ecc: %f", kit->adcs.sgp4_orbit_parameters.eccentricity);
					csp_printf("RAAN: %f", kit->adcs.sgp4_orbit_parameters.right_ascension);
					csp_printf("Perig: %f", kit->adcs.sgp4_orbit_parameters.perigee);
					csp_printf("Bstar: %f", kit->adcs.sgp4_orbit_parameters.b_star_drag_term);
					csp_printf("Motion: %f", kit->adcs.sgp4_orbit_parameters.mean_motion);
					csp_printf("Anomaly: %f", kit->adcs.sgp4_orbit_parameters.mean_anomaly);
					csp_printf("Epoch: %f", kit->adcs.sgp4_orbit_parameters.epoch);
					break;
				}
				case 192:
				{
					kit->adcs.getframe_configuration(&kit->adcs);
					csp_printf("Torq 1: %d", kit->adcs.configuration.magnetorquer_1_configuration);
					csp_printf("Torq 2: %d", kit->adcs.configuration.magnetorquer_2_configuration);
					csp_printf("Torq 3: %d", kit->adcs.configuration.magnetorquer_3_configuration);
					csp_printf("X dipole: %d", kit->adcs.configuration.magnetorquer_max_x_dipole_moment);
					csp_printf("Y dipole: %d", kit->adcs.configuration.magnetorquer_max_y_dipole_moment);
					csp_printf("Z dipole: %d", kit->adcs.configuration.magnetorquer_max_x_dipole_moment);
					csp_printf("Torq Max On: %d", kit->adcs.configuration.magnetorquer_max_on_time);
					csp_printf("Torq Reso: %d", kit->adcs.configuration.magnetorquer_on_time_resolution);
					csp_printf("Mag Cont: %d", kit->adcs.configuration.magnetic_control_selection);
					csp_printf("Wheel M 1: %d", kit->adcs.configuration.wheel_transform_angle_alpha);
					csp_printf("Wheel M 2: %d", kit->adcs.configuration.wheel_transform_angle_gamma);
					csp_printf("Wheel Pol: %d", kit->adcs.configuration.wheel_polarity);
					csp_printf("Wheel Iner: %d", kit->adcs.configuration.wheel_inertia);
					csp_printf("Wheel Max T: %d", kit->adcs.configuration.wheel_max_torque);
					csp_printf("Wheel Max V: %d", kit->adcs.configuration.wheel_max_speed);
					csp_printf("Wheel Gain: %d", kit->adcs.configuration.wheel_control_gain);
					csp_printf("Wheel Bckup: %d", kit->adcs.configuration.wheel_backup_control_mode);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("CSS1 Conf: %d", kit->adcs.configuration.css1_config);
					csp_printf("CSS2 Conf: %d", kit->adcs.configuration.css2_config);
					csp_printf("CSS3 Conf: %d", kit->adcs.configuration.css3_config);
					csp_printf("CSS4 Conf: %d", kit->adcs.configuration.css4_config);
					csp_printf("CSS5 Conf: %d", kit->adcs.configuration.css5_config);
					csp_printf("CSS6 Conf: %d", kit->adcs.configuration.css6_config);
					csp_printf("CSS1 Scale: %d", kit->adcs.configuration.css1_rel_scale);
					csp_printf("CSS2 Scale: %d", kit->adcs.configuration.css2_rel_scale);
					csp_printf("CSS3 Scale: %d", kit->adcs.configuration.css3_rel_scale);
					csp_printf("CSS4 Scale: %d", kit->adcs.configuration.css4_rel_scale);
					csp_printf("CSS5 Scale: %d", kit->adcs.configuration.css5_rel_scale);
					csp_printf("CSS6 Scale: %d", kit->adcs.configuration.css6_rel_scale);
					csp_printf("CSS Autof: %d", kit->adcs.configuration.css_autofill_missing_facets);
					csp_printf("CSS Thrsh: %d", kit->adcs.configuration.css_threshold);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Sun Angle 1: %d", kit->adcs.configuration.sun_sensor_transform_angle_alpha);
					csp_printf("Sun Angle 2: %d", kit->adcs.configuration.sun_sensor_transform_angle_beta);
					csp_printf("Sun Angle 3: %d", kit->adcs.configuration.sun_sensor_transform_angle_gamma);
					csp_printf("Sun Thrsh: %d", kit->adcs.configuration.sun_detection_threshold);
					csp_printf("Sun Auto: %d", kit->adcs.configuration.sun_sensor_auto_adjust_mode);
					csp_printf("Sun Expo: %d", kit->adcs.configuration.sun_sensor_exposure_time);
					csp_printf("Sun AGC: %d", kit->adcs.configuration.sun_sensor_AGC);
					csp_printf("Sun Blue: %d", kit->adcs.configuration.sun_blue_gain);
					csp_printf("Sun Red: %d", kit->adcs.configuration.sun_red_gain);
					csp_printf("Sun Bore X: %d", kit->adcs.configuration.sun_boresight_x);
					csp_printf("Sun Bore Y: %d", kit->adcs.configuration.sun_boresight_y);
					csp_printf("Sun Shift: %d", kit->adcs.configuration.sun_shift);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Nadir Angle 1: %d", kit->adcs.configuration.nadir_sensor_transform_angle_alpha);
					csp_printf("Nadir Angle 2: %d", kit->adcs.configuration.nadir_sensor_transform_angle_beta);
					csp_printf("Nadir Angle 3: %d", kit->adcs.configuration.nadir_sensor_transform_angle_gamma);
					csp_printf("Nadir Thrsh: %d", kit->adcs.configuration.nadir_detection_threshold);
					csp_printf("Nadir Auto: %d", kit->adcs.configuration.nadir_sensor_auto_adjust);
					csp_printf("Nadir Expo: %d", kit->adcs.configuration.nadir_sensor_exposure_time);
					csp_printf("Nadir AGC: %d", kit->adcs.configuration.nadir_sensor_AGC);
					csp_printf("Nadir Blue: %d", kit->adcs.configuration.nadir_blue_gain);
					csp_printf("Nadir Red: %d", kit->adcs.configuration.nadir_red_gain);
					csp_printf("Nadir Bore X: %d", kit->adcs.configuration.nadir_boresight_x);
					csp_printf("Nadir Bore Y: %d", kit->adcs.configuration.nadir_boresight_y);
					csp_printf("Nadir Shift: %d", kit->adcs.configuration.nadir_shift);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Min X 1: %d", kit->adcs.configuration.min_x_of_area_1);
					csp_printf("Max X 1: %d", kit->adcs.configuration.max_x_of_area_1);
					csp_printf("Min Y 1: %d", kit->adcs.configuration.min_y_of_area_1);
					csp_printf("Max Y 1: %d", kit->adcs.configuration.max_y_of_area_1);
					csp_printf("Min X 2: %d", kit->adcs.configuration.min_x_of_area_2);
					csp_printf("Max X 2: %d", kit->adcs.configuration.max_x_of_area_2);
					csp_printf("Min Y 2: %d", kit->adcs.configuration.min_y_of_area_2);
					csp_printf("Max Y 2: %d", kit->adcs.configuration.max_y_of_area_2);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Min X 3: %d", kit->adcs.configuration.min_x_of_area_3);
					csp_printf("Max X 3: %d", kit->adcs.configuration.max_x_of_area_3);
					csp_printf("Min Y 3: %d", kit->adcs.configuration.min_y_of_area_3);
					csp_printf("Max Y 3: %d", kit->adcs.configuration.max_y_of_area_3);
					csp_printf("Min X 4: %d", kit->adcs.configuration.min_x_of_area_4);
					csp_printf("Max X 4: %d", kit->adcs.configuration.max_x_of_area_4);
					csp_printf("Min Y 4: %d", kit->adcs.configuration.min_y_of_area_4);
					csp_printf("Max Y 4: %d", kit->adcs.configuration.max_y_of_area_4);
					csp_printf("Min X 5: %d", kit->adcs.configuration.min_x_of_area_5);
					csp_printf("Max X 5: %d", kit->adcs.configuration.max_x_of_area_5);
					csp_printf("Min Y 5: %d", kit->adcs.configuration.min_y_of_area_5);
					csp_printf("Max Y 5: %d", kit->adcs.configuration.max_y_of_area_5);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Mag Angle 1: %d", kit->adcs.configuration.magnetometer_transform_angle_alpha);
					csp_printf("Mag Angle 2: %d", kit->adcs.configuration.magnetometer_transform_angle_beta);
					csp_printf("Mag Angle 3: %d", kit->adcs.configuration.magnetometer_transform_angle_gamma);
					csp_printf("Mag Ch Off 1: %d", kit->adcs.configuration.magnetometer_channel1_offset);
					csp_printf("Mag Ch Off 2: %d", kit->adcs.configuration.magnetometer_channel2_offset);
					csp_printf("Mag Ch Off 3: %d", kit->adcs.configuration.magnetometer_channel3_offset);
					csp_printf("Mag Sens 11: %d", kit->adcs.configuration.magnetometer_sens_mat_s11);
					csp_printf("Mag Sens 12: %d", kit->adcs.configuration.magnetometer_sens_mat_s12);
					csp_printf("Mag Sens 13: %d", kit->adcs.configuration.magnetometer_sens_mat_s13);
					csp_printf("Mag Sens 21: %d", kit->adcs.configuration.magnetometer_sens_mat_s21);
					csp_printf("Mag Sens 22: %d", kit->adcs.configuration.magnetometer_sens_mat_s22);
					csp_printf("Mag Sens 23: %d", kit->adcs.configuration.magnetometer_sens_mat_s23);
					csp_printf("Mag Sens 31: %d", kit->adcs.configuration.magnetometer_sens_mat_s31);
					csp_printf("Mag Sens 32: %d", kit->adcs.configuration.magnetometer_sens_mat_s32);
					csp_printf("Mag Sens 33: %d", kit->adcs.configuration.magnetometer_sens_mat_s33);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("YRate Offs: %d", kit->adcs.configuration.y_rate_sensor_offset);
					csp_printf("YRate Angle 1: %d", kit->adcs.configuration.y_rate_sensor_transform_angle_alpha);
					csp_printf("YRate Angle 2: %d", kit->adcs.configuration.y_rate_sensor_transform_angle_gamma);
					csp_printf("Det Spin Gain: %f", kit->adcs.configuration.detumbling_spin_gain);
					csp_printf("Det Damp Gain: %f", kit->adcs.configuration.detumbling_damping_gain);
					csp_printf("YRate Ref: %d", kit->adcs.configuration.ref_spin_rate);
					csp_printf("YMom Cont Gain: %f", kit->adcs.configuration.y_momentum_control_gain);
					csp_printf("YMom Der Nut Gain: %f", kit->adcs.configuration.y_momentum_nut_derivative_gain);
					csp_printf("YMom Pro Nut Gain: %f", kit->adcs.configuration.y_momentum_nut_proportional_gain);
					csp_printf("YMom Der Gain: %f", kit->adcs.configuration.y_momentum_derivative_gain);
					csp_printf("YMom Pro Gain: %f", kit->adcs.configuration.y_momentum_proportional_gain);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Wheel Ref Mom: %f", kit->adcs.configuration.ref_wheel_momentum);
					csp_printf("Wheel Ref Mom Init: %f", kit->adcs.configuration.ref_wheel_momentum_init_pitch);
					csp_printf("Wheel Ref Tor Init: %d", kit->adcs.configuration.ref_wheel_torque_init_pitch);
					csp_printf("Mom Inert xx: %f", kit->adcs.configuration.moment_of_inertia_xx);
					csp_printf("Mom Inert yy: %f", kit->adcs.configuration.moment_of_inertia_yy);
					csp_printf("Mom Inert zz: %f", kit->adcs.configuration.moment_of_inertia_zz);
					csp_printf("Mom Inert xy: %f", kit->adcs.configuration.moment_of_inertia_xy);
					csp_printf("Mom Inert xz: %f", kit->adcs.configuration.moment_of_inertia_xz);
					csp_printf("Mom Inert yz: %f", kit->adcs.configuration.moment_of_inertia_yz);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Mag Rate Noise: %f", kit->adcs.configuration.magnetometer_rate_filter_system_noise);
					csp_printf("EKF Noise: %f", kit->adcs.configuration.EKF_system_noise);
					csp_printf("CSS Noise: %f", kit->adcs.configuration.css_meas_noise);
					csp_printf("Sun Noise: %f", kit->adcs.configuration.sun_sensor_meas_noise);
					csp_printf("Nadir Noise: %f", kit->adcs.configuration.nadir_sensor_meas_noise);
					csp_printf("Mag Noise: %f", kit->adcs.configuration.magnetometer_sensor_meas_noise);
					csp_printf("Sensor Mask: %x", kit->adcs.configuration.mask_senors_usage);
					csp_printf("Cam Period: %x", kit->adcs.configuration.sun_and_nadir_sampling_period);
					break;
				}
				case 193:
				{
					kit->adcs.getframe_edac_and_latchup_counters(&kit->adcs);
					csp_printf("Single SRAM: %d", kit->adcs.edac_and_latchup_counters.single_SRAM_upsets);
					csp_printf("Double SRAM: %d", kit->adcs.edac_and_latchup_counters.double_SRAM_upsets);
					csp_printf("Multiple SRAM: %d", kit->adcs.edac_and_latchup_counters.multiple_SRAM_upsets);
					csp_printf("SRAM1: %d", kit->adcs.edac_and_latchup_counters.SRAM1_latchups);
					csp_printf("SRAM2: %d", kit->adcs.edac_and_latchup_counters.SRAM2_latchups);
					break;
				}
				case 194:
				{
					kit->adcs.getframe_start_up_mode(&kit->adcs);
					csp_printf("Startup Mode: %d", kit->adcs.start_up_mode.mode);
					break;
				}
				case 230:
				{
					kit->adcs.getframe_status_of_image_capture_and_save_operation(&kit->adcs);
					csp_printf("Error: %d, PC: %d", kit->adcs.status_of_image_capture_and_save_operation.status, kit->adcs.status_of_image_capture_and_save_operation.complete);
					break;
				}
				case 240:
				{
					kit->adcs.getframe_file_information(&kit->adcs);
					csp_printf("Flags: %x, Size: %d", kit->adcs.file_information.file_info_flags, kit->adcs.file_information.filesize);
					csp_printf("Date: %d, Time: %d", kit->adcs.file_information.date, kit->adcs.file_information.time);
					csp_printf("Filename: %s", kit->adcs.file_information.filename);
					break;
				}
				case 241:
				{
					kit->adcs.getframe_file_block_crc(&kit->adcs);
					csp_printf("Flags: %x", kit->adcs.file_block_crc.file_block_CRC_flags);
					csp_printf("Length: %d", kit->adcs.file_block_crc.block_length);
					csp_printf("Number: %d, CHKSM: %x", kit->adcs.file_block_crc.block_number, kit->adcs.file_block_crc.CRC16_checksum);
					break;
				}
				case 242:
				{
					csp_printf("Starting transfer");
					FILE* fid;
					int block_size;
					uint8_t retries = 0;
					uint32_t iterations = 0;
					if(kit->adcs.file_block_crc.block_number != 0)
					{
						csp_printf("Not at start. Restart file download.");
						break;
					}
					for(uint32_t i=0;i<4096;i++)
					{
						if((256*i) >= kit->adcs.file_information.filesize)
						{
							iterations = i;
							csp_printf("%u blocks to transfer.",iterations);
							break;
						}
					}
					kit->adcs.advance_file_read_pointer.block = 0;
					while(kit->adcs.advance_file_read_pointer.block < iterations)
					{
						kit->adcs.getframe_file_block_crc(&kit->adcs);
						if(kit->adcs.file_block_crc.block_length == 0)
						{
							block_size = 256;
						}
						else
						{
							block_size = kit->adcs.file_block_crc.block_length;
						}
						if(retries >= 10)
						{
							csp_printf("Too many failed packets.");
							kit->adcs.advance_file_read_pointer.block = iterations;
							continue;
						}
						if(kit->adcs.advance_file_read_pointer.block != kit->adcs.file_block_crc.block_number)
						{
							vTaskDelay(2000);
							retries = retries + 1;
							continue;
						}
						kit->adcs.getframe_file_data_block(&kit->adcs);
						crc_result = CRC_calc(&kit->adcs.file_data_block.block[0], &kit->adcs.file_data_block.block[block_size]);
						if(crc_result != kit->adcs.file_block_crc.CRC16_checksum)
						{
							csp_printf("Bad CRC");
							vTaskDelay(500);
							retries = retries + 1;
							continue;
						}
						fid = fopen("/boot/adcs.bin","a");
						fwrite(kit->adcs.file_data_block.block,block_size,1,fid);
						fclose(fid);
						csp_printf("Wrote block %u", kit->adcs.advance_file_read_pointer.block);
						retries = 0;
						kit->adcs.advance_file_read_pointer.block = kit->adcs.advance_file_read_pointer.block + 1;
						kit->adcs.setframe_advance_file_read_pointer(&kit->adcs, &kit->adcs.advance_file_read_pointer);
					}
					csp_printf("Transfer done");
					break;
				}
				case 250:
				{
					kit->adcs.getframe_uploaded_program_status(&kit->adcs);
					csp_printf("Complete?: %d", kit->adcs.uploaded_program_status.programming_completed);
					csp_printf("CRC16: %d", kit->adcs.uploaded_program_status.CRC16_checksum);
					break;
				}
				case 251:
				{
					kit->adcs.getframe_get_flash_program_list(&kit->adcs);
					csp_printf("Prgm 1 Len: %d", kit->adcs.get_flash_program_list.program1_length);
					csp_printf("Prgm 1 CRC: %d", kit->adcs.get_flash_program_list.program1_CRC16);
					csp_printf("Prgm 1 Valid: %d", kit->adcs.get_flash_program_list.program1_valid);
					csp_printf("Prgm 1 Name: %0.15s", kit->adcs.get_flash_program_list.program1_description);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Prgm 2 Len: %d", kit->adcs.get_flash_program_list.program2_length);
					csp_printf("Prgm 2 CRC: %d", kit->adcs.get_flash_program_list.program2_CRC16);
					csp_printf("Prgm 2 Valid: %d", kit->adcs.get_flash_program_list.program2_valid);
					csp_printf("Prgm 2 Name: %0.15s", kit->adcs.get_flash_program_list.program2_description);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Prgm 3 Len: %d", kit->adcs.get_flash_program_list.program3_length);
					csp_printf("Prgm 3 CRC: %d", kit->adcs.get_flash_program_list.program3_CRC16);
					csp_printf("Prgm 3 Valid: %d", kit->adcs.get_flash_program_list.program3_valid);
					csp_printf("Prgm 3 Name: %0.15s", kit->adcs.get_flash_program_list.program3_description);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Prgm 4 Len: %d", kit->adcs.get_flash_program_list.program4_length);
					csp_printf("Prgm 4 CRC: %d", kit->adcs.get_flash_program_list.program4_CRC16);
					csp_printf("Prgm 4 Valid: %d", kit->adcs.get_flash_program_list.program4_valid);
					csp_printf("Prgm 4 Name: %0.15s", kit->adcs.get_flash_program_list.program4_description);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Prgm 5 Len: %d", kit->adcs.get_flash_program_list.program5_length);
					csp_printf("Prgm 5 CRC: %d", kit->adcs.get_flash_program_list.program5_CRC16);
					csp_printf("Prgm 5 Valid: %d", kit->adcs.get_flash_program_list.program5_valid);
					csp_printf("Prgm 5 Name: %0.15s", kit->adcs.get_flash_program_list.program5_description);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Prgm 6 Len: %d", kit->adcs.get_flash_program_list.program6_length);
					csp_printf("Prgm 6 CRC: %d", kit->adcs.get_flash_program_list.program6_CRC16);
					csp_printf("Prgm 6 Valid: %d", kit->adcs.get_flash_program_list.program6_valid);
					csp_printf("Prgm 6 Name: %0.15s", kit->adcs.get_flash_program_list.program6_description);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Prgm 7 Len: %d", kit->adcs.get_flash_program_list.program7_length);
					csp_printf("Prgm 7 CRC: %d", kit->adcs.get_flash_program_list.program7_CRC16);
					csp_printf("Prgm 7 Valid: %d", kit->adcs.get_flash_program_list.program7_valid);
					csp_printf("Prgm 7 Name: %0.15s", kit->adcs.get_flash_program_list.program7_description);
					vTaskDelay(ADCS_PRINT_DELAY);
					csp_printf("Prgm Int Len: %d", kit->adcs.get_flash_program_list.int_flash_length);
					csp_printf("Prgm Int CRC: %d", kit->adcs.get_flash_program_list.int_flash_CRC16);
					csp_printf("Prgm Int Valid: %d", kit->adcs.get_flash_program_list.int_flash_valid);
					csp_printf("Prgm Int Name: %0.15s", kit->adcs.get_flash_program_list.int_flash_description);
					break;
				}
				case 252:
				{
					kit->adcs.getframe_boot_index(&kit->adcs);
					csp_printf("Prgm Index: %d", kit->adcs.boot_index.program_index);
					csp_printf("Status: %d", kit->adcs.boot_index.boot_status);
					break;
				}
				case 77: // AlbertaSat Command - loads default config from code.
				{
					kit->adcs.configuration.magnetorquer_1_configuration = 0;
					kit->adcs.configuration.magnetorquer_2_configuration = 2;
					kit->adcs.configuration.magnetorquer_3_configuration = 4;
					kit->adcs.configuration.magnetorquer_max_x_dipole_moment = 1200;
					kit->adcs.configuration.magnetorquer_max_y_dipole_moment = 2000;
					kit->adcs.configuration.magnetorquer_max_z_dipole_moment = 2000;
					kit->adcs.configuration.magnetorquer_max_on_time = 80;
					kit->adcs.configuration.magnetorquer_on_time_resolution = 10;
					kit->adcs.configuration.magnetic_control_selection = 0;
					kit->adcs.configuration.wheel_transform_angle_alpha = 0;
					kit->adcs.configuration.wheel_transform_angle_gamma = 0;
					kit->adcs.configuration.wheel_polarity = 0;
					kit->adcs.configuration.wheel_inertia = 2000;
					kit->adcs.configuration.wheel_max_torque = 1500;
					kit->adcs.configuration.wheel_control_gain = 80;
					kit->adcs.configuration.wheel_backup_control_mode = 0x0;
					kit->adcs.configuration.css1_config = 0;
					kit->adcs.configuration.css2_config = 1;
					kit->adcs.configuration.css3_config = 2;
					kit->adcs.configuration.css4_config = 3;
					kit->adcs.configuration.css5_config = 4;
					kit->adcs.configuration.css6_config = 5;
					kit->adcs.configuration.css1_rel_scale = 1;
					kit->adcs.configuration.css2_rel_scale = 1;
					kit->adcs.configuration.css3_rel_scale = 1;
					kit->adcs.configuration.css4_rel_scale = 1;
					kit->adcs.configuration.css5_rel_scale = 1;
					kit->adcs.configuration.css6_rel_scale = 1;
					kit->adcs.configuration.css_autofill_missing_facets = 0x0;
					kit->adcs.configuration.css_threshold = 0;
					kit->adcs.configuration.sun_sensor_transform_angle_alpha = 9000;
					kit->adcs.configuration.sun_sensor_transform_angle_beta = 0;
					kit->adcs.configuration.sun_sensor_transform_angle_gamma = 18000;
					kit->adcs.configuration.sun_detection_threshold = 140;
					kit->adcs.configuration.sun_sensor_auto_adjust_mode = 0x0;
					kit->adcs.configuration.sun_sensor_exposure_time = 50;
					kit->adcs.configuration.sun_sensor_AGC = 20;
					kit->adcs.configuration.sun_blue_gain = 128;
					kit->adcs.configuration.sun_red_gain = 128;
					kit->adcs.configuration.sun_boresight_x = 350;
					kit->adcs.configuration.sun_boresight_y = 240;
					kit->adcs.configuration.sun_shift = 0x1;
					kit->adcs.configuration.nadir_sensor_transform_angle_alpha = 9000;
					kit->adcs.configuration.nadir_sensor_transform_angle_beta = 0;
					kit->adcs.configuration.nadir_sensor_transform_angle_gamma = 0;
					kit->adcs.configuration.nadir_detection_threshold = 140;
					kit->adcs.configuration.nadir_sensor_auto_adjust = 0x0;
					kit->adcs.configuration.nadir_sensor_exposure_time = 50;
					kit->adcs.configuration.nadir_sensor_AGC = 20;
					kit->adcs.configuration.nadir_blue_gain = 128;
					kit->adcs.configuration.nadir_red_gain = 128;
					kit->adcs.configuration.nadir_boresight_x = 350;
					kit->adcs.configuration.nadir_boresight_y = 240;
					kit->adcs.configuration.nadir_shift = 0x1;
					kit->adcs.configuration.min_x_of_area_1 = 0;
					kit->adcs.configuration.max_x_of_area_1 = 0;
					kit->adcs.configuration.min_y_of_area_1 = 0;
					kit->adcs.configuration.max_y_of_area_1 = 0;
					kit->adcs.configuration.min_x_of_area_2 = 0;
					kit->adcs.configuration.max_x_of_area_2 = 0;
					kit->adcs.configuration.min_y_of_area_2 = 0;
					kit->adcs.configuration.max_y_of_area_2 = 0;
					kit->adcs.configuration.min_x_of_area_3 = 0;
					kit->adcs.configuration.max_x_of_area_3 = 0;
					kit->adcs.configuration.min_y_of_area_3 = 0;
					kit->adcs.configuration.max_y_of_area_3 = 0;
					kit->adcs.configuration.min_x_of_area_4 = 0;
					kit->adcs.configuration.max_x_of_area_4 = 0;
					kit->adcs.configuration.min_y_of_area_4 = 0;
					kit->adcs.configuration.max_y_of_area_4 = 0;
					kit->adcs.configuration.min_x_of_area_5 = 0;
					kit->adcs.configuration.max_x_of_area_5 = 0;
					kit->adcs.configuration.min_y_of_area_5 = 0;
					kit->adcs.configuration.max_y_of_area_5 = 0;
					kit->adcs.configuration.magnetometer_transform_angle_alpha = 0;
					kit->adcs.configuration.magnetometer_transform_angle_beta = 0;
					kit->adcs.configuration.magnetometer_transform_angle_gamma = 9000;
					kit->adcs.configuration.magnetometer_channel1_offset = 0;
					kit->adcs.configuration.magnetometer_channel2_offset = 0;
					kit->adcs.configuration.magnetometer_channel3_offset = 0;
					kit->adcs.configuration.magnetometer_sens_mat_s11 = 2000;
					kit->adcs.configuration.magnetometer_sens_mat_s12 = 0;
					kit->adcs.configuration.magnetometer_sens_mat_s13 = 0;
					kit->adcs.configuration.magnetometer_sens_mat_s21 = 0;
					kit->adcs.configuration.magnetometer_sens_mat_s22 = 2000;
					kit->adcs.configuration.magnetometer_sens_mat_s23 = 0;
					kit->adcs.configuration.magnetometer_sens_mat_s31 = 0;
					kit->adcs.configuration.magnetometer_sens_mat_s32 = 0;
					kit->adcs.configuration.magnetometer_sens_mat_s33 = 2000;
					kit->adcs.configuration.y_rate_sensor_offset = 0;
					kit->adcs.configuration.y_rate_sensor_transform_angle_alpha = 0;
					kit->adcs.configuration.y_rate_sensor_transform_angle_gamma = 0;
					kit->adcs.configuration.detumbling_spin_gain = 20.0;
					kit->adcs.configuration.detumbling_damping_gain = 60.0;
					kit->adcs.configuration.ref_spin_rate = -2200;
					kit->adcs.configuration.y_momentum_control_gain = 20.0;
					kit->adcs.configuration.y_momentum_nut_derivative_gain = 50.0;
					kit->adcs.configuration.y_momentum_nut_proportional_gain = 2.0;
					kit->adcs.configuration.y_momentum_proportional_gain = 0.0004;
					kit->adcs.configuration.y_momentum_derivative_gain = 0.004;
					kit->adcs.configuration.ref_wheel_momentum = -0.0005;
					kit->adcs.configuration.ref_wheel_momentum_init_pitch = -0.0003;
					kit->adcs.configuration.ref_wheel_torque_init_pitch = 500;
					kit->adcs.configuration.moment_of_inertia_xx = 0.0027;
					kit->adcs.configuration.moment_of_inertia_yy = 0.0097;
					kit->adcs.configuration.moment_of_inertia_zz = 0.0091;
					kit->adcs.configuration.moment_of_inertia_xy = 0;
					kit->adcs.configuration.moment_of_inertia_xz = 0;
					kit->adcs.configuration.moment_of_inertia_yz = 0;
					kit->adcs.configuration.magnetometer_rate_filter_system_noise = 0.0001;
					kit->adcs.configuration.EKF_system_noise = 0.00000000001;
					kit->adcs.configuration.css_meas_noise = 1.0;
					kit->adcs.configuration.sun_sensor_meas_noise = 0.02;
					kit->adcs.configuration.nadir_sensor_meas_noise = 0.01;
					kit->adcs.configuration.magnetometer_sensor_meas_noise = 0.001;
					kit->adcs.configuration.mask_senors_usage = 0x03;
					kit->adcs.configuration.sun_and_nadir_sampling_period = 0x11;
					break;
				}
				case 78: // AlbertaSat Command - load first half of config.
				{							// Ends at max y area 5. 114 bytes long
					memcpy(&adcs_conf[0], &adcs_buf[2], 114);
					break;
				}
				case 79: // AlbertaSat Command - load second half of config.
				{							// Starts at mag angle 1.
					memcpy(&adcs_conf[114], &adcs_buf[2], 126);
					memcpy(&kit->adcs.configuration, adcs_conf, 240);
					break;
				}
				case 80:
				{
					kit->adcs.setframe_set_configuration(&kit->adcs, &kit->adcs.configuration);
					break;
				}
				case 122:
				{
					csp_printf("Starting upload");
					FILE* fid;
					uint32_t up_length;
					uint16_t up_crc;
					uint16_t block_crc = 0x0;
					uint16_t blocks = 0;
					uint16_t old_block = 1;
					uint8_t retries = 0;
					uint32_t iterations = 0;
					uint32_t partial = 0;
					fid = fopen("/sd/adcs_dat.bin","r");
					fread(&up_length,4,1,fid);
					fread(&up_crc,2,1,fid);
					fclose(fid);
					for(uint32_t i=0;i<4096;i++)
					{
						if((256*i) >= up_length)
						{
							iterations = i;
							csp_printf("%u blocks to upload.",iterations);
							break;
						}
					}
					csp_printf("Erasing data in program index");
					while(kit->adcs.uploaded_program_status.programming_completed != 1)
					{
						kit->adcs.erase_program.boot_index = 7;
						kit->adcs.setframe_erase_program(&kit->adcs, &kit->adcs.erase_program);
						vTaskDelay(2000);
						kit->adcs.getframe_uploaded_program_status(&kit->adcs);
					}
					fid = fopen("/sd/adcs_bin.bin","r");
					while(blocks < iterations)
					{
						if((old_block != blocks) && (blocks != iterations-1))
						{
							fread(kit->adcs.upload_program_block.data, 256, 1, fid);
							block_crc  = CRC_calc(&kit->adcs.upload_program_block.data[0], &kit->adcs.upload_program_block.data[256]);
							kit->adcs.upload_program_block.program_index = 7;
							kit->adcs.upload_program_block.block_number = blocks;
							old_block = blocks;
							kit->adcs.setframe_upload_program_block(&kit->adcs, &kit->adcs.upload_program_block);
							vTaskDelay(100);
						}
						else if((old_block != blocks) && (blocks == iterations-1))
						{
							partial = up_length - (256*(iterations-1));
							memset(kit->adcs.upload_program_block.data, 0, 256);
							fread(kit->adcs.upload_program_block.data, partial, 1, fid);
							block_crc  = CRC_calc(&kit->adcs.upload_program_block.data[0], &kit->adcs.upload_program_block.data[partial]);
							kit->adcs.upload_program_block.program_index = 7;
							kit->adcs.upload_program_block.block_number = blocks;
							old_block = blocks;
							kit->adcs.setframe_upload_program_block(&kit->adcs, &kit->adcs.upload_program_block);
							vTaskDelay(100);
						}
						if(retries >= 10)
						{
							csp_printf("Too many failed  blocks. Halting.");
							break;
						}
						kit->adcs.getframe_uploaded_program_status(&kit->adcs);
						if(kit->adcs.uploaded_program_status.programming_completed != 1)
						{
							csp_printf("Block not programmed");
							retries = retries + 1;
							vTaskDelay(500);
							continue;
						}
						kit->adcs.getframe_uploaded_program_status(&kit->adcs);
						//csp_printf("Calc crc: %x, ADCS crc: %x", block_crc, kit->adcs.uploaded_program_status.CRC16_checksum);
						if(kit->adcs.uploaded_program_status.CRC16_checksum != block_crc)
						{
							csp_printf("Failed crc");
							retries = retries + 1;
							vTaskDelay(500);
							kit->adcs.setframe_upload_program_block(&kit->adcs, &kit->adcs.upload_program_block);
							continue;
						}
						csp_printf("block %d uploaded", blocks);
						blocks = blocks + 1;
					}
					csp_printf("Upload complete. Run cmd 123.");
					fclose(fid);
					break;
				}
				case 123:
				{
					FILE* fid;
					uint32_t up_length;
					uint16_t up_crc;
					char prog_name[64] = "Testing";
					fid = fopen("/sd/adcs_dat.bin","r");
					fread(&up_length,4,1,fid);
					fread(&up_crc,2,1,fid);
					fclose(fid);
					kit->adcs.finalize_program_upload.boot_index = 7;
					kit->adcs.finalize_program_upload.program_length = up_length;
					kit->adcs.finalize_program_upload.checksum = up_crc;
					csp_printf("Length: %d, CRC: %x", kit->adcs.finalize_program_upload.program_length, kit->adcs.finalize_program_upload.checksum);
					memcpy(kit->adcs.finalize_program_upload.prgram_description, prog_name, 64);
					kit->adcs.setframe_finalize_program_upload(&kit->adcs, &kit->adcs.finalize_program_upload);
					break;
				}
				case 0:
				{
					csp_printf("Cmd is 0 in ADCS task.");
					break;
				}
				default:
				{
					error = adcs_set(adcs_buf, adcs_length);
					if(error == -1)
					{
						csp_printf("Cmd failed");
					}
				}
			}
			// Send command execution status to ground segment.
			csp_transaction(CSP_PRIO_NORM, 10, OBC_CMD_RSP_PORT, 1200, &error, sizeof(error), NULL, 0);
		}
	}
}

uint16_t CRC_calc(uint8_t *start, uint8_t *end)
{
	uint16_t crc = 0x0;
	uint8_t *data;
	for (data = start; data < end; data++)
	{
	crc = (crc >> 8) | (crc << 8);
	crc ^= *data;
	crc ^= (crc & 0xff) >> 4;
	crc ^= crc << 12;
	crc ^= (crc & 0xff) << 5;
	}
	return crc;
}

void initialize_adcs_nanomind( adcs_t *adcs )
{
	void adcs_get_timeout(xTimerHandle xTimer);
	timer=xTimerCreate((const signed char *)"ADCS_TIMEOUT",5000,pdFALSE,NULL,adcs_get_timeout);

	DEV_ASSERT( adcs );

	extern void initialize_adcs_( adcs_t* );
	initialize_adcs_( adcs );

	adcs->detumble_command = &detumble_command;

	adcs->getframe_identification = &adcs_getframe_identification;
	adcs->getframe_extended_identification = &adcs_getframe_extended_identification;
	adcs->getframe_communication_status = &adcs_getframe_communication_status;
	adcs->getframe_telecommand_acknowledge = &adcs_getframe_telecommand_acknowledge;
	adcs->getframe_actuator_commands = &adcs_getframe_actuator_commands;
	adcs->getframe_acp_execution_state = &adcs_getframe_acp_execution_state;
	adcs->getframe_acp_execution_times = &adcs_getframe_acp_execution_times;
	adcs->getframe_edac_and_latchup_counters = &adcs_getframe_edac_and_latchup_counters;
	adcs->getframe_start_up_mode = &adcs_getframe_start_up_mode;
	adcs->getframe_file_information = &adcs_getframe_file_information;
	adcs->getframe_file_block_crc = &adcs_getframe_file_block_crc;
	adcs->getframe_file_data_block = &adcs_getframe_file_data_block;
	adcs->getframe_power_control_selection = &adcs_getframe_power_control_selection;
	adcs->getframe_power_and_temperature_measurements = &adcs_getframe_power_and_temperature_measurements;
	adcs->getframe_adcs_state = &adcs_getframe_adcs_state;
	adcs->getframe_adcs_measurements = &adcs_getframe_adcs_measurements;
	adcs->getframe_current_time = &adcs_getframe_current_time;
	adcs->getframe_current_state = &adcs_getframe_current_state;
	adcs->getframe_estimated_attitude_angles = &adcs_getframe_estimated_attitude_angles;
	adcs->getframe_estimated_angular_rates = &adcs_getframe_estimated_angular_rates;
	adcs->getframe_satellite_position_llh = &adcs_getframe_satellite_position_llh;
	adcs->getframe_satellite_velocity_eci = &adcs_getframe_satellite_velocity_eci;
	adcs->getframe_magnetic_field_vector = &adcs_getframe_magnetic_field_vector;
	adcs->getframe_coarse_sun_vector = &adcs_getframe_coarse_sun_vector;
	adcs->getframe_fine_sun_vector = &adcs_getframe_fine_sun_vector;
	adcs->getframe_nadir_vector = &adcs_getframe_nadir_vector;
	adcs->getframe_rate_sensor_rates = &adcs_getframe_rate_sensor_rates;
	adcs->getframe_wheel_speed = &adcs_getframe_wheel_speed;
	adcs->getframe_cubesense_current = &adcs_getframe_cubesense_current;
	adcs->getframe_cubecontrol_current = &adcs_getframe_cubecontrol_current;
	adcs->getframe_peripheral_current_and_temperature_measurements = &adcs_getframe_peripheral_current_and_temperature_measurements;
	adcs->getframe_raw_sensor_measurements = &adcs_getframe_raw_sensor_measurements;
	adcs->getframe_angular_rate_covariance = &adcs_getframe_angular_rate_covariance;
	adcs->getframe_raw_nadir_sensor = &adcs_getframe_raw_nadir_sensor;
	adcs->getframe_raw_sun_sensor = &adcs_getframe_raw_sun_sensor;
	adcs->getframe_raw_css = &adcs_getframe_raw_css;
	adcs->getframe_raw_magnetometer = &adcs_getframe_raw_magnetometer;
	adcs->getframe_raw_gps_status = &adcs_getframe_raw_gps_status;
	adcs->getframe_raw_gps_time = &adcs_getframe_raw_gps_time;
	adcs->getframe_raw_gps_x = &adcs_getframe_raw_gps_x;
	adcs->getframe_raw_gps_y = &adcs_getframe_raw_gps_y;
	adcs->getframe_raw_gps_z = &adcs_getframe_raw_gps_z;
	adcs->getframe_estimation_data = &adcs_getframe_estimation_data;
	adcs->getframe_igrf_modelled_magnetic_field_vector = &adcs_getframe_igrf_modelled_magnetic_field_vector;
	adcs->getframe_modelled_sun_vector = &adcs_getframe_modelled_sun_vector;
	adcs->getframe_estimated_gyro_bias = &adcs_getframe_estimated_gyro_bias;
	adcs->getframe_estimation_innovation_vector = &adcs_getframe_estimation_innovation_vector;
	adcs->getframe_quaternion_error_vector = &adcs_getframe_quaternion_error_vector;
	adcs->getframe_quaternion_covariance = &adcs_getframe_quaternion_covariance;
	adcs->getframe_magnetorquer_command = &adcs_getframe_magnetorquer_command;
	adcs->getframe_wheel_speed_commands = &adcs_getframe_wheel_speed_commands;
	adcs->getframe_sgp4_orbit_parameters = &adcs_getframe_sgp4_orbit_parameters;
	adcs->getframe_configuration = &adcs_getframe_configuration;
	adcs->getframe_status_of_image_capture_and_save_operation = &adcs_getframe_status_of_image_capture_and_save_operation;
	adcs->getframe_uploaded_program_status = &adcs_getframe_uploaded_program_status;
	adcs->getframe_get_flash_program_list = &adcs_getframe_get_flash_program_list;
	adcs->getframe_boot_index = &adcs_getframe_boot_index;
	adcs->setframe_reset = &adcs_setframe_reset;
	adcs->setframe_set_unix_time = &adcs_setframe_set_unix_time;
	adcs->setframe_adcs_run_mode = &adcs_setframe_adcs_run_mode;
	adcs->setframe_selected_logged_data = &adcs_setframe_selected_logged_data;
	adcs->setframe_power_control = &adcs_setframe_power_control;
	adcs->setframe_deploy_magnetometer_boom = &adcs_setframe_deploy_magnetometer_boom;
	adcs->setframe_trigger_adcs_loop = &adcs_setframe_trigger_adcs_loop;
	adcs->setframe_set_attitude_estimation_mode = &adcs_setframe_set_attitude_estimation_mode;
	adcs->setframe_set_attitude_control_mode = &adcs_setframe_set_attitude_control_mode;
	adcs->setframe_set_commanded_attitude_angles = &adcs_setframe_set_commanded_attitude_angles;
	adcs->setframe_set_wheel = &adcs_setframe_set_wheel;
	adcs->setframe_set_magnetorquer_output = &adcs_setframe_set_magnetorquer_output;
	adcs->setframe_set_start_up_mode = &adcs_setframe_set_start_up_mode;
	adcs->setframe_set_sgp4_orbit_parameters = &adcs_setframe_set_sgp4_orbit_parameters;
	adcs->setframe_set_configuration = &adcs_setframe_set_configuration;
	adcs->setframe_set_magnetorquer = &adcs_setframe_set_magnetorquer;
	adcs->setframe_set_wheel_configuration = &adcs_setframe_set_wheel_configuration;
	adcs->setframe_set_css_configuration = &adcs_setframe_set_css_configuration;
	adcs->setframe_set_sun_sensor_configuration = &adcs_setframe_set_sun_sensor_configuration;
	adcs->setframe_set_nadir_sensor_configuration = &adcs_setframe_set_nadir_sensor_configuration;
	adcs->setframe_set_magnetometer_configuration = &adcs_setframe_set_magnetometer_configuration;
	adcs->setframe_set_rate_sensor_configuration = &adcs_setframe_set_rate_sensor_configuration;
	adcs->setframe_set_detumbling_control_parameters = &adcs_setframe_set_detumbling_control_parameters;
	adcs->setframe_set_y_momentum_control_parameters = &adcs_setframe_set_y_momentum_control_parameters;
	adcs->setframe_set_moment_of_inertia = &adcs_setframe_set_moment_of_inertia;
	adcs->setframe_set_estimation_parameters = &adcs_setframe_set_estimation_parameters;
	adcs->setframe_save_configuration = &adcs_setframe_save_configuration;
	adcs->setframe_save_orbit_parameters = &adcs_setframe_save_orbit_parameters;
	adcs->setframe_capture_and_save_image = &adcs_setframe_capture_and_save_image;
	adcs->setframe_reset_file_list = &adcs_setframe_reset_file_list;
	adcs->setframe_advance_file_list_index = &adcs_setframe_advance_file_list_index;
	adcs->setframe_initialize_file_download = &adcs_setframe_initialize_file_download;
	adcs->setframe_advance_file_read_pointer = &adcs_setframe_advance_file_read_pointer;
	adcs->setframe_erase_file = &adcs_setframe_erase_file;
	adcs->setframe_erase_all_files = &adcs_setframe_erase_all_files;
	adcs->setframe_set_boot_index = &adcs_setframe_set_boot_index;
	adcs->setframe_erase_program = &adcs_setframe_erase_program;
	adcs->setframe_upload_program_block = &adcs_setframe_upload_program_block;
	adcs->setframe_finalize_program_upload = &adcs_setframe_finalize_program_upload;
	adcs->setframe_reset_mcu = &adcs_setframe_reset_mcu;
	adcs->setframe_run_program_now = &adcs_setframe_run_program_now;
	adcs->setframe_copy_to_internal_flash = &adcs_setframe_copy_to_internal_flash;
	adcs->setframe_reset_boot_statistics = &adcs_setframe_reset_boot_statistics;
}
