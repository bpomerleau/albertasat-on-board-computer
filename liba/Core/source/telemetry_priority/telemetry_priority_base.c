/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telemetry_priority_base.c
 * @author Brendan Bruner
 * @date May 28, 2015
 */

#include <filesystems/file.h>
#include <telemetry_priority/telemetry_priority.h>
#include <packets/telemetry_packet.h>
#include <core_defines.h>
#include <printing.h>


/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define ASSERT_FS_OP( fs, file, err )											\
	do {																		\
		if( (err) != FS_OK ) {													\
			fs->close( fs, file );												\
			post_semaphore( _prio_global_config_lock_ );						\
			return TP_FS_ERR;													\
		}																		\
	} while( 0 )

#define ONE_BYTE 1

#define PRIO_NOT_INIT 	0
#define PRIO_IS_INIT 	1

#define TIMER_NAME (signed char*) "config timer"
#define TIMER_PERIOD CONFIG_LOCK_DELAY
#define TIMER_ID (void *) 0

/********************************************************************************/
/* Singletons																	*/
/********************************************************************************/
static semaphore_t _prio_global_config_lock_;
static software_timer_t _prio_config_time_out_;

static uint8_t _prio_is_init_ = PRIO_NOT_INIT;


/********************************************************************************/
/* Method Declares																*/
/********************************************************************************/
/**
 * @memberof telemetry_priority_t
 * @private
 * @brief
 * 		Reset config file to default.
 * @details
 * 		Reset config file to default.
 */
static tp_error_t _telemetry_priority_default_config_( telemetry_priority_t *self )
{
	DEV_ASSERT( self );

	/* New log file, use default priorities. */
	uint32_t 		written;
	filesystem_t 	*fs;
	file_t			*log_file;
	fs_error_t		fs_err;

	fs = self->_fs;

	/* Set cached copy to default. */
	lock_mutex( self->_data_mutex, BLOCK_FOREVER );
	self->_priority_._wod_priority = TELEMETRY_PRIORITY_DEFAULT_WOD;
	self->_priority_._dfgm_priority = TELEMETRY_PRIORITY_DEFAULT_DFGM;
	self->_priority_._mnlp_priority = TELEMETRY_PRIORITY_DEFAULT_MNLP;
	self->_priority_._cmnd_status_priority = TELEMETRY_PRIORITY_DEFAULT_CMND_STATUS;
	self->_priority_._state_priority = TELEMETRY_PRIORITY_DEFAULT_STATE;
	unlock_mutex( self->_data_mutex );

	/* Write default priorities to the log file. */
	take_semaphore( _prio_global_config_lock_, BLOCK_FOREVER );
	log_file = fs->open( fs, &fs_err, self->_log_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( fs, log_file, fs_err );
	fs_err = log_file->write( log_file, (uint8_t *) &self->_priority_, sizeof(self->_priority_), &written );
	ASSERT_FS_OP( fs, log_file, fs_err );
	fs->close( fs, log_file );
	post_semaphore( _prio_global_config_lock_ );

	if( written < sizeof(self->_priority_) )
	{
		return TP_FULL;
	}
	return TP_OK;
}

static void _prio_timer_call_back_( software_timer_t xTimer )
{
	UNUSED( xTimer );
	unlock_mutex( _prio_global_config_lock_ );
}

base_t telemetry_priority_lock_config_globally( block_time_t block_time )
{
	base_t success;
	success = take_semaphore( _prio_global_config_lock_, block_time );
	if( success == SEMAPHORE_ACQUIRED )
	{
		restart_timer( _prio_config_time_out_, BLOCK_FOREVER );
	}
	return success;
}

void telemetry_priority_unlock_config_globally( )
{
	post_semaphore( _prio_global_config_lock_ );
	stop_timer( _prio_config_time_out_, BLOCK_FOREVER );
}

tp_error_t telemetry_priority_refresh_configuration( telemetry_priority_t *self )
{
	DEV_ASSERT( self );

	fs_error_t 	fs_err;
	file_t 		*log_file;
	uint32_t 	read;
	filesystem_t *fs;
	telemetry_priority_config_t config;
	uint32_t iter;

	fs = self->_fs;


	/* Open log file. */
	take_semaphore( _prio_global_config_lock_, BLOCK_FOREVER );

	log_file = fs->open( fs, &fs_err, self->_log_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( fs, log_file, fs_err );

	fs_err = log_file->read( log_file, (uint8_t *) &config, sizeof(config), &read );
	ASSERT_FS_OP( fs, log_file, fs_err );
	fs->close( fs, log_file );

	post_semaphore( _prio_global_config_lock_ );


	/* Detect corruption. */
	uint8_t max_prio = 0;
	if( read < sizeof(config) )
	{
		/* Corruption. Config file is too small. */
		return _telemetry_priority_default_config_( self );
	}
	for( iter = 0; iter < sizeof(config); ++iter )
	{
		uint8_t config_value = ((uint8_t *) &config)[iter];
		if(  config_value > max_prio )
		{
			max_prio = config_value;
		}
	}

	if( max_prio > TELEMETRY_PRIORITY_MAX )
	{
		/* Corruption. One or more priorities are out of bounds. */
		return _telemetry_priority_default_config_( self );
	}

	/* Refresh cached data. */
	lock_mutex( self->_data_mutex, BLOCK_FOREVER );
	self->_priority_ = config;
	unlock_mutex( self->_data_mutex );


	return TP_OK;
}

uint8_t get_telemetry_priority_wod( telemetry_priority_t *prio )
{
	DEV_ASSERT( prio );

	uint8_t data;

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	data = prio->_priority_._wod_priority;
	unlock_mutex( prio->_data_mutex );

	return data;
}

uint8_t get_telemetry_priority_dfgm( telemetry_priority_t *prio )
{
	DEV_ASSERT( prio );

	uint8_t data;

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	data = prio->_priority_._dfgm_priority;
	unlock_mutex( prio->_data_mutex );

	return data;
}

uint8_t get_telemetry_priority_mnlp( telemetry_priority_t *prio )
{
	DEV_ASSERT( prio );

	uint8_t data;

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	data = prio->_priority_._mnlp_priority;
	unlock_mutex( prio->_data_mutex );

	return data;
}

uint8_t get_telemetry_priority_cmnd_status( telemetry_priority_t *prio )
{
	DEV_ASSERT( prio );

	uint8_t data;

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	data = prio->_priority_._cmnd_status_priority;
	unlock_mutex( prio->_data_mutex );

	return data;
}

uint8_t get_telemetry_priority_state( telemetry_priority_t *prio )
{
	DEV_ASSERT( prio );

	uint8_t data;

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	data = prio->_priority_._state_priority;
	unlock_mutex( prio->_data_mutex );

	return data;
}

tp_error_t set_telemetry_priority_wod( telemetry_priority_t *prio, uint8_t set_value )
{
	DEV_ASSERT( prio );

	filesystem_t *fs;
	file_t *log_file;
	fs_error_t	fs_err;
	uint32_t written;

	if( set_value > TELEMETRY_PRIORITY_MAX )
	{
		return TP_INV_PRM;
	}

	take_semaphore( _prio_global_config_lock_, BLOCK_FOREVER );

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	prio->_priority_._wod_priority = set_value;
	unlock_mutex( prio->_data_mutex );

	/* Open log file and write the new priority to it. */
	fs = prio->_fs;
	log_file = fs->open( fs, &fs_err, prio->_log_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( fs, log_file, fs_err );

	/* File is already allocated in memory, write cannot fail because out of memory. */
	fs_err = log_file->write( log_file, (uint8_t *) &prio->_priority_, sizeof(prio->_priority_), &written );
	ASSERT_FS_OP( fs, log_file, fs_err );

	fs->close( fs, log_file );
	post_semaphore( _prio_global_config_lock_ );

	return TP_OK;
}

tp_error_t set_telemetry_priority_dfgm( telemetry_priority_t *prio, uint8_t set_value )
{
	DEV_ASSERT( prio );

	filesystem_t *fs;
	file_t *log_file;
	fs_error_t	fs_err;
	uint32_t written;

	if( set_value > TELEMETRY_PRIORITY_MAX ) return TP_INV_PRM;

	take_semaphore( _prio_global_config_lock_, BLOCK_FOREVER );

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	prio->_priority_._dfgm_priority = set_value;
	unlock_mutex( prio->_data_mutex );

	/* Open log file and write the new priority to it. */
	fs = prio->_fs;
	log_file = fs->open( fs, &fs_err, prio->_log_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( fs, log_file, fs_err );

	/* File is already allocated in memory, write cannot fail because out of memory. */
	fs_err = log_file->write( log_file, (uint8_t *) &prio->_priority_, sizeof(prio->_priority_), &written );
	ASSERT_FS_OP( fs, log_file, fs_err );

	fs->close( fs, log_file );
	post_semaphore( _prio_global_config_lock_ );

	return TP_OK;
}

tp_error_t set_telemetry_priority_mnlp( telemetry_priority_t *prio, uint8_t set_value )
{
	DEV_ASSERT( prio );

	filesystem_t *fs;
	file_t *log_file;
	fs_error_t	fs_err;
	uint32_t written;

	if( set_value > TELEMETRY_PRIORITY_MAX ) return TP_INV_PRM;

	take_semaphore( _prio_global_config_lock_, BLOCK_FOREVER );

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	prio->_priority_._mnlp_priority = set_value;
	unlock_mutex( prio->_data_mutex );

	/* Open log file and write the new priority to it. */
	fs = prio->_fs;
	log_file = fs->open( fs, &fs_err, prio->_log_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( fs, log_file, fs_err );

	/* File is already allocated in memory, write cannot fail because out of memory. */
	fs_err = log_file->write( log_file, (uint8_t *) &prio->_priority_, sizeof(prio->_priority_), &written );
	ASSERT_FS_OP( fs, log_file, fs_err );

	fs->close( fs, log_file );
	post_semaphore( _prio_global_config_lock_ );

	return TP_OK;
}

tp_error_t set_telemetry_priority_cmnd_status( telemetry_priority_t *prio, uint8_t set_value )
{
	DEV_ASSERT( prio );

	filesystem_t *fs;
	file_t *log_file;
	fs_error_t	fs_err;
	uint32_t written;

	if( set_value > TELEMETRY_PRIORITY_MAX ) return TP_INV_PRM;

	take_semaphore( _prio_global_config_lock_, BLOCK_FOREVER );

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	prio->_priority_._cmnd_status_priority = set_value;
	unlock_mutex( prio->_data_mutex );

	/* Open log file and write the new priority to it. */
	fs = prio->_fs;
	log_file = fs->open( fs, &fs_err, prio->_log_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( fs, log_file, fs_err );

	/* File is already allocated in memory, write cannot fail because out of memory. */
	fs_err = log_file->write( log_file, (uint8_t *) &prio->_priority_, sizeof(prio->_priority_), &written );
	ASSERT_FS_OP( fs, log_file, fs_err );

	fs->close( fs, log_file );
	post_semaphore( _prio_global_config_lock_ );

	return TP_OK;
}

tp_error_t set_telemetry_priority_state( telemetry_priority_t *prio, uint8_t set_value )
{
	DEV_ASSERT( prio );

	filesystem_t *fs;
	file_t *log_file;
	fs_error_t	fs_err;
	uint32_t written;

	if( set_value > TELEMETRY_PRIORITY_MAX ) return TP_INV_PRM;

	take_semaphore( _prio_global_config_lock_, BLOCK_FOREVER );

	lock_mutex( prio->_data_mutex, BLOCK_FOREVER );
	prio->_priority_._state_priority = set_value;
	unlock_mutex( prio->_data_mutex );

	/* Open log file and write the new priority to it. */
	fs = prio->_fs;
	log_file = fs->open( fs, &fs_err, prio->_log_name, FS_OPEN_ALWAYS, BLOCK_FOREVER );
	ASSERT_FS_OP( fs, log_file, fs_err );

	/* File is already allocated in memory, write cannot fail because out of memory. */
	fs_err = log_file->write( log_file, (uint8_t *) &prio->_priority_, sizeof(prio->_priority_), &written );
	ASSERT_FS_OP( fs, log_file, fs_err );

	fs->close( fs, log_file );
	post_semaphore( _prio_global_config_lock_ );

	return TP_OK;
}


/********************************************************************************/
/* Initialization Method														*/
/********************************************************************************/
tp_error_t initialize_telemetry_priority( telemetry_priority_t *prio, filesystem_t *fs, char const *log_name )
{
	DEV_ASSERT( prio );
	DEV_ASSERT( fs );
	DEV_ASSERT( log_name );

	uint8_t iter;

	prio->_fs = fs;

	/* Initialize singleton lock for configuration changes. */
	if( _prio_is_init_ == PRIO_NOT_INIT )
	{
		new_semaphore( _prio_global_config_lock_, 1, 1 ); /* Posted binary semaphore. */
		if( _prio_global_config_lock_ == NULL )
		{
			return TP_INIT_ERR;
		}


		new_timer( _prio_config_time_out_, TIMER_NAME, TIMER_PERIOD, MANUAL_RESTART, TIMER_ID, _prio_timer_call_back_ );
		if( _prio_config_time_out_ == NULL )
		{
			delete_semaphore( _prio_global_config_lock_ );
			return TP_INIT_ERR;
		}
		stop_timer( _prio_config_time_out_, BLOCK_FOREVER );

		_prio_is_init_ = PRIO_IS_INIT;
	}

	/* Copy log name. */
	for( iter = 0; iter < FILESYSTEM_MAX_NAME_LENGTH; ++iter )
	{
		prio->_log_name[iter] = log_name[iter];
		if( log_name[iter] == '\0' )
		{
			break;
		}
	}
	if( iter <= (FILESYSTEM_MAX_NAME_LENGTH-1) )
	{
		prio->_log_name[FILESYSTEM_MAX_NAME_LENGTH] = '\0';
	}
	else
	{
		/* name is to long. */
		return TP_INV_PRM;
	}

	/* Create mutex. */
	/* Must be done before refreshing configuration file. */
	new_mutex( prio->_data_mutex );
	if( prio->_data_mutex == NULL )
	{
		return TP_INIT_ERR;
	}

	/* Cache the configuration file. */
	telemetry_priority_refresh_configuration( prio );

	return TP_OK;
}

void destroy_telemetry_priority( telemetry_priority_t *prio )
{
	DEV_ASSERT( prio );
	delete_mutex( prio->_data_mutex );
}
