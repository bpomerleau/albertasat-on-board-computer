/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file teledyne.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <teledyne/teledyne.h>
#include <eps/power_lines.h>
#include <driver_toolkit/driver_toolkit.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>
#include <dev/arm/ds1302.h>
#include <core_defines.h>
#include <inttypes.h>
/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/

extern spi_chip_t spi_udos_chip;
extern driver_toolkit_nanomind_t drivers;

/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
/**
 * @brief
 * 		Creates an event and logs the data to the event argument. A time stamp
 * 		is generated and logged to the event argument as well. The time stamp
 * 		is generated just before sampling the ADC
 * @param event
 * 		Pointer to valid event structure. Will be filled in with the event's data.
 * @param total_samples
 * 		The number of samples that can fit in an event.
 * @return
 * 		0 on success, -1 on failure to can access to the ADC's data bus.
 */
int udos_take_samples( struct udos_event_t* event )
{
	bool stat;
	size_t i;
	driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;
	uint32_t seconds;
	rtc_t *rtc;
	rtc = kit->rtc;

	stat = udos_start(&spi_udos_chip);
	if( stat == 0 ) {
		/* Do a dummy read to clear old hold
		 */
		udos_init_adc(&spi_udos_chip);
		for( i = 0; i < UDOS_ADC_CHANNELS; ++i ) {
			udos_read(&spi_udos_chip);
		}

		/* Take samples
		 */
		seconds = (uint32_t) rtc->get_ds1302_time();
		for( i = 0; i < UDOS_SAMPLES_PER_EVENT; i++ )
		{
			udos_init_adc(&spi_udos_chip);
			event->channel0[i] = udos_read(&spi_udos_chip);
			event->channel1[i] = udos_read(&spi_udos_chip);
			event->channel2[i] = udos_read(&spi_udos_chip);
			event->channel3[i] = udos_read(&spi_udos_chip);
			event->channel4[i] = udos_read(&spi_udos_chip);
			event->channel5[i] = udos_read(&spi_udos_chip);
		}
		event->time_stamp = seconds;
		udos_stop(&spi_udos_chip);
		spi_setup_chip(&spi_udos_chip); // Do this after to clear the register messes.
		udos_done(&spi_udos_chip);
		return 0;
	}
	return -1;
}

/**
 * @details
 * 		Initializes the default values of the packet
 * 		Should be static, but gets used by the test suite to verify
 * 		udos code. don't use this function outside of the test suite
 * 		and this compilation unit (ie, this file).
 */
void udos_init_packet( struct teledyne_t* self, uint8_t pid )
{
	struct udos_packet_t* packet = &self->packet;

	packet->dle = 0x10;
	packet->pid = pid;
	packet->pv = UDOS_PACKET_VERSION;
	packet->stx = 0x02;
	packet->etx = 0x03;
	packet->samples_per_event = UDOS_SAMPLES_PER_EVENT;
	packet->events_recorded = 0;
	packet->eot = 0x04;
}

/**
 * @brief
 * 		Inserts the event into the packet
 * @param packet
 * 		The packet to the log the event to
 * @param event
 * 		The event to be logged into a packet
 */
static void udos_packetize_event( teledyne_t* self, struct udos_event_t* event )
{
	struct udos_packet_t* packet = &self->packet;

	packet->events[packet->events_recorded] = *event;
	++packet->events_recorded;
}

/**
 * @brief
 * 		Initiates an event and logs the recorded data to the input argument packet
 */
static void udos_event( teledyne_t* self )
{
	struct udos_event_t event;

	/* Take samples.
	 */
	if( udos_take_samples(&event) < 0 ) {
		return;
	}

	/* Packetize.
	 */
	udos_packetize_event(self, &event);
}

/**
 * @brief
 * 		Logs the packet into non volatile memory.
 */
static void udos_log_packet( teledyne_t* self )
{
	file_t* file;
	uint32_t written, record_size;
	fs_error_t ferr;
	int serr;
	struct udos_packet_t* packet = &self->packet;
	struct telemetry_stream_t* stream = &self->event_stream;

	file = telemetry_stream_get_file(stream, &serr);
	if( serr == TELEMETRY_STREAM_FAIL || file == NULL ) {
		csp_printf("UDOS: failed to get stream file");
		return;
	}

	ferr = file->write(file, (uint8_t*) packet, sizeof(*packet), &written);
	if( ferr != FS_OK || written != sizeof(*packet) ) {
		csp_printf("UDOS: failed to right packet to stream");
		return;
	}

	record_size = file->size(file, &ferr);
	csp_printf("UDOS: packet written to stream. Stream size: %" PRIu32, record_size);
	telemetry_stream_close_file(stream);
}

/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
/**
 * @memberof teledyne_t
 * @brief
 * 		Turn the teledyne's power line on/off
 * @details
 * 		Turn the teledyne's power line on/off. This will invoke a hard power down / power on.
 * @param eps[in]
 * 		The eps which controls the teledyne's power line.
 * @param state
 * 		<b>true</b> to power on, <b>false</b> to power off.
 * @returns
 * 		<b>true</b> if successful, <b>false</b> otherwise.
 */
static bool_t power( teledyne_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );

	return true; // TODO add nanohub api calls
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof teledyne_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_teledyne_( teledyne_t* self )
{
	DEV_ASSERT( self );

	self->power = power;
	self->td = NULL;

	new_semaphore(self->power_ctrl, 1, 1);

	telemetry_stream_init(&self->event_stream, ((driver_toolkit_t*) &drivers)->udos_logger , UDOS_LOGGER_FILE_SIZE, TELEMETRY_STREAM_REUSE);
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
static void udos_task(void* param);
void dosimeter_start( teledyne_t* self )
{
	dosimeter_suspend(self);
	if( self->td == NULL ) {
		xTaskCreate(udos_task, DOSIMETER_TASK_NAME, DOSIMETER_TASK_STACK, self, DOSIMETER_TASK_PRIO, &self->td);
	}
}

void dosimeter_resume( teledyne_t* self )
{
	give_semaphore(self->power_ctrl);
}

bool_t dosimeter_suspend( teledyne_t* self )
{
	if( take_semaphore(self->power_ctrl, DOSIMETER_RESUME_TIMEOUT) == SEMAPHORE_BUSY ) {
		return false;
	}
	return true;
}

static void udos_task(void* param)
{
	teledyne_t* self = param;
	size_t i;

	udos_init_packet(self, 0);
	for(;;)
	{
		if( peek_semaphore(self->power_ctrl, BLOCK_FOREVER) == SEMAPHORE_AVAILABLE ) {

			/* Trigger a udos event and log the data to this packet
			 */
			udos_event(self);

			/* For ground model, provide debug prints of the samples taken.
			 */
#ifndef HARDWARE_DRIVERS_LOADED
			size_t event_index = self->packet.events_recorded == 0 ? self->packet.events_recorded : self->packet.events_recorded-1;
			for(i = 0; i < UDOS_SAMPLES_PER_EVENT; ++i) {
				csp_printf("udos sample %d", i);
				csp_printf("\t\t(%" PRIu16
						", %" PRIu16
						", %" PRIu16
						", %" PRIu16
						", %" PRIu16
						", %" PRIu16 ")",
						self->packet.events[event_index].channel0[i],
						self->packet.events[event_index].channel1[i],
						self->packet.events[event_index].channel2[i],
						self->packet.events[event_index].channel3[i],
						self->packet.events[event_index].channel4[i],
						self->packet.events[event_index].channel5[i]);
			}
#endif

			/* Log the packet to non volatile memory if it's full
			 */
			if( self->packet.events_recorded >= UDOS_EVENTS_PER_PACKET ) {
				udos_log_packet(self);
				udos_init_packet(self, (self->packet.pid + 1) % UINT8_MAX);
			}

		}
		vTaskDelay(UDOS_EVENT_PERIOD);
	}
}
