/*
 *
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file core_defines
 * @author Brendan Bruner
 * @date Oct. 2, 2017
 */

#include <core_defines.h>
#include <driver_toolkit/driver_toolkit.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>

struct driver_toolkit_nanomind_t  drivers;

/********************************************************************************/
/* SCV																			*/
/********************************************************************************/
volatile size_t ATHENA_LOGGER_MAX_CAPACITY = 			48;
volatile size_t ATHENA_LOGGER_FILE_SIZE = 				(100*1024);

volatile size_t DFGM_FILT1_LOGGER_MAX_CAPACITY =		4000;	/* 7 days of data @ 724 bytes / minute */
volatile size_t DFGM_FILT2_LOGGER_MAX_CAPACITY =		4000;	/* 7 days of data @ 724 bytes / minute */
volatile size_t MAX_DFGM_FILT_FILE_SIZE =				(100*1024);

volatile size_t DFGM_RAW_LOGGER_MAX_CAPACITY =			4000; /* */
volatile size_t MAX_DFGM_RAW_FILE_SIZE =				(100*1024);

volatile size_t ADCS_LOCALE_LOGGER_CAPACITY =			6;
volatile size_t ADCS_LOCALE_LOGGER_MAX_FILE_SIZE =		(10*1024);

volatile size_t MAX_DFGM_HK_FILE_SIZE =					1728;
volatile size_t DFGM_HK_LOGGER_MAX_CAPACITY =			14;

volatile size_t WOD_LOGGER_MAX_CAPACITY =				120;		// 5 days of WOD before rollover.
volatile size_t PACKET_TYPE_WOD_SIZE =					15720; // 131 bytes per eps hk, every 30 seconds for an hour.
volatile size_t WOD_LOG_CADENCE = 						30*1000;

volatile size_t UDOS_LOGGER_MAX_CAPACITY = 				15;
volatile size_t UDOS_LOGGER_FILE_SIZE = 				46650;
volatile size_t UDOS_EVENT_PERIOD = 					ONE_MS*30000;

