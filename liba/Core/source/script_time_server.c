/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file central_telecomander.c
 * @author Brendan Bruner
 * @date Nov 4, 2015
 */

#include <script_daemon.h>
#include <ground_station.h>
#include <script_time_server.h>
#include <portable_types.h>
#include <io/nanomind.h>
#include <printing.h>
#include <driver_toolkit/driver_toolkit_nanomind.h>
#include <dev/arm/ds1302.h>
#include <portable_types.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
extern driver_toolkit_nanomind_t drivers;

#define FLIGHT_SCHEDULE_PATH "/sd/script.bin"

/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/


/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/
static void script_time_server_load( script_time_server_t* self)
{
	//script_time_server_t* self = (script_time_server_t*) self_;
	FILE* fid;
	DEV_ASSERT( self );

	fid = fopen(FLIGHT_SCHEDULE_PATH,"r");
	if(fid != NULL) {
		fread(&self->schedule,sizeof(self->schedule),1,fid);
		fclose(fid);
		remove(FLIGHT_SCHEDULE_PATH);
		csp_printf("Loaded command script file");
	}
	else {
		csp_printf("Unable to open command script file");
	}
}

void script_time_server_kill( script_time_server_t* self )
{
	take_semaphore(self->sem, USE_POLLING);
}

static void script_time_server_task( void* self_ )
{
	script_time_server_t* self = (script_time_server_t*) self_;
	DEV_ASSERT( self );
	driver_toolkit_t* kit = (driver_toolkit_t*) &drivers;
	rtc_t *rtc;
	rtc = kit->rtc;
	uint8_t command_index;
	uint32_t unix_time;

	for( command_index = 0; command_index < self->schedule.total_commands; command_index++ ) {

		/* Wait until it's time to execute the next command in queue
		 */
		for( ;; ) {
			if( peek_semaphore(self->sem, USE_POLLING) == SEMAPHORE_BUSY ) {
				/* Time server stopped. delete it.
				 */
				vTaskDelete(NULL);
			}

			unix_time = (uint32_t) rtc->get_ds1302_time();
			if( unix_time >= self->schedule.slot[command_index].unix_time ) {
				break;
			}
			task_delay( SCRIPT_TIME_SERVER_PERIOD );
		}

		/* Queue the command into the parser's log
		 */
		ground_station_nanomind_insert( ((driver_toolkit_t*) &drivers)->gs, self->schedule.slot[command_index].command, self->schedule.slot[command_index].command_length);
	}

	/* Schedule is finished, delete the task.
	 */
	vTaskDelete(NULL);
}

/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/
static void destroy( script_time_server_t* self )
{
	DEV_ASSERT( self );
}


/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
bool_t initialize_script_time_server( script_time_server_t* self, ground_station_t* gs )
{
	DEV_ASSERT( self );
	DEV_ASSERT( gs );

	/* Link virtual methods. */
	self->destroy = destroy;
	self->load = script_time_server_load;

	/* Assign script daemon. */
	self->gs = gs;

	new_semaphore(self->sem, 1, 1);
	if( self->sem == NULL ) {
		return false;
	}

	/* Load flight schedule into memory.
	 */
	self->load(self);

	/* Create server task. */
	base_t err = create_task(	script_time_server_task,
								SCRIPT_TIME_SERVER_NAME,
								SCRIPT_TIME_SERVER_STACK,
								(void*) self,
								SCRIPT_TIME_SERVER_PRIO,
								&self->_.task_handle );
	if( err != TASK_CREATED ) {
		return false;
	}

	return true;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/


