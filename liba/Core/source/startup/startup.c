/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file startup.c
 * @author Alex Hamilton
 * @date Aug 10, 2015
 */

#include "startup.h"

#include "csp/csp.h"
#include "comm/comm_nanomind.h"
#include "adcs/adcs.h"
#include "comm/cmd_rparam.h"
#include "eps/eps.h"
#include <rparam_client.h>
#include "param/param.h"
#include "freertos/task.h"
#include <dev/arm/ds1302.h>
#include <dev/cpu.h>
#include "csp_internal.h"
#include <unistd.h>
#include "driver_toolkit/driver_toolkit.h"
#include "io/nanohub.h"

bootcause_t boot;
bootcause_t boot_check;
uint8_t hardware = 0;
uint8_t software = 0;


int StartupTest(int test_status, driver_toolkit_t * drivers){

	int CommTest(comm_t *comm);
	int PowerTest(eps_t *eps);
	//int ADCSTest(adcs_t * adcs);
	int HubTest(hub_t *hub);

	test_status |= PowerTest(drivers->eps);
	test_status |= CommTest(drivers->comm);
	//test_status |= ADCSTest(&drivers->adcs); //This needs 15 seconds each boot. Not sure what to do.
	test_status |= HubTest(drivers->hub); //TODO Fix hub tests.

	//hardware^=0xFF; // use this line once all tests are fixed.
	hardware^=0xCF; // Does not include ADCS bits.
	StartupLogH(test_status);

	if(test_status == 0)
	{
		return test_status;
	}
	csp_printf("Tests failed. Rebooting.");
	csp_reboot(1);
	return test_status;
}

void StartupLogH(uint8_t status)
{
	hardware|=status;
	boot.hard = hardware;
	FILE *fid;
	fid = fopen("/boot/bootcause.bin","w");
	fwrite(&boot,1,8,fid);
	fclose(fid);
}

void StartupLogS(uint8_t status)
{
	software|=status;
	boot.soft = software;
	FILE *fid;
	fid = fopen("/boot/bootcause.bin","w");
	fwrite(&boot,1,8,fid);
	fclose(fid);
}

static boot_status_e uffs_fail_handle( )
{
	csp_printf("Failed to open file in UFFS, all hope is lost.\nGoing into safe mode...");
	return SAFE_MODE_BOOT;
}

#define BOOT_MSG_MAX_LENGTH 256
static void start_up_log_cause( bootcause_t* boot_cause )
{
	FILE* fid;
	struct ds1302_clock date;
	time_t seconds;
	char boot_msg[BOOT_MSG_MAX_LENGTH];

	boot_cause->cause1 = (uint8_t) cpu_read_reset_cause();
	boot_cause->cause2 = (uint8_t) cpu_read_reset_cause2();
	boot_cause->sfm_cause = (uint8_t) cpu_read_reset_sfm();
	csp_printf_crit("reset cause: %d", (int) boot_cause->cause1);
	csp_printf_crit("reset cause 2: %d", (int) boot_cause->cause2);
	csp_printf_crit("reset sfm cause: %d", (int) boot_cause->sfm_cause);
	ds1302_clock_read_burst( &date );
	ds1302_clock_to_time(&seconds, &date);
	boot_cause->time = seconds;

	snprintf(boot_msg, BOOT_MSG_MAX_LENGTH,
			"{\n"
			"\tTIME = %" PRIu32 "\n"
			"\tCAUSE1 = %" PRIu8 "\n"
			"\tCAUSE2 = %" PRIu8 "\n"
			"\tSFM_CAUSE = %" PRIu8 "\n"
			"\tAPP_BOOT_COUNT = %" PRIu8 "\n"
			"\tFAT_BOOT_COUNT = %" PRIu8 "\n"
			"}\n",
			boot_cause->time, boot_cause->cause1, boot_cause->cause2, boot_cause->sfm_cause, boot_cause->application_boot_count, boot_cause->fat_boot_count);
	boot_msg[BOOT_MSG_MAX_LENGTH-1] = '\0';

	fid = fopen("/sd/boot.txt", "a");
	if( fid == NULL ) {
		return;
	}
	fwrite(boot_msg,strlen(boot_msg),sizeof(char),fid);
	fclose(fid);
}


boot_status_e start_up_diagnostics( bool_t fat_ok )
{
	FILE* fid;
	bootcause_t boot_cause = {0};

	/* Open boot cause file. */
	fid = fopen("/boot/bootcause.bin","r");
	if( fid!=NULL ) {
		/* Read out boot cause. */
		fread(&boot_cause,1,sizeof(boot_cause),fid);
		fclose(fid);
	}
	else {
		/* No boot cause file, create it. */
		csp_printf_crit("Boot file not found. Creating...");

		boot_cause = (bootcause_t){ 0 };
		fid = fopen("/boot/bootcause.bin","w");
		if( fid == NULL ) {
			/* Failed to open file in UFFS. */
			start_up_log_cause(&boot_cause);
			return uffs_fail_handle( );
		}

		/* Open file, write blank boot cause into it. */
		fwrite(&boot_cause,1,sizeof(boot_cause),fid);
		fclose(fid);
	}

	/* Alert user of fat boot count. */
	csp_printf_crit("Fat boot count: %d", boot_cause.fat_boot_count);
	if( !fat_ok ) {

		/* Fat mount failed, attempt fix. */
		if( boot_cause.fat_boot_count < MAX_FAT_REBOOT_ATTEMPTS ) {

			/* Fat mount failed, hard reboot to attempt a fix. */
			csp_printf_crit("Rebooting to fix FAT MOUNT FAIL");

			/* Increment fat boot count. */
			++boot_cause.fat_boot_count;
			fid = fopen("/boot/bootcause.bin","w");
			if( fid == NULL ) {
				/* Failed to open file in UFFS. */
				start_up_log_cause(&boot_cause);
				return uffs_fail_handle( );
			}

			/* Update fat boot count in boot cause file. */
			fwrite(&boot_cause,1,sizeof(boot_cause),fid);
			fclose(fid);

			/* Hard reset for fix. */
			start_up_log_cause(&boot_cause);
			eps_hardreset();
			vTaskDelay(2000);
		}
		else {
			/* Too many reboots to fix fat mount fail, go to safe mode. */
			csp_printf_crit("FAT BOOT OVERFLOW");
			start_up_log_cause(&boot_cause);
			return FAT_BOOT_OVERFLOW;
		}
	}

	/* Reset fat boot count to zero. */
	/* We do this to give three new boot tries the next time fat mount fails. */
	if( boot_cause.fat_boot_count != 0 ) {
		csp_printf_crit("\tReseting to zero..");
	}
	boot_cause.fat_boot_count = 0;
	fid = fopen("/boot/bootcause.bin","w");
	if( fid == NULL ) {
		start_up_log_cause(&boot_cause);
		return uffs_fail_handle( );
	}
	fwrite(&boot_cause,1,sizeof(boot_cause),fid);
	fclose(fid);

	/* Check for safe mode file, which indicates a command into safe mode. */
	fid = fopen("/boot/safemode.bin","r");
	if( fid != NULL ) {
		csp_printf_crit("Safemode file found. Safe boot commanded.");
		fclose(fid);
		start_up_log_cause(&boot_cause);
		return SAFE_MODE_BOOT;
	}
	else {
		csp_printf_crit("No safe boot command received...");
	}

	/* Increment application boot count. */
	/* This is used to track how many times the cpu has reset independent of fat mount resets. */
	csp_printf_crit("Application boot count: %d", boot_cause.application_boot_count);
	++boot_cause.application_boot_count;
	fid = fopen("/boot/bootcause.bin","w");
	if( fid == NULL ) {
		start_up_log_cause(&boot_cause);
		return uffs_fail_handle( );
	}
	fwrite(&boot_cause,1,sizeof(boot_cause),fid);
	fclose(fid);

	/* Check application reboot count. */
	if( boot_cause.application_boot_count >= MAX_APP_REBOOT_ATTEMPTS ) {
		/* Too many reboots, go to safe mode. */
		csp_printf_crit("APP REBOOT OVERFLOW");
		start_up_log_cause(&boot_cause);
		return APP_BOOT_OVERFLOW;
	}

	/* No errors, diagnostics ok. */
	start_up_log_cause(&boot_cause);
	return BOOT_OK;
}

int StartupTestInit(bool_t FS_alive, bool_t UFFS_OK){

//	cpu_reset_cause_t cause = cpu_read_reset_cause();
	return 0;
}


bool_t FSTest(bool_t FAT_OK, bool_t UFFS_OK){ // Return 0 if no errors found

	FILE* fid;
	uint8_t test_byte = 0xAF;
	uint8_t FAT_buf = 0;
	uint8_t UFFS_buf = 0;
	if(FAT_OK != 1 || UFFS_OK != 1)
	{
		csp_printf("File system check failed.");
		return 1;
	}

	fid = fopen("/boot/test.bin","w");
	fwrite(&test_byte,1,1,fid);
	fclose(fid);
	fid = fopen("/boot/test.bin","r");
	fread(&UFFS_buf,1,1,fid);
	fclose(fid);
	if(UFFS_buf != test_byte)
	{
		return 1;
	}
	remove("/boot/test.bin");

	fid = fopen("/sd/test.bin","w");
	fwrite(&test_byte,1,1,fid);
	fclose(fid);
	fid = fopen("/sd/test.bin","r");
	fread(&FAT_buf,1,1,fid);
	fclose(fid);
	if(FAT_buf != test_byte)
	{
		return 1;
	}
	remove("/sd/test.bin");

	return 0;
}

bool_t ClockTest(void){ // Return 0 if no errors found

	uint8_t write_bytes[4] = {0x4D,0x65,0x6F,0x77}; // random bytes
	uint8_t read_bytes[4] = {0};

	struct ds1302_clock clock;
	int32_t utime;
	ds1302_clock_read_burst(&clock);
	ds1302_clock_to_time(&utime,&clock);
	csp_printf("Clock: %d",utime);

	if(utime <= 0)
	{
		return 1;
	}

	ds1302_write_ram(0, &write_bytes[0], 4);
	ds1302_read_ram(0, &read_bytes[0], 4);

	for(uint8_t i=0;i<4;i++)
	{
		if(read_bytes[i] != write_bytes[i])
		{
			return 1;
		}
	}
	return 0;
}


int CommTest(comm_t * comm){

	int tests=0;
	int CommPingTest(void);
	int CommConf0Test(comm_t * comm);
	int CommConf4Test(comm_t * comm);

	StartupLogH(STARTLOG_COMM_PING);
	tests|=CommPingTest();
	if(tests!=0){
		return tests;
	}
	StartupLogH(STARTLOG_COMM_GET);
	tests|=CommConf0Test(comm);
	if(tests!=0){
		return tests;
	}
	StartupLogH(STARTLOG_COMM_GET);
	tests|=CommConf4Test(comm);
	if(tests!=0){
		return tests;
	}
	return 0;
}


int PowerTest(eps_t * eps){ /*Test the power board*/

	int tests=0;
	int PowerPingTest(void);
	int PowerConfTest(eps_t * eps);

	StartupLogH(STARTLOG_EPS_PING);
	tests|=PowerPingTest();
	if(tests!=0){
		return tests;
	}
	StartupLogH(STARTLOG_EPS_CMD);
	tests|=PowerConfTest(eps);
	if(tests!=0){
		return tests;
	}
	return 0;
}

int HubTest(hub_t *hub){
	int tests = 0;
	int HubPingTest(void);
	int HubGetTest(void);

	StartupLogH(STARTLOG_HUB_PING);
	tests|=HubPingTest();
	if(tests!=0)
	{
		return tests;
	}
	StartupLogH(STARTLOG_HUB_CMD);
	tests|=HubGetTest();
	if(tests!=0)
	{
		return tests;
	}
	return 0;
}
/*
int ADCSTest(adcs_t * adcs){ // This is broken but we don't use it.
	StartupLogH(STARTLOG_ADCS_TEST);
	return 0; // remove this line
	adcs->getframe_identification(adcs);
	//csp_printf("ADCS: %d, %d, %d",adcs->identification.node_type,adcs->identification.interface_version,adcs->identification.runtime_seconds);
	if(adcs->identification.node_type!=128){

		adcs->getframe_identification(adcs);
		if(adcs->identification.node_type!=128){

			adcs->getframe_identification(adcs);
			if(adcs->identification.node_type!=128){
				return STARTLOG_ADCS_TEST;
			}
		}
	}
	return 0;
}*/


int PowerPingTest(void){
	int result = 0;
	result=csp_ping(2,1000,100,CSP_O_NONE);
	if(result==-1){
		vTaskDelay(10/portTICK_RATE_MS);
		result=csp_ping(2,1000,100,CSP_O_NONE);
		if(result==-1){
			vTaskDelay(100/portTICK_RATE_MS);
			result=csp_ping(2,1000,100,CSP_O_NONE);
			if(result==-1){
				return STARTLOG_EPS_PING;
			}
		}
	}
	return 0;
}

int PowerConfTest(eps_t * eps){
	int result = 0;
	result=eps->hk_refresh(eps);
	if(sizeof(eps->_.hk)!=sizeof(eps_hk_t) && result!=1)
	{
		vTaskDelay(10/portTICK_RATE_MS);
		result=eps->hk_refresh(eps);
		if(sizeof(eps->_.hk)!=sizeof(eps_hk_t) && result!=1)
		{
			vTaskDelay(100/portTICK_RATE_MS);
			result=eps->hk_refresh(eps);
			if(sizeof(eps->_.hk)!=sizeof(eps_hk_t) && result!=1)
			{
				return STARTLOG_EPS_CMD;
			}
		}
	}
	return 0;
}


int CommPingTest(void){
	int tests = 0;
	/*Test the comm board*/
	tests=csp_ping(5,1000,100,CSP_O_NONE);
	if(tests==-1){ //ping failed
		vTaskDelay(10/portTICK_RATE_MS);
		tests=csp_ping(5,1000,100,CSP_O_NONE);
		if(tests==-1){ //ping failed
			vTaskDelay(100/portTICK_RATE_MS);
			tests=csp_ping(5,1000,100,CSP_O_NONE);
			if(tests==-1){ //ping failed
				return STARTLOG_COMM_PING;
			}
		}
	}
	return 0;
}

int CommConf0Test(comm_t * comm_test){
	int test,tests = 0;
	test=access("/sd/par-5-0.bin",R_OK);
	if(test!=-1){
		tests|=com_init("0");
		tests|=com_getall(comm_test);
	}
	else{
		tests|=com_download("0");
		tests|=com_getall(comm_test);
	}

	if((comm_test->preambflags!=56)||(comm_test->intfrmflags!=56))
	{
		return STARTLOG_COMM_GET;
	}
	else if(tests != 0)
	{
		return STARTLOG_COMM_GET;
	}
	else
	{
		return tests;
	}

}

int CommConf4Test(comm_t * comm_test){
	int test,tests = 0;
	test=access("/sd/par-5-4.bin",R_OK);
	if(test!=-1){
		tests|=com_init("4");
		tests|=com_getall(comm_test);
	}
	else{
		tests|=com_download("4");
		tests|=com_getall(comm_test);
	}
	if(tests!=0)
	{
		return STARTLOG_COMM_GET;
	}
	else
	{
		return tests;
	}
}

int HubPingTest(void){
	int tests = 0;
	tests=csp_ping(3,1000,100,CSP_O_NONE);
	if(tests==-1){
		vTaskDelay(10/portTICK_RATE_MS);
		tests=csp_ping(3,1000,100,CSP_O_NONE);
		if(tests==-1){
			vTaskDelay(100/portTICK_RATE_MS);
			tests=csp_ping(3,1000,100,CSP_O_NONE);
			if(tests==-1){
				return STARTLOG_HUB_PING;
			}
		}
	}
	return 0;
}
// fix up with hub class api like power and comm.
int HubGetTest(){
	nanohub_hk_t nanohub_hk;
	if(hub_get_hk(&nanohub_hk)<0){
		if(hub_get_hk(&nanohub_hk)<0){
			if(hub_get_hk(&nanohub_hk)<0){
				return STARTLOG_HUB_CMD;
			}
		}
	}
	return 0;
}
