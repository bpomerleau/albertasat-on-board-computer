/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file comm.c
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */

#include <comm/comm.h>
#include <eps/power_lines.h>
#include <core_defines.h>
#include <hub/deployment_lines.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Singleton Variable Defines													*/
/********************************************************************************/



/********************************************************************************/
/* Private Method Defines														*/
/********************************************************************************/



/********************************************************************************/
/* Virtual Method Defines														*/
/********************************************************************************/
/**
 * @memberof comm_t
 * @brief
 *		Turn the comm's power supply on/off
 * @details
 * 		Turn the comm's power supply on/off. Note, this does not turn
 * 		the comm board on/off, it turned the power running to it on/off. Therefore,
 * 		this will hard power down the comm board if used to turn it off.
 * 		Redundant calls have no effect (ie, calling this function with the
 * 		same parameters two or more times ).
 * @param eps
 * 		The eps_t object which manages the eps board.
 * @param state
 * 		<b>true</b> to turn the power on, <b>false</b> to turn the power off.
 * @returns
 * 		<b>true</b> if power was successfully turned on/off.
 * 		<br><b>false</b> if power was not successfully turned on/off.
 */

static bool_t power( comm_t* self, eps_t* eps, bool_t state )
{
	DEV_ASSERT( self );
	DEV_ASSERT( eps );

	if( state )
	{
		return eps->power_line( eps, COMM_3V3_POWER_CHANNEL, EPS_POWER_LINE_ACTIVE );
	}
	else
	{
		return eps->power_line( eps, COMM_3V3_POWER_CHANNEL, EPS_POWER_LINE_INACTIVE );
	}
}

/**
 * @memberof comm_t
 * @brief
 * 		Deploys the comm's antennas.
 * @details
 * 		Deploys the comm's antennas.
 * @param deployer[in]
 * 		A pointer to the hub which controls the hardware
 * 		that deploys antennas.
 */
static bool_t deploy( comm_t* self, hub_t* deployer )
{
	DEV_ASSERT( self );
	DEV_ASSERT( deployer );

	//return deployer->deploy( deployer, COMM_ANTENNA_DEPLOYMENT_LINE );
	return false;
}

/**
 * @memberof comm_t
 * @brief
 * 		Abstract method to get pcb temperature.
 * @details
 * 		Abstract method to get pcb temperature.
 * @returns
 * 		PCB temperature in C.
 */
static int16_t get_pcb_temp( comm_t* self )
{
	DEV_ASSERT( self );
	return 0;
}

/********************************************************************************/
/* Destructor Define															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Define															*/
/********************************************************************************/
/**
 * @memberof comm_t
 * @protected
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_comm_( comm_t* self )
{
	DEV_ASSERT( self );

	self->deploy = deploy;
	self->power = power;
	self->get_pcb_temp = get_pcb_temp;
}


/********************************************************************************/
/* Public Method Defines														*/
/********************************************************************************/
int16_t comm_get_pcb_temp( comm_t *comm )
{
	DEV_ASSERT( comm );

	return comm->get_pcb_temp( comm );
}

