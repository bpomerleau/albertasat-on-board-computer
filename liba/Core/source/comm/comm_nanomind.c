/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file comm_nanomind
 * @author Brendan Bruner
 * @author Alexander Hamilton
 * @date Feb 25, 2015
 */

#include "comm/cmd_rparam.h"
#include <comm/comm_nanomind.h>
#include "portable_types.h"
#include "command/command.h"
#include <stdint.h>
#include <unistd.h>
#include <hub/deployment_lines.h>

static int16_t get_pcb_temp( comm_t* self )
{
	DEV_ASSERT( self );

	/* Get ready for this super derp.. Yes, I know it makes no sense. */
	/* No, I can't do anything about, it's a gommy gom. */

	com_init( "4" );
	com_getall( self );
	return self->temp_brd;
}

int com_download(char * mem){
	struct command_context ctx;
	char cmd[]="download";
	char node[]="5";
	char * argv[3];
	argv[0]=cmd;
	argv[1]=node;
	argv[2]=mem;
	ctx.argv=argv;
	ctx.argc=3;

	int status;
	status=cmd_rparam_download(&ctx);
	return status;
}

int com_init(char * mem){

	struct command_context ctx;
	char cmd[]="init";
	char node[]="5";
	char * argv[3];
	argv[0]=cmd;
	argv[1]=node;
	argv[2]=mem;
	ctx.argv=argv;
	ctx.argc=3;

	int status;
	status=cmd_rparam_init(&ctx);
	return status;
}


int com_set(char * name, char * value,comm_t *com){
	struct command_context ctx;
	char cmd[]="set";
	char * argv[3];
	argv[0]=cmd;
	argv[1]=name;
	argv[2]=value;
	ctx.argv=argv;
	ctx.argc=3;

	int status;
	status=cmd_rparam_set(&ctx);

	status|=com_getall(com);
	return status;
}

extern void com_read_out(comm_t *com);
int com_getall(comm_t *com){

	struct command_context ctx;
	char cmd[]="getall";
	char * argv[1];
	argv[0]=cmd;
	ctx.argv=argv;
	ctx.argc=1;

	int status;
	status=cmd_rparam_getall(&ctx);
	com_read_out(com);
	return status;
}


int com_save(char * from, char * to){

	struct command_context ctx;
	char cmd[]="save";
	char * argv[3];
	argv[0]=cmd;
	argv[1]=from;
	argv[2]=to;
	ctx.argv=argv;
	ctx.argc=3;

	int status;
	status=cmd_rparam_save(&ctx);
	return status;
}

void initialize_comm_nanomind( comm_nanomind_t *comm){

	DEV_ASSERT( comm );

	/* Initialize super class. */
	extern void initialize_comm_( comm_t* );
	initialize_comm_( (comm_t*) comm );

	/* Override super class' methods. */
	((comm_t*) comm)->get_pcb_temp = get_pcb_temp;

	if(access("sd/par-5-4.bin",R_OK)!=-1){
		com_init("4");
	}
	else{
		com_download("4");
	}
	com_getall((comm_t*) comm);


	if(access("sd/par-5-0.bin",R_OK)!=-1){
		com_init("0");
	}
	else{
		com_download("0");
	}
	com_getall((comm_t*) comm);

}
