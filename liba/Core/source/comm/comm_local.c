/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file comm_lpc_local
 * @author Brendan Bruner
 * @date Feb 25, 2015
 */

#include <comm/comm_local.h>
#include <portable_types.h>


/************************************************************************/
/* Virtual method implementations										*/
/************************************************************************/
/**
 * @memberof comm_local_t
 * @attention Overrides comm_t::get_pcb_temp( )
 * @brief
 * 		Always returns 32C.
 * @details
 * 		Always returns 32C.
 * @returns
 * 		Temperature in C. Always returns 32C.
 */
static int16_t get_pcb_temp( comm_t* self )
{
	DEV_ASSERT( self );
	return 32;
}


/************************************************************************/
/* Initialization 														*/
/************************************************************************/
void initialize_comm_local( comm_local_t* self )
{
	DEV_ASSERT( self );

	extern void initialize_comm_( comm_t* );
	initialize_comm_( (comm_t*) self );

	((comm_t*) self)->get_pcb_temp = get_pcb_temp;
}
