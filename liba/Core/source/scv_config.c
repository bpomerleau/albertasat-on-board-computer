/*
 *
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file core_defines
 * @author Brendan Bruner
 * @date Oct. 2, 2017
 */
#include <stdio.h>
#include <core_defines.h>
#include <scv_config.h>

/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define SCV_CONFIG_VOID			0
#define SCV_CONFIG_SINGLE_USE	1
#define SCV_CONFIG_MULTI_USE		2

#define SCV_CONFIG_FILE_NAME "/boot/SCVConfig.bin"

/********************************************************************************/
/* Structures																	*/
/********************************************************************************/
/*
 * Structure written/read from file to maintain configuration;
 */
struct scv_config_meta_t
{
	/* Indicates how many times this data can be used.
	 * 		SCV_CONFIG_VOID
	 * 			Data cannot be used.
	 * 		SCV_CONFIG_SINGLE_USE
	 * 			Data can be used only once. usage must be set to
	 * 			SCV_CONFIG_VOID after the data is used.
	 * 		SCV_CONFIG_MULTI_USE
	 * 			The data can be used without restriction.
	 */
	int usage;
	struct scv_config_t config;
};


/********************************************************************************/
/* Functions																	*/
/********************************************************************************/
void scv_config_get_current( struct scv_config_t* config )
{
	config->athena_logger_capacity = ATHENA_LOGGER_MAX_CAPACITY;
	config->athena_logger_file_size = ATHENA_LOGGER_FILE_SIZE;
	config->dfgm_filt1_capacity = DFGM_FILT1_LOGGER_MAX_CAPACITY;
	config->dfgm_filt2_capacity = DFGM_FILT2_LOGGER_MAX_CAPACITY;
	config->dfgm_filt_file_size = MAX_DFGM_FILT_FILE_SIZE;
	config->dfgm_raw_capacity = DFGM_RAW_LOGGER_MAX_CAPACITY;
	config->dfgm_raw_file_size = MAX_DFGM_RAW_FILE_SIZE;
	config->adcs_locale_capacity = ADCS_LOCALE_LOGGER_CAPACITY;
	config->adcs_locale_file_size = ADCS_LOCALE_LOGGER_MAX_FILE_SIZE;
	config->dfgm_hk_file_size = MAX_DFGM_HK_FILE_SIZE;
	config->dfgm_hk_capacity = DFGM_HK_LOGGER_MAX_CAPACITY;
	config->wod_capacity = WOD_LOGGER_MAX_CAPACITY;
	config->wod_file_size = PACKET_TYPE_WOD_SIZE;
	config->wod_cadence = WOD_LOG_CADENCE;
	config->udos_capacity = UDOS_LOGGER_MAX_CAPACITY;
	config->udos_file_size = UDOS_LOGGER_FILE_SIZE;
	config->udos_cadence = UDOS_EVENT_PERIOD;
}

void scv_config_set_current( struct scv_config_t* config )
{
	ATHENA_LOGGER_MAX_CAPACITY = config->athena_logger_capacity;
	ATHENA_LOGGER_FILE_SIZE = config->athena_logger_file_size;
	DFGM_FILT1_LOGGER_MAX_CAPACITY = config->dfgm_filt1_capacity;
	DFGM_FILT2_LOGGER_MAX_CAPACITY = config->dfgm_filt2_capacity;
	MAX_DFGM_FILT_FILE_SIZE = config->dfgm_filt_file_size;
	DFGM_RAW_LOGGER_MAX_CAPACITY = config->dfgm_raw_capacity;
	MAX_DFGM_RAW_FILE_SIZE = config->dfgm_raw_file_size;
	ADCS_LOCALE_LOGGER_CAPACITY = config->adcs_locale_capacity;
	ADCS_LOCALE_LOGGER_MAX_FILE_SIZE = config->adcs_locale_file_size;
	MAX_DFGM_HK_FILE_SIZE = config->dfgm_hk_file_size;
	DFGM_HK_LOGGER_MAX_CAPACITY = config->dfgm_hk_capacity;
	WOD_LOGGER_MAX_CAPACITY = config->wod_capacity;
	PACKET_TYPE_WOD_SIZE = config->wod_file_size;
	WOD_LOG_CADENCE = config->wod_cadence;
	UDOS_LOGGER_MAX_CAPACITY = config->udos_capacity;
	UDOS_LOGGER_FILE_SIZE = config->udos_file_size;
	UDOS_EVENT_PERIOD = config->udos_cadence;
}

int scv_config_cache( )
{
	FILE* config_file;
	struct scv_config_meta_t config_data;

	/* Open and read scv configuration.
	 */
	config_file = fopen(SCV_CONFIG_FILE_NAME, "r+");
	if( config_file == NULL ) {
		return -1;
	}
	if( fread(&config_data, sizeof(char), sizeof(config_data), config_file) != sizeof(config_data) ) {
		fclose(config_file);
		return -2;
	}

	/* If the data is void, do nothing with it.
	 */
	if( config_data.usage == SCV_CONFIG_VOID ) {
		fclose(config_file);
		return 0;
	}

	/* Set configuration variables based on the configuration file's data.
	 */
	scv_config_set_current(&config_data.config);

	/* If the data is multi use, we're done.
	 */
	if( config_data.usage == SCV_CONFIG_MULTI_USE ) {
		fclose(config_file);
		return 0;
	}

	/* Data is not multi use, it is single use. Since we just used the data, we have
	 * to set it as void now.
	 */
	config_data.usage = SCV_CONFIG_VOID;
	if( fseek(config_file, 0, SEEK_SET) != 0 ) {
		fclose(config_file);
		return -3;
	}
	if( fwrite(&config_data, sizeof(char), sizeof(config_data), config_file) != sizeof(config_data) ) {
		fclose(config_file);
		return -4;
	}
	fclose(config_file);

	return 0;
}

int scv_config_flush( )
{
	FILE* config_file;
	struct scv_config_meta_t config_data;

	/* Open scv configuration.
	 */
	config_file = fopen(SCV_CONFIG_FILE_NAME, "w+");
	if( config_file == NULL ) {
		return -1;
	}

	/* Set data as single use and write it to file.
	 */
	config_data.usage = SCV_CONFIG_SINGLE_USE;
	scv_config_get_current(&config_data.config);
	if( fwrite(&config_data, sizeof(char), sizeof(config_data), config_file) != sizeof(config_data) ) {
		fclose(config_file);
		return -2;
	}
	fclose(config_file);

	return 0;
}

int scv_config_commit( )
{
	FILE* config_file;
	struct scv_config_meta_t config_data;

	/* Open and read scv configuration.
	 */
	config_file = fopen(SCV_CONFIG_FILE_NAME, "r+");
	if( config_file == NULL ) {
		return -1;
	}
	if( fread(&config_data, sizeof(char), sizeof(config_data), config_file) != sizeof(config_data) ) {
		fclose(config_file);
		return -2;
	}

	/* Set data to multi use and write back to file.
	 */
	config_data.usage = SCV_CONFIG_MULTI_USE;
	if( fseek(config_file, 0, SEEK_SET) != 0 ) {
		fclose(config_file);
		return -3;
	}
	if( fwrite(&config_data, sizeof(char), sizeof(config_data), config_file) != sizeof(config_data) ) {
		fclose(config_file);
		return -4;
	}
	fclose(config_file);

	return 0;
}
