/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telemetry_priority.h
 * @author Brendan Bruner
 * @date May 27, 2015
 */
#ifndef INCLUDE_TELEMETRY_PRIORITY_H_
#define INCLUDE_TELEMETRY_PRIORITY_H_

#include <filesystems/filesystem.h>
#include <stdint.h>
#include <portable_types.h>
#include "telemetry_priority_config.h"


/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define TELEMETRY_PRIORITY_BASE 0 /* !! Must be zero !! */
#define TELEMETRY_PRIORITY_MAX 4

#define TELEMETRY_PRIORITY_DEFAULT_WOD 	4
#define TELEMETRY_PRIORITY_DEFAULT_DFGM 3
#define TELEMETRY_PRIORITY_DEFAULT_MNLP 2
#define TELEMETRY_PRIORITY_DEFAULT_CMND_STATUS 1
#define TELEMETRY_PRIORITY_DEFAULT_STATE 0

#define MAX_TELEM_ITEMS 	PACKET_MAX_TYPES
#define WOD_INDEX 			PACKET_TYPE_WOD
#define DFGM_INDEX 			PACKET_TYPE_DFGM
#define MNLP_INDEX 			PACKET_TYPE_MNLP
#define CMND_STATUS_INDEX	PACKET_TYPE_CMND_STATUS
#define STATE_INDEX 		PACKET_TYPE_STATE


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct telemetry_priority_t
 * @brief
 * 		Defines the priority of telemetry for down linking.
 *
 * @details
 * 		Defines the priority of telemetry for down linking. This structure offers
 * 		mutex protected data, meaning any task can adjust a down link priority on
 * 		the fly without causing corruption.
 *
 * 		A higher priority value indicates a higher priority.
 */
typedef struct telemetry_priority_t telemetry_priority_t;

/** Error codes returned by telemetry_priority_t functions. */
typedef enum
{
	TP_OK = 0,	/*!< (0) No error. */
	TP_FS_ERR,	/*!< (1) Failed to do a file operations. */
	TP_INV_PRM, /*!< (2) Invalid parameter given. */
	TP_FULL,	/*!< (3) No room in non volatile memory to save priorities. */
	TP_INIT_ERR	/*!< (4) Synchronization objects did not initialize. */
} tp_error_t;

/********************************************************************************/
/* Structure Define																*/
/********************************************************************************/
struct telemetry_priority_t
{
	mutex_t 		_data_mutex; /* Mutex for protecting priority data. */
	filesystem_t 	*_fs;		 /* Used to maintain priorities through power cycles. */
	char			_log_name[FILESYSTEM_MAX_NAME_LENGTH+1];
								 /* Name of the file where priorities are saved to. */
	telemetry_priority_config_t _priority_;
};


/********************************************************************************/
/* Method Defines																*/
/********************************************************************************/
/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Get the priority of the telemetry WOD.
 *
 * @details
 * 		Get the priority of the telemetry WOD.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @returns
 * 		The requested priority.
 *
 */
uint8_t get_telemetry_priority_wod( telemetry_priority_t *prio );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Get the priority of the telemetry from the DFGM.
 *
 * @details
 * 		Get the priority of the telemetry from the DFGM.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @returns
 * 		The requested priority.
 *
 */
uint8_t get_telemetry_priority_dfgm( telemetry_priority_t *prio );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Get the priority of the telemetry from the MnLP.
 *
 * @details
 * 		Get the priority of the telemetry from the MnLP.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @returns
 * 		The requested priority.
 *
 */
uint8_t get_telemetry_priority_mnlp( telemetry_priority_t *prio );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Get the priority of the telemetry of command statuses.
 *
 * @details
 * 		Get the priority of the telemetry of command statuses.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @returns
 * 		The requested priority.
 *
 */
uint8_t get_telemetry_priority_cmnd_status( telemetry_priority_t *prio );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Get the priority of the telemetry for state transitions.
 *
 * @details
 * 		Get the priority of the telemetry for state transitions.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @returns
 * 		The requested priority.
 *
 */
uint8_t get_telemetry_priority_state( telemetry_priority_t *prio );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Set the priority of the telemetry WOD.
 *
 * @details
 * 		Set the priority of the telemetry WOD.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @param prio
 * 		The value of the priority. It will be rejected if the value is out of range.
 *
 * @returns
 * 		An error code.
 * 		<br><b>TP_OK</b>: if successful
 * 		<br><b>TP_FS_ERR</b>: The priority was set, however, it did not get set
 * 		in non volatile memory and the new priority will not be maintained after
 * 		a power cycle.
 * 		<br><b>TP_INV_PRM</b>: The priority was out of bounds and did not get set.
 */
tp_error_t set_telemetry_priority_wod( telemetry_priority_t *prio, uint8_t set_value );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Set the priority of the telemetry from the DFGM.
 *
 * @details
 * 		Set the priority of the telemetry from the DFGM.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @param prio
 * 		The value of the priority. It will be rejected if the value is out of range.
 *
 * @returns
 * 		An error code.
 * 		<br><b>TP_OK</b>: if successful
 * 		<br><b>TP_FS_ERR</b>: The priority was set, however, it did not get set
 * 		in non volatile memory and the new priority will not be maintained after
 * 		a power cycle.
 * 		<br><b>TP_INV_PRM</b>: The priority was out of bounds and did not get set.
 */
tp_error_t set_telemetry_priority_dfgm( telemetry_priority_t *prio, uint8_t set_value );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Set the priority of the telemetry from the MnLP.
 *
 * @details
 * 		Set the priority of the telemetry from the MnLP.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @param prio
 * 		The value of the priority. It will be rejected if the value is out of range.
 *
 * @returns
 * 		An error code.
 * 		<br><b>TP_OK</b>: if successful
 * 		<br><b>TP_FS_ERR</b>: The priority was set, however, it did not get set
 * 		in non volatile memory and the new priority will not be maintained after
 * 		a power cycle.
 * 		<br><b>TP_INV_PRM</b>: The priority was out of bounds and did not get set.
 */
tp_error_t set_telemetry_priority_mnlp( telemetry_priority_t *prio, uint8_t set_value );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Set the priority of the telemetry for command statuses.
 *
 * @details
 * 		Set the priority of the telemetry for command statuses.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @param prio
 * 		The value of the priority. It will be rejected if the value is out of range.
 *
 * @returns
 * 		An error code.
 * 		<br><b>TP_OK</b>: if successful
 * 		<br><b>TP_FS_ERR</b>: The priority was set, however, it did not get set
 * 		in non volatile memory and the new priority will not be maintained after
 * 		a power cycle.
 * 		<br><b>TP_INV_PRM</b>: The priority was out of bounds and did not get set.
 */
tp_error_t set_telemetry_priority_cmnd_status( telemetry_priority_t *prio, uint8_t set_value );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Set the priority of the telemetry for state transitions.
 *
 * @details
 * 		Set the priority of the telemetry for state transitions.
 * 		The priority value is in the range
 * 		[<b>TELEMETRY_PRIORITY_BASE</b>, <b>TELEMETRY_PRIORITY_MAX</b>]
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t struct to set the priority in.
 *
 * @param prio
 * 		The value of the priority. It will be rejected if the value is out of range.
 *
 * @returns
 * 		An error code.
 * 		<br><b>TP_OK</b>: if successful
 * 		<br><b>TP_FS_ERR</b>: The priority was set, however, it did not get set
 * 		in non volatile memory and the new priority will not be maintained after
 * 		a power cycle.
 * 		<br><b>TP_INV_PRM</b>: The priority was out of bounds and did not get set.
 */
tp_error_t set_telemetry_priority_state( telemetry_priority_t *prio, uint8_t set_value );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Lock the configuration files of all telemetry_priority_t structures.
 * @details
 * 		Lock the configuration files of all telemetry_priority_t structures. This
 * 		makes it safe to use the ftp service to remotely change the configuration file.
 * 		The lock has a 5 minute timeout.
 * @param block_time
 * 		The time to block for trying to lock.
 */
base_t telemetry_priority_lock_config_globally( block_time_t block_time );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Unlock configuration files for all telemetry_priority_t structures.
 * @details
 * 		Unlock files for all telemetry_priority_t structures. This
 * 		unlocks the lock placed on configuration files by telemetry_priority_lock_config_globally( ).
 */
void telemetry_priority_unlock_config_globally( );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Refresh the cached configuration file.
 * @details
 * 		Refresh the cached configuration file. This makes the priority structure refresh
 * 		its cached version of the configuration file with the configuration file in
 * 		non volatile memory.
 * @param self[in]
 * 		The telemetry_priority_t instance to refresh the configuration file of.
 */
tp_error_t telemetry_priority_refresh_configuration( telemetry_priority_t *self );

/********************************************************************************/
/* Initialization Method														*/
/********************************************************************************/
/**
 * @brief
 * 		Initialize a telemetry_priority_t structure.
 *
 * @details
 * 		Initialize a telemetry_priority_t structure.
 * 		The method will check if a file exists in the filesystem ,</b>fs</b>, by the
 * 		name of <b>log_name</b>. If it does, that file contains the most recently used
 * 		priorities and priorities will be initialized that way. Otherwise, it will
 * 		use the default priorities defined by:
 * 		<br><b>TELEMETRY_PRIORITY_DEFAULT_WOD</b>, <b>TELEMETRY_PRIORITY_DEFAULT_DFGM</b>, ...
 * 		<br>If any value is returned other than <b>TP_OK</b> it is NOT safe to use the
 * 		telemetry_priority_t structure in any function.
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t structure to initialize.
 *
 * @param fs
 * 		A pointer to the filesystem_t structure to use when doing file IO.
 *
 * @param log_name
 * 		The name to use for the file which will contain the most recently set value of
 * 		priorities. Must be null terminated.
 *
 * @returns
 * 		An error code.
 * 		<br><b>TP_OK</b>: On success.
 * 		<br><b>TP_INV_PRM</b>: If <b>log_name</b> is too long or there is some indication the
 * 		file <b>log_name</b> contains data external to telemetry_priority_t.
 * 		<br><b>TP_FS_ERR</b>: Failed to load priority values from memory, using defaults instead.
 * 		<br><b>TP_FULL</b>: Memory is full, failed to create new log file (only applicable if the log
 * 		file did not exist before hand.
 */
tp_error_t initialize_telemetry_priority( telemetry_priority_t *prio, filesystem_t *fs, char const *log_name );

/**
 * @memberof telemetry_priority_t
 * @brief
 * 		Destroy a telemetry_priority_t structure.
 *
 * @details
 * 		Destroy a telemetry_priority_t structure. When the structure is no longer need it must
 * 		be destroyed with this function to prevent memory leaks.
 * 		<b>NOTE</b>, this does not erase the priorities in non volatile memory.
 *
 * @param prio
 * 		A pointer to the telemetry_priority_t structure to destroy.
 */
void destroy_telemetry_priority( telemetry_priority_t *prio );


#endif /* INCLUDE_TELEMETRY_PRIORITY_H_ */
