/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_log_wod.h
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_LOG_WOD_H_
#define INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_LOG_WOD_H_

#include <telecommands/telecommand.h>
#include <states/state_relay.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
/* Array size. */
#define PACKET_RAW_WOD_SIZE 131




/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_log_wod_t
 * @brief
 * 		A command which will collect one packet worth of WOD and log it.
 * @details
 * 		A command which will collect one packet worth of WOD and log it.
 * 		Invoke this command every 60 seconds to meet the QB50 requirement
 * 		of logging WOD every 60 seconds.
 * @var telecommand_log_wod_t::_super_
 * 		<b>Private</b>
 * 		Super struct data.
 * @var telecommand_log_wod_t::_relay_
 * 		<b>Private</b>
 * 		Used to determine Operational state of satellite at time of WOD
 * 		collection
 */
typedef struct telecommand_log_wod_t telecommand_log_wod_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_log_wod_t
{
	telecommand_t _super_;
	state_relay_t *_relay_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @brief
 * 		A command which invokes the collection and storage of WOD.
 * @details
 * 		Invoke the collection and storage of WOD (to non volatile memory). The data collected,
 * 		processing done, and packetization scheme meets the requirements in
 * 		this file:
 * 		@htmlonly
 * 		<a href="https://bytebucket.org/bbruner0/albertasat-on-board-computer/wiki/1.%20Resources/1.1.%20DataSheets/WOD/QB50%20Whole%20Orbit%20Data%20-%20Iss4.pdf?rev=ff2ac4edfff4ca9f556ff4c7c149f38ad152f83">WOD Requirements</a>
 * 		@endhtmlonly
 * @param relay[in]
 * 		Used by the command to access the rtc API and logger API. In addition, it is required
 * 		to determine the operational mode at the time of logging WOD.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_telecommand_log_wod( telecommand_log_wod_t *self, state_relay_t *relay );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_LOG_WOD_H_ */
