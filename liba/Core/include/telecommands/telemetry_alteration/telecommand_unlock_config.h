/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_unlock_state_config.h
 * @author Brendan Bruner
 * @date Aug 24, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_UNLOCK_CONFIG_H_
#define INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_UNLOCK_CONFIG_H_

#include <telecommands/telecommand.h>
#include <states/state_relay.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_unlock_config_t
 * @brief
 * 		Unlock configuration files.
 * @details
 * 		Unlock configuration files. This lock is activated with
 * 		telecommand_lock_config_t and is unlocked with this telecommand. Note,
 * 		the lock has a five minute time out. This telecommand will cause all cached
 * 		configurations to be refreshed with the configuration file
 * 		in non volatile memory. An example transaction would be:
 * 		<br> - lock config file with telecommand
 * 		<br> - using ftp, put a new config file in memory
 * 		<br> - unlock config file with telecommand
 * 		Without the explicit unlock, the cached version of configuration files
 * 		are not refreshed.
 * @var telecommand_unlock_config_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct telecommand_unlock_config_t telecommand_unlock_config_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_unlock_config_t
{
	telecommand_t _super_; /* Must be first. */
	state_relay_t *_relay_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_unlock_config_t
 * @brief
 * 		Initialize for telecommand_unlock_config_t
 * @details
 * 		Initializer for telecommand_unlock_config_t
 * @param relay[in]
 * 		Used by the command to refresh configuration files.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_telecommand_unlock_config( telecommand_unlock_config_t *self, state_relay_t *relay );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_UNLOCK_CONFIG_H_ */
