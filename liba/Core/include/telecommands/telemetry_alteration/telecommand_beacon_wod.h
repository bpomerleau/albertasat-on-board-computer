/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_beacon_wod.h
 * @author Brendan Bruner
 * @date Oct 22, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_BEACON_WOD_H_
#define INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_BEACON_WOD_H_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_beacon_wod_t
 * @brief
 * 		Beacon an eight byte round of WOD.
 * @details
 * 		Beacon an eight byte round of WOD. Run this command every 30 seconds
 * 		to meet the requirement of beaconing WOD every 30 seconds.
 * @var telecommand_beacon_wod_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct telecommand_beacon_wod_t telecommand_beacon_wod_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_beacon_wod_t
{
	telecommand_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_beacon_wod_t
 * @brief
 * 		Initialize for telecommand_beacon_wod_t
 * @details
 * 		Initializer for telecommand_beacon_wod_t
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_telecommand_beacon_wod( telecommand_beacon_wod_t *self, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_TELEMETRY_ALTERATION_TELECOMMAND_BEACON_WOD_H_ */
