/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_deploy_boom.h
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_locale_logger_H_
#define INCLUDE_TELECOMMANDS_locale_logger_H_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define TELECOMMAND_LOCALE_LOGGER_COMMISSION_SYM		"commission"
#define TELECOMMAND_LOCALE_LOGGER_COMMISSION_SYM_LEN	10

#define TELECOMMAND_LOCALE_LOGGER_DECOMMISSION_SYM		"decommission"
#define TELECOMMAND_LOCALE_LOGGER_DECOMMISSION_SYM_LEN	12


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_locale_logger_t
 * @brief
 * 		Turn on/off dfgm.
 * @details
 * 		Turn on/off dfgm.
 */
typedef struct telecommand_locale_logger_t telecommand_locale_logger_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_locale_logger_t
{
	telecommand_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_locale_logger_t
 * @brief
 * 		Initialize
 * @details
 * 		Initializer
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
bool_t initialize_telecommand_locale_logger( telecommand_locale_logger_t *self, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_locale_logger_H_ */
