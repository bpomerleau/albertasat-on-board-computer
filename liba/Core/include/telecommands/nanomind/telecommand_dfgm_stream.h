/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_deploy_boom.h
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_DFGM_STREAM_H_
#define INCLUDE_TELECOMMANDS_DFGM_STREAM_H_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define TELECOMMAND_DFGM_STREAM1_SYM0 "raw"
#define TELECOMMAND_DFGM_STREAM1_SYM0_LEN 3
#define TELECOMMAND_DFGM_STREAM1_SYM1 "leop"
#define TELECOMMAND_DFGM_STREAM1_SYM1_LEN 4

#define TELECOMMAND_DFGM_STREAM2_SYM0 "filt"
#define TELECOMMAND_DFGM_STREAM2_SYM0_LEN 4
#define TELECOMMAND_DFGM_STREAM2_SYM1 "down"
#define TELECOMMAND_DFGM_STREAM2_SYM1_LEN 4

/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_dfgm_stream_t
 * @brief
 * 		Turn on/off dfgm.
 * @details
 * 		Turn on/off dfgm.
 */
typedef struct telecommand_dfgm_stream_t telecommand_dfgm_stream_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_dfgm_stream_t
{
	telecommand_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_dfgm_stream_t
 * @brief
 * 		Initialize
 * @details
 * 		Initializer
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
bool_t initialize_telecommand_dfgm_stream( telecommand_dfgm_stream_t *self, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_DFGM_STREAM_H_ */
