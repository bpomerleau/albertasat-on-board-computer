/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_prioritize_downlink.h
 * @author Brendan Bruner
 * @date Aug 12, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_MNLP_OFF_
#define INCLUDE_TELECOMMANDS_MNLP_OFF_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_prioritize_downlink_t
 * @brief
 * 		Used to change the priorities of data downlinked by the automated
 * 		downlinker.
 * @details
 * 		Used to change the priorities of data downlinked by the automated
 * 		downlinker.
 * @var telecommand_prioritize_downlink_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct telecommand_mnlp_off_t telecommand_mnlp_off_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_mnlp_off_t
{
	telecommand_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_prioritize_downlink_t
 * @brief
 * 		Used to change the priorities of data downlinked by the automated
 * 		downlinker.
 * @details
 * 		Used to change the priorities of data downlinked by the automated
 * 		downlinker.
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_telecommand_mnlp_off( telecommand_mnlp_off_t *self, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_MNLP_OFF_ */
