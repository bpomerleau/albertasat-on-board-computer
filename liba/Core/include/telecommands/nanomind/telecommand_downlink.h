/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_deploy_boom.h
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_downlink_H_
#define INCLUDE_TELECOMMANDS_downlink_H_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/

/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_downlink_t
 * @brief
 * 		Print diagnostics to csp-term
 * @details
 * 		Print diagnostics to csp-term
 */
typedef struct telecommand_downlink_t telecommand_downlink_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_downlink_t
{
	telecommand_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_downlink_t
 * @brief
 * 		Initialize
 * @details
 * 		Initializer
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
bool_t initialize_telecommand_downlink( telecommand_downlink_t *self, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_downlink_H_ */
