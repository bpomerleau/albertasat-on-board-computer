/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telecommand_deploy_boom.h
 * @author Brendan Bruner
 * @date Nov 6, 2015
 */
#ifndef INCLUDE_TELECOMMANDS_SUBSYSTEM_ALTERATION_TELECOMMAND_DEPLOY_BOOM_H_
#define INCLUDE_TELECOMMANDS_SUBSYSTEM_ALTERATION_TELECOMMAND_DEPLOY_BOOM_H_

#include <telecommands/telecommand.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends telecommand_t
 * @struct telecommand_deploy_boom_t
 * @brief
 * 		Deploys DFGM boom.
 * @details
 * 		Deploys DFGM boom.
 * @var telecommand_deploy_boom_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct telecommand_deploy_boom_t telecommand_deploy_boom_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct telecommand_deploy_boom_t
{
	telecommand_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof telecommand_deploy_boom_t
 * @brief
 * 		Initialize for telecommand_deploy_boom_t
 * @details
 * 		Initializer for telecommand_deploy_boom_t
 * @param kit[in]
 * 		Used by the command to access the rtc API and logger API.
 * @returns
 * 		<b>TELECOMMAND_SUCCESS</b>: Successful initialization.
 * 		<br><b>TELECOMMAND_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
bool_t initialize_telecommand_deploy_boom( telecommand_deploy_boom_t *self, driver_toolkit_t *kit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_TELECOMMANDS_SUBSYSTEM_ALTERATION_TELECOMMAND_DEPLOY_BOOM_H_ */
