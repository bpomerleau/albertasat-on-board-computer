/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file comm
 * @author Brendan Bruner
 * @author Jeff Ryan
 * @date Feb 25, 2015
 */

#ifndef INCLUDE_COMM_H_
#define INCLUDE_COMM_H_

#include <portable_types.h>
#include <stdint.h>
#include <eps/eps.h>
#include <hub/hub.h>

/* Required typedefs */
typedef struct comm_t comm_t;


/**
 * @struct comm_t
 * @brief
 * 		Structure used with API for communications board.
 * @details
 * 		Structure used with API for communications board.
 */
struct comm_t
{
	//Configuration
	uint32_t rx_freq;
	uint32_t rx_baud;
	uint32_t rx_bw;
	uint32_t tx_freq;
	uint32_t tx_baud;
	float tx_modindex;
	int8_t preamb;
	uint8_t preamblen;
	uint8_t preambflags;
	int8_t intfrm;
	uint8_t intfrmlen;
	uint8_t intfrmflags;
	uint16_t tx_max_time;
	uint16_t tx_guard;
	int16_t tx_rssibusy;
	uint8_t rx_idle_s;
	uint8_t mode; //1 RAW, 2: SYNC, 3: HDLC, 4:HDLC+FEC
	uint8_t fcs; //0:crc32, 1:Reed-Solomon
	uint8_t pllrang_rx;
	uint8_t pllrang_tx;
	int16_t max_temp;
	uint8_t csp_node;
	uint16_t reboot_in;
	uint32_t tx_inhibit;
	uint32_t bcn_interval;
	uint32_t bcn_holdoff;
	uint8_t bcn_epsdata;


	//Telemetry "4"
	int16_t temp_brd;
	int16_t temp_pa;
	int16_t last_rssi;
	int16_t last_rferr;
	uint32_t tx_count;
	uint32_t rx_count;
	uint32_t tx_bytes;
	uint32_t rx_bytes;
	uint16_t boot_count;
	int32_t boot_cause;
	uint32_t last_contact;
	uint32_t tot_tx_count;
	uint32_t tot_rx_count;
	uint32_t tot_tx_bytes;
	uint32_t tot_rx_bytes;

	bool_t (*power)( comm_t*, eps_t*, bool_t );
	bool_t (*deploy)( comm_t*, hub_t* );
	int16_t (*get_pcb_temp)( comm_t* );
};

/**
 * @brief
 * 		Returns temperature of COMM board in degC.
 * @details
 * 		Returns temperature of COMM board in degC.
 * @param comm
 * 		A poiner to the comm_t structure being used to get temp.
 * @returns
 * 		The temperature of the COMM board in degC.
 */
int16_t comm_get_pcb_temp( comm_t *comm );


#endif /* INCLUDE_COMM_H_ */
