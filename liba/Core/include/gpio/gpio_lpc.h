/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file gpio_lpc17xx_lpc40xx.h
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */
#ifndef INCLUDE_GPIO_GPIO_LPC_H_
#define INCLUDE_GPIO_GPIO_LPC_H_

#include "gpio.h"
#include <stdint.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct gpio_lpc_t
 * @extends gpio_t
 * @brief
 * 		A GPIO class which works with LPC chips under the LPCOpen API.
 * @details
 * 		A GPIO class which works with LPC chips under the LPCOpen API.
 */
typedef struct gpio_lpc_t gpio_lpc_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct gpio_lpc_t
{
	gpio_t s_; /* MUST be first. */
	struct
	{
		uint8_t port;
		uint8_t pin;
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof gpio_lpc_t
 * @brief
 *		Constructor for gpio_lpc_t.
 * @details
 * 		Constructor for gpio_lpc_t. Initializes a hardware pin on an LPC chip.
 * @param port
 * 		The port the pin is on.
 * @param pin
 * 		The pin number.
 * @param wire
 * 		Extra wiring done on the pin. The LPC17xx only supports the options:
 * 		<br>GPIO_PULLUP,
 * 		<br>GPIO_PULLDOWN,
 * 		<br>GPIO_BARE,
 * 		<br>GPIO_REPEATER
 */
void initialize_gpio_lpc( gpio_lpc_t*, uint8_t port, uint8_t pin, gpio_wire_t wire );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_GPIO_GPIO_LPC_H_ */
