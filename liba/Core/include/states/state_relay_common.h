/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file system_state_relay_common.h
 * @author Brendan Bruner
 * @date Jun 23, 2015
 */
#ifndef INCLUDE_SYSTEM_STATE_RELAY_COMMON_H_
#define INCLUDE_SYSTEM_STATE_RELAY_COMMON_H_


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct state_relay_t
 * @brief
 * 		A structure for relaying information to the systems current state.
 *
 * @details
 * 		A structure for relaying information to the systems current state.
 * 		All access to state behavior is made through this structure. It takes
 * 		commands (telecommand_t structures), which perform state specific actions, and
 * 		relays that telecommand_t to the current state of the system. The current
 * 		state of the system is represented by a state_t structure.
 *
 * 		For example, lets say we have telecommand_t which will turn on a power
 * 		hungry subsystem to briefly do IO with it.
 * 			@code
 * 			telecommand_t command;
 *
 * 			initialize_power_hungry_mnlp_command( &command, NULL );
 * 			relay_command_mnlp( &relay, &command );
 * 			@endcode
 * 		The variable 'relay' is of type state_relay_t and
 * 		has been initialized somewhere else. In this example, the power
 * 		hungry command will be executed based on what the current state is.
 * 		If the current state is power safe then the command will not be
 * 		executed, but if the current state is the science state then the
 * 		command will be executed.
 *
 * @attention
 * 		The current state is only updated through calls to relay_next_state( ). This method
 * 		assesses the current state and makes a state transition if there is a new state it should
 * 		be in. Note, this method will invoke some blocks and should not be called in an ISR.
 *
 * @var state_relay_t::_current_state_
 * 		<b>Private<b>
 * 		Points to the current state.
 *
 * @var state_relay_t::bring_up
 * 		<b>Public</b>
 * 		An instance of the state_bring_up_t. One of several possible current states.
 *
 * @var state_relay_t::low_power
 * 		<b>Public</b>
 * 		An instance of the state_low_power_t. One of several possible current states.
 *
 * @var state_relay_t::alignment
 * 		<b>Public</b>
 * 		An instance of the state_alignment_t. One of several possible current states.
 *
 * @var state_relay_t::science
 * 		<b>Public</b>
 * 		An instance of the state_science_t. One of several possible current states.
 *
 * @var state_relay_t::_state_mutex_
 * 		<b>Private</b>
 * 		Provides mutual exclusion to some variables.
 *
 * @var state_relay_t::drivers
 * 		<b>Public</b>
 * 		A pointer to a driver_toolkit_t structure, This variable should never
 * 		be written to, but can be read from safetly. The states being managed
 * 		by the relay will use these drivers as part of their next state logic and
 * 		possibly for other functionality.
 */
typedef struct state_relay_t state_relay_t;

#endif /* INCLUDE_SYSTEM_STATE_RELAY_COMMON_H_ */
