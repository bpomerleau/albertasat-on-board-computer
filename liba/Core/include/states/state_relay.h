/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_relay
 * @author Brendan Bruner
 * @date 2015-01-28
 */
#ifndef INCLUDE_STATE_RELAY_H_
#define INCLUDE_STATE_RELAY_H_

#include "state.h"
#include "state_leop_check.h"
#include "leop/state_black_out.h"
#include "leop/state_detumble.h"
#include "leop/state_leop_power_charge.h"
#include "leop/state_boom_deployment.h"
#include "leop/state_antenna_deployment.h"
#include "operations/state_bring_up.h"
#include "operations/state_low_power.h"
#include "operations/state_alignment.h"
#include "operations/state_science.h"
#include "state_relay_common.h"

#include <non_volatile/file_variable.h>

#include <telecommands/telecommand.h>
#include <driver_toolkit/driver_toolkit.h>
#include <portable_types.h>

#define STATE_RELAY_SUCCESS 1
#define STATE_RELAY_FAILURE 0

struct state_relay_t
{
	state_t 			*_current_state_;

	file_variable_t				leop_flag;

	state_leop_check_t			leop_check;
	state_black_out_t			black_out;
	state_detumble_t			detumble;
	state_leop_power_charge_t	power_charge;
	state_boom_deployment_t		boom_deploy;
	state_antenna_deployment_t	antenna_deploy;
	state_bring_up_t			bring_up;
	state_low_power_t			low_power;
	state_alignment_t			alignment;
	state_science_t				science;

	driver_toolkit_t	*drivers;

	mutex_t				_state_mutex_;
};

/**
 * @brief
 * 		Initializes a state_relay_t structure.
 * @details
 * 		Initializes a state_relay_t structure.
 * @param drivers[in]
 * 		A pointer to the driver_toolkit_t structure which will be used.
 * 		This is the driver toolkit which will be made available to all
 * 		the system_states.
 * @param leop_log_file[in]
 * 		The name of the log file used as a leop flag. When leop finishes, this
 * 		log file gets flagged, indicating leop has finished. All successive state
 * 		transistions will then ignore transistions into leop states.
 * @param black_out_time_out
 * 		The duration of the black out state in seconds.
 * @param black_out_log_file[in]
 * 		The name of the log file which keeps track of the black out state's duration.
 * @param detumble_time_out
 * 		The maximum amount of time that can be spent detumbling while in leop, in seconds.
 * @param detumble_log_file[in]
 * 		The name of the log file which keeps track of leop detumble's duration.
 * @returns
 * 		<b>STATE_RELAY_SUCCESS</b>: If initialized successfully.
 * 		<br><b>STATE_RELAY_FAILURE</b>: If initialization failed. The
 * 		structure is not safe to use.
 *
 * @memberof state_relay_t
 */
uint8_t initialize_state_relay
(
	state_relay_t *relay,
	driver_toolkit_t *drivers,
	char const* leop_log_file,
	uint32_t black_out_time_out,
	char const* black_out_log_file,
	uint32_t detumble_time_out,
	char const* detumble_log_file
);

/**
 * @memberof state_relay_t
 * @brief
 * 		Initializes a state_relay_t structure.
 * @details
 * 		Initializes a state_relay_t structure. This methods fills in the parameters
 * 		of initialize_state_relay( ) with variables defined in core_defines.h
 * @returns
 * 		See initialize_state_relay( ). Return values are the same.
 */
uint8_t initialize_state_relay_simple( state_relay_t* relay, driver_toolkit_t* drivers );

/**
 * @memberof state_relay_t
 * @brief
 * 		Destroy a state_relay_t structure.
 * @details
 * 		Destroy a state_relay_t structure. This must be called to avoid
 * 		memory leaks when done with the structure.
 * @param relay
 * 		A pointer to the state_relay_t structure to destroy.
 */
void destroy_state_relay( state_relay_t *relay );

/**
 * @brief
 * 		Forcefully changes the current state of the system.
 * @details
 * 		Forcefully changes the current state of the system. The current states exit()
 * 		method is called. The state is changed, then the new states entry()
 * 		method is called.
 * @attention
 * 		Using this method will likely have unintended side effects. Only
 * 		use this method if you know what you're doing.
 * @param relay
 * 		A pointer to the state_relay_t structure to change
 * 		the state of.
 * @param next
 * 		A pointer to the state_t structure which will be the new
 * 		state. Typically, this pointer will be a member of the state_relay_t
 * 		structure passed in as argument 1. For example,
 * 			@code
 * 			relay_change_system_state( &relay, &relay.low_power );
 * 			@endcode
 * 		However, if this is not the case, then the state_t structure
 * 		must remain valid in memory for at least the duration it is the current
 * 		state.
 * @memberof state_relay_t
 */
void relay_force_change_state(	state_relay_t *relay, state_t *next );

/**
 * @brief
 * 		Relays a command to collect housekeeping data to the current state.
 * @details
 * 		Relays a command to collect housekeeping data to the current state.
 * @param relay
 *		A pointer to the state_relay_t structure which will
 *		be used to relay the command.
 * @param command
 * 		The command which will collect housekeeping data. This will be
 * 		relayed to the current state. If this is NULL the function
 * 		immediately returns.
 * @memberof state_relay_t
 */
void relay_command_collect_housekeeping(	state_relay_t *relay, telecommand_t *command );

/**
 * @brief
 * 		Relays a command to transmit data to the current state.
 * @details
 * 		Relays a command which will transmit data to a ground station.
 * 		This command is relayed to the current state.
 * @param relay
 *		A pointer to the state_relay_t structure which will
 *		be used to relay the command.
 * @param command
 * 		A transmit data command to relay to the current state. If this is NULL
 * 		the function immediately returns.
 * @memberof state_relay_t
 */
void relay_command_transmit_data(	state_relay_t *relay, telecommand_t *command );

/**
 * @brief
 * 		Relays commands which handle the MnLP to the current state.
 * @details
 *		Relays commands which handle the MnLP to the current state.
 * @param relay
 *		A pointer to the state_relay_t structure which will
 *		be used to relay the command.
 * @param command
 * 		The command which will handle the MnLP. This command is relayed
 * 		to the current state. If this is NULL the function immediately
 * 		returns.
 * @memberof state_relay_t
 */
void relay_command_handle_MnLP(	state_relay_t *relay, telecommand_t *command );

/**
 * @brief
 * 		Relays commands which handle the DFGM to the current state.
 * @details
 * 		Relays commands which handle the DFGM to the current state.
 *
 * @param relay
 *		A pointer to the state_relay_t structure which will
 *		be used to relay the command.
 * @param command
 * 		The command which will handle the DFGM. This command is relayed
 * 		to the current state. If this is NULL the function immediately
 * 		returns.
 * @memberof state_relay_t
 */
void relay_command_handle_DFGM(	state_relay_t *relay, telecommand_t *command );

/**
 * @memberof state_relay_t
 * @brief
 * 		Relays a command which transmits data to ground station in response
 * 		to a request.
 * @details
 * 		Relays a command which transmits data to ground station in response
 * 		to a request.
 *
 * 		This command is only executed if the current state supports the
 * 		execution of respond to request command types.
 *
 * 		This function assumes the command is a respond to request command
 * 		and does not assert this condition.
 * @param relay
 * 		A pointer to the state_relay_t structure which is used to relay
 * 		the command.
 * @param command
 * 		A pointer to the command which will be relayed. If this is NULL the
 * 		function returns.
 */
void relay_command_respond_to_request( 	state_relay_t *relay, telecommand_t *command );

/**
 * @memberof state_relay_t
 * @brief
 * 		Relay a command which performs diagnostics.
 * @details
 * 		Relay a command which performs diagnostics.
 *
 * 		This command is only executed if the current state supports the
 * 		execution of respond to request command types.
 *
 * 		This function does not assert the command passed in is a diagnostics
 * 		command.
 * @param relay
 * 		A pointer to the state_relay_t structure which is used to relay
 * 		the command.
 * @param command
 * 		A pointer to the diagnostics command being relayed. If this is NULL the
 * 		function returns.
 */
void relay_command_diagnostics( state_relay_t *relay, telecommand_t *command );

/**
 * @brief
 * 		Gets the integer id of the current state.
 * @details
 *		Gets the integer id of the current state by invoking the
 *		query_state_id( ) method of the current state.
 *@param relay
 *		A pointer to the state_relay_t structure who's current state
 *		id is of interest.
 * @returns
 * 		The id of the current state.
 * @memberof state_relay_t
 */
uint8_t relay_state_id( state_relay_t *relay );

/**
 * @memberof state_relay_t
 * @brief
 * 		Update the current state.
 * @details
 * 		Updates the current state. Then, logs the state change to memory.
 */
void relay_next_state( state_relay_t *relay );

#endif /* INCLUDE_STATE_RELAY_H_ */
