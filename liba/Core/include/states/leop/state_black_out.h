/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file state_black_out.h
 * @author Brendan Bruner
 * @date Sep 14, 2015
 */
#ifndef INCLUDE_STATES_LEOP_STATE_BLACK_OUT_H_
#define INCLUDE_STATES_LEOP_STATE_BLACK_OUT_H_

#include <states/state.h>
#include <ptimer/ptimer.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends state_t
 * @struct state_black_out_t
 * @brief
 * 		Black out state of leop.
 * @details
 * 		Black out state of leop. This state cannot be exited until the black out completes.
 * 		It persistent though power cycles.
 * @var state_black_out_t::_super_
 * 		<b>Private</b> Super struct data.
 * @var state_black_out_t::_timer_
 * 		<b>Private</b> Timer used to track 30 minutes delay.
 * @var state_black_out_t::_black_out_
 * 		<b>Private</b> Mutex used to poll completion of thirty minute delay.
 */
typedef struct state_black_out_t state_black_out_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct state_black_out_t
{
	state_t _super_;
	struct
	{
		ptimer_t timer;
		void (*supers_destroy)( state_t* );
	}_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_black_out_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 * @param fs[in]
 * 		A pointer to the filesystem_t structure which will be used when reading / writing
 * 		to the state's configuration file and creating a persistent file to track the
 * 		black outs duration.
 * @param time_out
 * 		The duration the black out should last in seconds.
 * @param log_file
 * 		Name of file which will be used to track time between power cycles.
 * @returns
 * 		<b>STATE_SUCCESS</b>: Successful initialization.
 * 		<br><b>STATE_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_state_black_out( state_black_out_t *state, filesystem_t *fs, uint32_t time_out, char const* log_file );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/


#endif /* INCLUDE_STATES_LEOP_STATE_BLACK_OUT_H_ */
