/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file bring_up.h
 * @author Brendan Bruner
 * @date Aug 10, 2015
 */
#ifndef INCLUDE_STATES_OPERATIONS_STATE_BRING_UP_H_
#define INCLUDE_STATES_OPERATIONS_STATE_BRING_UP_H_

#include <states/state.h>


/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @extends state_t
 * @struct state_bring_up_t
 * @brief
 * 		Operation's bring up state.
 * @details
 * 		Operation's bring up state.
 * @var state_bring_up_t::_super_
 * 		<b>Private</b> Super struct data.
 */
typedef struct state_bring_up_t state_bring_up_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct state_bring_up_t
{
	state_t _super_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof state_bring_up_t
 * @brief
 * 		Operation's bring up state.
 * @details
 * 		Operation's bring up state.
 * @param fs[in]
 * 		A pointer to the filesystem_t structure which will be used when reading / writing
 * 		to the state's configuration file.
 * @returns
 * 		<b>STATE_SUCCESS</b>: Successful initialization.
 * 		<br><b>STATE_FAILURE</b>: Fatal error initializing. The structure is not safe to use.
 */
uint8_t initialize_state_bring_up( state_bring_up_t *state, filesystem_t *fs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_STATES_OPERATIONS_STATE_BRING_UP_H_ */
