/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps.h
 * @author Collin Cupido
 * @author Brendan Bruner
 * @author Stefan Damkjar
 * @date 2014-12-28
 */

#ifndef EPS_H_
#define EPS_H_

#include <stdint.h>
#include <portable_types.h>
#include <printing.h>

/* Use nanomind library if on nanomind, otherwise, use mock up. */
#ifdef NANOMIND
#include <io/nanopower2.h>
#else
#include "eps_hk.h"
#endif

/****************************************************************************/
/* Typedefs used by eps														*/
/****************************************************************************/
typedef struct eps_t 		eps_t;
typedef struct hk_handler_t hk_handler_t;
typedef uint8_t		        eps_tlm_id_t;
typedef enum
{
	EPS_MODE_CRITICAL = 0,
	EPS_MODE_POWER_SAFE,
	EPS_MODE_NORMAL,
	EPS_MODE_OPTIMAL
} eps_mode_t;

/**
* @struct hk_itm_t
* @brief
*		Describes a housekeeping item or housekeeping table with a pointer to the
*		item or table and its size in bytes
* @details
* 		This structure type allows a housekeeping item or housekeeping table of
* 		arbitrary type or size to be passed or returned.
* 		An invalid housekeeping item request is indicated by a size of zero
* @var size
* 		Indicated the size of the corresponding housekeeping item or table in bytes
* @var ptr
* 		Indicated the physical memory location of the housekeeping item or table
*/
struct hk_handler_t
{
	uint16_t size;
	void*   ptr;
};

/* Telemetry Item Data Types */
typedef uint16_t                    eps_vboost_t[];
typedef uint16_t                    eps_vbatt_t;
typedef uint16_t                    eps_curin_t[];
typedef uint16_t                    eps_cursun_t;
typedef uint16_t                    eps_cursys_t;
typedef uint16_t                    eps_reserved_t;
typedef uint16_t                    eps_curout_t[];
typedef uint8_t                     eps_output_t[];
typedef uint16_t                    eps_output_on_delta_t[];
typedef uint16_t                    eps_output_off_delta_t[];
typedef uint16_t                    eps_latchup_t[];
typedef uint32_t                    eps_wdt_i2c_time_left_t;
typedef uint32_t                    eps_wdt_gnd_time_left_t;
typedef uint8_t                     eps_wdt_csp_pings_left_t[];
typedef uint32_t                    eps_counter_wdt_i2c_t;
typedef uint32_t                    eps_counter_wdt_gnd_t;
typedef uint32_t                    eps_counter_wdt_csp_t[];
typedef uint32_t                    eps_counter_boot_t;
typedef uint16_t                    eps_temp_t[];
typedef uint8_t                     eps_bootcause_t;
typedef uint8_t                     eps_battmode_t;
typedef uint8_t                     eps_pptmode_t;
typedef uint16_t                    eps_reserved2_t;

/* Telemetry Request Error Code Define Statements */
#define TLM_ERR_INVAL               ((hk_handler_t){ .size = 0, .ptr = 0 })

/* Used to index curout for 3v3 and 5v0 outputs. */
#define CUROUT_3V3_NUM	4
#define CUROUT_5V0_NUM	2
#define CUROUT_INDEX_3V3 curout_3v3_hash
#define CUROUT_INDEX_5V0 curout_5v0_hash

/* Used to index temperatures. */
#define PCB_TEMP_NUM 4
#define BATT_TEMP_NUM 2
#define PCB_TEMP_INDEX pcb_temp_hash
#define BATT_TEMP_INDEX batt_temp_hash

/* Telemetry ID Define Statements */
#define EPS_VBOOST                  ((eps_tlm_id_t)  0)
#define EPS_VBATT                   ((eps_tlm_id_t)  1)
#define EPS_CURIN                   ((eps_tlm_id_t)  2)
#define EPS_CURSUN                  ((eps_tlm_id_t)  3)
#define EPS_CURSYS                  ((eps_tlm_id_t)  4)
#define EPS_RESERVED                ((eps_tlm_id_t)  5)
#define EPS_CUROUT                  ((eps_tlm_id_t)  6)
#define EPS_OUTPUT                  ((eps_tlm_id_t)  7)
#define EPS_OUTPUT_ON_DELTA         ((eps_tlm_id_t)  8)
#define EPS_OUTPUT_OFF_DELTA        ((eps_tlm_id_t)  9)
#define EPS_LATCHUP                 ((eps_tlm_id_t) 10)
#define EPS_WDT_I2C_TIME_LEFT       ((eps_tlm_id_t) 11)
#define EPS_WDT_GND_TIME_LEFT       ((eps_tlm_id_t) 12)
#define EPS_WDT_CSP_PINGS_LEFT      ((eps_tlm_id_t) 13)
#define EPS_COUNTER_WDT_I2C         ((eps_tlm_id_t) 14)
#define EPS_COUNTER_WDT_GND         ((eps_tlm_id_t) 15)
#define EPS_COUNTER_WDT_CSP         ((eps_tlm_id_t) 16)
#define EPS_COUNTER_BOOT            ((eps_tlm_id_t) 17)
#define EPS_TEMP                    ((eps_tlm_id_t) 18)
#define EPS_BOOTCAUSE               ((eps_tlm_id_t) 19)
#define EPS_BATTMODE                ((eps_tlm_id_t) 20)
#define EPS_PPTMODE                 ((eps_tlm_id_t) 21)
#define EPS_RESERVED2               ((eps_tlm_id_t) 22)

/** Enum defining all power lines of eps. */
typedef enum
{
	EPS_POWER_LINE0 = 0,
	EPS_POWER_LINE1 = 1,
	EPS_POWER_LINE2 = 2,
	EPS_POWER_LINE3 = 3,
	EPS_POWER_LINE4 = 4,
	EPS_POWER_LINE5 = 5
} eps_power_line_enum_t;
#define EPS_TOTAL_POWER_LINES 6

/** Enum defining possible states of power lines. */
typedef enum
{
	EPS_POWER_LINE_INACTIVE = 0,						/*!< (0) Power line is not supplying power. */
	EPS_POWER_LINE_ACTIVE = !EPS_POWER_LINE_INACTIVE	/*!< (1) Power line is supplying power. */
} eps_power_line_state_enum_t;


/****************************************************************************/
/* Structure Define															*/
/****************************************************************************/
/**
 * @struct eps_t
 * @brief
 *		Abstract structure used for communicating with the electronic power supply.
 * @details
 * 		A structure used for communicating with the electronic power supply.
 * 		Each instance of this structure can support IO with exactly one
 * 		power board and must be properly initialized.
 * 		All variables inside the eps_t structure must be treated as private.
 * 		They must never be accessed directly.
 */
struct eps_t
{
	bool_t (*power_line)( eps_t*, eps_power_line_enum_t, eps_power_line_state_enum_t );
	hk_handler_t (*hk_get)(eps_t *, eps_tlm_id_t);
	eps_vbatt_t (*get_vbatt)(eps_t *);
	eps_mode_t (*mode)(eps_t *);
	bool_t (*hk_refresh)(eps_t *);
	void (*destroy)( eps_t* );
	struct
	{
		eps_hk_t hk;
	}_; /* Private. */
};

/****************************************************************************/
/* Public Methods															*/
/****************************************************************************/
/**
 * @memberof eps_t
 * @brief
 * 		Returns battery voltage of EPS in mV.
 * @details
 * 		Returns battery voltage of EPS in mV.
 * @param eps
 * 		A pointer to the eps_t structure being polled.
 * @returns
 * 		The battery voltage in mV.
 */
uint16_t eps_get_vbatt( eps_t *eps );

/**
 * @memberof eps_t
 * @brief
 * 		Returns battery current of EPS in mA.
 * @details
 * 		Returns battery current of EPS in mA.
 * @param eps
 * 		A pointer to the eps_t structure being polled.
 * @returns
 * 		The battery current in mA.
 */
uint16_t eps_get_batt_current( eps_t *eps );

/**
 * @memberof eps_t
 * @brief
 * 		Returns the current on the EPS' 3v3 bus in mA
 * @details
 * 		Returns the current on the EPS' 3v3 bus in mA. If there is more than
 * 		one 3v3 bus then the sum of the current on all 3v3 busses is returned.
 * 		This includes configurable and permanent lines.
 * @param eps
 * 		A pointer to the eps_t structure being polled.
 * @returns
 * 		The 3v3 bus current in mA.
 */
uint16_t eps_get_3v3_current( eps_t *eps );

/**
 * @memberof eps_t
 * @brief
 * 		Returns the current on the EPS' 5v0 bus in mA
 * @details
 * 		Returns the current on the EPS' 5v0 bus in mA. If there is more than
 * 		one 5v0 bus then the sum of the current on all 5v0 busses is returned.
 * 		This includes configurable and permanent lines.
 * @param eps
 * 		A pointer to the eps_t structure being polled.
 * @returns
 * 		The 5v0 bus current in mA.
 */
uint16_t eps_get_5v0_current( eps_t *eps );

/**
 * @memberof eps_t
 * @brief
 * 		Returns the temperature of the EPS board in degC.
 * @details
 *		Returns the temperature of the EPS board in degC.
 * @param eps
 * 		A pointer to the eps_t structure being polled.
 * @returns
 * 		The EPS board temperature in degC.
 */
int16_t	 eps_get_pcb_temp( eps_t *eps );

/**
 * @memberof eps_t
 * @brief
 * 		Returns the temperature of the EPS battery in degC.
 * @details
 *		Returns the temperature of the EPS batter in degC. If there is more
 *		than one battery then this is the average temperature.
 * @param eps
 * 		A pointer to the eps_t structure being polled.
 * @returns
 * 		The EPS battery temperature in degC.
 */
int16_t  eps_get_batt_temp( eps_t *eps );


#endif /* EPS_H_ */
