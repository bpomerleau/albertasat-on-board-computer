/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file eps_local.h
 * @author Brendan Bruner
 * @date Oct 23, 2015
 */
#ifndef INCLUDE_EPS_EPS_LOCAL_H_
#define INCLUDE_EPS_EPS_LOCAL_H_

#include "eps.h"
#include <gpio/gpio.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
#define EPS_LOCAL_STATE_GPIO_COUNT 3


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct eps_local_t
 * @extends eps_t
 * @brief
 * 		A mock up of the eps board which runs locally in software.
 * @details
 * 		A mock up of the eps board which runs locally in software. GPIO
 * 		pins are toggle high / low to simulate power lines turning on / off.
 * 		<br>
 * 		<b>Overrides</b>: eps_t::power_line( )
 * 		<b>Overrides</b>: eps_t::hk_refresh( )
 */
typedef struct eps_local_t eps_local_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct eps_local_t
{
	eps_t s_; /* Must be first. */
	struct
	{
		gpio_t* power_line_gpios[EPS_TOTAL_POWER_LINES];
		bool_t using_power_lines;
		gpio_t* state_gpios[EPS_LOCAL_STATE_GPIO_COUNT];
		bool_t using_state_gpios;
		eps_mode_t (*s_mode)(eps_t *); /* Overriding eps_t::mode, but keeping reference to it. */
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof eps_local_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 * @param power_line_gpios
 * 		An array of pointers to gpio's which will be used for the power lines. The array
 * 		MUST be at least <b>EPS_TOTAL_POWER_LINES</b> long. Each element
 * 		of the array is a unique gpio. These gpios must remain valid in
 * 		memory for the life of the eps_local_t object initialized.
 * 		<br>Element zero of the array is power line 0.
 * 		<br>Element one of the array is power line 1.
 * 		<br>And so on.
 * 		<br>The gpio is written high when a power line is turned on, and written low otherwise.
 * 		<br>NOTE, if the functionality of power lines toggling gpio pins is not
 * 		wanted, pass in <b>NULL</b> to disable.
 * @param state_gpios
 * 		An array of pointers to gpio's that will serve to communicate the eps' state. They are used
 * 		in the same manner as the <b>power_line_gpios</b> array. The array must be at least
 * 		<b>EPS_LOCAL_STATE_GPIO_COUNT</b> long.
 * 		<br>Element zero is set high when in power safe mode.
 * 		<br>Element one is set high when in normal mode.
 * 		<br>Element two  is set high when in optimal mode.
 * 		<br>All other elements are set to low.
 * 		<br>Pass in <b>NULL</b> if this functionality is not wanted.
 */
void initialize_eps_local( eps_local_t*, gpio_t** power_line_gpios, gpio_t** state_gpios );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_EPS_EPS_LOCAL_H_ */
