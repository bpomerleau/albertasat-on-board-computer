/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file teledyne_dm.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_teledyne_teledyne_EXTERNAL_H_
#define INCLUDE_teledyne_teledyne_EXTERNAL_H_

#include <teledyne/teledyne.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct teledyne_external_t
 * @extends teledyne_t
 * @brief
 * 		Interface to physical teledyne of satellite.
 * @details
 * 		Interface to physical teledyne of satellite.
 */
typedef struct teledyne_external_t teledyne_external_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct teledyne_external_t
{
	teledyne_t s_; /* MUST be first. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof teledyne_external_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_teledyne_external( teledyne_external_t* );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_teledyne_teledyne_EXTERNAL_H_ */
