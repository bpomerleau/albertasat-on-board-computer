/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file teledyne.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_teledyne_teledyne_H_
#define INCLUDE_teledyne_teledyne_H_

#include <eps/eps.h>
#include <dev/spi.h>
#include <core_defines.h>
#include <logger/logger.h>
#include <logger/telemetry_stream.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/

#define CONFIG_UDOS_CS 3
#define UDOS_ADC_CHANNELS 6

/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct teledyne_t
 * @brief
 * 		Abstract class defining interface to the teledyne.
 * @details
 * 		Abstract class defining interface to the teledyne.
 */
typedef struct teledyne_t teledyne_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct __attribute__((__packed__)) udos_event_t
{
	uint32_t time_stamp;
	uint16_t channel0[UDOS_SAMPLES_PER_EVENT];
	uint16_t channel1[UDOS_SAMPLES_PER_EVENT];
	uint16_t channel2[UDOS_SAMPLES_PER_EVENT];
	uint16_t channel3[UDOS_SAMPLES_PER_EVENT];
	uint16_t channel4[UDOS_SAMPLES_PER_EVENT];
	uint16_t channel5[UDOS_SAMPLES_PER_EVENT];
};

struct __attribute__((packed)) udos_packet_t
{
	uint8_t dle;				/* data link escape, 0x10 */
	uint8_t pid;				/* packed id, rolling counter */
	uint8_t pv;					/* packet version number. */
	uint8_t samples_per_event;	/* number of samples in each event. */
	uint8_t events_recorded;	/* Number of events in a packet. */
	uint8_t reserved0[2];
	uint8_t stx;				/* start of text, 0x02 */
	struct udos_event_t events[UDOS_EVENTS_PER_PACKET];
	uint8_t etx;				/* end of text, 0x03 */
	uint8_t reserved1[2];
	uint8_t eot;				/* end of transmission, 0x04 */
};

struct teledyne_t
{
	bool_t (*power)( teledyne_t*, eps_t*, bool_t );
	semaphore_t power_ctrl;
	struct telemetry_stream_t event_stream;
	xTaskHandle td;
	struct udos_packet_t packet;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/

/**
 * @details
 * 		Take UDOS_SAMPLES_PER_EVENT samples from the udos ADC.
 * @param event
 * 		Samples are put into the memory pointed to by this.
 * @return
 * 		0 on success, -1 otherwise. event should be discarded if -1 is returned.
 */
int udos_take_samples( struct udos_event_t* event );
void dosimeter_start( teledyne_t* self );
bool_t dosimeter_suspend( teledyne_t* self );
void dosimeter_resume( teledyne_t* self );

#endif /* INCLUDE_teledyne_teledyne_H_ */
