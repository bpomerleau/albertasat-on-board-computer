/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm_local.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_DFGM_DFGM_LOCAL_H_
#define INCLUDE_DFGM_DFGM_LOCAL_H_

#include "dfgm.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct dfgm_local_t
 * @extends dfgm_t
 * @brief
 * 		Interface to a local software mock up of dfgm.
 * @details
 * 		Interface to a local software mock up of dfgm.
 */
typedef struct dfgm_local_t dfgm_local_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct dfgm_local_t
{
	dfgm_t s_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof dfgm_local_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_dfgm_local( dfgm_local_t* );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_DFGM_DFGM_LOCAL_H_ */
