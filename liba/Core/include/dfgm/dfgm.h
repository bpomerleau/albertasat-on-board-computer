/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_DFGM_DFGM_H_
#define INCLUDE_DFGM_DFGM_H_

#include <eps/eps.h>
#include <hub/hub.h>
#include "dfgm_config.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct dfgm_t
 * @brief
 * 		Abstract class defining interface to the DFGM.
 * @details
 * 		Abstract class defining interface to the DFGM.
 */
typedef struct dfgm_t dfgm_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct dfgm_t
{
	bool_t (*power)( dfgm_t*, eps_t*, bool_t );
	bool_t (*deploy)( dfgm_t*, hub_t* );
	struct dfgm_config_t dfgm_config;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_DFGM_DFGM_H_ */
