/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm_dm.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_DFGM_DFGM_EXTERNAL_H_
#define INCLUDE_DFGM_DFGM_EXTERNAL_H_

#include "dfgm.h"
#include "dfgm_config.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct dfgm_external_t
 * @extends dfgm_t
 * @brief
 * 		Interface to physical dfgm of satellite.
 * @details
 * 		Interface to physical dfgm of satellite.
 */
typedef struct dfgm_external_t dfgm_external_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct dfgm_external_t
{
	dfgm_t s_; /* MUST be first. */
	struct
	{
		bool_t (*supers_power)( dfgm_t*, eps_t*, bool_t );
	}_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof dfgm_external_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_dfgm_external( dfgm_external_t* );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_DFGM_DFGM_EXTERNAL_H_ */
