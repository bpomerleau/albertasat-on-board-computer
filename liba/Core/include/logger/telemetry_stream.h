/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file telemetry_stream
 * @author Brendan Bruner
 * @date Oct. 20, 2017
 */
#include <logger/logger.h>

#ifndef LIB_LIBCORE_INCLUDE_LOGGER_TELEMETRY_STREAM_H_
#define LIB_LIBCORE_INCLUDE_LOGGER_TELEMETRY_STREAM_H_

#define TELEMETRY_STREAM_OK 0
#define TELEMETRY_STREAM_FAIL -1
#define TELEMETRY_STREAM_NEW_FILE -2

#define TELEMETRY_STREAM_NEW 0
#define TELEMETRY_STREAM_REUSE 1
/********************************************************************************/
/* Structures																	*/
/********************************************************************************/
struct telemetry_stream_t;
typedef int(*telemetry_stream_next_f)(struct telemetry_stream_t*, file_t*);
struct telemetry_stream_t
{
	logger_t* data_bank;
	file_t* data_file;
	telemetry_stream_next_f next;
	int do_next_file;
	size_t max_bucket_size;
};

/********************************************************************************/
/* Functions																	*/
/********************************************************************************/
/**
 * @memberof telemetry_stream_t
 * @details
 * 		Creates an object that simplifies the interface to the logger_t class.
 * 		It provides a file interface for logging data.
 *
 * 		The stream object is NOT thread safe. Do not write to a stream in more than one thread.
 * @param stream
 * 		The object to initialize.
 * @param logger
 * 		The logger object who's interface will be simplified.
 * @param next
 * 		A function pointer.
 * 		Returns +1 if the file argument meets conditions to be closed and a new file
 * 		opened for data logging
 * 		Returns -1 for the inverse of the above case.
 * 		Do not close the input file in your custom implementation of this function.
 * @param strat
 * 		Defines the initialization strategy.
 * 		0:
 * 			The stream is initialized with a new bucket from the logger.
 * 		1:
 * 			The stream is initialized using the same file that was being used in the previous boot.
 * @return
 * 		0 on success, -1 on failure.
 */
int telemetry_stream_init_custom( struct telemetry_stream_t* stream, struct logger_t* logger, telemetry_stream_next_f next, int strat );

/**
 * @memberof telemetry_stream_t
 * @details
 * 		Same as telemetry_stream_init_custom(), except the @next argument is replaced with @max_bucket_size.
 * 		The @next function is implemented within the stream to insert a new bucket into the logger once the
 * 		current bucket reaches @max_bucket_size bytes.
 */
int telemetry_stream_init( struct telemetry_stream_t* stream, struct logger_t* logger, size_t max_bucket_size, int strat );



/**
 * @memberof telemetry_stream_t
 * @details
 * 		Gets a file for logging data to.
 * @param stream
 * 		The telemetry stream to write data to.
 * @param err
 * 		Error code of function.
 * 		TELEMETRY_STREAM_NEW_FILE => An empty file was returned.
 * 		TELEMETRY_STREAM_OK => The file returned has had data logged to it previously.
 * 		TELEMETRY_STREAM_FAIL => Failure in ring buffer code or invalid input arguments. Returned file is NULL.
 * @return
 * 		A file to write data to. Do not close this file. Use telemetry_stream_next_file() or
 * 		telemetry_stream_close_file() if it needs to be closed.
 */
file_t* telemetry_stream_get_file( struct telemetry_stream_t* stream, int* err );

/**
 * @memberof telemetry_stream_t
 * @details
 * 		Force the stream to close the current file. The next call to telemetry_stream_get_file()
 * 		will re-open the file.
 * @param stream
 * 		The telemetry stream to act on.
 * @return
 * 		TELEMETRY_STREAM_OK => no error.
 * 		TELEMETRY_STREAM_FAIL => invalid input argument.
 */

int telemetry_stream_close_file( struct telemetry_stream_t* stream );

/**
 * @memberof telemetry_stream_t
 * @details
 * 		Force the stream to close the current file. The next call to telemetry_stream_get_file()
 * 		will return a new empty file.
 * 		Use this function when the current file has exceeded it's maximum size and a new empty file
 * 		is required.
 * @param stream
 * 		The telemetry stream to act on.
 * @return
 * 		TELEMETRY_STREAM_OK => no error.
 * 		TELEMETRY_STREAM_FAIL => invalid input argument.
 */
int telemetry_stream_next_file( struct telemetry_stream_t* stream );


#endif /* LIB_LIBCORE_INCLUDE_LOGGER_TELEMETRY_STREAM_H_ */
