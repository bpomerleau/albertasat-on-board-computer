/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file non_volatile_variable.h
 * @author Brendan Bruner
 * @date Oct 21, 2015
 */
#ifndef INCLUDE_NON_VOLATILE_VARIABLE_H_
#define INCLUDE_NON_VOLATILE_VARIABLE_H_

#include <portable_types.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct non_volatile_variable_t
 * @brief
 * 		Abstract class / interface for non volatile variables.
 * @details
 * 		Abstract class / interface for non volatile variables. Variables
 * 		accessed through this interface reside in non volatile memory and
 * 		retain their value through power cycles.
 */
typedef struct non_volatile_variable_t non_volatile_variable_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct non_volatile_variable_t
{
	void (*destroy)( non_volatile_variable_t* );
	bool_t (*set)( non_volatile_variable_t*, void* );
	bool_t (*get)( non_volatile_variable_t*, void* );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
bool_t non_volatile_variable_set( non_volatile_variable_t*, void* );
bool_t non_volatile_variable_get( non_volatile_variable_t*, void* );


#endif /* INCLUDE_NON_VOLATILE_VARIABLE_H_ */
