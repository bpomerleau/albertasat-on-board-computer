/*
 * mnlp.h
 *
 *  Created on: 22-Jun-2015
 *      Author: Atri Bhattacharyya
 */

#include <stdint.h>
#include "filesystems/filesystem.h"
#include "portable_types.h"

#define ENABLE_MNLP 1

#define MNLP_USART_HANDLE 1
#define MNLP_USART_BAUD 9600
#define SCPT_SEQ_ONE 	0x41
#define SCPT_SEQ_TWO 	0x42
#define SCPT_SEQ_THREE 	0x43
#define SCPT_SEQ_FOUR 	0x44
#define SCPT_SEQ_FIVE 	0x45
#define END_OF_TABLE 	0x55

#define OBC_SU_ON 	0xF1
#define OBC_SU_OFF	0xF2
#define SU_RESET	0x02
#define SU_LDP		0x05
#define SU_HC		0x06
#define SU_CAL		0x07
#define SU_SCI		0x08
#define SU_HK		0x09
#define SU_STM		0x0A
#define SU_DUMP		0x0B
#define SU_BIAS_ON	0x53
#define SU_BIAS_OFF	0xC9
#define SU_MTEE_ON	0x35
#define SU_MTEE_OFF	0x9C
#define SU_ERR		0xBB
#define OBC_SU_ERR	0xFA
#define OBC_EOT		0xFE

#define SU_LDP_PKT	0x05
#define SU_HC_PKT	0x06
#define SU_CAL_PKT	0x07
#define SU_SCI_PKT	0x08
#define SU_HK_PKT	0x09
#define SU_STM_PKT	0x0A
#define SU_DUMP_PKT	0x0B
#define SU_ERR_PKT	0xBB
#define OBC_SU_ERR	0xFA

#define DATA_PACKET_SIZE	174

#define RX_TIMEOUT_ERR 	1	//Error code if rx timed out without packet

#define QB50_EPOCH_OFFSET_FROM_UNIX_EPOCH	946684800			//Offset from unix epoch(adcs) to QB50 epoch(mNLP)

/* mNLP init return codes */

typedef enum {
	NO_ERROR,
	FOPEN_ERROR,
	FILESYSTEM_NOT_OPENED,
	MULTIPLE_ERRORS,
	VOLUME_FULL,
	OUTPUT_FILE_NOT_OPENED,
	NO_FILE
} mNLPInitResult;

typedef struct
{
	uint16_t script_length;					//Script length in BYTES of COMPLETE script file
	uint32_t start_time;					//UTC time at which to start running the script
} mnlp_script_hdr_t;


typedef struct
{
	uint8_t time_seconds;		//Seconds field of time to start script sequence [0-59]
	uint8_t time_minutes;		//Minutes field of time to start script sequence [0-59]
	uint8_t time_hours;			//Hours field of time to start script sequence [0-23]
	uint8_t script_index;		//Script index
}mnlp_times_table_t;

typedef struct
{
	struct
	{
		unsigned int script_error 	: 1; 	//If an error is detected in the script, this is set
		unsigned int done_today		: 1;	//If the script has finished running for the day, this is set
	}status;
	struct
	{
		unsigned int num_seq			: 3;	//Number of script sequences for this script
		unsigned int num_tt_entries	:10;	//Number of entries in times table
		unsigned int daily_start_time:17;	//Number of seconds from start of day that script starts
	}info;
	file_t *fp;						//File pointer for file holding this script
	// *current_script_seq;		//File pointers to 5 files which shall be created to store the 5 script sequences for this script
	char script_file_name[12];		//Name of script
	uint8_t raw_header[12];			//raw header entries
	mnlp_script_hdr_t script_hdr;	//script header
	mnlp_times_table_t next_seq[1024];	//List of all times table entries. Number is listed in info above.
	uint16_t xsum;
} mnlp_script_t;

typedef struct
{
	uint8_t delSec;					//Minutes to wait after command
	uint8_t delMin;					//Seconds to wait after command
	uint8_t CMD_ID;					//Command ID
	uint8_t LEN;					//Number of parameter bytes
	uint8_t SEQ_CNT;				//Command Sequence number
}mnlp_seq_cmd_t;


typedef struct
{
	uint32_t utc;
	uint16_t roll;
	uint16_t pitch;
	uint16_t yaw;
	uint16_t rolldot;
	uint16_t pitchdot;
	uint16_t yawdot;
	int16_t x_eci;
	int16_t y_eci;
	int16_t z_eci;
} __attribute__ ((packed)) mnlp_science_hdr_t;

typedef struct
{
	uint8_t rsp_err_code;
	uint16_t XSUM_R;
	struct
	{
		uint32_t UTC_start;
		uint32_t header_sn;
		uint8_t sw_ver_id;
		uint8_t sw_type_md;
	}__attribute__ ((packed)) hdr_R;
	struct
	{
		uint16_t XSUM_n;
		struct
		{
			uint32_t UTC_start;
			uint32_t header_sn;
			uint8_t sw_ver_id;
			uint8_t sw_type_md;
		}__attribute__ ((packed)) hdr_n;
	} script_info[7];
	uint8_t reserved[75];
} __attribute__ ((packed)) mnlp_obc_err_pkt_data;

typedef struct
{
	uint16_t DAC_bias_man;
	uint16_t DAC_1_bias;
	uint16_t DAC_2_bias;
	uint16_t DAC_3_bias;
	uint16_t DAC_4_bias;
	uint16_t AGC_gain_man;
	uint16_t AGC_gain;
	uint16_t Science_Fs;
	uint16_t ADC_res;
	uint16_t HK_Fs;
	uint16_t AGC_high;
	uint16_t AGC_low;
	uint16_t AGC_timer;
	uint16_t MTEE_power; 			//Low byte MTEE_PWR High byte MTEE_downStep
	uint16_t MTEE_manual_voltage; 	//Low byte MTEE_voltage_man High byte MTEE_upStep
	uint16_t MTEE_voltage;			//Low byte MTEE_voltage High byte MTEE_voltageLimit
	uint16_t HK_t_sample_1;
	uint16_t STM_t_sample_1;
	uint16_t CH1a_g0_coeff;
	uint16_t CH1b_g0_coeff;
	uint16_t CH2a_g0_coeff;
	uint16_t CH2b_g0_coeff;
	uint16_t CH3a_g0_coeff;
	uint16_t CH3b_g0_coeff;
	uint16_t CH4a_g0_coeff;
	uint16_t CH4b_g0_coeff;
	uint16_t CHxy_g1_coeff[8];
	uint16_t CHxy_g2_coeff[8];
	uint16_t CHxy_g3_coeff[8];
	uint16_t CHxy_g4_coeff[8];
	uint16_t CH1a_bias_coeff;
	uint16_t CH1b_bias_coeff;
	uint16_t CH2a_bias_coeff;
	uint16_t CH2b_bias_coeff;
	uint16_t CH3a_bias_coeff;
	uint16_t CH3b_bias_coeff;
	uint16_t CH4a_bias_coeff;
	uint16_t CH4b_bias_coeff;
	uint16_t MTEE_auto_H;
	uint16_t MTEE_auto_L;
	uint16_t Num_pac;
	uint16_t Mask;
	uint8_t  reserved[32];
}__attribute__ ((packed)) mnlp_ldp_pkt_data;

typedef struct
{
	uint8_t SU_Temp_OK;
	uint8_t V15_V_OK;
	uint8_t V15_I_OK;
	uint8_t V25_V_OK;
	uint8_t V25_I_OK;
	uint8_t V33_V_OK;
	uint8_t V33_I_OK;
	uint8_t V50_V_OK;
	uint8_t V50_I_OK;
	uint8_t V120_V_OK;
	uint8_t V120_I_OK;
	uint8_t reserved[161];
}__attribute__ ((packed)) mnlp_hc_pkt_data;

typedef struct
{
	uint8_t CH_CAL;
	uint8_t T_del;
	uint16_t Probe_Current0;
	uint16_t Probe_Bias0;
	uint16_t Probe_Current1;
	uint16_t Probe_Bias1;
	uint16_t Probe_Current2;
	uint16_t Probe_Bias2;
	uint16_t Probe_Current3;
	uint16_t Probe_Bias3;
	uint16_t Probe_Current4;
	uint16_t Probe_Bias4;
	uint16_t Probe_Current5;
	uint16_t Probe_Bias5;
	uint16_t Probe_Current6;
	uint16_t Probe_Bias6;
	uint16_t Probe_Current7;
	uint16_t Probe_Bias7;
	uint16_t Probe_Current8;
	uint16_t Probe_Bias8;
	uint16_t Probe_Current9;
	uint16_t Probe_Bias9;
	uint16_t Probe_Current10;
	uint16_t Probe_Bias10;
	uint16_t Probe_Current11;
	uint16_t Probe_Bias11;
	uint16_t Probe_Current12;
	uint16_t Probe_Bias12;
	uint16_t Probe_Current13;
	uint16_t Probe_Bias13;
	uint16_t Probe_Current14;
	uint16_t Probe_Bias14;
	uint16_t Probe_Current15;
	uint16_t Probe_Bias15;
	uint16_t Probe_Current16;
	uint16_t Probe_Bias16;
	uint16_t Probe_Current17;
	uint16_t Probe_Bias17;
	uint16_t Probe_Current18;
	uint16_t Probe_Bias18;
	uint16_t Probe_Current19;
	uint16_t Probe_Bias19;
	uint16_t Probe_Current20;
	uint16_t Probe_Bias20;
	uint16_t Probe_Current21;
	uint16_t Probe_Bias21;
	uint16_t Probe_Current22;
	uint16_t Probe_Bias22;
	uint16_t Probe_Current23;
	uint16_t Probe_Bias23;
	uint16_t Probe_Current24;
	uint16_t Probe_Bias24;
	uint16_t Probe_Current25;
	uint16_t Probe_Bias25;
	uint16_t Probe_Current26;
	uint16_t Probe_Bias26;
	uint16_t Probe_Current27;
	uint16_t Probe_Bias27;
	uint16_t Probe_Current28;
	uint16_t Probe_Bias28;
	uint16_t Probe_Current29;
	uint16_t Probe_Bias29;
	uint16_t Probe_Current30;
	uint16_t Probe_Bias30;
	uint16_t Probe_Current31;
	uint16_t Probe_Bias31;
	uint16_t Probe_Current32;
	uint16_t Probe_Bias32;
	uint16_t Probe_Current33;
	uint16_t Probe_Bias33;
	uint16_t Probe_Current34;
	uint16_t Probe_Bias34;
	uint16_t Probe_Current35;
	uint16_t Probe_Bias35;
	uint16_t Probe_Current36;
	uint16_t Probe_Bias36;
	uint16_t Probe_Current37;
	uint16_t Probe_Bias37;
	uint16_t Probe_Current38;
	uint16_t Probe_Bias38;
	uint16_t Probe_Current39;
	uint16_t Probe_Bias39;
	uint16_t Probe_Current40;
	uint16_t Probe_Bias40;
	uint16_t Probe_Current41;
	uint16_t Probe_Bias41;
	uint8_t reserved[2];
}__attribute__ ((packed)) mnlp_cal_pkt_data;


//This encode scheme is for 16-bit data only. For other bit resolutions, the encoding is different.
typedef struct
{
	uint8_t FS;
	uint8_t R_squared;
	struct
	{
		uint8_t Floating_Pot;
		uint16_t Elec_Density[10];
	}__attribute__ ((packed)) data[8];
	uint8_t Elec_Density_byte;			//After nine groups of floating potential and elec density,
										//there is an extra byte of electron density data
										//Not sure if intentional
	uint8_t reserved;
}__attribute__ ((packed)) mnlp_sci_pkt_data;

typedef struct
{
	struct
	{
		uint16_t temp;
		uint16_t V_1V5;
		uint16_t I_1V5;
		uint16_t V_3V3;
		uint16_t I_3V3;
		uint16_t V_5V0;
		uint16_t I_5V0;
		uint16_t V_12V0;
		uint16_t I_12V0;
		uint16_t HK_bits;
		uint16_t MTEE_V;
		uint16_t MTEE_I;
	}data[7];
	uint8_t reserved[4];
}__attribute__ ((packed)) mnlp_hk_pkt_data;

typedef struct
{
	struct
	{
		uint16_t STM_TH_0;
		uint16_t STM_TH_1;
		uint16_t STM_TH_2;
		uint16_t STM_TH_3;
		uint16_t STM_TH_4;
		uint16_t STM_TH_5;
	}data[14];
	uint8_t reserved[4];
}__attribute__ ((packed)) mnlp_stm_pkt_data;

//There seems to be an error in this encoding too. Error lies in the documentation
//Please rectify after checking
typedef struct
{
	uint16_t TEMP;
	uint16_t V_1V5;
	uint16_t I_1V5;
	uint16_t V_3V3;
	uint16_t I_3V3;
	uint16_t V_5V0;
	uint16_t I_5V0;
	uint16_t V_12V0;
	uint16_t I_12V0;
	uint8_t FS;
	uint8_t R_squared;
	struct
	{
		uint8_t Floating_pot;
		uint16_t Elec_density[10];
	}__attribute__ ((packed)) data[7];
	uint8_t Floating_pot;
	uint8_t reserved[4];
}__attribute__ ((packed)) mnlp_dump_pkt_data;

typedef struct
{
	uint8_t text[172];
}mnlp_error_packet_t;

typedef struct
{
	uint8_t rsp_id;		//Response packet ID
	uint8_t seq_cnt;	//Sequence count
	uint8_t data[172];	//Packet data     //or union data
} __attribute__ ((packed)) mnlp_response_pkt_t;


//Public functions
void mnlp_init(void * time_mult);
void mnlp_deinit( void *pvParameters);
void mnlp_usart_callback();
bool_t mnlp_start(void);
bool_t mnlp_stop(void);
void mnlp_preinit(void);
