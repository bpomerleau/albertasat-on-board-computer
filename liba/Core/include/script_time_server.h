/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file script_daemon.h
 * @author Brendan Bruner
 * @date Nov 4, 2015
 */
#ifndef INCLUDE_SCRIPT_TIME_SERVER_H_
#define INCLUDE_SCRIPT_TIME_SERVER_H_

#include <ground_station.h>
#include <core_defines.h>
#include <portable_types.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct script_daemon_t
 * @brief
 * 		An active class which listens for scripts from the ground station.
 * @details
 * 		An active class which listens for scripts from the ground station.
 * 		Incoming scripts are parsed into an expression (syntax tree) then
 * 		interpreted. This process loops for ever.
 */
typedef struct script_time_server_t script_time_server_t;

struct command_t
{
	uint32_t unix_time;
	uint8_t command_length;
	char command[COMMAND_STRING_MAX_LENGTH];
} __attribute__((packed));

struct schedule_t
{
	uint8_t total_commands;
	struct command_t slot[MAX_COMMANDS_PER_SCHEDULE];
} __attribute__((packed));


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct script_time_server_t
{
	struct schedule_t schedule;
	semaphore_t sem;
	struct
	{
		task_t task_handle;
	}_; /* Private. */
	ground_station_t* gs;
	void (*destroy)( script_time_server_t* );
	void (*load)(script_time_server_t* );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof script_time_server_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 * @param gs[in]
 * 		Server that reads and handles OBC command script files.
 */
bool_t initialize_script_time_server( script_time_server_t*, ground_station_t* gs );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
void script_time_server_kill( script_time_server_t* );


#endif /* INCLUDE_SCRIPT_TIME_SERVER_H_ */
