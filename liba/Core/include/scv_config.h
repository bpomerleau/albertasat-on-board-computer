/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file core_defines
 * @author Brendan Bruner
 * @date Oct. 2, 2017
 */

#ifndef LIB_LIBCORE_INCLUDE_SCV_SCV_CONFIG_H_
#define LIB_LIBCORE_INCLUDE_SCV_SCV_CONFIG_H_

/********************************************************************************/
/* Structures																	*/
/********************************************************************************/
struct scv_config_t
{
	size_t athena_logger_capacity;
	size_t athena_logger_file_size;

	size_t dfgm_filt1_capacity;
	size_t dfgm_filt2_capacity;
	size_t dfgm_filt_file_size;

	size_t dfgm_raw_capacity;
	size_t dfgm_raw_file_size;

	size_t adcs_locale_capacity;
	size_t adcs_locale_file_size;

	size_t dfgm_hk_capacity;
	size_t dfgm_hk_file_size;

	size_t wod_capacity;
	size_t wod_file_size;
	size_t wod_cadence;

	size_t udos_capacity;
	size_t udos_file_size;
	size_t udos_cadence;
};

/********************************************************************************/
/* Functions																	*/
/********************************************************************************/
/*
 * Cache scv configuration from non volatile memory into the program's RAM
 * returns 0 on success, < 0 on failure.
 */
int scv_config_cache( );

/*
 * Get the current configuration being used by the satellite.
 */
void scv_config_get_current( struct scv_config_t* );

/*
 * Set the current configuration being used by the satellite.
 * This has no effect on data logging because the configuration
 * is read at boot. It is not continuously updated
 */
void scv_config_set_current( struct scv_config_t* );

/*
 * The current configuration (set by scv_config_set_current( )) is written to non volatile memory. The data written can only
 * be cached once and afterwords, the data gets deleted. Use scv_config_commit() to prevent
 * deletion.
 * returns 0 on success, < 0 on failure.
 */
int scv_config_flush( );

/*
 * The configuration in non volatile memory becomes permanent. Call this after using
 * scv_config_flush( ) to allow multiple caaches of a configuration.
 * returns 0 on success, < 0 on failure.
 */
int scv_config_commit( );


#endif /* LIB_LIBCORE_INCLUDE_SCV_SCV_CONFIG_H_ */
