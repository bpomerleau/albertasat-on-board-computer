/*
 * telemetry_cleanup_daemon.h
 *
 *  Created on: Jun 29, 2016
 *      Author: bbruner
 */

#ifndef LIB_LIBCORE_INCLUDE_TELEMETRY_CLEANUP_H_
#define LIB_LIBCORE_INCLUDE_TELEMETRY_CLEANUP_H_

#include <stdint.h>
#include <portable_types.h>

/* Remove all telemetry which is time stamped from before 'epoch'. */
bool_t telemetry_cleanup_all( uint32_t epoch );

/* Initialize telemetry cleanup service. */
bool_t telemetry_cleanup_init( );

#endif /* LIB_LIBCORE_INCLUDE_TELEMETRY_CLEANUP_H_ */
