/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file filesystem_fatfs.h
 * @author Brendan Bruner
 * @date Aug 14, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_FATFS_FILESYSTEM_FATFS_H_
#define INCLUDE_FILESYSTEMS_FATFS_FILESYSTEM_FATFS_H_

#include "../filesystem.h"
#include "file_fatfs.h"
#include "dir_fatfs.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct filesystem_fatfs_t
 * @extends filesystem_t
 * @brief
 * 		Abstract structure for opening, closing, and deleting files in non volatile
 * 		memory using the fatfs filesystem.
 * @details
 * 		Abstract structure for opening, closing, and deleting files in non volatile
 * 		memory using the fatfs filesystem.
 * @var filesystem_fatfs_t::_super_
 * 		<b>Private</b>
 * 		Super structure data.
 * @var filesystem_fatfs_t::_files_
 * 		<b>Private</b>
 * 		A pointer to an array of handles which will be returned by filesystem_t::open.
 * 		Handles are not dynamically allocated, there are fixed number of them and this
 * 		array contains all the handles. In addition, the array is a singleton, so every
 * 		instance of this structure uses the same array of handles.
 * @var filesystem_fatfs_t::_num_files_
 * 		<b>Private</b>
 * 		The number of handles in the filesystem_fatfs_t::_files_ array.
 * @var filesystem_fatfs_t::_dirs_
 * 		<b>Private</b>
 * 		A pointer to an array of directories which will be returned by filesystem_t::open_dir.
 * 		dirs are not dynamically allocated, there are fixed number of them and this
 * 		array contains all the dirs. In addition, the array is a singleton, so every
 * 		instance of this structure uses the same array of dirs.
 * @var filesystem_fatfs_t::_num_dirs_
 * 		<b>Private</b>
 * 		The number of directory handles in filesystem_fatfs_t::_dirs_ array.
 */
typedef struct filesystem_fatfs_t filesystem_fatfs_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct filesystem_fatfs_t
{
	filesystem_t 	_super_; /* MUST be first  variable. */

	file_fatfs_t	*_files_;
	int16_t 		_num_files_;

	dir_fatfs_t		*_dirs_;
	int16_t			_num_dirs_;
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof filesystem_fatfs_t
 * @protected
 * @brief
 * 		Initialize a filesystem_fatfs_t structure.
 * @details
 * 		Initialize a filesystem_fatfs_t structure. This method is protected since
 * 		the structure is abstract.
 * @returns
 * 		<b>FILESYSTEM_SUCCESS</b>: On success
 * 		<br><b>FILESYSTEM_FAILURE</b>: On failure. The structure is not safe to use.
 */
uint8_t _initialize_filesystem_fatfs( filesystem_fatfs_t * );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



/********************************************************************************/
/* Protected Method Declares													*/
/********************************************************************************/
/**
 * @memberof filesystem_fatfs_t
 * @protected
 * @brief
 * 		Retrieve the next available handle.
 * @details
 * 		Retrieve the next available (free, unused, unopened, etc) handle from the
 * 		filesystem_fatfs_t::_files_ array.
 * @param block_time
 * 		The time to block for a free handle.
 * @returns
 * 		The next free handle. However, <b>NULL</b> if there are no free handles
 * 		left.
 */
file_fatfs_t *_filesystem_fatfs_get_file( filesystem_fatfs_t *self, block_time_t block_time );

/**
 * @memberof filesystem_fatfs_t
 * @protected
 * @brief
 * 		Retrieve the next available directory handle.
 * @details
 * 		Retrieve the next available (free, unused, unopened, etc) directory handle from the
 * 		filesystem_fatfs_t::_dirs_ array.
 * @param block_time
 * 		The time to block for a free handle.
 * @returns
 * 		The next free directory. However, <b>NULL</b> if there are no free directory handles
 * 		left.
 */
dir_fatfs_t *_filesystem_fatfs_get_dir( filesystem_fatfs_t *self, block_time_t block_time);

/**
 * @memberof filesystem_fatfs_t
 * @protected
 * @brief
 * 		Free a used handle.
 * @details
 * 		Free a used handle. this is the complement of _filesystem_fatfs_get_handle( ).
 * 		This function has no effect if the handle is already free.
 * @param handle[in]
 * 		The handle to free.
 */
void _filesystem_fatfs_free_handle( filesystem_fatfs_t *self, fs_handle_t *handle );

/**
 * @memberof filesystem_fatfs_t
 * @protected
 * @brief
 * 		Free a used directory.
 * @details
 * 		Free a used directory. this is the complement of _filesystem_fatfs_get_dir( ).
 * 		This function has no effect if the directory is already free.
 * @param handle[in]
 * 		The handle to free.
 */
void _filesystem_fatfs_free_dir( filesystem_fatfs_t *self, fs_handle_t *handle );


#endif /* INCLUDE_FILESYSTEMS_FATFS_FILESYSTEM_FATFS_H_ */
