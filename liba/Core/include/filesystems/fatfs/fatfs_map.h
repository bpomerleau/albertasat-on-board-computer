/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file fatfs_map.h
 * @author Brendan Bruner
 * @date Aug 17, 2015
 */
#ifndef INCLUDE_FILESYSTEMS_FATFS_FATFS_MAP_H_
#define INCLUDE_FILESYSTEMS_FATFS_FATFS_MAP_H_


extern fs_error_t _fatfs_result_map[];

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/
/* Asserts the handle is not free. By doing this we are making sure that */
/* dir_fatfs_t::_native_file_ is opened to do IO. Next, we assert */
/* the handle is in fact a fatfs handle. This ensures that fs_handle_t::_native_handle_ */
/* points to dir_fatfs_t::_native_file_ or file_fatfs_t::_native_file_, depending */
/* on what handle is being asserted. */
#define GET_ASSERT_FATFS_HANDLE( handle, err ) \
	do {																				\
		(err) = FS_OK;																	\
		if( 																			\
			fs_handle_get_busy_status( (fs_handle_t *) (handle) ) != HANDLE_BUSY ||		\
			fs_handle_get_type( (fs_handle_t *) (handle) ) != HANDLE_FATFS 				\
		  ) { 																			\
			(err) = FS_INVALID_OBJECT;													\
		}																				\
	} while( 0 )

#define ASSERT_FATFS_HANDLE_IS_FATFS( handle ) \
	do {																				\
		if( 																			\
			fs_handle_get_type( (fs_handle_t *) (handle) ) != HANDLE_FATFS 				\
		  ) { 																			\
			return FS_INVALID_OBJECT;													\
		}																				\
	} while( 0 )

/* Calls GET_ASSERT_HANDLE then returns with an error if the error is not FS_OK. */
#define ASSERT_FATFS_HANDLE_RETURN_ERR( handle )	\
		do {																			\
			fs_error_t err;																\
			GET_ASSERT_FATFS_HANDLE( (handle), err );									\
			if( err != FS_OK ) {														\
				return err;																\
			}																			\
		} while( 0 )

/* Calls GET_ASSERT_HANDLE and returns with 0 if the error is not FS_OK. */
#define ASSERT_FATFS_HANDLE_RETURN_ZERO( handle, err )	\
		do {																			\
			GET_ASSERT_FATFS_HANDLE( (handle), (err) );										\
			if( (err) != FS_OK ) {														\
				return 0;																\
			}																			\
		} while( 0 )

#define ASSERT_FATFS_HANDLE_RETURN( handle, err, ret )	\
		do {																				\
			GET_ASSERT_FATFS_HANDLE( (handle), (err) );										\
			if( (err) != FS_OK ) {														\
				return (ret);																\
			}																			\
		} while( 0 )


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/



/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/



/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_FILESYSTEMS_FATFS_FATFS_MAP_H_ */
