/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file fs_error.h
 * @author Brendan Bruner
 * @date Jun 23, 2015
 */
#ifndef FS_ERROR_H_
#define FS_ERROR_H_

#include <portable_types.h>


/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
/* Time between retry = 100ms */
#define _DISK_ERR_RECOVERY_DELAY( )	task_delay( 1000 )

/* Max times to retry */
#define _DISK_RETRY_LIMIT			3

/* Configuration defines */
#define FILESYSTEM_FATFS_MAX_FILE_HANDLES 	20
#ifdef NANOMIND
#define FILESYSTEM_FATFS_MAX_DIR_HANDLES 	0
#else
#define FILESYSTEM_FATFS_MAX_DIR_HANDLES 	1
#endif
#define FILESYSTEM_MAX_NAME_LENGTH 			12


/********************************************************************************/
/* Common Forward Declaration													*/
/********************************************************************************/
typedef struct filesystem_t filesystem_t;


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct fs_error_t
 * @brief
 * 		Error codes returned by filesystem_t methods. This is an enum not a struct
 * 		(if you are reading this documentation from the compiled doxygen it will say
 * 		this is a struct).
 * @details
 * 		Error codes returned by filesystem_t methods.
 * 		<br>FS_OK: (0) Succeeded
 * 		<br>FS_DISK_ERR: (1) A hard error occurred in the low level disk I/O layer
 * 		<br>FS_INT_ERR: (2) Assertion failed
 * 		<br>FS_NOT_READY: (3) The physical drive cannot work
 * 		<br>FS_NO_FILE: (4) Could not find the file
 * 		<br>FS_NO_PATH: (5) Could not find the path
 * 		<br>FS_INVALID_NAME: (6) The path name format is invalid
 * 		<br>FS_DENIED: (7) Access denied due to prohibited access or directory full
 * 		<br>FS_EXIST: (8) Access denied due to prohibited access
 * 		<br>FS_INVALID_OBJECT: (9) The file/directory object is invalid
 * 		<br>FS_WRITE_PROTECTED: (10) The physical drive is write protected
 * 		<br>FS_INVALID_DRIVE: (11) The logical drive number is invalid
 * 		<br>FS_NOT_ENABLED: (12) The volume has no work area
 * 		<br>FS_NO_FILESYSTEM: (13) There is no valid FAT volume
 * 		<br>FS_MKFS_ABORTED: (14) The f_mkfs() aborted due to any parameter error
 * 		<br>FS_TIMEOUT: (15) Could not get a grant to access the volume within defined period
 * 		<br>FS_LOCKED: (16) The operation is rejected according to the file sharing policy
 * 		<br>FS_NOT_ENOUGH_CORE: (17) LFN working buffer could not be allocated
 * 		<br>FS_TOO_MANY_OPEN_FILES: (18) Number of open files > _FS_SHARE or max file handles
 * 		<br>FS_INVALID_PARAMETER: (19) Given parameter is invalid
 * 		<br>FS_NULL_HANDLE: (20) Indicates the null structure was returned or is being used. functions
 * 		done on this handle will have no effect. This error usually means all available file handles are
 * 		opened else where. Try closing an open file to free up space.
 * 		<br>FS_FATAL: (21) Unrecoverable internal error.
 * 		<br>FS_NO_MEM: (22) Memory is full, cannot make appends to filesystem.
 * 		<br>FS_CACHED: (23) Written data is cached in RAM, not in non volatile memory.
 * 		<br>FS_KILL_SWAP: (24) Failure in name change.
 * 		<br>FS_TOO_MANY_OPEN_DIRS: (25) Too many directories are opened
 * 		<br>FS_UNABLE: (26) Function not implemented.
 */
typedef enum {
	FS_OK = 0,
	FS_DISK_ERR,
	FS_INT_ERR,
	FS_NOT_READY,
	FS_NO_FILE,
	FS_NO_PATH,
	FS_INVALID_NAME,
	FS_DENIED,
	FS_EXIST,
	FS_INVALID_OBJECT,
	FS_WRITE_PROTECTED,
	FS_INVALID_DRIVE,
	FS_NOT_ENABLED,
	FS_NO_FILESYSTEM,
	FS_MKFS_ABORTED,
	FS_TIMEOUT,
	FS_LOCKED,
	FS_NOT_ENOUGH_CORE,
	FS_TOO_MANY_OPEN_FILES,
	FS_INVALID_PARAMETER,
	FS_NULL_HANDLE,
	FS_FATAL,
	FS_NO_MEM,
	FS_CACHED,
	FS_KILL_SWAP,
	FS_TOO_MANY_OPEN_DIRS,
	FS_UNABLE
} fs_error_t;


#endif /* FS_ERROR_H_ */
