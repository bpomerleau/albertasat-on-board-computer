/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file fs_handle.h
 * @author Brendan Bruner
 * @date May 7, 2015
 *
 * In all of the filesystem and handle code, 'native' refers to the original filesystem being
 * wrapped by these function. Fatfs is a native filesystem.
 */
#ifndef INCLUDE_FS_HANDLE_H_
#define INCLUDE_FS_HANDLE_H_

#include <portable_types.h>
#include <filesystems/fs_common.h>

/********************************************************************************/
/* Defines																		*/
/********************************************************************************/
#define FS_HANDLE_SUCCESS	1
#define FS_HANDLE_FAILURE	0

#define BEGINNING_OF_FILE 		0


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct fs_handle_t
 * @brief
 * 		Abstract structure for use as a handle by the filesystem_t structure.
 * @details
 * 		Abstract structure for use as a handle by the filesystem_t structure.
 * @var fs_handle_t::destroy
 * 		<b>Public</b>
 * 		@code
 * 			void destroy( fs_handle_t *self );
 * 		@endcode
 * 		Destructor
 * @var fs_handle_t::_native_handle_
 * 		<b>Private</b>
 * 		A pointer to the native handle. For example, with the fatfs filesystem,
 * 		the native handle for files is of type <b>FIL</b>. This would point to
 * 		an instance of a <b>FIL</b> structure.
 * @var fs_handle_t::_busy_status_
 * 		<b>Private</b>
 * 		The status of the handle.
 * @var fs_handle_t::_type_
 * 		<b>Private</b>
 * 		The underlying file system used.
 * @var fs_handle_t::_busy_mutex_
 * 		<b>Private</b>
 * 		Used to provide mutual exclusion.
 * @var fs_handle_t::_name_
 * 		<b>Private</b>
 * 		A name for the handle. This may refer to a directory name, file name, etc.
 * @var filesystem_t::_fs_
 * 		<b>Private</b>
 * 		The file system managing this handle.
 */
typedef struct fs_handle_t fs_handle_t;

/** The possible status' of an fs_handle_t structure. */
typedef enum
{
	HANDLE_BUSY = 0,	/*!< (0) The handle is being used. */
	HANDLE_FREE = 1,	/*!< (1) The handle is free to be used. */
	HANDLE_NULL = 2		/*!< (2) The handle should never be used. */
} fs_handle_status_t;

/** The underlying file system used by the handle. */
typedef enum
{
	HANDLE_ABSTRACT = 0,/*!< (0) The handle has no underlying file system. */
	HANDLE_FATFS = 1	/*!< (1) The handle is for use with fatfs API. */
} fs_handle_type_t;

/********************************************************************************/
/* Structure Definitions														*/
/********************************************************************************/
struct fs_handle_t
{
	void 				*_native_handle_;
	fs_handle_status_t	_busy_status_;
	fs_handle_type_t	_type_;
	mutex_t 			_busy_mutex_;
	char				_name_[FILESYSTEM_MAX_NAME_LENGTH+1];

	filesystem_t 		*_fs_;

	void (*destroy)( fs_handle_t * );
};


/********************************************************************************/
/* Public Methods																*/
/********************************************************************************/
/**
 * @memberof fs_handle_t
 * @brief
 * 		Get the underlying file system used by the handle.
 * @details
 * 		Get the underlying file system used by the handle.
 * 		For example, the handle may be compatible with
 * 		a fatfs or uffs file system. Application code
 * 		need not worry about this, it is used internally.
 * @returns
 * 		The underlying file system used.
 */
fs_handle_type_t fs_handle_get_type( fs_handle_t * );

/**
 * @memberof fs_handle_t
 * @brief
 * 		Get the busy status of an fs_handle_t structure.
 * @details
 * 		Get the busy status of an fs_handle_t structure.
 * @param handle
 * 		The fs_handle_t structure to query.
 * @returns
 * 		The busy status
 */
fs_handle_status_t fs_handle_get_busy_status( fs_handle_t *handle );

/**
 * @memberof fs_handle_t
 * @brief
 * 		Set the busy status of an fs_handle_t structure as HANDLE_BUSY.
 *
 * @details
 * 		Set the busy status of an fs_handle_t structure as HANDLE_BUSY.
 * 		This does not enable an fs_handle_t structure to do IO on a file.
 * 		Only use this function if you know what you're doing, it may have
 * 		unintended side effects.
 *
 * @param handle
 * 		A pointer to the fs_handle_t structure to set as busy.
 */
void fs_handle_set_busy( fs_handle_t *handle );

/**
 * @memberof fs_handle_t
 * @brief
 * 		Set the busy status of an fs_handle_t structure as HANDLE_FREE.
 *
 * @details
 * 		Set the busy status of an fs_handle_t structure as HANDLE_FREE.
 * 		This does not disable file IO. An fs_handle_t structure should
 * 		always be freed with filesystem_t::close. Only use this
 * 		function if you know what you're doing, it may have unintended
 * 		side effects.
 *
 * @param handle
 * 		A pointer to the fs_handle_t structure to set as busy.
 */
void fs_handle_set_free( fs_handle_t *handle );

/**
 * @memberof fs_handle_t
 * @brief
 * 		Get a pointer to the native file handle.
 *
 * @details
 * 		Get a pointer to the native file handle. For example,
 * 		if an lpc fatfs filesystem is being used, the native
 * 		handle will be of type FIL. Other filesystem types
 * 		and hardware may have different native file handles.
 * 		The native handle should never be used to do file IO.
 *
 * @param handle
 * 		A pointer to the fs_handle_t structure to get the native
 * 		handle of.
 *
 * @returns
 * 		A void pointer to the native file handle. It must be cast
 * 		to the correct type. For example, if an lpc fatfs filesystem
 * 		is being used, the return value has to cast to a FIL pointer.
 */
void *fs_handle_get_native( fs_handle_t *handle );

/**
 * @memberof fs_handle_t
 * @brief
 * 		Set the name of the structure.
 * @details
 * 		Set the name of the structure.
 * @param name[in]
 * 		The null terminated name to set the structure. It is copied into the structure.
 * 		A maximum of <b>FILESYSTEM_MAX_NAME_LENGTH</b> characters
 * 		will be copied, after that, silent truncation of the name
 * 		will occur.
 */
void fs_handle_set_name( fs_handle_t *handle, char const *name );

/**
 * @memberof fs_handle_t
 * @brief
 * 		Get the name of the handle.
 * @details
 * 		Get the name of the handle. Never write to the returned string.
 * @returns
 * 		The null terminated name, a maximum of <b>FILESYSTEM_MAX_NAME_LENGTH</b> characters
 * 		long.
 */
char const *fs_handle_get_name( fs_handle_t * );

/**
 * @memberof fs_handle_t
 * @protected
 * @brief
 * 		Get a pointer to the file system managing this handle.
 * @details
 * 		Get a pointer to the file system managing this handle.
 * @returns
 * 		A pointer to the file system managing this handle.
 */
filesystem_t *_fs_handle_get_fs( fs_handle_t * );


/********************************************************************************/
/* Constructor																	*/
/********************************************************************************/
/**
 * @memberof fs_handle_t
 * @protected
 * @brief
 * 		Initializer for fs_handle_t structure.
 * @details
 * 		Initializer for fs_handle_t structure.
 * @param fs[in]
 * 		The file system which is managing this handle.
 * @param type[in]
 * 		The handle type being initialized.
 * @returns
 * 		<b>FS_HANDLE_SUCCESS</b>: On successful initialization.
 * 		<br><b>FS_HANDLE_FAILURE</b>: On failure to initialize. Structure is not
 * 		safe to use.
 */
uint8_t _initialize_fs_handle( fs_handle_t *, filesystem_t *fs, fs_handle_type_t type );



#endif /* INCLUDE_FS_HANDLE_H_ */
