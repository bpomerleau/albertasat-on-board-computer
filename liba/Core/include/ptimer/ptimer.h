/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file ptimer.h
 * @author Brendan Bruner
 * @date Sep 14, 2015
 */
#ifndef INCLUDE_PERSISTENT_TIMER_H_
#define INCLUDE_PERSISTENT_TIMER_H_

#include <portable_types.h>
#include <stdint.h>
#include <filesystems/filesystem.h>
#include <filesystems/file.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/


/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct ptimer_t
 * @brief
 * 		A software timer which uses non volatile memory to persist through
 * 		power cycles.
 * @details
 * 		A software timer which uses non volatile memory to persist through
 * 		power cycles. When timer is initializes, it creates a flag in non volatile
 * 		memory indicating it is unfinished. Upon completion, it updates the flag
 * 		to indicate it is finished.
 *
 * 		It is important to note, power cycles which occur during the timers execution
 * 		will result in a reset of the timer. It will not take off where it started.
 * @var ptimer_t::is_expired
 * 		<b>Public</b>
 * 		@code
 * 			bool_t is_expired( ptimer_t *self, block_time_t wait_time );
 * 		@endcode
 * 		This function is used to check if the timer has expired. A block time is
 * 		specified. This function will return for two reason. First, the block
 * 		time has expired, but the timer has not expired. Last, the timer has
 * 		expired.
 *
 * 		When the timer expires, it will stop.
 *
 * 		<b>Parameters</b>
 * 		<ul>
 * 		<li><b>wait_time</b>: Time to block for when polling the timer.</li>
 * 		</ul>
 *
 * 		<b>Returns</b>
 * 		<br>true if the timer is finished, false otherwise.
 * @var ptimer_t::start
 * 		<b>Public</b>
 * 		@code
 * 			bool_t start( ptimer_t * );
 * 		@endcode
 *		Starts a timer which was constructed via initialize_ptimer( ).
 *		If the timer is expired, this function will have no effect.
 *
 *		<b>Returns</b>
 *		<br>true if started, false otherwise.
 * @var ptimer_t::destroy
 * 		<b>Public</b>
 * 		@code
 * 			void destroy( ptimer_t *self );
 * 		@endcode
 * 		Destructor for the timer. Releases the memory allocated for the timer.
 */
typedef struct ptimer_t ptimer_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct ptimer_t
{
	struct
	{
		semaphore_t 	timer_completion_guard;
		filesystem_t*	fs;
		uint32_t		duration;
		uint32_t		resolution;
		uint32_t		seconds_elpased;
		char			log_file[FILESYSTEM_MAX_NAME_LENGTH+1];
	}_; /* Private */

	bool_t (*start)( ptimer_t* );
	bool_t (*is_expired)( ptimer_t *, block_time_t );
	void (*destroy)( ptimer_t* );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof ptimer_t
 * @brief
 * 		Initialize a persistent timer.
 * @details
 * 		Initialize a persistent timer. The timer is stopped by default and must
 * 		be started with a call to ptimer_t::start. However, if the timer
 * 		has previously run its duration (based on the input log file), a call to
 * 		ptimer_t::start will have no effect.
 *
 * @param duration
 * 		The duration of the timer in seconds.
 * @param resolution
 * 		The period, in seconds, at which to update non volatile memory.
 * @param fs[in]
 * 		The filesystem to use when maintaining a log of the timer.
 * @param timer_log_file
 * 		The name to give the log file.
 * @returns
 * 		true on successful construction, false otherwise.
 */
bool_t initialize_ptimer
(
	ptimer_t *self,
	uint32_t duration,
	uint32_t resolution,
	filesystem_t *fs,
	char const * timer_log_file
);


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/
/**
 * @memberof ptimer_t
 * @brief
 * 		Updates non volatile memory with time elapsed.
 * @details
 * 		Updates non volatile memory with time elapsed. This method is
 * 		used by the ptimer_controller to invoke updates in non volatile memory.
 * 		This method should never be called directly by the application.
 * @param time_elapsed
 * 		time elapsed, in ms, since the last call to this method.
 * @returns
 * 		<b>true</b> when updated successfully, <b>false</b> on failure to update.
 */
void ptimer_update( ptimer_t*, uint32_t time_elapsed );


#endif /* INCLUDE_PERSISTENT_TIMER_H_ */
