/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file hub.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_HUB_HUB_H_
#define INCLUDE_HUB_HUB_H_

#include <eps/eps.h>

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct hub_t
 * @brief
 * 		Abstract class for an API to Gomspace's nanohub.
 * @details
 * 		Abstract class for an API to Gomspace's nanohub. Implementing classes
 * 		include a mock up of the nanohub that runs in software, and the actual
 * 		API to the nanohub.
 */
typedef struct hub_t hub_t;

/** Definitions of power lines controlled by hub. */
typedef enum
{
	HUB_POWER_LINE0 = 0,
	HUB_POWER_LINE1
} hub_power_lines_t;
#define HUB_TOTAL_POWER_LINES 2

/** Possible states of power lines controlled by hub. */
typedef enum
{
	HUB_POWER_LINE_ACTIVE = 1,								/*!< (1) Power line is turned on. */
	HUB_POWER_LINE_INACTIVE = !HUB_POWER_LINE_ACTIVE		/*!< (0) Power line is turned off. */
} hub_power_line_state_t;

/** Possible deployment lines. */
typedef enum
{
	HUB_DEPLOYMENT_LINE0 = 0, 	// Knife 0, boom.
	HUB_DEPLOYMENT_LINE1		// Knife 1, antennas and probes
} hub_deployment_line_t;
#define HUB_TOTAL_DEPLOYMENT_LINES 2

/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct hub_t
{
	bool_t (*power)( hub_t*, eps_t*, bool_t );
	bool_t (*power_line)( hub_t*, hub_power_lines_t, hub_power_line_state_t );
	bool_t (*deploy)( hub_t*, hub_deployment_line_t );
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/



/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_HUB_HUB_H_ */
