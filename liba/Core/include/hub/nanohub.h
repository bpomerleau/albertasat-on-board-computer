/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file nanohub.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_HUB_NANOHUB_H_
#define INCLUDE_HUB_NANOHUB_H_

#include "hub.h"

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct nanohub_t
 * @extends hub_t
 * @brief
 * 		Implements inherited methods to communicate with Gomspace's nanohub.
 * @details
 * 		Implements inherited methods to communicate with Gomspace's nanohub.
 */
typedef struct nanohub_t nanohub_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct nanohub_t
{
	hub_t s_; /* MUST be first. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @memberof nanohub_t
 * @brief
 * 		Constructor.
 * @details
 * 		Constructor.
 */
void initialize_nanohub( nanohub_t* );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_HUB_NANOHUB_H_ */
