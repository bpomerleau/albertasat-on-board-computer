/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file startup.h
 * @author Alexander Hamilton
 * @date Aug 10, 2015
 */

/* bootcause.bin contains boot information.
 * Byte 0-3: UTC time boot was attempted.
 * Byte 4: boot cause from MCU.
 * Byte 5: number of attempts since start
 * Byte 6: software init error flags
 * Byte 7: hardware init error flags
 *
 */


#ifndef STARTUP_H_
#define STARTUP_H_
#include <stdint.h>
#include <stdbool.h>
#include "driver_toolkit/driver_toolkit.h"
#ifdef LPC1763

#else /*NANOMIND*/

#define SAFE_MODE_BOOT_DELAY 60000
#define MAX_FAT_REBOOT_ATTEMPTS 3
#define MAX_APP_REBOOT_ATTEMPTS 30

int StartupTest(int test_status,driver_toolkit_t * driver);
void StartupLogH(uint8_t status);
void StartupLogS(uint8_t status);
int StartupTestInit(bool_t FS_alive, bool_t UFFS_OK);
bool_t FSTest(bool_t FAT_OK, bool_t UFFS_OK);
bool_t ClockTest(void);

typedef struct
{
	uint32_t time; // time boot attempted
	uint8_t cause1, cause2, sfm_cause; // MCU logged boot cause
	uint8_t application_boot_count; // number of boot attemps by application code after last clean boot.
	uint8_t fat_boot_count;			// number of boot attempts to resolve FAT_MOUNT_FAIL since last clean mount.
	uint8_t soft; // software error flags
	uint8_t hard; // hardware error flags
} __attribute__ ((packed)) bootcause_t;

typedef enum
{
	FAT_MOUNT_FAIL  = 0,
	SAFE_MODE_BOOT = 1,
	APP_BOOT_OVERFLOW = 2,
	FAT_BOOT_OVERFLOW = 3,
	BOOT_OK = 4
} boot_status_e;

boot_status_e start_up_diagnostics( bool_t fat_ok );

/* These values are enumerations for hardware error flags.
 * Byte 8 out of 8 contains these flags in bootcause.bin.
 */

#define STARTLOG_EPS_PING		0x01
#define STARTLOG_EPS_CMD		0x02
#define STARTLOG_COMM_PING		0x04
#define STARTLOG_COMM_GET		0x08
#define STARTLOG_ADCS_TEST		0x10
#define STARTLOG_ADCS_OTHER		0x20
#define STARTLOG_HUB_PING		0x40
#define STARTLOG_HUB_CMD		0x80

/* These values are enumerations for hardware error flags.
 * Byte 7 out of 8 contains these flags in bootcause.bin.
 */
#define STARTLOG_DRIVERS		0x01
#define STARTLOG_STATE_M		0x02
#define STARTLOG_FATFS			0x04
#define STARTLOG_RES1			0x08
#define STARTLOG_RES2			0x10
#define STARTLOG_RES3			0x20
#define STARTLOG_RES4			0x40
#define STARTLOG_RES5			0x80





#endif /*LPC1763*/
#endif /*STARTUP_H_*/
