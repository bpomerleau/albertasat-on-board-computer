/*
 * Copyright (C) 2015 Collin Cupido, Brendan Bruner, Stefan Damkjar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file dfgm.h
 * @author Alexander Hamilton
 * @date 2015-08-20
 */

#ifndef DFGM_H_
#define DFGM_H_

#include <stdint.h>
#include <portable_types.h>

#define ENABLE_DFGM 1
//#define DFGM_TESTING

#define DFGM_USART_HANDLE 2
#define DFGM_USART_BAUD 115200

/* Expected value of certain data points in a dfgm packet. */
#define DFGM_DLE_VAL 0x10
#define DFGM_STX_VAL 0x02
#define DFGM_ETX_VAL 0x03

/* Byte offset into a dfgm packet for specific data. */
#define DFGM_DLE 0
#define DFGM_STX 1
#define DFGM_PID 2
#define DFGM_HK00 12
#define DFGM_PPS_OFFSET 8

#define DFGM_HK_SIZE (2*12+4) /* 2 bytes HK * 12 HK items + 4 byte PPS offset */
#define DFGM_HK_START DFGM_PPS_OFFSET

#define DFGM_PACKET_SIZE 1248
#define DFGM_FS 100

#define DFGM_QUEUE_SIZE 2*4096

#define DFGM_FILTER_ORDER 101


struct __attribute__((packed)) dfgm_packet_t{
	uint8_t pid;
	uint8_t packet_type;
	uint8_t packet_length;
	uint16_t fs;
	uint32_t pps_offset;
	int16_t hk[12];
	int32_t x[DFGM_FS];
	int32_t y[DFGM_FS];
	int32_t z[DFGM_FS];
	uint16_t board_id;
	uint16_t sensor_id;
	uint16_t crc;

};

struct __attribute__((packed)) dfgm_1Hz_file_t{
	uint32_t timestamp;
	uint8_t pid;
	uint32_t pps_offset;
	int32_t x;
	int32_t y;
	int32_t z;
	int16_t hk[12];
};

void vTaskDFGM(void * pvParameters);
void vTaskDFGMWrite(void * pvParameters);
//void vTaskDFGMfft(void * pvParameters);

int32_t DFGM_filter(int32_t *buffer);
bool_t DFGM_init(void);
bool_t DFGM_start(void);
void DFGM_stop(void);
void DFGM_lock(void);
void DFGM_unlock(void);
void DFGM_printConfig(void);
bool_t DFGM_is_running( );

#endif /*DFGM_H_*/
