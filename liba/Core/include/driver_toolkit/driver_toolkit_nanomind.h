/*
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file driver_toolkit_nanomind.h
 * @author Brendan Bruner
 * @date Oct 27, 2015
 */
#ifndef INCLUDE_DRIVER_TOOLKIT_DRIVER_TOOLKIT_NANOMIND_H_
#define INCLUDE_DRIVER_TOOLKIT_DRIVER_TOOLKIT_NANOMIND_H_

#include "driver_toolkit.h"
#include <eps/eps_nanomind.h>
#include <comm/comm_nanomind.h>
#include <filesystems/fatfs/filesystem_fatfs_nanomind.h>
#include <hub/nanohub.h>
#include <dfgm/dfgm_external.h>
#include <mnlp/mnlp_external.h>
#include <teledyne/teledyne_external.h>

extern struct driver_toolkit_nanomind_t  drivers;
#define driver_kit ((driver_toolkit_t*) &drivers)

/********************************************************************************/
/* #defines																		*/
/********************************************************************************/



/********************************************************************************/
/* Structure Documentation														*/
/********************************************************************************/
/**
 * @struct driver_toolkit_nanomind_t
 * @extends driver_toolkit_t
 * @brief
 * 		A toolkit which works on an nanomind board.
 * @details
 * 		A toolkit which works on an nanomind board.
 */
typedef struct driver_toolkit_nanomind_t driver_toolkit_nanomind_t;


/********************************************************************************/
/* Structure Defines															*/
/********************************************************************************/
struct driver_toolkit_nanomind_t
{
	driver_toolkit_t s_; /* MUST be first. */
	struct
	{
		eps_nanomind_t 			eps;
		comm_nanomind_t			comm;
		ground_station_t		gs;
		nanohub_t				hub;

		filesystem_fatfs_nanomind_t	fs;

		dfgm_external_t 		dfgm;
		mnlp_external_t			mnlp;
		teledyne_external_t		teledyne;
	}_; /* Private. */
};


/********************************************************************************/
/* Constructor Declare															*/
/********************************************************************************/
/**
 * @brief
 * 		Initializes the driver_toolkit_t structure to use nanomind drivers
 *
 * @details
 * 		Initializes the driver_toolkit_t structure to use nanomind drivers
 *
 * @param toolkit
 * 		A pointer to the driver_toolkit_t structure to be initialized.
 *
 * @returns
 * 		<br>true</b> on successful initialization.
 * 		<br>false</b> on failure. The structure is not safe to use.
 *
 * @memberof driver_toolkit_t
 */
bool_t initialize_driver_toolkit_nanomind( driver_toolkit_nanomind_t *toolkit );


/********************************************************************************/
/* Public Method Declares														*/
/********************************************************************************/



#endif /* INCLUDE_DRIVER_TOOLKIT_DRIVER_TOOLKIT_NANOMIND_H_ */
