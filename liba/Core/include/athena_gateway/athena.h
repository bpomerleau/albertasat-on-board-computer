/*
 *
 * Copyright (C) 2015  Brendan Bruner
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * bbruner@ualberta.ca
 */
/**
 * @file athena.h
 * @author Brendan Bruner
 * @date 2016-06-9
 */

#include <portable_types.h>

#ifndef LIB_LIBCORE_INCLUDE_ATHENA_ATHENA_H_
#define LIB_LIBCORE_INCLUDE_ATHENA_ATHENA_H_

bool_t athena_init( );
bool_t athena_push_command( char );
bool_t athena_suspend( );
bool_t athena_resume( );
bool_t athena_is_suspended( );


#endif /* LIB_LIBCORE_INCLUDE_ATHENA_ATHENA_H_ */
